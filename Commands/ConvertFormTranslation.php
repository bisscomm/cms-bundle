<?php
namespace Bci\CmsBundle\Commands;

use Bci\CmsBundle\Entity\FormItem;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConvertFormTranslation extends Command
{
    protected static $defaultName = 'cms:form-items:convert';

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription("Convert old form-item to newly desing one's")

            ->setHelp('This command allows you to convert formItems to new translation format')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $error = $this->reGenerateFormWithNoTranslations($output);

        return $error ? 1 : 0;
    }

    /**
     *
     */
    private function reGenerateFormWithNoTranslations(OutputInterface $output)
    {
        $ERROR = false;
        $output->writeln('Getting formItems repository ...');
        $formItemRepository = $this->entityManager->getRepository(FormItem::class);
        $output->writeln('Getting formItems (FR) untranslated ...');
        $frenchFormItemsAsArrayNoDoctrine = $formItemRepository->getFrenchFormItemsAsArrayNoDoctrine();

        $output->writeln('Applying translation to existing formItems ...');
        foreach ($frenchFormItemsAsArrayNoDoctrine as $formFormItem)
        {
            $doctrineObject = $formItemRepository->find($formFormItem['id']);

            if ($doctrineObject)
            {
                $doctrineObject
                    ->setTranslatableLocale($formFormItem['locale'])
                    ->setLabel($formFormItem['label'])
                    ->setHelp($formFormItem['help'])
                    ->setConfig($formFormItem['config']);
                $this->entityManager->persist($doctrineObject);
                try
                {
                    $this->entityManager->flush($doctrineObject);
                }catch (DBALException $exception)
                {
                    $output->writeln('************EXCEPTION**************');
                    $output->writeln('Code :');
                    $output->writeln($exception->getCode());
                    $output->writeln('Trace :');
                    $output->writeln($exception->getTraceAsString());
                    $output->writeln('Message :');
                    $output->writeln($exception->getMessage());
                    $output->writeln('************EXCEPTION**************');
                    $ERROR = true;
                    $this->entityManager->rollback();
                    return $ERROR;
                }

            }
        }
        $output->writeln('Deleting old formItems (EN) ...');
        $englishFormItemsToDelete = $formItemRepository->findBy(['locale' => 'en']);
        foreach ($englishFormItemsToDelete as $toDelete)
        {
            $this->entityManager->remove($toDelete);
        }
        try
        {
            $this->entityManager->flush();
        }catch (DBALException $exception)
        {
            $output->writeln('************EXCEPTION**************');
            $output->writeln('Code :');
            $output->writeln($exception->getCode());
            $output->writeln('Trace :');
            $output->writeln($exception->getTraceAsString());
            $output->writeln('Message :');
            $output->writeln($exception->getMessage());
            $output->writeln('************EXCEPTION**************');
            $ERROR = true;
            $this->entityManager->rollback();
            return $ERROR;
        }
        $output->writeln('DONE');
    }
}