<?php
namespace Bci\CmsBundle\Commands;

use Bci\CmsBundle\Entity\FormItem;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Gedmo\Translatable\Entity\Translation;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateJobTranslations extends Command
{
    protected static $defaultName = 'cms:job_translation:link';

    private const OLD_OBJECT_CLASS = 'App\Entity\Job';

    private const NEW_OBJECT_CLASS = 'Bci\CmsBundle\Entity\Job';

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription("Link old job translation from project to cms desing one's")

            ->setHelp('This command allows you to link job translations preiously made in project to new entity emplacement.<br> From App/Entity/Job to Bci\CmsBundle\Entity\Job ')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        return $this->updateJobTranslationObject_class($output);
    }

    /**
     * @param OutputInterface $output
     */
    private function updateJobTranslationObject_class(OutputInterface $output)
    {
        $ERROR = false;
        $output->writeln("<info>Getting translation's repository ...<info>");

        $translationsRepository = $this->entityManager->getRepository(Translation::class);

        $output->writeln("<info>Generating the query ...<info>");
        /**
         * @var QueryBuilder $queryBuilder
         */
        $queryBuilder = $translationsRepository->createQueryBuilder('t')
            ->update()
            ->set('t.objectClass', ':NEW_OBJECT_CLASS')
            ->setParameter('NEW_OBJECT_CLASS', self::NEW_OBJECT_CLASS)
            ->setParameter('OLD_OBJECT_CLASS', self::OLD_OBJECT_CLASS)
            ->andWhere('t.objectClass = :OLD_OBJECT_CLASS');

        $output->writeln("<info>Executing the query ...<info>");
        $results = $queryBuilder->getQuery()->getResult();

        $output->writeln('');
        $output->writeln('==============================================');
        $output->writeln("<info>Updated object_class field of " . $results . " translation(s)<info>");
        $output->writeln('==============================================');

    }
}