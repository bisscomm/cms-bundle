<?php

namespace Bci\CmsBundle\Controller;

use Bci\CmsBundle\Helper\ControllerTrait;
use Bci\CmsBundle\Entity\ArticleCategory;
use Bci\CmsBundle\Entity\Blog;
use Bci\CmsBundle\Repository\ArticleCategoryRepository;
use Bci\CmsBundle\Repository\BlogRepository;
use Doctrine\Common\Collections\Collection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ArticleCategoryController extends AbstractController
{
    use ControllerTrait;

    /**
     * @Route("cms/new/article_category/{blogId}", name="article_category")
     */
    public function newCategory(ArticleCategoryRepository $articleCategoryRepository, Request $request, $blogId)
    {

        /**
         * @var Blog $blog
         */
        $blog =  $this->getDoctrine()->getRepository(Blog::class)->find($blogId);

        $categoryFR = $request->get('categoryNameFr');
        $categoryEN = $request->get('categoryNameEn');
        if (empty($categoryFR) || empty($categoryEN))
        {
            return new Response(
                ($request->getLocale() == 'fr') ? "Tout les champs doivent être complétés " : "All fields must be complete",
                Response::HTTP_NOT_ACCEPTABLE
            );
        }
        elseif (!$blog)
        {
            return new Response(
                ($request->getLocale() == 'fr') ? "Blogue introuvable " : "Blog can't be find ",
                Response::HTTP_NOT_ACCEPTABLE
            );
        }


        $categoriesFr = $articleCategoryRepository->findOneCategoryByBlog($blog, mb_strtolower($categoryFR), 'fr');
        $categoriesEn = $articleCategoryRepository->findOneCategoryByBlog($blog, mb_strtolower($categoryEN), 'en');
        

        if ($categoriesFr || $categoriesEn)
        {
            return new Response(
                ($request->getLocale() == 'fr') ? "Cette catégorie existe déja " : "Category already exist ",
                Response::HTTP_FORBIDDEN
            );
        }
        else
        {
            /**
             * Create a new category en/fr and assigned
             * it to the current blog so it is available for
             * all articles in the blog.
             */
            $newCategory = new ArticleCategory();
            $newCategory
                ->setTitle(mb_strtolower($categoryFR))
                ->setTranslatableLocale('fr');
            $this->getDoctrine()->getManager()->persist($newCategory);
            $this->getDoctrine()->getManager()->flush();
            $newCategory
                ->setTitle(mb_strtolower($categoryEN))
                ->setTranslatableLocale('en');
            $this->getDoctrine()->getManager()->persist($newCategory);
            $this->getDoctrine()->getManager()->flush();

            $blog->addArticleCategory($newCategory);
            $this->getDoctrine()->getManager()->persist($blog);
            $this->getDoctrine()->getManager()->flush();
            $jsonData = [
                'name' => ($request->getLocale() == 'fr') ? $categoryFR : $categoryEN,
                'id' => $newCategory->getId(),
                'locale' => $request->getLocale()
            ];
            return new JsonResponse($jsonData);
        }
    }

    /**
     * @Route("cms/delete/article_category", name="article_category_delete")
     */
    public function delete(Request $request)
    {
        $articleCategoryRepository = $this->getDoctrine()->getRepository(ArticleCategory::class);
        $articleCategory = $articleCategoryRepository->find($request->get('articleCategoryId'));
        $articleCategory->setTranslatableLocale($request->getLocale());
        $this->getDoctrine()->getManager()->refresh($articleCategory);
        $categoryName = $articleCategory->getTitle();
        $this->remove($articleCategory);
        $jsonData = [
            'name' => $categoryName,
            'locale' => $request->getLocale()
        ];
        return new JsonResponse($jsonData);
    }

    /**
     * @Route("cms/NumberOfRelatedArticles/article_category", name="article_category_number")
     */
    public function getNumberOfRelatedArticles (Request $request)
    {
        $articleCategoryRepository = $this->getDoctrine()->getRepository(ArticleCategory::class);
        $numberOfRelatedArticles = $articleCategoryRepository->find($request->get('articleCategoryId'))->getArticles()->count();
        $jsonData = [
            'numberOfRelatedArticles' => $numberOfRelatedArticles
        ];
        return new JsonResponse($jsonData);
    }
}
