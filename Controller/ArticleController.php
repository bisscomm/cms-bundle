<?php

namespace Bci\CmsBundle\Controller;


use Bci\CmsBundle\Entity\Meta;
use Bci\CmsBundle\Helper\ControllerTrait;
use Bci\CmsBundle\Entity\Article;
use Bci\CmsBundle\Entity\Blog;
use Bci\CmsBundle\Entity\Menu;
use Bci\CmsBundle\Form\ArticleType;
use Bci\CmsBundle\Repository\ArticleRepository;
use Bci\CmsBundle\Repository\HeadingRepository;
use Bci\CmsBundle\Service\MetaService;
use Doctrine\DBAL\DBALException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Transliterator;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Gedmo\Translatable\Entity\Repository\TranslationRepository;

/**
 * @Route("")
 */
class ArticleController extends AbstractController
{
    use ControllerTrait;

    /**
     * @Route("cms/blog/{id}/articles", name="bci_cms_article_index", methods={"GET"})
     */
    public function index(Blog $blog): Response
    {

        return $this->render('@BciCms/article/index.html.twig', [
            'articles' => $blog->getArticles(),
            'blogId' => $blog->getId()
        ]);
    }

    /**
     * @Route("cms/blog/{id}/article/new/{locale}",
     *      name="bci_cms_article_new",
     *      methods={"GET","POST"},
     *     requirements={
     *        "locale": "en|fr"
     *     })
     */
    public function new(Request $request, Blog $blog, $locale, UploaderHelper $uploaderHelper): Response
    {
        $article = new Article();
        $article->setTranslatableLocale($locale);
        $article->setBlog($blog);
        $form = $this->createForm(ArticleType::class, $article);


        if ($this->isValid($form, $request))
        {
            $article->setTranslatableLocale($locale);
            $this->preDeleteArticleImage($article);
            foreach ($article->getMetas() as $meta)
            {
                $meta->setTranslatableLocale($locale);
                $this->preDeleteMetaImage($meta);
//                if ($meta->getImage())
//                {
//                    $path = $request->getSchemeAndHttpHost();
//                    $path .= $uploaderHelper->asset($meta->getImage(), 'tempFile');
//                    $meta->setContent($path);
//                }
            }
            $this->getEm()->persist($article);
            $this->getEm()->flush();

            return $this->redirectToRoute('bci_cms_article_index', [
                'id' => $blog->getId(),
                'locale' => $locale
            ]);

        }

        return $this->render('@BciCms/article/new.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
            'blogId' => $blog->getId(),
            'locale' => $locale
        ]);
    }

    /**
     * @Route("cms/blog/article/{id}/edit/{locale}",
     *      name="bci_cms_article_edit",
     *      methods={"GET","POST"},
     *      defaults={"locale"= "fr"},
     *      requirements={
     *        "locale": "en|fr"
     *     }
     * )
     */
    public function edit(Request $request, Article $article, $locale, UploaderHelper $uploaderHelper): Response
    {
        $this->refreshArticleAndMetasTranslation($article, $locale);
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            if ($form->get('preview')->isClicked())
            {
                $articleHasChange = false;
                $uow = $this->getEm()->getUnitOfWork();
                $uow->computeChangeSets();
                $changeSet = $uow->getEntityChangeSet($article);
                if(!empty($changeSet)){
                    $articleHasChange = true;
                }
                $request->getSession()->set('article', [
                    'article' => $article,
                    'hasChange' => $articleHasChange
                ]);

                return $this->redirectToRoute('bci_cms_article_preview', [
                    'locale' => $locale
                ]);
            }

            $article->setTranslatableLocale($locale);
            $this->preDeleteArticleImage($article);
            foreach ($article->getMetas() as $meta)
            {
                $meta->setTranslatableLocale($locale);
                $this->preDeleteMetaImage($meta);
            }
            $this->getEm()->persist($article);
            $this->getEm()->flush();

            return $this->redirectToRoute('bci_cms_article_index', [
                'id' => $article->getBlog()->getId(),
                'locale' => $locale
            ]);
        }

        return $this->render('@BciCms/article/edit.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
            'blogId' => $article->getBlog()->getId(),
            'locale' => $locale
        ]);
    }

    /**
     * This route generate an article preview using the real template.
     * 
     * @Route("/cms/blog/article/preview/{locale}", name="bci_cms_article_preview", methods={"GET|POST"}, requirements={"locale":"fr|en"}, defaults={"locale":"fr"})
     * @param Request $request
     * @param $locale
     * @return Response
     */
    public function preview(Request $request, $locale, TranslatorInterface $translator)
    {

        // Keeping request and session locale
        $oldRequestLocale = $request->getLocale();
        $oldSessionLocale = $request->getSession()->get('_locale');

        // Set the requested local to render page in the correct locale
        $request->setLocale($locale);
        $request->getSession()->set('_locale', $locale);


        $article = $request->getSession()->remove('article');
        $hasChange = $article['hasChange'];
        $article = $article['article'];

        if (!$article)
        {
            throw $this->createNotFoundException('Not Found');
        }

        $hasChange
            ?
            $this->addFlash('CMS_DRAFT_OVERRIDE', $translator->trans('bci.cms.general.action.preview.flash.hasChange', [], 'cms_bundle'))
            :
            $this->addFlash('CMS_DRAFT_OVERRIDE', $translator->trans('bci.cms.general.action.preview.flash.noChange', [], 'cms_bundle'));
        

        //Render view in a variable using the BciCms/article/show.html.twig can/is overide in project
        $viewRendered = $this->render('@BciCms/article/show.html.twig', [
            'article' => $article,
        ]);
//        $diseableUserInteraction = "<style>*{pointer-events: none!important;}</style></head>";
//        $viewRendered->setContent(str_replace('</head>', $diseableUserInteraction,$viewRendered->getContent()));

        // Re setting the system to previously saved locale (request/session)
        // So the cmsUser locale isn't being change on next request.
        $request->setLocale($oldRequestLocale);
        $request->getSession()->set('_locale', $oldSessionLocale);

        return $viewRendered;
    }

    /**
     * @param Article $article
     * @throws \Doctrine\ORM\ORMException
     * Verify if UplodableFileCms have to be delete.
     */
    private function preDeleteArticleImage(Article $article)
    {

        if ($article->getImage()->isToDelete())
        {
            $this->getEm()->remove($article->getImage());
            $article->setImage(null);
        }

    }

    /**
     * @param Meta $meta
     * @throws \Doctrine\ORM\ORMException
     */
    private function preDeleteMetaImage(Meta $meta)
    {
        if (strpos($meta->getProperty(), 'image') !== false)
        {
            if ($meta->getImage() ? $meta->getImage()->isToDelete() : false)
            {
                $this->getEm()->remove($meta->getImage());
                $meta->setImage(null);
                $meta->setContent("");
            }
        }
        else
        {
            $meta->setImage(null);
        }
    }
   
//    /**
//     * @Route("actualites/article/{slug}", name="article_show", methods={"GET","POST"})
//     * @Route("news/article/{slug}", name="article_show_en", methods={"GET","POST"})
//     *
//     * @param Request $request
//     * @param ArticleRepository $articleRepository
//     * @param HeadingRepository $headingRepository
//     * @param $slug
//     * @return Response
//     * @throws \Doctrine\ORM\ORMException
//     */
//    public function show(Request $request, ArticleRepository $articleRepository, HeadingRepository $headingRepository, $slug)
//    {
//        $article = $articleRepository->findOneBySlug($slug,$request->getSession()->get('_locale'));
//        if ($article)
//        {
//            $article->setTranslatableLocale($request->getLocale());
//            $this->getEm()->refresh($article);
//
//            $heading = $headingRepository->findOneBy(array('slug' => $request->getLocale() == 'fr' ? "actualites/article" : "news/article"));
////            if ($heading->getLocale() != $request->getLocale())
////            {
////                return $this->redirect($request->getLocale() == 'fr' ? "actualites/article/" : "news/article/" . $slug);
////            }
//            $request->setLocale($heading->getLocale());
//            $page = $heading->getPage();
//
//            return $this->render('templates/template_' . $page->getTemplate()->getId() . '.html.twig', [
//                'page' => $page,
//                'title' => $page->getTitle(),
//                'menu_principal' => $this->getDoctrine()->getRepository(Menu::class)->find(1),
//                'article' => $article
//            ]);
//        }
//
//        try
//        {
//            return $this->redirectToRoute('bci_cms_page_show',['slug' => 'news']);
//        }
//        catch (\Exception $e)
//        {
//            throw $this->createNotFoundException('Page introuvable');
//        }
//
//    }
//
//    /**
//     * @Route("actualites/{category}", name="article_show_category", methods={"GET","POST"})
//     * @Route("news/{category}", name="article_show_category", methods={"GET","POST"})
//     * @param Request $request
//     * @param ArticleRepository $articleRepository
//     * @param HeadingRepository $headingRepository
//     * @param $category
//     * @param string $slug
//     * @return Response
//     */
//    public function showFilteredCategory(Request $request, CategoryRepository $categoryRepository, HeadingRepository $headingRepository, $category)
//    {
//        if ($categoryRepository->findByTitleLocale(strtolower($category),$request->getLocale()) != null)
//        {
//            $heading = $headingRepository->findOneBy(array('slug' => $request->getLocale() == 'fr' ? "actualites" : "news"));
//            $request->setLocale($heading->getLocale());
//            $page = $heading->getPage();
//
//            return $this->render('templates/template_' . $page->getTemplate()->getId() . '.html.twig', [
//                'page' => $page,
//                'title' => $page->getTitle(),
//                'menu_principal' => $this->getDoctrine()->getRepository(Menu::class)->find(1),
//                'category' => $category
//            ]);
//        }
//        try
//        {
//            return $this->redirectToRoute('bci_cms_page_show',['slug' => 'news']);
//        }
//        catch (\Exception $e)
//        {
//            throw $this->createNotFoundException('Page introuvable');
//        }
//    }
//
//    /**
//     * @Route("actualites/archives/{year}", name="article_show_year", methods={"GET","POST"})
//     * @Route("news/archives/{year}", name="article_show_year", methods={"GET","POST"})
//     * @param Request $request
//     * @param ArticleRepository $articleRepository
//     * @param HeadingRepository $headingRepository
//     * @param $year
//     * @param string $slug
//     * @return Response
//     * @throws \Exception
//     */
//    public function showFilteredYear(Request $request, ArticleRepository $articleRepository, HeadingRepository $headingRepository, $year)
//    {
//        if (is_numeric($year) && count($articleRepository->findByYear($year)) > 0)
//        {
//            $heading = $headingRepository->findOneBy(array('slug' => $request->getLocale() == 'fr' ? "actualites" : "news"));
//            $request->setLocale($heading->getLocale());
//            $page = $heading->getPage();
//
//            return $this->render('templates/template_' . $page->getTemplate()->getId() . '.html.twig', [
//                'page' => $page,
//                'title' => $page->getTitle(),
//                'menu_principal' => $this->getDoctrine()->getRepository(Menu::class)->find(1),
//                'year' => $year
//            ]);
//        }
//        try
//        {
//            return $this->redirectToRoute('bci_cms_page_show',['slug' => 'news']);
//        }
//        catch (\Exception $e)
//        {
//            throw $this->createNotFoundException('Page introuvable');
//        }
//    }
//


    /**
     * @Route("cms/article/{id}/{locale}", name="bci_cms_article_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Article $article, $locale ): Response
    {
        $blogId = $article->getBlog()->getId();
        if ($this->isCsrfTokenValid('delete' . $article->getId(), $request->request->get('_token')))
        {
            $articleTranslations = $this->getEm()->getRepository('Gedmo\Translatable\Entity\Translation')->findTranslations($article);
            if (count($articleTranslations) <= 1)
            {
                $this->getEm()->remove($article);
                $this->getEm()->flush();
            }
            else
            {
                foreach ($articleTranslations[$locale] as $field => $content)
                {
                    $translationObj = $this->getEm()->getRepository('Gedmo\Translatable\Entity\Translation')->findBy([
                        'field' => $field,
                        'content' => $content,
                        'foreignKey' => $article->getId(),
                        'locale' => $locale
                    ]);
                    $this->getEm()->remove($translationObj[0]);
                }
                $this->getEm()->flush();
            }
        }
        return $this->redirectToRoute('bci_cms_article_index', [
            'id' => $blogId,
            'locale' => $request->getLocale(),
        ]);

    }
    /**
     * @Route("/cms/article/{id}/delete/{locale}", name="bci_cms_article_translation_delete", methods={"DELETE"})
     */
    public function deleteTranslation(Request $request, Article $article, $locale ): Response
    {
        if ($this->isCsrfTokenValid('delete' . $article->getId(), $request->request->get('_token')))
        {
            $articleTranslations = $this->getEm()->getRepository('Gedmo\Translatable\Entity\Translation')->findTranslations($article);

            if (array_key_exists($locale, $articleTranslations))
            {
                foreach ($articleTranslations[$locale] as $field => $content)
                {
                    $translationObj = $this->getEm()->getRepository('Gedmo\Translatable\Entity\Translation')->findBy([
                        'field' => $field,
                        'content' => $content,
                        'foreignKey' => $article->getId(),
                        'locale' => $locale
                    ]);
                    $this->getEm()->remove($translationObj[0]);
                }
                $this->getEm()->flush();
            }
            else
            {
                $this->addFlash(FlashType::TYPE_WARNING, $request->getLocale() === 'fr' ? 'Aucune traduction [' . strtoupper($locale) . '] à supprimer' : 'There is no translation [' . strtoupper($locale) . '] to be deleted');
            }

        }
        return $this->redirectToRoute('bci_cms_article_index', [
            'id' => $article->getBlog()->getId(),
            'locale' => $locale
        ]);

    }


    /**
     * @param Article $article
     * @param $locale
     * @return Article
     */
    private function refreshArticleAndMetasTranslation(Article $article, $locale)
    {
        $article->setTranslatableLocale($locale);
        $this->getEm()->refresh($article);
        foreach ($article->getMetas() as $meta)
        {
            $meta->setTranslatableLocale($locale);
            $this->getEm()->refresh($meta);
        }

        return $article;
    }
}
