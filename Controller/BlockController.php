<?php

namespace Bci\CmsBundle\Controller;

use Bci\CmsBundle\Entity\Block;
use Bci\CmsBundle\Enum\FlashType;
use Bci\CmsBundle\Form\BlockType;
use Bci\CmsBundle\Repository\BlockRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Config;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;


/**
 * @Config\Route("/cms/block")
 */
class BlockController extends Controller
{
    /**
     * @Config\Route("s", name="bci_cms_block_index", methods="GET")
     *
     * @Config\Template
     * @return array
     */
    public function index(BlockRepository $blockRepository)
    {
        $this->get('bci_cms.block_manager')->getBlockFolder();
        return [
            'blocks' => $blockRepository->findAll()
        ];
    }

    /**
     * @Config\Route("/new", name="bci_cms_block_new", methods="GET|POST")
     *
     * @Config\Template
     * @return array
     */
    public function new(Request $request)
    {
        $block = new Block();
        $form = $this->createForm(BlockType::class, $block);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $block->setCreatedAt(new \DateTime());
                $block->setUpdatedAt(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($block);
                $em->flush();
                $this->get('bci_cms.block_manager')->writeBlock($block);
                $this->addFlash(FlashType::TYPE_SUCCESS, $block->getName() . ' sauvegardé avec succès !');
                return $this->redirectToRoute('bci_cms_block_edit', ['id' => $block->getId()]);
            } catch (\Exception $e) {
                $this->addFlash(FlashType::TYPE_DANGER, $e->getMessage());
            }
        }
        return [
            'block' => $block,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Config\Route("/{id}/edit", name="bci_cms_block_edit", methods="GET|POST")
     *
     * @Config\Template
     * @return array
     */
    public function edit(Request $request, Block $block)
    {
        $block->setContent($this->get('bci_cms.block_manager')->getFileBlockContent($block));
        $form = $this->createForm(BlockType::class, $block);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $block->setUpdatedAt(new \DateTime());
                $this->getDoctrine()->getManager()->flush();
                $this->get('bci_cms.block_manager')->writeBlock($block);
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash(FlashType::TYPE_SUCCESS, $block->getName() . ' sauvegardé avec succès !');
            } catch (\Exception $e) {
                $this->addFlash(FlashType::TYPE_DANGER, $e->getMessage());
            } finally {
                return $this->redirectToRoute('bci_cms_block_edit', ['id' => $block->getId()]);
            }
        }

        return [
            'block' => $block,
            'form' => $form->createView()
        ];
    }

    /**
     * @Config\Route("/{id}", name="bci_cms_block_delete", methods="DELETE")
     */
    public function delete(Request $request, Block $block): Response
    {
        try {
            if ($this->isCsrfTokenValid('delete' . $block->getId(), $request->request->get('_token'))) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($block);
                $em->flush();
                $this->addFlash(FlashType::TYPE_SUCCESS, $block->getName() . ' supprimé avec succès !');
            }
        } catch (ForeignKeyConstraintViolationException $e) {
            $this->addFlash(FlashType::TYPE_DANGER, $block->getName() . ' est présentement utilisé dans un gabarit.');
            return $this->redirectToRoute('bci_cms_block_edit', ['id' => $block->getId()]);
        }
        return $this->redirectToRoute('bci_cms_block_index');
    }
}
