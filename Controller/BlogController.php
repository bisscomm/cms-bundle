<?php

namespace Bci\CmsBundle\Controller;


use Bci\CmsBundle\Entity\Blog;
use Bci\CmsBundle\Form\BlogType;
use Bci\CmsBundle\Repository\BlogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("")
 */
class BlogController extends AbstractController
{
    /**
     * @Route("cms/blogs", name="bci_cms_blog_index", methods={"GET"})
     */
    public function index(BlogRepository $blogRepository): Response
    {
        return $this->render('@BciCms/blog/index.html.twig', [
            'blogs' => $blogRepository->findAll(),
        ]);
    }

    /**
     * @Route("cms/blog/new/{locale}",
     *      name="bci_cms_blog_new",
     *      methods={"GET","POST"},
     *     requirements={
     *        "locale": "en|fr"
     *     })
     */
    public function new(Request $request, $locale): Response
    {
        $blog = new Blog();
        $blog->setTranslatableLocale($locale);
        $form = $this->createForm(BlogType::class, $blog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($blog);
            $entityManager->flush();

            return $this->redirectToRoute('bci_cms_blog_index');
        }

        return $this->render('@BciCms/blog/new.html.twig', [
            'blog' => $blog,
            'form' => $form->createView(),
            'locale' => $locale
        ]);
    }

    /**
     * @Route("cms/blog/{id}/edit/{locale}",
     *      name="bci_cms_blog_edit",
     *      methods={"GET","POST"},
     *      defaults={"locale"= "fr"},
     *      requirements={
     *        "locale": "en|fr"
     *     })
     */
    public function edit(Request $request, Blog $blog, $locale): Response
    {
        $blog->setTranslatableLocale($locale);
        $this->getDoctrine()->getManager()->refresh($blog);

        $form = $this->createForm(BlogType::class, $blog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($blog);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('bci_cms_blog_index');
        }

        return $this->render('@BciCms/blog/edit.html.twig', [
            'blog' => $blog,
            'form' => $form->createView(),
            'locale' => $locale

        ]);
    }

    /**
     * @Route("cms/blog/{id}/delete", name="bci_cms_blog_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Blog $blog): Response
    {
        if ($this->isCsrfTokenValid('delete'.$blog->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($blog);
            $entityManager->flush();
        }

        return $this->redirectToRoute('bci_cms_blog_index');
    }
}
