<?php

namespace Bci\CmsBundle\Controller;

use Bci\CmsBundle\Entity\Carousel;
use Bci\CmsBundle\Form\CarouselType;
use Bci\CmsBundle\Repository\CarouselRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("cms/carousel")
 */
class CarouselController extends AbstractController
{
    /**
     * @Route("s/", name="bci_cms_carousel_index", methods={"GET"})
     */
    public function index(CarouselRepository $carouselRepository): Response
    {
        return $this->render('@BciCms/carousel/index.html.twig', [
            'carousels' => $carouselRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="bci_cms_carousel_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $carousel = new Carousel();
        $form = $this->createForm(CarouselType::class, $carousel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($carousel);
            $entityManager->flush();

            return $this->redirectToRoute('bci_cms_carousel_index');
        }

        return $this->render('@BciCms/carousel/new.html.twig', [
            'carousel' => $carousel,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="bci_cms_carousel_show", methods={"GET"})
     */
    public function show(Carousel $carousel): Response
    {
        return $this->render('@BciCms/carousel/show.html.twig', [
            'carousel' => $carousel,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="bci_cms_carousel_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Carousel $carousel): Response
    {
        $form = $this->createForm(CarouselType::class, $carousel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('bci_cms_carousel_index');
        }

        return $this->render('@BciCms/carousel/edit.html.twig', [
            'carousel' => $carousel,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="bci_cms_carousel_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Carousel $carousel): Response
    {
        if ($this->isCsrfTokenValid('delete'.$carousel->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($carousel);
            $entityManager->flush();
        }

        return $this->redirectToRoute('bci_cms_carousel_index');
    }
}
