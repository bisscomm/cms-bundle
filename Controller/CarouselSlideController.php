<?php

namespace Bci\CmsBundle\Controller;


use Bci\CmsBundle\Enum\FlashType;
use Bci\CmsBundle\Helper\ControllerTrait;
use Bci\CmsBundle\Entity\CarouselSlide;
use Bci\CmsBundle\Entity\Carousel;
use Bci\CmsBundle\Form\CarouselSlideType;
use Bci\CmsBundle\Repository\CarouselSlideRepository;
use Doctrine\DBAL\DBALException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Transliterator;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Gedmo\Translatable\Entity\Repository\TranslationRepository;

/**
 * @Route("")
 */
class CarouselSlideController extends AbstractController
{
    use ControllerTrait;

    /**
     * @Route("cms/carousel/{id}/slides", name="bci_cms_slide_index", methods={"GET"})
     */
    public function index(Carousel $carousel): Response
    {

        return $this->render('@BciCms/article/index.html.twig', [
            'slides' => $carousel->getSlides(),
            'carouselId' => $carousel->getId()
        ]);
    }

    /**
     * @Route("cms/carousel/{id}/slide/new/{locale}",
     *      name="bci_cms_slide_new",
     *      methods={"GET","POST"},
     *     requirements={
     *        "locale": "en|fr"
     *     })
     */
    public function new(Request $request, Carousel $carousel, $locale, UploaderHelper $uploaderHelper): Response
    {
        $carouselSlide = new CarouselSlide();
        $carouselSlide->setTranslatableLocale($locale);
        $carouselSlide->setCarousel($carousel);
        $form = $this->createForm(CarouselSlideType::class, $carouselSlide);


        if ($this->isValid($form, $request))
        {
            $carouselSlide->setTranslatableLocale($locale);
            $this->preDeleteCarouselSlideImage($carouselSlide);
            $this->setNewPosition($carouselSlide);
            $this->getEm()->persist($carouselSlide);
            $this->getEm()->flush();

            return $this->redirectToRoute('bci_cms_carousel_edit', [
                'id' => $carousel->getId(),
                'locale' => $locale
            ]);

        }

        return $this->render('@BciCms/slide/new.html.twig', [
            'slides' => $carouselSlide,
            'form' => $form->createView(),
            'carouselId' => $carousel->getId(),
            'locale' => $locale
        ]);
    }

    /**
     * @Route("cms/carousel/slide/{id}/edit/{locale}",
     *      name="bci_cms_slide_edit",
     *      methods={"GET","POST"},
     *      defaults={"locale"= "fr"},
     *      requirements={
     *        "locale": "en|fr"
     *     }
     * )
     */
    public function edit(Request $request, CarouselSlide $carouselSlide, $locale, UploaderHelper $uploaderHelper): Response
    {
        $this->refreshCarouselSlideTranslation($carouselSlide, $locale);
        $form = $this->createForm(CarouselSlideType::class, $carouselSlide);

        if ($this->isValid($form, $request))
        {
            if (!is_numeric($carouselSlide->getPosition()))
            {
                $this->setNewPosition($carouselSlide);
            }
            $carouselSlide->setTranslatableLocale($locale);
            $this->preDeleteCarouselSlideImage($carouselSlide);
            
            $this->getEm()->persist($carouselSlide);
            $this->getEm()->flush();

            return $this->redirectToRoute('bci_cms_carousel_edit', [
                'id' => $carouselSlide->getCarousel()->getId(),
                'locale' => $locale
            ]);
        }

        return $this->render('@BciCms/slide/edit.html.twig', [
            'slide' => $carouselSlide,
            'form' => $form->createView(),
            'carouselId' => $carouselSlide->getCarousel()->getId(),
            'locale' => $locale
        ]);
    }

    /**
     * @param CarouselSlide $carouselSlide
     * @throws \Doctrine\ORM\ORMException
     * Verify if UplodableFileCms have to be delete.
     */
    private function preDeleteCarouselSlideImage(CarouselSlide $carouselSlide)
    {

        if ($carouselSlide->getImage()->isToDelete())
        {
            $this->getEm()->remove($carouselSlide->getImage());
            $carouselSlide->setImage(null);
        }

    }
    

    /**
     * @Route("cms/slide/{id}/{locale}", name="bci_cms_slide_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CarouselSlide $carouselSlide, $locale ): Response
    {
        $carouselID = $carouselSlide->getCarousel()->getId();
        if ($this->isCsrfTokenValid('delete' . $carouselSlide->getId(), $request->request->get('_token')))
        {
            $this->remove($carouselSlide);
        }
        return $this->redirectToRoute('bci_cms_carousel_edit', [
            'id' => $carouselID,
            'locale' => $request->getLocale(),
        ]);

    }


    /**
     * @Route("/cms/slide/{id}/delete/{locale}", name="bci_cms_slide_translation_delete", methods={"DELETE"})
     */
    public function deleteTranslation(Request $request, CarouselSlide $slide, $locale ): Response
    {
        if ($this->isCsrfTokenValid('delete' . $slide->getId(), $request->request->get('_token')))
        {
            $slideTranslations = $this->getEm()->getRepository('Gedmo\Translatable\Entity\Translation')->findTranslations($slide);

            if (array_key_exists($locale, $slideTranslations))
            {
                foreach ($slideTranslations[$locale] as $field => $content)
                {
                    $translationObj = $this->getEm()->getRepository('Gedmo\Translatable\Entity\Translation')->findBy([
                        'field' => $field,
                        'content' => $content,
                        'foreignKey' => $slide->getId(),
                        'locale' => $locale
                    ]);
                    $this->getEm()->remove($translationObj[0]);
                }
                $this->getEm()->flush();
            }
            else
            {
                $this->addFlash(FlashType::TYPE_WARNING, $request->getLocale() === 'fr' ? 'Aucune traduction [' . strtoupper($locale) . '] à supprimer' : 'There is no translation [' . strtoupper($locale) . '] to be deleted');
            }

        }
        return $this->redirectToRoute('bci_cms_carousel_edit', [
            'id' => $slide->getCarousel()->getId(),
            'locale' => $locale
        ]);

    }

    /**
     * @param CarouselSlide $carouselSlide
     * @param $locale
     * @return CarouselSlide
     */
    private function refreshCarouselSlideTranslation(CarouselSlide $carouselSlide, $locale)
    {
        $carouselSlide->setTranslatableLocale($locale);
        $this->getEm()->refresh($carouselSlide);

        return $carouselSlide;
    }

    /**
     * @param CarouselSlide $carouselSlide
     */
    private function setNewPosition(CarouselSlide $carouselSlide)
    {
        $maxVals = $carouselSlide->getCarousel()->getSlides()->map(function (CarouselSlide $carouselSlide){
            return $carouselSlide->getPosition();
        });
        if (count($maxVals) > 0)
        {
            $maxPos = max($maxVals->toArray());
            $carouselSlide->setPosition($maxPos+1);
        }
        else
        {
            $carouselSlide->setPosition(0);
        }
    }
}
