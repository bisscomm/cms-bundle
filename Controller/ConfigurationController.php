<?php

namespace Bci\CmsBundle\Controller;

use Bci\CmsBundle\Entity\Configuration;
use Bci\CmsBundle\Entity\Log;
use Bci\CmsBundle\Enum\FlashType;
use Bci\CmsBundle\Form\ConfigurationType;
use Bci\CmsBundle\Helper\ControllerTrait;
use Bci\CmsBundle\Repository\ConfigurationRepository;
use Doctrine\DBAL\Connection;
use Monolog\Logger;
use Spatie\DbDumper\Compressors\GzipCompressor;
use Spatie\DbDumper\Databases\PostgreSql;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("cms/configuration")
 */
class ConfigurationController extends AbstractController
{
    private static $DUPMFILE = 'dump.sql.gz';

    use ControllerTrait;

    /**
     * @Route("s/", name="bci_cms_configuration_index", methods={"GET"})
     */
    public function index(ConfigurationRepository $configurationRepository): Response
    {
        $configuration = $this->configurationExistOrCreate($configurationRepository);

        return $this->redirectToRoute('bci_cms_configuration_edit',[

        ]);
    }

    /**
     * @Route("/edit", name="bci_cms_configuration_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ConfigurationRepository $configurationRepository): Response
    {
        $configuration = $this->configurationExistOrCreate($configurationRepository);
        $form = $this->createForm(ConfigurationType::class, $configuration);

        if ($this->isValid($form, $request))
        {
            $this->save($configuration);
            $this->addFlash(FlashType::TYPE_SUCCESS, $request->getLocale() == 'fr' ? 'Configuration sauvegardé avec succès' : "Your configuration has been saved successfully");
        }
        return $this->render('@BciCms/configuration/edit.html.twig', [
            'configuration' => $configuration,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/dump/database", name="bci_cms_configuration_dump_database", methods={"GET","POST"})
     */
    public function dumpCurrentDatabase(Request $request)
    {
        if ($this->isGranted('ROLE_SUPER_ADMIN'))
        {
            $currentConnectionParams = $this->getDoctrine()->getConnection()->getParams();
            $databaseName = $currentConnectionParams['dbname'];
            $userName = $currentConnectionParams['user'];
            $password = $currentConnectionParams['password'];
            $host = $currentConnectionParams['host'];
            $port = $currentConnectionParams['port'];

            // Prevent postgressqlclient version change error
            $finder = new Finder();
            $finder->in($this->getParameter('kernel.project_dir') . '/../../../../usr/lib/postgresql/*')->directories();
            $dumpBinaryPath = '';
            foreach ($finder as $directory)
            {
                if ($directory->getRelativePathname() == 'bin')
                {
                    $dumpBinaryPath = $directory->getRealPath();
                }
            }

            PostgreSql::create()
                ->setDbName($databaseName)
                ->setUserName($userName)
                ->setPassword($password)
                ->setHost($host)
                ->setPort($port)
                ->useCompressor(new GzipCompressor())
                ->setDumpBinaryPath($dumpBinaryPath)
                ->addExtraOption('--inserts')
                ->addExtraOption('--clean')
                ->addExtraOption('--if-exists')
                ->dumpToFile('../' . self::$DUPMFILE);

            $sqlFileToReturn = new File($this->getParameter('kernel.project_dir') . "/" . self::$DUPMFILE);
            $this->logDatabaseDump('SUCCESSFULLY');
            return $this->file($sqlFileToReturn, date_format(new \DateTime('now'), 'Y-m-d H:i:s') . ".sql.gz", ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        }
        $this->logDatabaseDump('TRY TO');
        return Response::HTTP_FORBIDDEN;
    }

    /**
     * Add a log entry when a user dump the database
     */
    private function logDatabaseDump($action)
    {
        $logEntry = new Log();
        $logEntry
            ->setMessage('User: '.$this->getUser()->getUsername().' -- '. $action ." DUMP THE DATABASE")
            ->setLevel(Logger::NOTICE)
            ->setLevelName('NOTICE')
            ->setChannel('DATABASE DUMP');
        $this->save($logEntry);
    }

    /**
     * @param ConfigurationRepository $configurationRepository
     * @return Configuration
     */
    private function configurationExistOrCreate(ConfigurationRepository $configurationRepository)
    {
        if (count($configurationRepository->findAll()) == 0)
        {
            $configuration = new Configuration();
            $this->save($configuration);
            return $configuration;
        }
        return $configurationRepository->findAll()[0];
    }

}
