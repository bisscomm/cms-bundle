<?php

namespace Bci\CmsBundle\Controller;

use Bci\CmsBundle\Helper\HelperTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Config;

class Controller
    extends BaseController
{
   use HelperTrait;
}