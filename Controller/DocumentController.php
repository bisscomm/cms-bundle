<?php

namespace Bci\CmsBundle\Controller;

use Bci\CmsBundle\Entity\Block;
use Bci\CmsBundle\Form\BlockType;
use Bci\CmsBundle\Repository\BlockRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Config;

/**
 * @Config\Route("/cms/document")
 */
class DocumentController extends Controller
{
    /**
     * @Config\Route("s", name="bci_cms_document_index", methods="GET")
     *
     * @Config\Template
     * @return array
     */
    public function index(BlockRepository $blockRepository)
    {
        return [];
    }
}
