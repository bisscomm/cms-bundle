<?php

namespace Bci\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class FileManagerController extends AbstractController
{

    /**
     * @Route("/cms/filemanager", name="filemanager_index", methods={"GET"})
     */
    public function index()
    {

        return $this->render('@BciCms/filemanager/index.html.twig', [

        ]);
    }


}