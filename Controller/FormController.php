<?php

namespace Bci\CmsBundle\Controller;

use Bci\CmsBundle\Entity\Document;
use Bci\CmsBundle\Entity\Form;
use Bci\CmsBundle\Entity\FormItem;
use Bci\CmsBundle\Entity\FormSubscription;
use Bci\CmsBundle\Entity\FormSubscriptionAnswer;
use Bci\CmsBundle\Entity\FormTranslations;
use Bci\CmsBundle\Enum\FlashType;
use Bci\CmsBundle\Form\FormType;
use Bci\CmsBundle\Model\Fields\DateTime;
use Bci\CmsBundle\Repository\FormItemRepository;
use Bci\CmsBundle\Repository\FormRepository;
use Bci\CmsBundle\Repository\FormSubscriptionAnswerRepository;
use Bci\CmsBundle\Repository\FormSubscriptionRepository;
use Bci\CmsBundle\Repository\FormTranslationsRepository;
use Bci\CmsBundle\Service\FormManager;
use Bci\CmsBundle\Service\PostMarkService;
use Bci\CmsBundle\Service\SendGridService;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Config;
use Doctrine\DBAL\Connection;
use Postmark\Models\PostmarkException;
use function Symfony\Component\String\s;


/**
 * @Route("/")
 */
class FormController extends AbstractController
{

    private $sendgrid;

    private $postMarkService;

    public function __construct(SendGridService $sendgrid, PostMarkService $postMarkService, FormManager $formManager)
    {
        $this->sendgrid = $sendgrid;
        $this->postMarkService = $postMarkService;
        $this->formManager = $formManager;
    }

    /**
     * @Config\Route("cms/forms", name="form_index", methods={"GET"})
     *
     * @Config\Template
     * @return array
     */
    public function index(FormRepository $formRepository): array
    {
        return [
            'forms' => $formRepository->findAll(),
        ];
    }

    /**
     * @Config\Route(
     *     "cms/form/new/{locale}",
     *     name="form_new",
     *     methods={"GET","POST"},
     *     requirements={
     *         "locale": "en|fr"
     *     })
     *
     * @param string $locale
     *
     * @Config\Template
     * @return array
     */
    public function new(Request $request, $locale)
    {
        $entity = new Form();
        $formTranslations = new FormTranslations();
        $formTranslations->setLocale($locale);
        $entity->addFormTranslations($formTranslations);

        $form = $this->createForm(FormType::class, $entity, ['locale' => $locale]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            try
            {
                $em = $this->getDoctrine()->getManager();
                foreach ($entity->getFormTranslations() as $formTranslation)
                {
                    if ($formTranslation->getLocale() == $locale)
                    {
                        $em->persist($formTranslation);
                    }
                }
                foreach ($entity->getItems() as $item)
                {
                    $item
                        ->setTranslatableLocale($locale)
                        ->setForm($entity)
                        ->setCreatedAt(new \DateTime())
                        ->setUpdatedAt(new \DateTime());
                    $em->persist($item);
                    $item->setToken($item->slugify($item->getLabel()) . '_' . $item->getId());
                    $em->persist($item);
                }
                $entity
                    ->setCreatedAt(new \DateTime())
                    ->setUpdatedAt(new \DateTime());
                $em->persist($entity);
                $em->flush();
                $this->addFlash(FlashType::TYPE_SUCCESS, $entity->getTitle() . ' sauvegardé avec succès !');
                return $this->redirectToRoute('form_edit', ['id' => $entity->getId(), 'locale' => $locale]);
            }
            catch (\Exception $e)
            {
                $this->addFlash(FlashType::TYPE_DANGER, $e->getMessage());
            }
        }

        return [
            'entity' => $entity,
            'form' => $form->createView(),
            'locale' => $locale
        ];
    }

    /**
     * @Config\Route(
     *     "cms/form/{id}/edit/{locale}",
     *     name="form_edit",
     *     methods={"GET","POST"},
     *     requirements={
     *         "locale": "en|fr"
     *     })
     *
     * @param string $locale
     *
     * @Config\Template
     * @return array
     */
    public function edit(Request $request, Form $entity, $locale)
    {
        $otherLocaleItems = $entity->getItems();

        if (!$entity->hasTranslationsLocale($locale))
        {
            $formTranslations = new FormTranslations();
            $formTranslations->setLocale($locale);
            $entity->addFormTranslations($formTranslations);
        }
        $this->refreshFormItemsLocale($entity, $locale);
        $form = $this->createForm(FormType::class, $entity, ['locale' => $locale]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            try
            {
                $em = $this->getDoctrine()->getManager();
                foreach ($entity->getFormTranslations() as $formTranslation)
                {
                    if ($formTranslation->getLocale() == $locale)
                    {
                        $em->persist($formTranslation);
                    }
                }
                foreach ($entity->getItems() as $item)
                {
                    $item->setTranslatableLocale($locale);
                    $item->setForm($entity)->setUpdatedAt(new \DateTime());
                    $em->persist($item);
                    if (!$item->getToken())
                    {
                        $item->setToken($item->slugify($item->getLabel()) . '_' . $item->getId());
                        $em->persist($item);
                    }
                }

                $entity->setUpdatedAt(new \DateTime());
                $em->persist($entity);
                $em->flush();
                $this->addFlash(FlashType::TYPE_SUCCESS, $entity->getTitle() . ' sauvegardé avec succès !');
            }
            catch (\Exception $e)
            {
                $this->addFlash(FlashType::TYPE_DANGER, $e->getMessage());
            }
            finally
            {
                return $this->redirectToRoute('form_edit', ['id' => $entity->getId(), 'locale' => $locale]);
            }
        }

        return [
            'entity' => $entity,
            'form' => $form->createView(),
            'locale' => $locale
        ];
    }

    /**
     * @Config\Route(
     *     "cms/form/{id}/posted",
     *     name="form_posted_data",
     *     methods={"GET"},
     *     requirements={
     *         "locale": "en|fr"
     *     })
     *
     * @Config\Template
     * @return array
     */
    public function postedData(Request $request, Form $entity)
    {

        $filteredItems = $entity->getItems()->filter(function (FormItem $formItem)
        {
            return $formItem->getPresentInPostedData() != null && $formItem->getPresentInPostedData() != 0;
        });

        $iterator = $filteredItems->getIterator();
        $iterator->uasort(function ($a, $b)
        {
            return ($a->getPresentInPostedData() < $b->getPresentInPostedData()) ? -1 : 1;
        });

        $orderedItems = new ArrayCollection(iterator_to_array($iterator));
        //The user haven't selected a particular order.        
        if ($orderedItems->count() == 0)
        {
            $orderedItems = new ArrayCollection();
            foreach ($entity->getItems() as $key => $formItem)
            {
                if ($key <= 3)
                {
                    $orderedItems->add($formItem);
                }
                else
                {
                    break;
                }
            }
        }
        return [
            'entity' => $entity,
            'toDisplayItems' => $orderedItems,
            'subscriptions' => $entity->getSubscriptions()
        ];

    }

    /**
     * @Config\Route(
     *     "cms/form/{form_id}/posted/{id}",
     *     name="form_view_posted",
     *     methods={"GET"},
     *     requirements={
     *         "locale": "en|fr"
     *     })
     *
     * @param string $locale
     *
     * @Config\Template
     * @return array
     */
    public function viewPostedData(Request $request, FormSubscription $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $date = new \DateTime();
        $entity->setStatut($date->format('Y-m-d H:i:s'));
        $em->persist($entity);
        $em->flush();

        return [
            'entity' => $entity
        ];
    }

    /**
     * @Config\Route(
     *     "cms/form/{form_id}/posted/{id}",
     *     name="form_delete_posted",
     *     methods={"DELETE"})
     *
     * @param string $locale
     *
     * @Config\Template
     * @return array
     */
    public function deletePostedData(Request $request, FormSubscription $entity)
    {
        if ($this->isCsrfTokenValid('delete' . $entity->getId(), $request->request->get('_token')))
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();
            $this->addFlash(FlashType::TYPE_SUCCESS, 'Soumission supprimé avec succès !');
        }

        return $this->redirectToRoute('form_posted_data', ['id' => $entity->getForm()->getId()]);

    }

    /**
     * @Config\Route(
     *     "form/{token}/post/{locale}",
     *     name="form_post_data",
     *     methods={"POST"},
     *     requirements={
     *         "locale": "en|fr"
     *     })
     *
     * @param string $locale
     *
     * @return Response
     */
    public function postData(Request $request, Form $entity, $locale)
    {
        

        if ($this->formManager->getRecaptchaValidation())
        {
            $emailArray = [];
            $translation = $this->getDoctrine()->getRepository(FormTranslations::class)->findOneBy(
                ['form' => $entity->getId(), 'locale' => $locale]);

            $subscription = new FormSubscription();
            $subscription
                ->setCreatedAt(new \DateTime())
                ->setUpdatedAt(new \DateTime())
                ->setForm($entity)
                ->setReferer($request->headers->get('referer'));

            $em = $this->getDoctrine()->getManager();
            $em->persist($subscription);


            $emailArray['title'] = $locale == 'fr' ? 'Nouvelle demande pour le formulaire' : 'New request for the form';
            $emailArray['formTitle'] = $translation->getTitle();
            $emailArray['url'] = $request->headers->get('referer') ?? 'Not Available';
            $emailArray['date'] = $subscription->getCreatedAt()->format('Y/m/d');
            $emailArray['hour'] = $subscription->getCreatedAt()->format('H:i:s');
            $emailArray['content'] = [];

            if (!empty($request->request->all()))
            {
                foreach ($request->request->all()[$entity->getToken()] as $field => $value)
                {
                    $answer = new FormSubscriptionAnswer();

                    if (is_array($value))
                    {
                        $value = json_encode($value, JSON_UNESCAPED_UNICODE);
                    }

                    $answer
                        ->setSubscription($subscription)
                        ->setValue($value)
//                    ->setStatut(0)
                        ->setItem($this->getDoctrine()->getRepository(FormItem::class)->findOneBy(['form' => $entity->getId(), 'token' => $field]))
                        ->setCreatedAt(new \DateTime())
                        ->setUpdatedAt(new \DateTime());

                    $em->persist($answer);

                    if (is_array(json_decode($answer->getValue())))
                    {
                        $value = json_decode($answer->getValue());
                        $answer_value = trim(preg_replace('/\s+/', ', ', implode($value)));
                    }
                    else
                    {
                        $answer_value = $answer->getValue();
                    }

                    if ($answer->getItem()->getType() == 'radio')
                    {
                        $emailArray['content'][$answer->getItem()->getLabel()] = $answer->getValue();
                    }
                    else
                    {
                        $emailArray['content'][$answer->getItem()->getLabel()] = $answer_value;
                    }

                }
            }

            if (!empty($request->files->all()))
            {
                foreach ($request->files->all()[$entity->getToken()] as $field => $file)
                {
                    if ($file != null)
                    {
                        $path = '../public/upload/files/';

                        $document = new Document();

                        $document
                            ->setOriginalFilename($file->getClientOriginalName())
                            ->setAssetFilename($file->getClientOriginalName())
                            ->setMimeType($file->getMimeType())
                            ->setPath($path)
                            ->setSize($file->getSize());

                        $em->persist($document);

                        $document->setAssetFilename($document->getId() . "-" . $document->getOriginalFilename());
                        $em->persist($document);

                        $filename = $document->getAssetFilename();

                        $file->move(
                            $path,
                            $filename
                        );

                        $answer = new FormSubscriptionAnswer();
                        $answer
                            ->setSubscription($subscription)
                            ->setValue($filename)
                            ->setItem($this->getDoctrine()->getRepository(FormItem::class)->findOneBy(['form' => $entity->getId(), 'token' => $field]))
                            ->setCreatedAt(new \DateTime())
                            ->setUpdatedAt(new \DateTime());

                        $em->persist($answer);

                        //Check wheter the host is secure or not and build sheme + base domain to generate email file link.
                        $host = ($request->isSecure() ? 'https://' : 'http://').$request->headers->get('host');

                        $emailArray['file'][$answer->getItem()->getHelp() ? $answer->getItem()->getHelp() : $answer->getItem()->getLabel()] = $host . '/upload/files/' . $answer->getValue();
                    }

                }
            }

            $em->flush();
            $emails = explode(',', $entity->getEmail());


            if (array_key_exists("postmark", $this->getParameter('bci_cms')))
            {
                try
                {
                    $sendResult = $this->postMarkService->send(
                        null,
                        $emails,
                        $emailArray['title'],
                        $this->render('@BciCms/email/form_received.html.twig', [
                            'emailArray' => $emailArray,
                        ])->getContent()
                    );

                }
                catch (PostmarkException $ex)
                {
                    // If client is able to communicate with the API in a timely fashion,
                    // but the message data is invalid, or there's a server error,
                    // a PostmarkException can be thrown.


                    //********* LOG THIS **********                   
//                    echo $ex->httpStatusCode;
//                    echo $ex->message;
//                    echo $ex->postmarkApiErrorCode;
                    //*****************************

                }
                catch (Exception $generalException)
                {
                    // A general exception is thrown if the API
                    // was unreachable or times out.
                }
                return $this->redirect(
                    $entity->getPage()
                        ? $this->generateUrl('bci_cms_page_show', ['slug' => $entity->getPage()->getSlugForLocale($request->getLocale())])
                        : $request->headers->get('referer')
                );

            }
            elseif (array_key_exists("sendgrid", $this->getParameter('bci_cms')))
            {
                foreach ($emails as $email)
                {
                    $this->sendgrid->send(
                        [
                            'email' => $this->getParameter('bci_cms')['sendgrid']['noreply'],
                            'name' => $this->getParameter('bci_cms')['sendgrid']['noreplyName'],
                        ],
                        [
                            'email' => $email,
                            'name' => $this->getParameter('bci_cms')['sendgrid']['notificationName']
                        ],
                        $emailArray['title'],
                        [
                            [
                                'type' => 'text/html',
                                'body' => $this->render('@BciCms/email/form_received.html.twig', [
                                    'emailArray' => $emailArray,
                                ])->getContent()
                            ]
                        ]
                    );
                }
                return $this->redirect(
                    $entity->getPage()
                        ? $this->generateUrl('bci_cms_page_show', ['slug' => $entity->getPage()->getSlugForLocale($request->getLocale())])
                        : $request->headers->get('referer')
                );
            }
        }
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Config\Route("cms/form/{id}/delete", name="form_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Form $form): Response
    {
        if ($this->isCsrfTokenValid('delete' . $form->getId(), $request->request->get('_token')))
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($form);
            $em->flush();
            $this->addFlash(FlashType::TYPE_SUCCESS, $form->getTitle() . ' supprimé avec succès !');
        }
        return $this->redirectToRoute('form_index');
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

    /**
     * Use to refresh each Form FormItems in their specific local
     * @param Form $form
     * @param $locale
     */
    private function refreshFormItemsLocale(Form $form, $locale)
    {
        foreach ($form->getItems() as $formItem)
        {
            $formItem->setTranslatableLocale($locale);
            $this->getDoctrine()->getManager()->refresh($formItem);
        }
    }
}
