<?php

namespace Bci\CmsBundle\Controller;

use Bci\CmsBundle\Helper\ControllerTrait;
use Bci\CmsBundle\Entity\JobCategory;
use Bci\CmsBundle\Repository\JobCategoryRepository;
use Doctrine\Common\Collections\Collection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class JobCategoryController extends AbstractController
{
    use ControllerTrait;

    /**
     * @Route("cms/new/job_category", name="job_category")
     */
    public function newCategory(JobCategoryRepository $jobCategoryRepository, Request $request)
    {

        $categoryFR = $request->get('categoryNameFr');
        $categoryEN = $request->get('categoryNameEn');
        if (empty($categoryFR) || empty($categoryEN))
        {
            return new Response(
                ($request->getLocale() == 'fr') ? "Tout les champs doivent être complétés " : "All fields must be complete",
                Response::HTTP_NOT_ACCEPTABLE
            );
        }


        $categoriesFr = $jobCategoryRepository->findByTitleLocale( mb_strtolower($categoryFR), 'fr');
        $categoriesEn = $jobCategoryRepository->findByTitleLocale( mb_strtolower($categoryEN), 'en');

        if ($categoriesFr || $categoriesEn)
        {
            return new Response(
                ($request->getLocale() == 'fr') ? "Cette catégorie existe déja " : "Category already exist ",
                Response::HTTP_FORBIDDEN
            );
        }
        else
        {
            /**
             * Create a new category en/fr
             * so it is available for
             * all jobs in the website.
             */
            $newCategory = new JobCategory();
            $newCategory
                ->setTitle(mb_strtolower($categoryFR))
                ->setTranslatableLocale('fr');
            $this->getDoctrine()->getManager()->persist($newCategory);
            $this->getDoctrine()->getManager()->flush();
            $newCategory
                ->setTitle(mb_strtolower($categoryEN))
                ->setTranslatableLocale('en');
            $this->getDoctrine()->getManager()->persist($newCategory);
            $this->getDoctrine()->getManager()->flush();

            $jsonData = [
                'name' => ($request->getLocale() == 'fr') ? $categoryFR : $categoryEN,
                'id' => $newCategory->getId(),
                'locale' => $request->getLocale()
            ];
            return new JsonResponse($jsonData);
        }
    }

    /**
     * @Route("cms/delete/job_category", name="job_category_delete")
     */
    public function delete(Request $request)
    {
        $jobCategoryRepository = $this->getDoctrine()->getRepository(JobCategory::class);
        $jobCategory = $jobCategoryRepository->find($request->get('jobCategoryId'));
        $jobCategory->setTranslatableLocale($request->getLocale());
        $this->getDoctrine()->getManager()->refresh($jobCategory);
        $categoryName = $jobCategory->getTitle();
        $this->remove($jobCategory);
        $jsonData = [
            'name' => $categoryName,
            'locale' => $request->getLocale()
        ];
        return new JsonResponse($jsonData);
    }

    /**
     * @Route("cms/NumberOfRelatedJobs/job_category", name="job_category_number")
     */
    public function getNumberOfRelatedArticles (Request $request)
    {
        $jobCategoryRepository = $this->getDoctrine()->getRepository(JobCategory::class);
        $numberOfRelatedJobs = $jobCategoryRepository->find($request->get('jobCategoryId'))->getJobs()->count();
        $jsonData = [
            'numberOfRelatedJobs' => $numberOfRelatedJobs
        ];
        return new JsonResponse($jsonData);
    }
}
