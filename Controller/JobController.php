<?php

namespace  Bci\CmsBundle\Controller;

use Bci\CmsBundle\Entity\Job;
use Bci\CmsBundle\Entity\Meta;
use Bci\CmsBundle\Form\JobType;
use Bci\CmsBundle\Helper\ControllerTrait;
use Bci\CmsBundle\Repository\JobRepository;
use Bci\CmsBundle\Service\JobService;
use Bci\CmsBundle\Entity\Form;
use Bci\CmsBundle\Entity\FormItem;
use Bci\CmsBundle\Entity\FormSubscription;
use Bci\CmsBundle\Entity\Template;
use Bci\CmsBundle\Enum\FlashType;
use Bci\CmsBundle\Repository\FormItemRepository;
use Bci\CmsBundle\Repository\FormRepository;
use Bci\CmsBundle\Repository\FormSubscriptionRepository;
use Bci\CmsBundle\Service\OwnerService;
use Bci\CmsBundle\Service\TranslationService;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Config;


class JobController extends AbstractController
{
    use ControllerTrait;

    protected $careerFormID;

    protected $formItemDropDownToken;

    protected $careersTemplate;

    protected $careerTemplate;

    protected $ownerService;
    
    protected $jobCategoryEnabled;

    public function __construct(ParameterBagInterface $parameterBag, OwnerService $ownerService)
    {
        $this->careerFormID = $parameterBag->get('bci_cms')['default_job']['career_form_id'];
        
        $this->formItemDropDownToken = $parameterBag->get('bci_cms')['default_job']['career_form_item_select_token'];

        $this->careersTemplate = $parameterBag->get('bci_cms')['default_job']['careers_template'];

        $this->careerTemplate = $parameterBag->get('bci_cms')['default_job']['career_template'];

        $this->ownerService = $ownerService;

        $this->jobCategoryEnabled = isset($parameterBag->get('bci_cms')['default_job']['job_category_enabled']) ? $parameterBag->get('bci_cms')['default_job']['job_category_enabled'] : false ;
    }

    /**
     *  @Route("/cms/jobs/", name="bci_cms_job_index", methods={"GET"})
     *
     * @Config\Template
     * @return array
     */
    public function index(JobRepository $jobRepository): array
    {
        return [
            'jobs' => $jobRepository->findAll(),
        ];
    }

    /**
     * @Route("/cms/job/new/{locale}",
     * name="bci_cms_job_new",
     * methods={"GET","POST"},
     * requirements={
     *        "locale": "en|fr"
     *     }
     * )
     * @Config\Template
     * @return mixed
     */
    public function new(Request $request, FormItemRepository $formItemRepository, $locale)
    {
        $job = new Job();
        $this->setWebsiteOwnerAddress($job, $locale);
        $form = $this->createForm(JobType::class, $job);

        if ($this->isValid($form, $request))
        {
            $job->setTranslatableLocale($locale);
            /**
             * @var $formItem FormItem
             */

            $formItem = $formItemRepository->findOneBy(['token' => $this->formItemDropDownToken]);
            $formItem->setTranslatableLocale($locale);
            $this->getEm()->refresh($formItem);


            $newConfig = $formItem->getConfig();
            if (stristr($newConfig, $job->getTitle()))
            {
                $this->addFlash(FlashType::TYPE_DANGER, $request->getLocale() === 'fr' ? 'Le titre de ce poste est déja utilisé' : 'The job title is already used');
                return [
                    'job' => $job,
                    'form' => $form->createView()
                ];
            }
            $newConfig .= "\r\n" . $job->getTitle();
            $formItem->setConfig($newConfig);

            foreach ($job->getMetas() as $meta)
            {
                $meta->setTranslatableLocale($locale);
                $this->preDeleteMetaImage($meta);
            }

            $this->save($job);
            $this->hideShowJobFromForm($formItem, $job, $locale);
            $this->save($formItem);

//            $this->addFlash(FlashType::TYPE_WARNING,$request->getLocale() === 'fr' ? 'Compléter la traduction du nouveau poste.' : 'Complete new job translation');
            return $this->redirectToRoute('bci_cms_job_index');
        }

        return [
            'job' => $job,
            'form' => $form->createView(),
            'jobCategoryEnabled' => (boolean) $this->jobCategoryEnabled
        ];
    }

    /**
     * @Route("/cms/job/{id}/edit/{locale}",
     *  name="bci_cms_job_edit",
     *  methods={"GET","POST"},
     *  requirements={
     *        "locale": "en|fr"
     *     }
     * )
     * @param Request $request
     *
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @Config\Template
     */
    public function edit(Request $request, Job $job, FormItemRepository $formItemRepository, $locale)
    {
        $this->refreshJobAndMetasTranslation($job, $locale);
        $oldJob = clone $job;

        $form = $this->createForm(JobType::class, $job);

        if ($this->isValid($form, $request))
        {
            if ($form->get('preview')->isClicked())
            {
                $jobHasChange = false;
                $uow = $this->getEm()->getUnitOfWork();
                $uow->computeChangeSets();
                $changeSet = $uow->getEntityChangeSet($job);
                if(!empty($changeSet)){
                    $jobHasChange = true;
                }
                $request->getSession()->set('job', [
                    'job' => $job,
                    'hasChange' => $jobHasChange
                ]);
                return $this->redirectToRoute('bci_cms_job_preview', [
                    'locale' => $locale
                ]);
            }

            $job->setTranslatableLocale($locale);
            /**
             * @var $formItem FormItem
             */
            $formItem = $formItemRepository->findOneBy(['token' => $this->formItemDropDownToken]);

            $formItem->setTranslatableLocale($locale);
            $this->getEm()->refresh($formItem);

            $newConfig = $formItem->getConfig();
            if ($oldJob->getTitle() != $job->getTitle() && $oldJob->getTitle())
            {
                $newConfig = str_ireplace($oldJob->getTitle(), $job->getTitle(), $newConfig);
                $formItem->setConfig($newConfig);
            }
            else
            {
                if (!stristr($newConfig, $job->getTitle()))
                {
                    $newConfig .= "\r\n" . $job->getTitle();
                    $formItem->setConfig($newConfig);
                }
            }
            foreach ($job->getMetas() as $meta)
            {
                $meta->setTranslatableLocale($locale);
                $this->preDeleteMetaImage($meta);
            }
            $this->save($job);
            $this->hideShowJobFromForm($formItem, $job, $locale);
            $this->save($formItem);

            return $this->redirectToRoute('bci_cms_job_index');
        }

        return [
            'job' => $job,
            'form' => $form->createView(),
            'locale' => $locale,
            'jobCategoryEnabled' => (boolean) $this->jobCategoryEnabled
        ];
    }


    /**
     * This route generate an job offer preview using:
     * careersTemplate for page
     * careerTemplate for the actual template
     * Both of them are automaticaly defined on fixtures load of new project,
     * Defined in config/package/bci_cms.yaml -> key: default_job
     *
     * @Route("/cms/job/preview/{locale}", name="bci_cms_job_preview", methods={"GET|POST"}, requirements={"locale":"fr|en"}, defaults={"locale":"fr"})
     * @param Request $request
     * @param $locale
     * @return Response
     */
    public function preview(Request $request, $locale)
    {

        // Keeping request and session locale
        $oldRequestLocale = $request->getLocale();
        $oldSessionLocale = $request->getSession()->get('_locale');

        // Set the requested local to render page in the correct locale
        $request->setLocale($locale);
        $request->getSession()->set('_locale', $locale);

        $job = $request->getSession()->remove('job');
        $hasChange = $job['hasChange'];
        $job = $job['job'];

        if (!$job)
        {
            throw $this->createNotFoundException('Not Found');
        }

        $hasChange
            ?
            $this->addFlash('CMS_DRAFT_OVERRIDE', "Cette page contient des changements non sauvegardés, n'oubliez pas d'enregistré vos changement dans l'onglet précédent.")
            :
            $this->addFlash('CMS_DRAFT_OVERRIDE', "Cette page est un apperçu.");


        //**************DISPLAY PART*******************

        // Get the template that is associate with the page careers -> /careers
        $template = $this->getDoctrine()->getRepository(Template::class)->find($this->careersTemplate);
        $page = $template->getPages()[0];
        //Render view in a variable using the real template to display ONE JOB -> /careers/ONE_JOB_ID
        $viewRendered = $this->render('templates/template_' . $this->careerTemplate . '.html.twig', [
            'job' => $job,
            'selectHidden' => true,
            'page' => $page
        ]);

        //Get the html content of the view
        $viewHtmlContent = $viewRendered->getContent();

        //Find the form job's selectPicker and tick it to the right job.
        $viewHtmlContent = str_replace('<option value="' . $job->getTitle() . '">', '<option selected value="' . $job->getTitle() . '">', $viewHtmlContent);


        //Set modified html content to the view.
        $viewRendered->setContent($viewHtmlContent);

        // Re setting the system to previously saved locale (request/session)
        // So the cmsUser locale isn't being change on next request.
        $request->setLocale($oldRequestLocale);
        $request->getSession()->set('_locale', $oldSessionLocale);




        return $viewRendered;
    }

//    /**
//     * @Route("/carrieres/{slug}", name="bci_cms_job_show_fr", methods={"GET"})
//     * @Route("/careers/{slug}", name="bci_cms_job_show_en", methods={"GET"})
//     */
    public function show(Request $request, $slug, JobRepository $jobRepository)
    {
        /**
         * @var $job Job
         */
        $job = $jobRepository->findBySlugLocale($slug, $request->getLocale());

        if (($request->attributes->get('_route') === 'job_show_fr' && $request->getLocale() == 'en')
            || ($request->attributes->get('_route') === 'job_show_en' && $request->getLocale() == 'fr')) {

            return $this->redirectToRoute($request->getLocale() == 'fr' ? 'job_show_fr' : 'job_show_en', [
                'slug' => $job->getSlug()
            ]);
        }
        if (!$job)
        {
            throw $this->createNotFoundException('Page introuvable');
        }

        // Get the template that is associate with the page careers -> /careers
        $template = $this->getDoctrine()->getRepository(Template::class)->find($this->careersTemplate);
        $page = $template->getPages()[0];

        //Render view in a variable using the real template to display ONE JOB -> /careers/ONE_JOB_ID
        $viewRendered = $this->render('templates/template_'.$this->careerTemplate.'.html.twig', [
            'job' => $job,
            'selectHidden' => true,
            'page' => $page
        ]);

        //Get the html content of the view
        $viewHtmlContent = $viewRendered->getContent();

        //Find the form job's selectPicker and tick it to the right job.
        $viewHtmlContent = str_replace('<option value="'.$job->getTitle().'">','<option selected value="'.$job->getTitle().'">', $viewHtmlContent);

        //Set modified html content to the view.
        $viewRendered->setContent($viewHtmlContent);


        return $viewRendered;
    }

    /**
     * @Route("/cms/job/{id}/subscriptions", name="bci_cms_job_subscriptions", methods={"GET"})
     */
    public function formSubscriptionByJobs(Request $request, Job $job, FormSubscriptionRepository $formSubscriptionRepository)
    {
        //Get the form associated with jobs
        $careerForm = $this->getDoctrine()->getRepository(Form::class)->find($this->careerFormID);

        //Get the subscription based on aswered values (EN/FR)
        $subscriptions = $this->findJobSubscriptionsBillingual($formSubscriptionRepository,$job);

        // Filter formitems to the right order.
        $filteredItems = $careerForm->getItems()->filter(function (FormItem $formItem)
        {
            return $formItem->getPresentInPostedData() != null && $formItem->getPresentInPostedData() != 0;
        });

        $iterator = $filteredItems->getIterator();
        $iterator->uasort(function ($a, $b)
        {
            return ($a->getPresentInPostedData() < $b->getPresentInPostedData()) ? -1 : 1;
        });

        $orderedItems = new ArrayCollection(iterator_to_array($iterator));

        //The user haven't selected a particular order.
        if ($orderedItems->count() == 0)
        {
            $orderedItems = new ArrayCollection();
            foreach ($careerForm->getItems() as $key => $formItem)
            {
                if ($key <= 3)
                {
                    $orderedItems->add($formItem);
                }
                else
                {
                    break;
                }
            }
        }


        return $this->render('@BciCms/job/subscriptions.html.twig', [
            'job' => $job,
            'subscriptions' => $subscriptions,
            'toDisplayItems' => $orderedItems,
        ]);
    }

    /**
     * @Route("cms/job/{job_id}/subscription/{id}", name="bci_cms_job_view_subscription", methods={"GET","POST"})
     * @ParamConverter("job", options={"id" = "job_id"})
     */
    public function subscription(Request $request, Job $job, FormSubscription $subscription): Response
    {
        $this->setSubscriptionNotificationOff($subscription);
        
        return $this->render('@BciCms/job/subscription.html.twig', [
            'job' => $job,
            'subscription' => $subscription
        ]);
    }

    /**
     * @Route("/cms/job/{id}/delete", name="bci_cms_job_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Job $job, FormItemRepository $formItemRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $job->getId(), $request->request->get('_token')))
        {
            foreach (['fr','en'] as $availLocale)
            {
                $job->setTranslatableLocale($availLocale);
                $this->getEm()->refresh($job);
                if (!empty($job->getTitle()))
                {
                    $formItem = $formItemRepository->findOneBy(['token' => $this->formItemDropDownToken]);
                    $formItem->setTranslatableLocale($availLocale);
                    $this->getEm()->refresh($formItem);

                    $newConfig = $formItem->getConfig();
                    $newConfig = str_ireplace("\r\n" . $job->getTitle(), "", $newConfig);
                    $formItem->setConfig($newConfig);
                    $this->save($formItem);
                }
            }
            $this->remove($job);
        }
        return $this->redirectToRoute('bci_cms_job_index');
    }

    /**
     * @Route("/cms/job/{id}/delete/{locale}", name="bci_cms_job_translation_delete", methods={"DELETE"})
     */
    public function deleteTranslation(Request $request, Job $job, FormItemRepository $formItemRepository, $locale ): Response
    {
        if ($this->isCsrfTokenValid('delete' . $job->getId(), $request->request->get('_token')))
        {
            if ($this->deleteJobFromFormItem($job, $formItemRepository, $locale))
            {
                $jobTranslation = $this->getEm()->getRepository('Gedmo\Translatable\Entity\Translation')->findTranslations($job);
                foreach ($jobTranslation[$locale] as $field => $content)
                {
                    $translationObj = $this->getEm()->getRepository('Gedmo\Translatable\Entity\Translation')->findBy([
                        'field' => $field,
                        'content' => $content,
                        'foreignKey' => $job->getId(),
                        'locale' => $locale
                    ]);
                    $this->getEm()->remove($translationObj[0]);
                }
                $this->getEm()->flush();
            }
            else
            {
                $this->addFlash(FlashType::TYPE_WARNING,$request->getLocale() === 'fr' ? 'Aucune traduction ['.strtoupper ($locale).'] à supprimer' : 'There is no translation ['.strtoupper ($locale).'] to be deleted');
            }

        }
        return $this->redirectToRoute('bci_cms_job_index');

    }
    /**
     * @param FormSubscriptionRepository $formSubscriptionRepository
     * @param Job $job
     * @return array
     */
    private function findJobSubscriptionsBillingual(FormSubscriptionRepository $formSubscriptionRepository, Job $job)
    {
        $locales = ['fr','en'];
        $subscriptions = [];
        foreach ($locales as $locale)
        {
            //Get the subscription based on aswered values (EN/FR)
            $job->setTranslatableLocale($locale);
            $this->getEm()->refresh($job);
            $subscriptions[$locale] = $formSubscriptionRepository->createQueryBuilder('f')
                ->innerJoin('f.answers', 'fanswers')
                ->andWhere('fanswers.value = :jobTitle')
                ->andWhere('f.form = :formId')
                ->setParameter('formId', $this->careerFormID)
                ->setParameter('jobTitle', $job->getTitle())
                ->getQuery()
                ->getResult();
        }
        $subscriptions = array_merge($subscriptions['fr'],$subscriptions['en']);

        return $subscriptions;
    }

    /**
     * @param FormItem $formItem
     * @param Job $job
     * @param $locale
     */
    private function hideShowJobFromForm(FormItem $formItem, Job $job, $locale)
    {
        foreach (['fr','en'] as $availLocale)
        {
            $formItem->setTranslatableLocale($availLocale);
            $job->setTranslatableLocale($availLocale);
            $this->getEm()->refresh($formItem);
            $this->getEm()->refresh($job);
            if ($job->getTitle() != "" && $job->getSlug() != null)
            {
                $newConfig = $formItem->getConfig();
                //if JobStatus is false dont show in formItem dropdown.
                if (!$job->getStatus())
                {
                    $newConfig = str_ireplace("\r\n" . $job->getTitle(), '', $newConfig);
                    $formItem->setConfig($newConfig);
                }
                else //Check if config already contain the job, if not add it.
                {
                    if (!stristr($newConfig, $job->getTitle()))
                    {
                        $newConfig .= "\r\n" . $job->getTitle();
                        $formItem->setConfig($newConfig);
                    }
                }
                $this->save($formItem);
            }
        }

        $job->setTranslatableLocale($locale);
        $this->getEm()->refresh($job);
    }

    /**
     * @param Job $job
     * @param FormItemRepository $formItemRepository
     * @param $locale
     * Delete the job title of the TO_BE_DELETED Job from the formItems dropDown choice.
     */
    private function deleteJobFromFormItem(Job $job, FormItemRepository $formItemRepository, $locale)
    {
        $job->setTranslatableLocale($locale);
        $this->getEm()->refresh($job);

        if (empty($job->getTitle()) )
        {
            return false;
        }

        $formItem = $formItemRepository->findOneBy(['token' => $this->formItemDropDownToken]);
        $formItem->setTranslatableLocale($locale);
        $this->getEm()->refresh($formItem);

        $newConfig = $formItem->getConfig();
        $newConfig = str_ireplace("\r\n".$job->getTitle(), "", $newConfig);
        $formItem->setConfig($newConfig);
        $this->save($formItem);

        return true;
    }
    
    private function setWebsiteOwnerAddress( Job $job, $locale)
    {
        $ownwer = $this->ownerService->getOwnerLocale($locale);
        if ($ownwer)
        {
            $job->setCivicNumber($ownwer->getCivicNumber());
            $job->setStreet($ownwer->getStreet());
            $job->setCity($ownwer->getCity());
            $job->setPostalCode($ownwer->getPostalCode());
            $job->setProvince($ownwer->getProvince());
            $job->setCountry($ownwer->getCountry());

        }
        else
            {
                $this->addFlash(FlashType::TYPE_WARNING, $locale == 'fr' ? "<b>Les informations de contact de l'entreprise n'ont pas été complétées, complétées les pour activer le remplissage automatique de l'adresse.</b>" : "<b>Company contact information has not been completed, complete the information to enable auto-fill of the address.</b>" );
            }
    }

    /**
     * @param Meta $meta
     * @throws \Doctrine\ORM\ORMException
     */
    private function preDeleteMetaImage(Meta $meta)
    {
        if (strpos($meta->getProperty(), 'image') !== false)
        {
            if ($meta->getImage() ? $meta->getImage()->isToDelete() : false)
            {
                $this->getEm()->remove($meta->getImage());
                $meta->setImage(null);
                $meta->setContent("");
            }
        }
        else
            {
                $meta->setImage(null);
            }
    }

    /**
     * @param Job $job
     * @param $locale
     * @return Article
     */
    private function refreshJobAndMetasTranslation(Job $job, $locale)
    {
        $job->setTranslatableLocale($locale);
        $this->getEm()->refresh($job);
        foreach ($job->getMetas() as $meta)
        {
            $meta->setTranslatableLocale($locale);
            $this->getEm()->refresh($meta);
        }

        return $job;
    }

    /**
     * @param FormSubscription $formSubscription
     * This function set the status notification with a dateTime
     * so the view know whether or not to show notification indicator.
     * Ex: if $formSubscription->getStatus() == null -> show notification
     * else have been seen -> dont show
     */
    private function setSubscriptionNotificationOff(FormSubscription $formSubscription)
    {
        $date = new \DateTime('now');
        $formSubscription->setStatut($date->format('Y-m-d H:i:s'));
        $this->save($formSubscription);
    }
}
