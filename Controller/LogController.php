<?php

namespace Bci\CmsBundle\Controller;


use Bci\CmsBundle\Entity\Log;
use Bci\CmsBundle\Repository\LogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Config;

class LogController extends Controller
{
    /**
     * @Config\Route("/cms/logs", name="bci_cms_log_index", methods="GET")
     *
     * @Config\Template
     * @return array
     */
    public function index(LogRepository $logRepository)
    {
        $logRepository->deleteSevenDaysOlderLogs();
        return [
            'logs' => $logRepository->findAll()
        ];
    }

    /**
     * @Config\Route("/cms/logs/{id}/show", name="bci_cms_log_show", methods="GET")
     *
     * @Config\Template
     * @return array
     */
    public function show(Log $log, LogRepository $logRepository)
    {
        return [
            'log' => $log,
        ];
    }

    /**
     * @Config\Route("/cms/logs/{id}/delete", name="bci_cms_log_delete", methods="DELETE")
     */
    public function delete(Request $request, Log $log)
    {
        if ($this->isCsrfTokenValid('delete'.$log->getId(), $request->request->get('_token')))
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($log);
            $em->flush();
        }

        return $this->redirectToRoute('bci_cms_log_index');
    }

    /**
     * @Config\Route("/cms/logs/delete", name="bci_cms_logs_delete", methods="DELETE")
     */
    public function deleteAll(Request $request, LogRepository $logRepository)
    {
        if ($this->isCsrfTokenValid('delete'.'all', $request->request->get('_token')))
        {
            $em = $this->getDoctrine()->getManager();
            $logRepository->deleteAllLogs();
            $em->flush();
        }
        return $this->redirectToRoute('bci_cms_log_index');
    }
    
    
    /**
     * Takes the log and format it to an html table
     * @param $arrayLog
     * @return string
     */
    private function formatHtml($arrayLog)
    {
        $result = "<table>";
        foreach ($arrayLog AS $key => $value)
        {
            if (is_array($value))
            {
                $result .= "<tr><td style='padding-left: 40px'><div class='key'>$key :</div>" .$this->formatHtml($value) . "</td></tr>";
            }
            else
            {
                $result.= "<tr><td style='padding-left: 40px'><div class='key'>$key :</div> $value</td></tr>";
            }
        }
        return $result .= "</table>";
    }

}
