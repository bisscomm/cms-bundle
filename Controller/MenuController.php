<?php

namespace Bci\CmsBundle\Controller;


use Bci\CmsBundle\Entity\Menu;
use Bci\CmsBundle\Entity\MenuItem;
use Bci\CmsBundle\Entity\Page;
use Bci\CmsBundle\Enum\FlashType;
use Bci\CmsBundle\Form\MenuType;
use Bci\CmsBundle\Repository\MenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Config;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;

/**
 * @Config\Route("/cms/menu")
 */
class MenuController
    extends Controller {

    /**
     * @Config\Route("/", name="bci_cms_menu_index", methods="GET")
     *
     * @Config\Template
     * @return array
     */
    public function index(MenuRepository $menuRepository)
    {
        return [
            'menus' => $menuRepository->findAll()
        ];
    }

    /**
     * @Config\Route("/new", name="bci_cms_menu_new", methods="GET|POST")
     *
     * @Config\Template
     * @return array
     */
    public function new(Request $request)
    {
        $menu = new Menu();
        $form = $this->createForm(MenuType::class, $menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->rebuildMenu($menu, $request);
                $this->getDoctrine()->getManager()->persist($menu);
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash(FlashType::TYPE_SUCCESS, $menu->getTitle() . ' sauvegardé avec succès !');
                return $this->redirectToRoute('bci_cms_menu_edit', ['id' => $menu->getId()]);
            } catch (\Exception $e) {
                $this->addFlash(FlashType::TYPE_DANGER, $e->getMessage());
            }
        }
        return [
            'menu' => $menu,
            'form' => $form->createView(),
            'headingLocale' => 'fr'
        ];
    }

    /**
     * @Config\Route("/{id}/edit", name="bci_cms_menu_edit", methods="GET|POST")
     *
     * @Config\Template
     * @return array
     */
    public function edit(Request $request, Menu $menu)
    {
        $form = $this->createForm(MenuType::class, $menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                foreach ($menu->getItems() as $menuItem) {
                    $this->getDoctrine()->getManager()->remove($menuItem);
                }
                $this->rebuildMenu($menu, $request);
                $this->getDoctrine()->getManager()->persist($menu);
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash(FlashType::TYPE_SUCCESS, $menu->getTitle() . ' sauvegardé avec succès !');
            } catch (\Exception $e) {
                $this->addFlash(FlashType::TYPE_DANGER, $e->getMessage());
            } finally {
                return $this->redirectToRoute('bci_cms_menu_edit', ['id' => $menu->getId()]);
            }
        }

        return [
            'menu' => $menu,
            'form' => $form->createView(),
            'headingLocale' => 'fr'
        ];
    }

    /**
     * @Config\Route("/{id}", name="bci_cms_menu_delete", methods="DELETE")
     */
    public function delete(Request $request, Menu $menu): Response
    {
        try {
            if ($this->isCsrfTokenValid('delete' . $menu->getId(), $request->request->get('_token'))) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($menu);
                $em->flush();
                $this->addFlash(FlashType::TYPE_SUCCESS, $menu->getTitle() . ' supprimé avec succès !');
            }
        } catch (ForeignKeyConstraintViolationException $e) {
            $this->addFlash(FlashType::TYPE_DANGER, $menu->getTitle() . ' est présentement utilisé dans une page.');
            return $this->redirectToRoute('bci_cms_menu_edit', ['id' => $menu->getId()]);
        }
        return $this->redirectToRoute('bci_cms_menu_index');
    }

    private function rebuildMenu(Menu $menu, Request $request)
    {
        $order = 0;
        foreach ($menu->unserializedMenu() as $menuItem) {
            $order++;
            $this->extractMenuItem($request, $menu, $menuItem, $order);
        }
    }

    private function extractMenuItem(Request $request, $menu, $item, $position = 0, $parent = null)
    {
        $em = $this->getDoctrine()->getManager();

        $menuItem = new MenuItem();

        $menuItem
            ->setMenu($menu)
            ->setLocale('fr')
            ->setLabel(isset($item->name) ? $item->name : '')
            ->setOriginal(isset($item->original) ? $item->original : null)
            ->setParent($parent)
            ->setPosition($position);

        if ($item->id) {
            $page = $em->getRepository(Page::class)->find($item->id);
            $menuItem->setPage($page);
            $menuItem->setLabel(isset($item->name) ? $item->name : ($page->getHeadingLocale($request->getLocale())->getTitle() != null ? $page->getHeadingLocale($request->getLocale())->getTitle() : $page->getName()));
        } elseif (isset($item->url)) {
            $menuItem->setSlug($item->url);
        }

        $em->persist($menuItem);

        if (isset($item->children) && count($item->children) > 0) {
            $order = 0;
            foreach ($item->children as $child) {
                $order++;
                $this->extractMenuItem($request, $menu, $child, $order, $menuItem);
            }
        }
    }
}