<?php

namespace Bci\CmsBundle\Controller;

use Bci\CmsBundle\Entity\Owner;
use Bci\CmsBundle\Enum\FlashType;
use Bci\CmsBundle\Form\OwnerType;
use Bci\CmsBundle\Helper\ControllerTrait;
use Bci\CmsBundle\Repository\OwnerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("cms/owner")
 */
class OwnerController extends AbstractController
{
    use ControllerTrait;
    
    /**
     * @Route("/edit/{locale}",
     * name="bci_cms_owner_edit",
     * methods={"GET","POST"},
     * requirements={"locale": "en|fr"},
     * defaults={"locale"= "fr"})
     *
     * @param Request $request
     * @param Owner $owner
     * @param $locale
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     */
    public function edit(Request $request, OwnerRepository $ownerRepository, $locale): Response
    {
        $owner = $ownerRepository->findAll();
        if (empty($owner))
        {
            $owner = new Owner();
            $this->save($owner);
        }
        elseif (count($owner) > 1)
        {
            $this->addFlash(FlashType::TYPE_DANGER, "There is 2 owner's in database, only one should be present, you are now editing the first retreived. <br>  Please delete one of them before continuing. ");
            $owner = $owner[0];
        }
        else
        {
            $owner = $owner[0];
        }

        $owner->setTranslatableLocale($locale);
        $this->getEm()->refresh($owner);
        $form = $this->createForm(OwnerType::class, $owner);

        if ($this->isValid($form, $request))
        {
            $owner->setTranslatableLocale($locale);
            $this->save($owner);

            return $this->redirectToRoute('bci_cms_owner_edit',[
                'locale' => $locale
            ]);
        }

        return $this->render('@BciCms/owner/edit.html.twig', [
            'owner' => $owner,
            'locale' => $locale,
            'form' => $form->createView(),
        ]);
    }
}
