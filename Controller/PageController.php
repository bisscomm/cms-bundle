<?php

namespace Bci\CmsBundle\Controller;

use Bci\CmsBundle\Entity\Document;
use Bci\CmsBundle\Entity\Heading;
use Bci\CmsBundle\Entity\Meta;
use Bci\CmsBundle\Entity\Page;
use Bci\CmsBundle\Entity\PageTranslation;
use Bci\CmsBundle\Entity\Template;
use Bci\CmsBundle\Entity\TemplateBlock;
use Bci\CmsBundle\Enum\FlashType;
use Bci\CmsBundle\Form\PageType;
use Bci\CmsBundle\Helper\ControllerTrait;
use Bci\CmsBundle\Repository\PageRepository;
use Bci\CmsBundle\Repository\PageTranslationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Config;

class PageController
    extends Controller
{
    use ControllerTrait;
    /**
     * @Config\Route("/cms/pages", name="bci_cms_page_index", methods="GET")
     *
     * @Config\Template
     * @return array
     */
    public function index(PageRepository $pageRepository)
    {
        return [
            'pages' => $pageRepository->findAll()
        ];
    }

    /**
     * @Config\Route("/cms/page/new", name="bci_cms_page_new", methods="GET|POST")
     *
     * @Config\Template
     * @return array
     */
    public function new(Request $request)
    {
        $page = new Page();
        $form = $this->createForm(PageType::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->replaceHome($page);
                $em = $this->getDoctrine()->getManager();
                $em->persist($page);
                $em->flush();
                $this->addFlash(FlashType::TYPE_SUCCESS, $page->getName() . ' sauvegardé avec succès !');
                return $this->redirectToRoute('bci_cms_page_edit', ['id' => $page->getId()]);
            } catch (\Exception $e) {
                $this->addFlash(FlashType::TYPE_DANGER, $e->getMessage());
            }
        }

        return [
            'page' => $page,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Config\Route(
     *     "/cms/page/new/{locale}",
     *     name="bci_cms_page_new_locale",
     *     methods="GET|POST",
     *     requirements={
     *         "locale": "en|fr"
     *     }
     * )
     * @param string $locale
     *
     * @Config\Template("@BciCms/page/new.html.twig")
     * @return array
     */
    public function newTranslatablePage(Request $request, $locale)
    {
        $page = new Page();
        $heading = new Heading();
        $heading->setLocale($locale);
        $page->addHeading($heading);

        $form = $this->createForm(PageType::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->replaceHome($page);
                $em = $this->getDoctrine()->getManager();
                foreach ($page->getHeadings() as $heading) {
                    if ($heading->getLocale() == $locale) {
                        $heading->setSlug(str_replace(',', '/', $heading->getSlug()));
                        $em->persist($heading);
                        foreach ($heading->getMetas() as $meta) {
                            $this->preDeleteMetaImage($meta);
                            $meta->setHeading($heading);
                            $meta->setTranslatableLocale('fr');
                            $em->persist($meta);
                            $em->flush();
                            $meta->setTranslatableLocale('en');
                            $em->persist($meta);
                            $em->flush();
                        }
                    }
                }
                $em->persist($page);
                $em->flush();
                $this->addFlash(FlashType::TYPE_SUCCESS, $page->getName() . ' sauvegardé avec succès !');
                return $this->redirectToRoute('bci_cms_page_edit_locale', ['id' => $page->getId(), 'locale' => $locale]);
            } catch (\Exception $e) {
                $this->addFlash(FlashType::TYPE_DANGER, $e->getMessage());
            }
        }

        return [
            'page' => $page,
            'form' => $form->createView(),
            'headingLocale' => $locale
        ];
    }


    /**
     * @Config\Route(
     *     "/cms/page/{id}/edit/{locale}",
     *     name="bci_cms_page_edit_locale",
     *     methods="GET|POST",
     *     requirements={
     *         "locale": "en|fr"
     *     }
     * )
     * @param Request $request
     * @param Page $page
     * @param string $locale
     *
     * @Config\Template("@BciCms/page/edit.html.twig")
     * @return array
     */
    public function editTranslatablePage(Request $request, Page $page, $locale, UploaderHelper $uploaderHelper)
    {
        if (!$page->hasHeadingLocale($locale)) {
            $heading = new Heading();
            $heading->setLocale($locale);
            $page->addHeading($heading);
        }
        $metas = [];
        foreach ($page->getHeadings() as $heading) {
            if ($heading->getLocale() == $locale) {
                $heading->setSlug(str_replace('/', ',', $heading->getSlug()));
                foreach ($heading->getMetas() as $meta)
                    $metas[$meta->getId()] = $meta;
            }
        }
        /**
         * @var Meta $meta
         */
        foreach ($page->getHeadingLocale($locale)->getMetas() as $meta){
            $meta->getType();
            $meta->setTranslatableLocale($locale);
            $this->getEm()->refresh($meta);
        }
        $form = $this->createForm(PageType::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {

                //Added 2020-07-01;
                // Prevent Saving
                /**
                 * @var Meta $meta
                 */
                foreach ($page->getHeadingLocale($locale)->getMetas() as $meta){
                    if ($meta->getHeading() === null){
                        $meta->setHeading($page->getHeadingLocale($locale));
                    }
                }
                $this->replaceHome($page);
                $em = $this->getDoctrine()->getManager();
                foreach ($page->getHeadings() as $heading) {
                    if ($heading->getLocale() == $locale) {
                        $heading->setSlug(str_replace(',', '/', $heading->getSlug()));
                        $em->persist($heading);

                        foreach ($heading->getMetas() as $meta) {
                            unset($metas[$meta->getId()]);
                            $meta->setHeading($heading);
//                            $em->persist($meta);
                            $this->preDeleteMetaImage($meta);
//                            if ($meta->getImage() && strpos($meta->getAccessor(), 'image') !== false)
//                            {
//                                $path = $request->getSchemeAndHttpHost();
//                                $path .= $uploaderHelper->asset($meta->getImage(), 'tempFile');
//                                $meta->setContent($path);
//                            }
                            $meta->setTranslatableLocale('fr');
                            $em->persist($meta);
                            $em->flush();
                            $meta->setTranslatableLocale('en');
                            $em->persist($meta);
                            $em->flush();
                        }
                    }
                }
                $em->persist($page);
                foreach ($metas as $meta) {
                    $em->remove($meta);
                }
                $em->flush();
                $this->addFlash(FlashType::TYPE_SUCCESS, $page->getName() . ' sauvegardé avec succès !');
            } catch (\Exception $e) {
                $this->addFlash(FlashType::TYPE_DANGER, $e->getMessage());
            }

            return $this->redirectToRoute('bci_cms_page_edit_locale', ['id' => $page->getId(), 'locale' => $locale]);
        }

        return [
            'page' => $page,
            'form' => $form->createView(),
            'headingLocale' => $locale
        ];
    }

    /**
     * @param Meta $meta
     * @throws \Doctrine\ORM\ORMException
     */
    private function preDeleteMetaImage(Meta $meta)
    {
        if (strpos($meta->getProperty(), 'image') !== false)
        {
            if ($meta->getImage() ? $meta->getImage()->isToDelete() : false)
            {
                $this->getEm()->remove($meta->getImage());
                $meta->setImage(null);
                $meta->setContent("");
            }
        }
        else
        {
            $meta->setImage(null);
        }
    }

    private function finishMetaImagesDescription()
    {
        $this->getEm()->getRepository(Meta::class)->findAll();
    }


    /**
     * @Config\Route(
     *     "/cms/page/{id}/editable",
     *     name="bci_cms_page_show_edit",
     *     methods="GET"
     *  )
     *
     * @return array
     */
    public function showEdit(Page $page)
    {
        return $this->render('templates/template_' . $page->getTemplate()->getId() . '.html.twig', [
            'page' => $page,
            'title' => $page->getTitle(),
            'editable' => true
        ]);
    }

    /**
     * @Config\Route(
     *     "/cms/page/{id}/save_content",
     *     name="bci_cms_page_save_content",
     *     methods="POST"
     *  )
     *
     * @return JsonResponse
     */
    public function save(Request $request, Page $page)
    {
        $locale = $request->request->get('locale');
        $contents = $request->request->get('contents');

        $em = $this->getDoctrine()->getManager();

        foreach ($contents as $content) {
            $templateBlock = $em->getRepository(TemplateBlock::class)->find($content['tplBlockId']);

            $page->addTranslation(
                $templateBlock,
                $content['field'],
                $locale,
                $content['value']
            );
        }

        $em->persist($page);
        $em->flush();


        return new JsonResponse([
            'msg' => 'success'
        ]);
    }

    /**
     * @Config\Route(
     *     "/cms/page/{id}/upload-image",
     *     name="bci_cms_page_upload_image",
     *     methods="POST"
     *  )
     *
     * @return JsonResponse
     */
    public function uploadImage(Request $request, Page $page)
    {
        $locale = $request->request->get('locale');
        /**
         * @var $img \Symfony\Component\HttpFoundation\File\UploadedFile
         */
        $img = $request->files->get('image');
        $path = '../public/uploads/files/';

        $filename = $this->generateUniqueFileName() . '.' . $img->guessExtension();

        $document = new Document();

        $document
            ->setOriginalFilename($img->getClientOriginalName())
            ->setAssetFilename($filename)
            ->setMimeType($img->getMimeType())
            ->setPath($path)
            ->setSize($img->getSize());

        $img->move(
            $path,
            $filename
        );

        $em = $this->getDoctrine()->getManager();

        $em->persist($document);
        $em->flush();

        return new JsonResponse([
            'id' => $document->getId(),
            'url' => $this->generateUrl('bci_cms_page_display_image', [
                'id' => $document->getId(),
                'originalFilename' => $document->getOriginalFilename()
            ]),
            'size' => getimagesize('../public/uploads/files/' . $document->getAssetFilename())
        ]);
    }

    /**
     * @Config\Route(
     *     "/uploaded/images/{id}/{originalFilename}",
     *     name="bci_cms_page_display_image",
     *     methods="GET"
     *  )
     *
     * @return JsonResponse
     */
    public function displayImage(Request $request, Document $document)
    {
//        $expires = new \DateTime();
//        $expires->modify('+3600 seconds');

        $response = new BinaryFileResponse('../public/uploads/files/' . $document->getAssetFilename());

//        $response->setEtag(md5($response->getContent()));
//        $response->isNotModified($request);
//        $response->setMaxAge(3600);
//        $response->setSharedMaxAge(3600);
//        $response->setExpires($expires);
//        $response->setDate($document->getCreatedAt());
//        $response->setLastModified($document->getCreatedAt());
        $response->setPublic();

        return $response;
    }

    /**
     * @Config\Route(
     *     "/cms/image/{id}/insert-image",
     *     name="bci_cms_page_insert_image",
     *     methods="POST"
     *  )
     *
     * @return JsonResponse
     */
    public function insertImage(Request $request, Document $document)
    {
        return new JsonResponse([
            'url' => $this->generateUrl('bci_cms_page_display_image', [
                'id' => $document->getId(),
                'originalFilename' => $document->getOriginalFilename()
            ]),
            'alt' => $document->getOriginalFilename(),
            'size' => getimagesize('../public/uploads/files/' . $document->getAssetFilename())
        ]);
    }


    /**
     * @Config\Route("/cms/page/{id}/edit", name="bci_cms_page_edit", methods="GET|POST")
     *
     * @Config\Template
     * @return array
     */
    public function edit(Request $request, Page $page)
    {
        $form = $this->createForm(PageType::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash(FlashType::TYPE_SUCCESS, $page->getName() . ' sauvegardé avec succès !');
            } catch (\Exception $e) {
                $this->addFlash(FlashType::TYPE_DANGER, $e->getMessage());
            } finally {
                return $this->redirectToRoute('bci_cms_page_edit', ['id' => $page->getId()]);
            }
        }

        return [
            'page' => $page,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Config\Route("/cms/page/{id}", name="bci_cms_page_delete", methods="DELETE")
     */
    public function delete(Request $request, Page $page): Response
    {
        if ($this->isCsrfTokenValid('delete' . $page->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($page);
            $em->flush();
            $this->addFlash(FlashType::TYPE_SUCCESS, $page->getName() . ' supprimé avec succès !');
        }

        return $this->redirectToRoute('bci_cms_page_index');
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

    private function replaceHome($page)
    {
        $em = $this->getDoctrine()->getManager();
        if ($page->isHome()) {
            $homes = $em->getRepository(Page::class)->findBy(['isHome' => true]);
            foreach ($homes as $home) {
                if ($home != $page)
                {
                    $home->setIsHome(false);
                    $em->persist($home);
                }
            }
            $em->flush();
        }

    }


    /**
     * @Config\Route(
     *     "/cms/page/{locale}/{id}/{tplBlockId}/",
     *     name="bci_cms_page_history",
     *     methods="GET"
     *  )
     *
     * @return JsonResponse
     */
    public function getHistory(Request $request, $locale, $id, $tplBlockId)
    {
        $em = $this->getDoctrine()->getManager();
        $translations = $em->getRepository(PageTranslation::class)->findBy(['page' => $id, 'templateBlock' => $tplBlockId],['createdAt' => 'DESC'], 10, 0);
        $content = array();
        foreach ($translations as $translation) {
            if ($translation->getTranslation()->getLocale() == $locale) {
                $content[] = [
                    'date' => $translation->getCreatedAt()->format('Y-m-d H:i:s'),
                    'body' => $translation->getTranslation()->getValue()
                ];
            }
        }
        return new JsonResponse([
            'content' => $content
        ]);
    }

    /**
     * @return false|mixed
     * If user is logged in, check if user have preference for live_edit or not and return it
     */
    protected function isLoggedAndLiveEdit()
    {
        $liveEdit = false;
        if ( $this->isGranted('ROLE_ADMIN'))
        {
            if (isset($this->getUser()->getConfig()['prefered_view']))
            {
                $liveEdit = $this->getUser()->getConfig()['prefered_view'];
            }
        }

        return $liveEdit;
    }
}
