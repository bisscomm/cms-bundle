<?php

namespace Bci\CmsBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/cms/permission")
 */
class PermissionController
    extends Controller {

    /**
     * @Route("s", name="bci_cms_permissions")
     *
     * @Template
     * @return array
     */
    public function index()
    {
        return [
            'title' => 'CMS - Permissions',
        ];
    }

    /**
     * @Route("/", name="bci_cms_permission_show")
     *
     * @Template
     * @return array
     */
    public function show()
    {
        return [
            'title' => 'CMS - Permission',
        ];
    }

    /**
     * @Route("/new", name="bci_cms_add_permission")
     *
     * @Template
     * @return array
     */
    public function add()
    {
        return [
            'title' => 'CMS - Add Permission',
        ];
    }

    /**
     * @Route("/edit", name="bci_cms_edit_permission")
     *
     * @Template
     * @return array
     */
    public function edit()
    {
        return [
            'title' => 'CMS - Edit Permission',
        ];
    }
}