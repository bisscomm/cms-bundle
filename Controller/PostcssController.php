<?php

namespace Bci\CmsBundle\Controller;

use Bci\CmsBundle\Service\PostCssService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Config;
use Symfony\Component\HttpFoundation\Response;

class PostcssController extends BaseController
{
    private $postcss;

    public function __construct(PostCssService $postcss)
    {
        $this->postcss = $postcss;
    }

    /**
     * @Config\Route("/cms/postcss/edit", name="bci_cms_postcss_edit", methods="GET|POST")
     */
    public function edit(Request $request)
    {
        $files = $this->postcss->listFiles();
        $content = $this->postcss->getScss();

        return $this->render('@BciCms/postcss/edit.html.twig', [
            'content' => $content,
            'files' => $files
        ]);
    }

    /**
     * @Config\Route("/cms/postcss/show", name="bci_cms_postcss_show", methods="GET|POST")
     *
     * @return JsonResponse
     */
    public function showContent(Request $request)
    {
        $file = $request->request->get('filePath');
        $content = $this->postcss->getScss($file);

        return new JsonResponse($content);
    }

    /**
     * @Config\Route("/cms/postcss/save", name="bci_cms_postcss_save", methods="POST")
     *
     * @return JsonResponse
     */
    public function save(Request $request)
    {
        $twig = $this->container->get('twig');
        $scss = $request->request->get('content');
        $file = $request->request->get('fileName');

        try {
            $this->postcss->compileScss($file, $scss);
            $response = [
                'isValid' => true,
                'message' => 'Modifications sauvegardé avec succès !'
            ];

            return new JsonResponse($response);
        } catch (\Exception $e) {
            $response = [
                'isValid' => false,
                'message' => $e->getMessage()
            ];

            return new JsonResponse($response);
        }
    }
}