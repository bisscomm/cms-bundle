<?php

namespace Bci\CmsBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/cms")
 */
class RandomController
    extends Controller {

    /**
     * @Route("/", name="bci_cms_random")
     * @Route("/", name="bci_cms_index")
     *
     * @Template
     * @return array
     */
    public function index(Request $request)
    {
        return $this->redirectToRoute("bci_cms_page_index");

       /* return [
            'title' => 'CMS - Roles',
        ];*/
    }

    /**
     * @Route("/phpinfo", name="bci_cms_phpinfo")
     *
     * @Template
     * @return array
     */
    public function phpinfo(Request $request)
    {
        ob_start();
        phpinfo();
        $phpinfo = ob_get_clean();

       return [
           'title' => 'CMS - PHPINFO',
           'phpinfo' => $phpinfo
       ];
    }

    /**
     * @Route("/bobby", name="bci_cms_bobby")
     *
     * @Template
     * @return array
     */
    public function bobby()
    {
        return [
            'title' => 'CMS - Roles',
        ];
    }


}