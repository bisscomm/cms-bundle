<?php

namespace Bci\CmsBundle\Controller;


use Bci\CmsBundle\Entity\Redirection;
use Bci\CmsBundle\Enum\FlashType;
use Bci\CmsBundle\Form\RedirectionType;
use Bci\CmsBundle\Helper\ControllerTrait;
use Bci\CmsBundle\Repository\RedirectionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("cms/redirection")
 */
class RedirectionController extends AbstractController
{
    use ControllerTrait;

    /**
     * @Route("s/", name="bci_cms_redirection_index", methods={"GET"})
     */
    public function index(RedirectionRepository $redirectionRepository): Response
    {
        return $this->render('@BciCms/redirection/index.html.twig', [
            'redirections' => $redirectionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="bci_cms_redirection_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $redirection = new Redirection();
        $form = $this->createForm(RedirectionType::class, $redirection);

        if ($this->isValid($form, $request))
        {
            if ($dulicated = $this->isDuplicate($redirection))
            {
                $this->addFlash(FlashType::TYPE_DANGER, $request->getLocale() == 'fr' ? 'Url déja redirigé. Voir la redirection ID: '.$dulicated->getId() : 'Url already redirected. See redirection ID: '.$dulicated->getId());
                return $this->render('@BciCms/redirection/new.html.twig', [
                    'redirection' => $redirection,
                    'form' => $form->createView(),
                ]);
            }
            $this->save($redirection);
            return $this->redirectToRoute('bci_cms_redirection_index');
        }

        return $this->render('@BciCms/redirection/new.html.twig', [
            'redirection' => $redirection,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="bci_cms_redirection_show", methods={"GET"})
     */
    public function show(Redirection $redirection): Response
    {
        return $this->render('@BciCms/redirection/show.html.twig', [
            'redirection' => $redirection,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="bci_cms_redirection_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Redirection $redirection): Response
    {
        $form = $this->createForm(RedirectionType::class, $redirection);

        if ($this->isValid($form, $request))
        {
            if ($dulicated = $this->isDuplicate($redirection))
            {
                $this->addFlash(FlashType::TYPE_DANGER, $request->getLocale() == 'fr' ? 'Url déja redirigé. Voir la redirection ID: '.$dulicated->getId() : 'Url already redirected. See redirection ID: '.$dulicated->getId());
                return $this->render('@BciCms/redirection/edit.html.twig', [
                    'redirection' => $redirection,
                    'form' => $form->createView(),
                ]);
            }
            $this->save($redirection);
            return $this->redirectToRoute('bci_cms_redirection_index');
        }

        return $this->render('@BciCms/redirection/edit.html.twig', [
            'redirection' => $redirection,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="bci_cms_redirection_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Redirection $redirection): Response
    {
        if ($this->isCsrfTokenValid('delete' . $redirection->getId(), $request->request->get('_token')))
        {
            $this->getEm()->remove($redirection);
            $this->getEm()->flush();
        }
        return $this->redirectToRoute('bci_cms_redirection_index');
    }

    /**
     * @param Redirection $redirection
     * @return Redirection|false
     */
    private function isDuplicate(Redirection $redirection)
    {
        $inDatabase = $this->getEm()->getRepository(Redirection::class)->findBy(['fromUrl'=>$redirection->getFromUrl()]);

        if ($inDatabase && $inDatabase[0] == $redirection)
        {
            return false;
        }
        elseif ($inDatabase)
        {
            return $inDatabase[0];
        }

        return false;
    }
}
