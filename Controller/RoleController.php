<?php

namespace Bci\CmsBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/cms/role")
 */
class RoleController
    extends Controller {

    /**
     * @Route("s", name="bci_cms_roles")
     *
     * @Template
     * @return array
     */
    public function index()
    {
        return [
            'title' => 'CMS - Roles',
        ];
    }

    /**
     * @Route("/", name="bci_cms_role_show")
     *
     * @Template
     * @return array
     */
    public function show()
    {
        return [
            'title' => 'CMS - Role',
        ];
    }

    /**
     * @Route("/new", name="bci_cms_add_role")
     *
     * @Template
     * @return array
     */
    public function add()
    {
        return [
            'title' => 'CMS - Add Role',
        ];
    }

    /**
     * @Route("/edit", name="bci_cms_edit_role")
     *
     * @Template
     * @return array
     */
    public function edit()
    {
        return [
            'title' => 'CMS - Edit Role',
        ];
    }
}