<?php

namespace Bci\CmsBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class SecurityController extends AbstractController {

    /**
     * @Route("/cms/login", name="security_login")
     *
     * @Template
     * @return array
     */
    public function login(AuthenticationUtils $helper)
    {
        return [
            'last_username' => $helper->getLastUsername(),
            // La derniere erreur de connexion (si il y en a une)
            'error' => $helper->getLastAuthenticationError(),
        ];
    }

    /**
     * @Route("/cms/logout", name="security_logout")
     *
     * @Template
     * @return null
     */
    public function logout()
    {
        throw new \Exception('This should never be reached!');
    }

}