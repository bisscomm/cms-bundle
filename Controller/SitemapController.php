<?php

namespace Bci\CmsBundle\Controller;

use Bci\CmsBundle\Entity\Heading;
use Bci\CmsBundle\Entity\SiteMap;
use Bci\CmsBundle\Helper\SiteMapableTrait;
use Bci\CmsBundle\Repository\SiteMapRepository;
use Bci\CmsBundle\Service\SitemapService;
use Bci\CmsBundle\Service\TranslationService;
use Doctrine\DBAL\DBALException;
use Gedmo\Translatable\Entity\Translation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Router;
use ReflectionClass;

class SitemapController extends AbstractController
{
    /**
     * @Route("/sitemap.xml", name="sitemap", defaults={"_format"="xml"})
     */
    public function index(Request $request, SiteMapRepository $siteMapRepository)
    {
        $hostname = $request->getSchemeAndHttpHost();

        /*======================= ROUTE STATIC CONNUE =======================*/
//        $urls[] = ['loc' => $this->generateUrl('accueil')];
//        $urls[] = ['loc' => $this->generateUrl('app_register')];

        $urls = [];
        $links = $siteMapRepository->findAllByLocale('fr');
        if (count($links) > 0) {
            $urls[] = $this->generateUrlHttpOrHttps('sitemap_fr',[]);
        }

        $links = $siteMapRepository->findAllByLocale('en');
        if (count($links) > 0) {
            $urls[] = $this->generateUrlHttpOrHttps('sitemap_en',[]);
        }

        $response = new Response(
            $this->renderView('@BciCms/sitemap/index.xml.twig', [
                'siteMaps' => $urls,
                'hostname' => $hostname])
        );

        $response->headers->set('Content-Type', 'application/xml');
        $response->setPublic();
        $response->setMaxAge(86400);
        $response->setExpires(new \DateTime('now +1day'));
        $response->setLastModified(new \DateTime("now"));

        return $response;
    }

    /**
     * @Route("/sitemap_fr.xml", name="sitemap_fr", defaults={"_format"="xml"})
     */
    public function siteMapFr(Request $request, SiteMapRepository $siteMapRepository)
    {
        $urls = $siteMapRepository->findAllByLocale('fr');
        $response = new Response(
            $this->renderView('@BciCms/sitemap/_partial/sitemap.xml.twig', [
                'urls' => $urls,
            ])
        );

        $response->headers->set('Content-Type', 'application/xml');
        $response->setPublic();
        $response->setMaxAge(86400);
        $response->setExpires(new \DateTime('now +1day'));
        $response->setLastModified(new \DateTime("now"));

        return $response;
    }

    /**
     * @Route("/sitemap_en.xml", name="sitemap_en", defaults={"_format"="xml"})
     */
    public function siteMapEn(Request $request, SiteMapRepository $siteMapRepository)
    {
        $urls = $siteMapRepository->findAllByLocale('en');

        $response = new Response(
            $this->renderView('@BciCms/sitemap/_partial/sitemap.xml.twig', [
                'urls' => $urls,
            ])
        );

        $response->headers->set('Content-Type', 'application/xml');
        $response->setPublic();
        $response->setMaxAge(86400);
        $response->setExpires(new \DateTime('now +1day'));
        $response->setLastModified(new \DateTime("now"));

        return $response;
    }

    /**
     * @Route("cms/sitemap/48939040A431F2403DE3CCB1EE521247", name="regenerate_sitemap")
     */
    public function regenerateSiteMap(Request $request, SitemapService $sitemapService)
    {
        $this->deleteSitemapEntity();
        $this->generateEntitySiteMapable();
        $this->generateHeadingTypeSitemapable();
        $sitemapService->sendSitemapToGoogle();

        return new JsonResponse([
            'sitemapUrl' => [
                'index' => $this->generateUrl('sitemap'),
                'fr' => $this->generateUrl('sitemap_fr'),
                'en' => $this->generateUrl('sitemap_en')
            ]
        ], Response::HTTP_OK, [], false);
    }
    /**
     * @Route("cms/sitemap/basedomain", name="bci_cms_get_base_domain_url")
     */
    public function getBaseDomainUrl(Request $request)
    {
        
        return new JsonResponse([
            'baseUrl' => $_SERVER['HTTP_ORIGIN']
        ], Response::HTTP_OK, [], false);
    }

    /**
     *  Generate sitemap link for ALL entities that use SiteMapableTrait and Use Gedmo translations
     */
    private function generateEntitySiteMapable()
    {
        $siteMapConfig = $this->getParameter('bci_cms')['sitemap'];

        /* @var $entityManager EntityManager */
        $entityManager = $this->getDoctrine()->getManager();
        $entitiesClassNames = $entityManager->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();
        $classesToRegenerate=[];
        foreach ($entitiesClassNames as $entityClassName)
        {
            //Heading::Class is handle separatly because it's not gedmo 'translatable'
            if ( in_array(SiteMapableTrait::class, class_uses($entityClassName)) && $entityClassName !== Heading::class )
            {
                $classesToRegenerate[] = $entityClassName;
            }
        }
        $siteMapableEntities=null;
        foreach ($classesToRegenerate as $classToRegenerate )
        {
            $classToRegenerateRepo = $this->getDoctrine()->getManager()->getRepository($classToRegenerate);
            $siteMapableEntities[(new \ReflectionClass($classToRegenerate))->getShortName()] = array_values($classToRegenerateRepo->findAll());
        }
        foreach ($siteMapableEntities as $entityClass => $siteMapableEntitiesArray )
        {
            foreach ($siteMapableEntitiesArray as $siteMapableEntity)
            {
                if ($siteMapableEntity->isSiteMapable())
                {
                    $translationsLocale = array_keys($this->getDoctrine()->getRepository(Translation::class)->findTranslations($siteMapableEntity));

                    foreach ($translationsLocale as $locale)
                    {
                        $siteMapableEntity->setTranslatableLocale($locale);
                        $this->getDoctrine()->getManager()->refresh($siteMapableEntity);

                        $entityAsArray = $siteMapableEntity->toArray();
                        if (in_array($entityClass, array_keys($siteMapConfig)))
                        {
                            $routes = $siteMapConfig[$entityClass]['routes'][$entityAsArray['locale']];
                        }

                        if (!empty($routes))
                        {
                            foreach ($routes as $routeName => $fields)
                            {
                                $routeParams = [];
                                foreach ($fields as $field)
                                {
                                    $routeParams[$field] = $entityAsArray[$field];
                                }
                                $url = $this->generateUrlHttpOrHttps($routeName, $routeParams);
                            }

                            $siteMapField = new SiteMap();
                            $siteMapField
                                ->setUrl($url)
                                ->setLocale($entityAsArray['locale'])
                                ->setSiteMappedEntityId($siteMapableEntity->getId())
                                ->setSiteMappedEntityClass($entityClass)
                                ->setCreatedAt(new \DateTime('now'))
                                ->setUpdatedAt(new \DateTime('now'));

                            $this->getDoctrine()->getManager()->persist($siteMapField);
                        }
                    }
                }
            }
        }
        $this->getDoctrine()->getManager()->flush();
    }

    /**
     *  Generate sitemap link for PageType(Heading) entities
     */
    private function generateHeadingTypeSitemapable()
    {
        foreach ($this->getDoctrine()->getRepository(Heading::class)->findAll() as $entity)
        {
            if ($entity->isSiteMapable() && $entity->canDisplay())
            {
                $reflect = new ReflectionClass($entity);
                $entityClass = $reflect->getShortName();
                $url =
                    $this->generateUrlHttpOrHttps('bci_cms_page_show', [
                        'slug' => $entity->getSlug(),
                    ]);

                $siteMapField = new SiteMap();
                $siteMapField
                    ->setUrl($url)
                    ->setSiteMappedEntityId($entity->getId())
                    ->setSiteMappedEntityClass($entityClass)
                    ->setLocale($entity->getLocale())
                    ->setCreatedAt(new \DateTime('now'))
                    ->setUpdatedAt(new \DateTime('now'));

                $this->getDoctrine()->getManager()->persist($siteMapField);
            }
        }
        $this->getDoctrine()->getManager()->flush();
    }

    /**
     * Delete * from Sitemap in Database
     */
    private function deleteSitemapEntity()
    {
        $qb = $this->getDoctrine()->getRepository(SiteMap::class)->createQueryBuilder('s');
        $query = $qb->getEntityManager()->createQuery('delete from '.SiteMap::class);
        $query->execute();
    }

    /**
     * @param $routeName
     * @param array $routeParams
     * @return string
     */
    private function generateUrlHttpOrHttps($routeName, array $routeParams)
    {
//        $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https:' : 'http:';
        $protocol = 'https:';
        return $protocol.$this->generateUrl($routeName, $routeParams, Router::NETWORK_PATH);
    }
}
