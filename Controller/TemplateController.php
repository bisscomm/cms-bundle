<?php

namespace Bci\CmsBundle\Controller;

use Bci\CmsBundle\Entity\Block;
use Bci\CmsBundle\Entity\PageTranslation;
use Bci\CmsBundle\Entity\Template;
use Bci\CmsBundle\Entity\TemplateBlock;
use Bci\CmsBundle\Enum\FlashType;
use Bci\CmsBundle\Form\TemplateType;
use Bci\CmsBundle\Repository\TemplateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Config;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;

/**
 * @Config\Route("/cms/template")
 */
class TemplateController extends Controller
{
    /**
     * @Config\Route("s", name="bci_cms_template_index", methods="GET")
     *
     * @Config\Template
     * @return array
     */
    public function index(TemplateRepository $templateRepository)
    {
        return [
            'templates' => $templateRepository->findAll()
        ];
    }

    /**
     * @Config\Route("/new", name="bci_cms_template_new", methods="GET|POST")
     *
     * @Config\Template
     * @return array
     */
    public function new(Request $request)
    {
        $template = new Template();
        $template->setStructure(
            '{"sections":[{"params":{"name":"header"},"children":[]},{"params":{"name":"main"},"children":[]},{"params":{"name":"footer"},"children":[]}]}'
        );
        $form = $this->createForm(TemplateType::class, $template);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $structure = $template->explodeStructure();
                foreach ($structure['sections'] as $key => $section) {
                    $structure['sections'][$key] = $this->buildStructure($section, $template);
                }
                $template->setStructure(json_encode($structure));
                $this->getDoctrine()->getManager()->persist($template);
                $this->getDoctrine()->getManager()->flush();
                $this->get('bci_cms.template_manager')->writeTemplate($template);
                $this->addFlash(FlashType::TYPE_SUCCESS, $template->getName() . ' sauvegardé avec succès !');
                return $this->redirectToRoute('bci_cms_template_edit', ['id' => $template->getId()]);
            } catch (\Exception $e) {
                $this->addFlash(FlashType::TYPE_DANGER, $e->getMessage());
            }
        }

        return [
            'template' => $template,
            'form' => $form->createView(),
            'availBlocks' => $this->getAvailBlocks()
        ];
    }

    /**
     * @Config\Route("/{id}", name="bci_cms_template_show", methods="GET")
     *
     * @Config\Template
     * @return array
     */
    public function show(Template $template)
    {
        return ['template' => $template];
    }

    /**
     * @Config\Route("/{id}/edit", name="bci_cms_template_edit", methods="GET|POST")
     *
     * @Config\Template
     * @return array
     */
    public function edit(Request $request, Template $template)
    {
        $form = $this->createForm(TemplateType::class, $template);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $structure = $template->explodeStructure();
                foreach ($structure['sections'] as $key => $section) {
                    $structure['sections'][$key] = $this->buildStructure($section, $template);
                }
                $template->setStructure(json_encode($structure));
                $this->deleteTemplateBlockRelation($template);
                $this->getDoctrine()->getManager()->persist($template);
                $this->getDoctrine()->getManager()->flush();
                $this->get('bci_cms.template_manager')->writeTemplate($template);
                $this->addFlash(FlashType::TYPE_SUCCESS, $template->getName() . ' sauvegardé avec succès !');
            } catch (\Exception $e) {
                $this->addFlash(FlashType::TYPE_DANGER, $e->getMessage());
            } finally {
                return $this->redirectToRoute('bci_cms_template_edit', ['id' => $template->getId()]);
            }
        }

        return [
            'template' => $template,
            'form' => $form->createView(),
            'availBlocks' => $this->getAvailBlocks()
        ];
    }

    /**
     * @Config\Route("/{id}", name="bci_cms_template_delete", methods="DELETE")
     */
    public function delete(Request $request, Template $template): Response
    {
        try {
            if ($this->isCsrfTokenValid('delete' . $template->getId(), $request->request->get('_token'))) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($template);
                $em->flush();
                $this->addFlash(FlashType::TYPE_SUCCESS, $template->getName() . ' supprimé avec succès !');
            }
        } catch (ForeignKeyConstraintViolationException $e) {
            $this->addFlash(FlashType::TYPE_DANGER, $template->getName() . ' est présentement utilisé dans une page.');
            return $this->redirectToRoute('bci_cms_template_edit', ['id' => $template->getId()]);
        }


        return $this->redirectToRoute('bci_cms_template_index');
    }

    private function getAvailBlocks()
    {
        return $this->getDoctrine()->getRepository(Block::class)->findAll();
    }

    /**
     * @Config\Route("/{id}/fields", name="bci_cms_template_fields", methods="GET|POST")
     * @param Template $template
     *
     * @return JsonResponse
     */
    public function getTemplateFields(Request $request, Template $template)
    {
        $blocks = ['blocks' => []];
        foreach ($template->getBlocks() as $templateBlock) {
            $fields = [];
            foreach ((array) $templateBlock->getBlock()->getFields() as $field) {
                $fields[] = [
                    'name' => $field->getName(),
                    'type' => $field->getType(),
                    'descriptition' => $field->getDescription(),
                    'identifier' => $field->getIdentifier()
                ];
            }

            $blocks['blocks'][] = [
                'tb_id' => $templateBlock->getId(),
                'fields' => $fields
            ];
        }

        return new JsonResponse($blocks);
    }


    public function buildStructure($section, $template)
    {
        foreach ($section['children'] as $key => $child) {
            if ($child['params']['type'] == 'block') {
                $block = $this->getDoctrine()->getRepository('BciCmsBundle:Block')->find($child['params']['id']);
                if (!isset($section['children'][$key]['params']['link'])
                    || $section['children'][$key]['params']['link'] == ''
                    || $section['children'][$key]['params']['link'] == null
                ) {
                    $link = new TemplateBlock();
                    $link->setBlock($block);
                    $link->setTemplate($template);
                    $this->getDoctrine()->getManager()->persist($link);
                    $section['children'][$key]['params']['link'] = $link->getId();
                }
            }

            if (isset($child['children'])) {
                $section['children'][$key] = $this->buildStructure($child, $template);
            }
        }

        return $section;
    }

    /**
     * @param Template $template
     * This function is necessary to retrive and remove every linkage from the object TemplateBlock.
     * TemplateBlock obj is the link between a Template and a Block.
     * TemplateBlock is created during the BuildStructure process, sice we creating this object "manually"
     * we also need to remove him manually.
     *
     * This function explode the newly generated Template structure and loop to find all childs of type block
     * and get their associated TemplateBlock ID.
     * Then we compare them to the database TemplateBlock and delete every TemplateBlock no longer present in the new structure.
     * DELETION PROCESS:
     * 1. Remove the TemplateBlock from the current template.
     * 2. Delete the TemplateBlock from the database (preFlush)
     * 3. Retrieve every PageTranslations associated with the TemplateBlock that we just delete.
     * 4. Delete the every PageTranslations associated with the TemplateBlock from the database (preFlush)
     *
     * The flush is made in the edit function of this controller
     */
    private function deleteTemplateBlockRelation(Template $template)
    {
        $templateBlocks = $this->getDoctrine()->getRepository(TemplateBlock::class)->findBy(['template' => $template]);
        $presentTemplateBlockIdToKeep = [];
        $structure = $template->explodeStructure();
        foreach ($structure['sections'] as $key => $section)
        {
            if (count($section['children']) > 0)
            {
                foreach ($section['children'] as $child)
                {
                    if ($child['params']['type'] == 'block')
                    {
                        array_push($presentTemplateBlockIdToKeep, $child['params']['link']);
                    }
                }
            }
        }
        foreach ($templateBlocks as $templateBlock)
        {
            if (!in_array($templateBlock->getId(),$presentTemplateBlockIdToKeep))
            {
                $template->removeBlock($templateBlock);
                $this->getDoctrine()->getManager()->remove($templateBlock);
                $relatedPageTranslations = $this->getDoctrine()->getRepository(PageTranslation::class)->findBy(['templateBlock'=>$templateBlock]);
                foreach ($relatedPageTranslations as $relatedPageTranslation)
                {
                    $this->getDoctrine()->getManager()->remove($relatedPageTranslation);
                }
            }
        }
    }
}
