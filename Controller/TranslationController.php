<?php

namespace Bci\CmsBundle\Controller;

use Bci\CmsBundle\Entity\Translation;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class TranslationController
    extends Controller
{
     /**
     * @Route("/cms/translation", name="bci_cms_translation")
     *
     * @Template("@BciCms/translation/index.html.twig")
     * @return array
     */
     public function index(){
     	return [];
     }
}