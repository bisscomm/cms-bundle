<?php

namespace  Bci\CmsBundle\Controller;


use Bci\CmsBundle\Entity\Website;
use Bci\CmsBundle\Form\WebsiteType;
use Bci\CmsBundle\Repository\WebsiteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("cms/website")
 */
class WebsiteController extends AbstractController
{
    /**
     * @Route("s/", name="bci_cms_website_index", methods={"GET"})
     */
    public function index(WebsiteRepository $websiteRepository): Response
    {
        return $this->render('@BciCms/website/index.html.twig', [
            'websites' => $websiteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="bci_cms_website_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $website = new Website();
        $form = $this->createForm(WebsiteType::class, $website);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($website);
            $entityManager->flush();

            return $this->redirectToRoute('bci_cms_website_index');
        }

        return $this->render('@BciCms/website/new.html.twig', [
            'website' => $website,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="bci_cms_website_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Website $website): Response
    {
        $form = $this->createForm(WebsiteType::class, $website);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('bci_cms_website_index');
        }

        return $this->render('@BciCms/website/edit.html.twig', [
            'website' => $website,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="bci_cms_website_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Website $website): Response
    {
        if ($this->isCsrfTokenValid('delete'.$website->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($website);
            $entityManager->flush();
        }

        return $this->redirectToRoute('bci_cms_website_index');
    }
}
