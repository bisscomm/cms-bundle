<?php

namespace Bci\CmsBundle\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

class ExtractDate extends FunctionNode
{
    public $extractTypeExpression = null;
    public $fieldExpression = null;
    public $unit = null;

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $parser->match(Lexer::T_IDENTIFIER);

        $this->extractTypeExpression = $parser->getLexer()->token['value'];

        $parser->match(Lexer::T_FROM);

        $this->fieldExpression = $parser->ArithmeticPrimary();

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return 'EXTRACT(' .
            $this->extractTypeExpression . ' FROM ' .
            $this->fieldExpression->dispatch($sqlWalker) .
            ')';
    }
}