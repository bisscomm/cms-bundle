<?php
namespace Bci\CmsBundle\DataFixtures;

use Bci\CmsBundle\Entity\Block;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class BlockFixtures extends Fixture
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        foreach ($this->getBlockData() as [$name, $description, $definition, $content, $ref]) {
            $block = new Block();
            
            $block
                ->setName($name)
                ->setDescription($description)
                ->setDefinition($definition)
                ->setContent($content)
                ->setCreatedAt(new \DateTime())
                ->setUpdatedAt(new \DateTime());

            $manager->persist($block);

            $this->container->get('bci_cms.block_manager')->writeBlock($block);

            $this->addReference($ref, $block);
        }

        $manager->flush();
    }

    private function getBlockData(): array
    {
        return [
            [
                'Header',
                'Entête',
                null,
                '<header></header>',
                'header'
            ],
            [
                'Footer',
                'Pied de page',
                null,
                '<footer></footer>',
                'footer'
            ],
            [
                'Block Section',
                'Section',
                null,
                '<section></section>',
                'section'
            ],
        ];
    }
}
