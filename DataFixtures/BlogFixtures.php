<?php
namespace Bci\CmsBundle\DataFixtures;


use Bci\CmsBundle\Entity\Blog;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Console\Output\ConsoleOutput;
class BlogFixtures extends Fixture
{
    private  $BCI_CMS_YAML_FILE_PATH;


    public function __construct(ContainerInterface $container)
    {
        $this->BCI_CMS_YAML_FILE_PATH = $container->getParameter('kernel.project_dir').'/config/packages/bci_cms.yaml';
    }

    public function load(ObjectManager $manager)
    {
        $locale = 'fr';
        $blog = new Blog();
        $blog
            ->setTitle('Blogue')
            ->setTranslatableLocale($locale);
        $manager->persist($blog);
        $manager->flush();

        $this->writeIdToYaml($blog->getId());

    }

    private function writeIdToYaml($blogID)
    {

        // Parse YAML to array
        $array_bci_cms = Yaml::parseFile($this->BCI_CMS_YAML_FILE_PATH);

        try
        {
            $ok_present = $array_bci_cms['bci_cms']['navigation']['sidebar']['module']['children']['blog'];

            $array_bci_cms['bci_cms']['navigation']['sidebar']['module']['children']['blog']['routeParameters']['id'] = $blogID;

            // Parse array to YAML
            $yaml_bci_cms = Yaml::dump($array_bci_cms, 100, 2);

            file_put_contents($this->BCI_CMS_YAML_FILE_PATH, $yaml_bci_cms);
        }catch (\Exception $exception)
        {
            $output = new ConsoleOutput();
            $output->writeln("<error> Could not found ['bci_cms']['navigation']['sidebar']['blog']['routeParameters']['id'] in : " . $this->BCI_CMS_YAML_FILE_PATH."</error>");
            $output->writeln("<error> Make sure it's present and re-run doctrine:fixtures:load</error>");
        }



    }


}
