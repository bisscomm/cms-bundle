<?php
namespace Bci\CmsBundle\DataFixtures;


use Bci\CmsBundle\Entity\Configuration;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Console\Output\ConsoleOutput;
class ConfigurationFixtures extends Fixture
{
    
    public function load(ObjectManager $manager)
    {
        $configuration = new Configuration();
        $manager->persist($configuration);
        $manager->flush();
        
    }

   

}
