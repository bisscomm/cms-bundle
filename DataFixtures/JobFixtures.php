<?php
namespace Bci\CmsBundle\DataFixtures;

use Bci\CmsBundle\Entity\Form;
use Bci\CmsBundle\Entity\FormItem;
use Bci\CmsBundle\Entity\FormTranslations;
use Bci\CmsBundle\Entity\Job;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Console\Output\ConsoleOutput;
class JobFixtures extends Fixture
{
    private  $BCI_CMS_YAML_FILE_PATH;


    public function __construct(ContainerInterface $container)
    {
        $this->BCI_CMS_YAML_FILE_PATH = $container->getParameter('kernel.project_dir').'/config/packages/bci_cms.yaml';
    }

    public function load(ObjectManager $manager)
    {
        $locale = 'fr';
        $form = new Form();
        $form
            ->setTitle('Carrière')
            ->setCreatedAt(new \DateTime())
            ->setUpdatedAt(new \DateTime());
        foreach (['fr' , 'en'] as $locale)
        {
            $formTranslations = new FormTranslations();
            $formTranslations->setLocale($locale);
            $formTranslations->setTitle($this->getCareerFormTitle()[$locale]);
            $form->addFormTranslations($formTranslations);
            $manager->persist($form);
            $manager->flush();
        }

        $formID = $form->getId();
        $formItemToken = "";
        $cmpt=0;
        foreach ($this->getCareerFormItemsData() as [$label, $help, $type, $config, $require])
        {
            $locale = 'fr';
            $formItem = new FormItem();
            $formItem->setTranslatableLocale($locale);
            $formItem
                ->setLabel($label[$locale])
                ->setHelp($help)
                ->setType($type)
                ->setConfig($type == 'select' ? $this->getSpontaneousApplicationJobData()[$locale] : null)
                ->setForm($form)
                ->setLocale($locale)
                ->setCreatedAt(new \DateTime())
                ->setUpdatedAt(new \DateTime())
                ->setRequired($require);

            $manager->persist($formItem);
            $formItem->setToken($formItem->slugify($formItem->getLabel()) . '_' . $formItem->getId());
            $manager->persist($formItem);
            $manager->flush();

            if ($formItem->getLabel() == $this->getCareerFormItemsData()[5][0]['fr'])
            {
                $formItemToken = $formItem->getToken();
            }

            //            EN
            $locale = 'en';
            $formItem->setTranslatableLocale($locale);
            $manager->refresh($formItem);
            $formItem
                ->setLabel($label[$locale])
                ->setConfig($type == 'select' ? $this->getSpontaneousApplicationJobData()[$locale] : null);

            $manager->persist($formItem);
            $manager->flush();

        }

        //  Job Spontaneous Application
        $locale = 'fr';
        $job = new Job();
        $job->setTranslatableLocale($locale);
        $job
            ->setTitle($this->getSpontaneousApplicationJobData()[$locale])
            ->setOnlyShowInForm(true)
            ->setStatus(true)
            ->setCreatedAt(new \DateTime())
            ->setUpdatedAt(new \DateTime());
        $manager->persist($job);
        $manager->flush();

        $locale = 'en';
        $job->setTranslatableLocale($locale);
        $job->setTitle($this->getSpontaneousApplicationJobData()[$locale]);
        $manager->persist($job);
        $manager->flush();

        $manager->persist($form);
        $manager->flush();

        //Write career_form_id & career_form_item_select_token in bci_cms.yaml
        $this->writeIdToYaml($formID, $formItemToken);


    }

    private function writeIdToYaml($formId, $formItemToken )
    {
        // Parse YAML to array
        $array_bci_cms = Yaml::parseFile($this->BCI_CMS_YAML_FILE_PATH);

        $array_bci_cms['bci_cms']['default_job']['career_form_item_select_token'] = $formItemToken;
        $array_bci_cms['bci_cms']['default_job']['career_form_id'] = $formId;

        // Parse array to YAML
        $yaml_bci_cms = Yaml::dump($array_bci_cms, 100,2);

        file_put_contents($this->BCI_CMS_YAML_FILE_PATH, $yaml_bci_cms);

    }

    /**
     * @return string[]
     */
    private function getCareerFormTitle()
    {
        return [
            'fr' => 'Formulaire de carrière',
            'en' => 'Career form'
        ];
    }
    private function getCareerFormItemsData(): array
    {
        return [
// $formItem = [$label, $help, $type, $config, $require];
            [
                [
                    'fr' => 'Prénom',
                    'en' => 'First name'
                ], null, 'text', null, true],
            [
                [
                    'fr' => 'Nom',
                    'en' => 'Last name'
                ], null, 'text', null, true],
            [
                [
                    'fr' => 'Adresse',
                    'en' => 'Address'
                ], null, 'text', null, true],
            [
                [
                    'fr' => 'Téléphone',
                    'en' => 'Phone number'
                ], null, 'text', null, true],
            [
                [
                    'fr' => 'Courriel',
                    'en' => 'Email'
                ], null, 'email', null, true],
            [
                [
                    'fr' => 'Poste convoité',
                    'en' => 'Desired position'
                ], null, 'select', "", true],
            [
                [
                    'fr' => 'Joindre votre C.V',
                    'en' => 'Attach your C.V'
                ], null, 'file', null, true],
        ];
    }

    private function getSpontaneousApplicationJobData(): array
    {
        return [
            // $job = [$title];
            'fr' => 'Candidature spontanée',
            'en' => 'Spontaneous application'

        ];
    }


}
