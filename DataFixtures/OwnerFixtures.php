<?php
namespace Bci\CmsBundle\DataFixtures;

use Bci\CmsBundle\Entity\Owner;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Console\Output\ConsoleOutput;

class OwnerFixtures extends Fixture
{
    
    public function load(ObjectManager $manager)
    {
        $locale = 'fr';
        $owner = new Owner();
        $owner->setTranslatableLocale($locale);
        $owner->setCountry('Canada');
        $owner->setCreatedAt(new \DateTime('now'));
        $owner->setUpdatedAt(new \DateTime('now'));
        $manager->persist($owner);
        $manager->flush();
    }

}
