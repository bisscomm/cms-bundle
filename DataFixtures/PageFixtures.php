<?php
namespace Bci\CmsBundle\DataFixtures;

use Bci\CmsBundle\Entity\Heading;
use Bci\CmsBundle\Entity\Meta;
use Bci\CmsBundle\Entity\Page;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PageFixtures
    extends Fixture
    implements DependentFixtureInterface
{

    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        foreach ($this->getPageData() as [$name, $template, $isHome, $headings]) {
            $page = new Page();

            $page
                ->setName($name)
                ->setTemplate($template)
                ->setIsHome($isHome);
            foreach ($headings as [$title, $slug, $metas, $locale]) {
                $heading = new Heading();
                $heading
                    ->setTitle($title)
                    ->setSlug($slug)
                    ->setLocale($locale)
                    ->setDraft(false)
                    ->setPublishedAt(new \DateTime());
                foreach ($metas as [$name, $property, $description]) {
                    $meta = new Meta();
                    $meta
                        ->setName($name)
                        ->setProperty($property)
                        ->setDescription($description);
                    $heading->addMeta($meta);

                    $manager->persist($meta);
                }
                $page->addHeading($heading);

                $manager->persist($heading);
            }

            $manager->persist($page);
        }

        $manager->flush();
    }

    private function getPageData(): array
    {
        return [
            [
                'Accueil',
                $this->getReference('accueil'),
                true,
                [
                    [
                        'header',
                        'accueil',
                        [],
                        'fr'
                    ],
                    [
                        'header',
                        'home',
                        [],
                        'en'
                    ],

                ],
            ]
        ];
    }

    public function getDependencies()
    {
        return [
            TemplateFixtures::class,
        ];
    }
}
