<?php
namespace Bci\CmsBundle\DataFixtures;

use Bci\CmsBundle\Entity\Template;
use Bci\CmsBundle\Entity\TemplateBlock;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TemplateFixtures
    extends Fixture
    implements DependentFixtureInterface
{
    private $container;
    private $manager;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        foreach ($this->getTemplateData() as [$name, $structure, $ref]) {
            $template = new Template();

            $template
                ->setName($name)
                ->setStructure(json_encode($structure));

            $manager->persist($template);

          //  var_dump($template->explodeStructure());

            $structure = $template->explodeStructure();

            foreach($structure['sections'] as $key => $section) {
                $structure['sections'][$key] = $this->buildStructure($section, $template);
            }

            $this->container->get('bci_cms.template_manager')->writeTemplate($template);

            $this->addReference($ref, $template);
        }

        $manager->flush();
    }

    public function buildStructure($section, $template)
    {
        foreach($section['children'] as $key => $child) {
            if ($child['params']['type'] == 'block') {
                $block = $this->manager->getRepository('BciCmsBundle:Block')->find($child['params']['id']);
               /* if (!isset($section['children'][$key]['params']['link'])
                    || $section['children'][$key]['params']['link'] == ''
                    || $section['children'][$key]['params']['link'] == null
                ) { */
                    $link = new TemplateBlock();
                    $link->setBlock($block);
                    $link->setTemplate($template);
                    $this->manager->persist($link);
                    $section['children'][$key]['params']['link'] = $link->getId();
               // }
            }

            if (isset($child['children'])) {
                $section['children'][$key] = $this->buildStructure($child, $template);
            }
        }

        return $section;
    }

    private function getTemplateData(): array
    {
        return [
            [
                'Accueil',
                [
                    'sections' => [
                        [
                            'params' => [
                                'name'          => 'header',
                                'class'         => '',
                                'attributes'    => '',
                                'wrapper'       => 0,
                                'wrapperClass'  => ''
                            ],
                            'children' => [
                                [
                                    'params' => [
                                        'type' => 'block',
                                        'id'    => $this->getReference('header')->getId(),
                                        'name'  => $this->getReference('header')->getName(),
                                        'link'  => 1
                                    ]
                                ]
                            ]
                        ],
                        [
                            'params' => [
                                'name'          => 'main',
                                'class'         => '',
                                'attributes'    => '',
                                'wrapper'       => 0,
                                'wrapperClass'  => ''
                            ],
                            'children' => [
                                [
                                    'params' => [
                                        'type' => 'block',
                                        'id'    => $this->getReference('section')->getId(),
                                        'name'  => $this->getReference('section')->getName(),
                                        'link'  => 3
                                    ]
                                ]
                            ]
                        ],
                        [
                            'params' => [
                                'name'          => 'footer',
                                'class'         => '',
                                'attributes'    => '',
                                'wrapper'       => 0,
                                'wrapperClass'  => ''
                            ],
                            'children' => [
                                [
                                    'params' => [
                                        'type' => 'block',
                                        'id'    => $this->getReference('footer')->getId(),
                                        'name'  => $this->getReference('footer')->getName(),
                                        'link'  => 2
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                'accueil'
            ]
        ];
    }

    public function getDependencies()
    {
        return [
            BlockFixtures::class,
        ];
    }
}
