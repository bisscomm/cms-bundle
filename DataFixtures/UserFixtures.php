<?php
namespace Bci\CmsBundle\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        foreach ($this->getUserData() as [$firstName, $lastName, $username, $password, $email, $roles]) {
            $user = new User();
            $user->setFirstName($firstName);
            $user->setLastName($lastName);
            $user->setUsername($username);
            $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
            $user->setEmail($email);
            $user->setRoles($roles);
            $user->setResetPassword(true);

            $manager->persist($user);
            $this->addReference($username, $user);
        }

        $manager->flush();
    }

    private function getUserData(): array
    {
        return [
            // $userData = [$fullname, $username, $password, $email, $roles];
            ['Nadir',       'Pelletier',         'npelletier',   '1qazxsw2', 'npelletier@bisscomm.com',      ['ROLE_SUPER_ADMIN']],
            ['Shami',       'Garneau',           'sgarneau',     '1qazxsw2', 'sgarneau@bisscomm.com',        ['ROLE_SUPER_ADMIN']],
            ['Mathieu',     'Boucher',           'mboucher',   '1qazxsw2',   'mboucher@bisscomm.com',        ['ROLE_SUPER_ADMIN']],

        ];
    }


}
