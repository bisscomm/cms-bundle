<?php

namespace Bci\CmsBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Yaml\Yaml;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class BciCmsExtension
    extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );

        $loader->load("services.yaml");

        $this->loadNavigation($container, $configs);
    }

    private function loadNavigation(ContainerBuilder $container, $configs)
    {
        $nav = Yaml::parse(file_get_contents(__DIR__.'/../Resources/config/menu/sidebar.yaml'));
        $configs[0]['navigation']['sidebar'] = array_merge($nav['sidebar'],  $configs[0]['navigation']['sidebar']);

       // var_dump($configs[0]['navigation']['sidebar']); die();

        $container->setParameter('bci_cms', $configs[0]);
    }
}
