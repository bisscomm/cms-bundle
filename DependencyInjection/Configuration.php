<?php

namespace Bci\CmsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration
    implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('bci_cms');

        $treeBuilder
            ->getRootNode()
                ->children()
                    ->arrayNode('project')
                        ->children()
                            ->scalarNode('name')->end()
                            ->scalarNode('live_url')->defaultValue('www.project.com')->end()
                        ->end()
                    ->end()
                    ->arrayNode('sendgrid')
                        ->children()
                            ->scalarNode('apikey')->end()
                            ->scalarNode('notificationEmail')->defaultValue('support@bisscomm.com')->end()
                            ->scalarNode('notificationName')->defaultValue('config form not setted')->end()
                            ->scalarNode('noreply')->defaultValue('support@bisscomm.com')->end()
                            ->scalarNode('noreplyName')->defaultValue('config form not setted')->end()
                        ->end()
                    ->end()
                    ->arrayNode('postmark')
                        ->children()
                            ->scalarNode('serverAPItokens')->end()
                            ->scalarNode('from')->defaultValue('support@bisscomm.com')->end()
                            ->scalarNode('to')->defaultValue('config form not setted')->end()
                            ->scalarNode('replyTo')->defaultValue('support@bisscomm.com')->end()
                        ->end()
                    ->end()
                    ->variableNode('navigation')->end()
                    ->variableNode('sitemap')->end()
                    ->variableNode('default_job')->end()
                    ->variableNode('api')->end()
                    ->arrayNode('recaptcha')
                        ->children()
                            ->scalarNode('siteKey')->end()
                            ->scalarNode('secretKey')->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
