<?php

namespace Bci\CmsBundle\Entity;

use Bci\CmsBundle\Entity\Blog;
use Bci\CmsBundle\Helper\EntityTrait;
use Bci\CmsBundle\Helper\SiteMapableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="Bci\CmsBundle\Repository\ArticleRepository")
 * @Vich\Uploadable()
 */
class Article
{
    use EntityTrait;
    use SiteMapableTrait;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    private $locale;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subtitle;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     */
    private $contentSummary;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $publishedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $disabled;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Gedmo\Slug(fields={"title","id"})
     */
    private $slug;

    /**
     * @ORM\OneToOne(targetEntity="Bci\CmsBundle\Entity\UploadableFileCms", cascade={"persist", "remove"})
     */
    private $image;

    /**
     * @ORM\ManyToMany(targetEntity="Bci\CmsBundle\Entity\ArticleCategory", inversedBy="articles")
     */
    private $categories;

    /**
     * @ORM\ManyToOne(targetEntity=Blog::class, inversedBy="articles", cascade={"persist"} )
     */
    private $blog;

    /**
     * @ORM\OneToMany(targetEntity="Bci\CmsBundle\Entity\Meta", mappedBy="article", cascade={"remove","persist"})
     */
    private $metas;

    public function __construct()
    {
        $this->publishedAt = new \DateTime('now');
        $this->categories = new ArrayCollection();
        $this->metas = new ArrayCollection();
    }
    
    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getDisabled(): ?bool
    {
        return $this->disabled;
    }

    public function setDisabled(?bool $disabled): self
    {
        $this->disabled = $disabled;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getImage(): ?UploadableFileCms
    {
        return $this->image;
    }

    public function setImage(?UploadableFileCms $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|ArticleCategory[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(ArticleCategory $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addArticle($this);
        }

        return $this;
    }

    public function removeCategory(ArticleCategory $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
        }

        return $this;
    }

    public function getBlog(): ?Blog
    {
        return $this->blog;
    }

    public function setBlog(?Blog $blog): self
    {
        $this->blog = $blog;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * @param mixed $subtitle
     * @return Article
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContentSummary()
    {
        return $this->contentSummary;
    }

    /**
     * @param mixed $contentSummary
     * @return Article
     */
    public function setContentSummary($contentSummary)
    {
        $this->contentSummary = $contentSummary;
        return $this;
    }

    /**
     * @return Collection|Meta[]
     */
    public function getMetas(): Collection
    {
        return $this->metas;
    }

    public function addMeta(Meta $meta): self
    {
        if (!$this->metas->contains($meta)) {
            $this->metas[] = $meta;
            $meta->setArticle($this);
        }

        return $this;
    }

    public function removeMeta(Meta $meta): self
    {
        if ($this->metas->contains($meta)) {
            $this->metas->removeElement($meta);
            // set the owning side to null (unless already changed)
            if ($meta->getArticle() === $this) {
                $meta->setArticle(null);
            }
        }

        return $this;
    }
    
}
