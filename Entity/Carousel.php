<?php

namespace Bci\CmsBundle\Entity;

use Bci\CmsBundle\Repository\CarouselRepository;
use Bci\CmsBundle\Helper\EntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CarouselRepository::class)
 */
class Carousel
{
    use EntityTrait;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity=CarouselSlide::class, mappedBy="carousel", orphanRemoval=true)
     */
    private $slides;

    public function __construct()
    {
        $this->slides = new ArrayCollection();
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|CarouselSlide[]
     */
    public function getSlides(): Collection
    {
        $iterator = $this->slides->getIterator();
        $iterator->uasort(function (CarouselSlide $a, CarouselSlide $b)
        {
            return ($a->getPosition() < $b->getPosition()) ? -1 : 1;
        });

        return $orderedItems = new ArrayCollection(iterator_to_array($iterator));
    }

    public function addSlide(CarouselSlide $slide): self
    {
        if (!$this->slides->contains($slide)) {
            $this->slides[] = $slide;
            $slide->setCarousel($this);
        }

        return $this;
    }

    public function removeSlide(CarouselSlide $slide): self
    {
        if ($this->slides->removeElement($slide)) {
            // set the owning side to null (unless already changed)
            if ($slide->getCarousel() === $this) {
                $slide->setCarousel(null);
            }
        }

        return $this;
    }
}
