<?php

namespace Bci\CmsBundle\Entity;

use Bci\CmsBundle\Helper\EntityTrait;
use Bci\CmsBundle\Repository\ConfigurationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConfigurationRepository::class)
 */
class Configuration
{
    use EntityTrait;

    /**
     * @ORM\OneToMany(targetEntity=ScriptTag::class, mappedBy="configuration", orphanRemoval=true, cascade={"persist"})
     */
    private $scriptTags;

    public function __construct()
    {
        $this->scriptTags = new ArrayCollection();
    }

    /**
     * @return Collection|ScriptTag[]
     */
    public function getScriptTags(): Collection
    {
        return $this->scriptTags;
    }

    /**
     * @return Collection|ScriptTag[]
     */
    public function getScriptTagsByPosition($position): Collection
    {
        return $this->scriptTags->filter(
            function (ScriptTag $scriptTag) use ($position)
            {
                return $scriptTag->getPosition() == $position;
            }
        );
    }


    public function addScriptTag(ScriptTag $scriptTag): self
    {
        if (!$this->scriptTags->contains($scriptTag)) {
            $this->scriptTags[] = $scriptTag;
            $scriptTag->setConfiguration($this);
        }

        return $this;
    }

    public function removeScriptTag(ScriptTag $scriptTag): self
    {
        if ($this->scriptTags->removeElement($scriptTag)) {
            // set the owning side to null (unless already changed)
            if ($scriptTag->getConfiguration() === $this) {
                $scriptTag->setConfiguration(null);
            }
        }

        return $this;
    }
}
