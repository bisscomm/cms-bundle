<?php

namespace Bci\CmsBundle\Entity;

use Bci\CmsBundle\Helper\EntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Bci\CmsBundle\Repository\FormRepository")
 */
class Form
{
    use EntityTrait;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $htmlId;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Bci\CmsBundle\Entity\FormTranslations", mappedBy="form", cascade={"persist","remove"})
     */
    private $formTranslations;

    /**
     * @ORM\OneToMany(targetEntity="Bci\CmsBundle\Entity\FormItem", mappedBy="form", orphanRemoval=true)
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $items;

    /**
     * @ORM\OneToMany(targetEntity="Bci\CmsBundle\Entity\FormSubscription", mappedBy="form", orphanRemoval=true)
     */
    private $subscriptions;

    /**
     * @var Page|null
     *
     * @ORM\ManyToOne(targetEntity="Bci\CmsBundle\Entity\Page")
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id", nullable=true)
     */
    private $page;

    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
        $this->formTranslations = new ArrayCollection();
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        $this->setToken($this->slugify($this->title));

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }


    /**
     * @return Collection|FormTranslations[]
     */
    public function getFormTranslations(): Collection
    {
        return $this->formTranslations;
    }

    public function addFormTranslations(FormTranslations $formTranslations): self
    {
        if (!$this->formTranslations->contains($formTranslations)) {
            $this->formTranslations[] = $formTranslations;
            $formTranslations->setForm($this);
        }

        return $this;
    }

    public function hasTranslationsLocale($locale)
    {
        $formTranslations = $this->formTranslations->filter(
            function($entry) use ($locale) {
                return $entry->getLocale() == $locale;
            }
        );

        return !$formTranslations->isEmpty();
    }

    /**
     * @return Collection|FormItem[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    /**
     * @return Collection|FormItem[]
     */
    public function getItemsByLocale($locale = 'fr'): Collection
    {
        return $this->items->filter(
            function($item) use ($locale) {
                return $item->getLocale() == $locale;
            }
        );
    }

    public function addItem(FormItem $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setForm($this);
        }

        return $this;
    }

    public function removeItem(FormItem $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getForm() === $this) {
                $item->setForm(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FormSubscription[]
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }
    
    public function hasSubscriptions(): bool
    {
        return $this->subscriptions->count() > 0 ? true : false;
    }

    public function hasNewSubscriptions()
    {
        return $this->subscriptions->filter(
            function($item){
                return $item->getStatut() == null;
            }
        );
    }

    public function addSubscription(FormSubscription $subscription): self
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions[] = $subscription;
            $subscription->setForm($this);
        }

        return $this;
    }

    public function removeSubscription(FormSubscription $subscription): self
    {
        if ($this->subscriptions->contains($subscription)) {
            $this->subscriptions->removeElement($subscription);
            // set the owning side to null (unless already changed)
            if ($subscription->getForm() === $this) {
                $subscription->setForm(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHtmlId()
    {
        return $this->htmlId;
    }

    /**
     * @param mixed $htmlId
     * @return Form
     */
    public function setHtmlId($htmlId)
    {
        $this->htmlId = $htmlId;
        return $this;
    }
    
    

    /**
     * @return Page|null
     */
    public function getPage(): ?Page
    {
        return $this->page;
    }

    /**
     * @param Page|null $page
     * @return Form
     */
    public function setPage(?Page $page): Form
    {
        $this->page = $page;
        return $this;
    }
}
