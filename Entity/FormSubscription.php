<?php

namespace Bci\CmsBundle\Entity;

use Bci\CmsBundle\Helper\EntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Bci\CmsBundle\Repository\FormSubscriptionRepository")
 */
class FormSubscription
{
    use EntityTrait;

    /**
     * @ORM\ManyToOne(targetEntity="Bci\CmsBundle\Entity\Form", inversedBy="subscriptions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $form;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $referer;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $statut;

    /**
     * @ORM\OneToMany(targetEntity="Bci\CmsBundle\Entity\FormSubscriptionAnswer", mappedBy="subscription", orphanRemoval=true, cascade={"remove"})
     */
    private $answers;

    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    public function getForm(): ?Form
    {
        return $this->form;
    }

    public function setForm(?Form $form): self
    {
        $this->form = $form;

        return $this;
    }

    public function getReferer(): ?string
    {
        return $this->referer;
    }

    public function setReferer(?string $referer): self
    {
        $this->referer = $referer;

        return $this;
    }

    /**
     * @return Collection|FormSubscriptionAnswer[]
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    public function addAnswer(FormSubscriptionAnswer $answer): self
    {
        if (!$this->answers->contains($answer)) {
            $this->answers[] = $answer;
            $answer->setSubscription($this);
        }

        return $this;
    }

    public function removeAnswer(FormSubscriptionAnswer $answer): self
    {
        if ($this->answers->contains($answer)) {
            $this->answers->removeElement($answer);
            // set the owning side to null (unless already changed)
            if ($answer->getSubscription() === $this) {
                $answer->setSubscription(null);
            }
        }

        return $this;
    }

    public function getStatut()
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * @param FormItem $formItem
     * @return FormSubscriptionAnswer|mixed|string
     */
    public function getAnswerByFormItem(FormItem $formItem)
    {
        foreach ($this->getAnswers() as $answer)
        {
            if ($answer->getItem() === $formItem)
            {
                return $answer;
            }
        }
        return new FormSubscriptionAnswer();
    }
}
