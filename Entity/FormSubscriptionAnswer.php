<?php

namespace Bci\CmsBundle\Entity;

use Bci\CmsBundle\Helper\EntityTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Bci\CmsBundle\Repository\FormSubscriptionAnswerRepository")
 */
class FormSubscriptionAnswer
{
    use EntityTrait;

    /**
     * @ORM\ManyToOne(targetEntity="Bci\CmsBundle\Entity\FormSubscription", inversedBy="answers")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $subscription;

    /**
     * @ORM\ManyToOne(targetEntity="Bci\CmsBundle\Entity\FormItem")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $item;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $value;

    public function getSubscription(): ?FormSubscription
    {
        return $this->subscription;
    }

    public function setSubscription(?FormSubscription $subscription): self
    {
        $this->subscription = $subscription;

        return $this;
    }

    public function getItem(): ?FormItem
    {
        return $this->item;
    }

    public function setItem(?FormItem $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
