<?php

namespace Bci\CmsBundle\Entity;

use Bci\CmsBundle\Helper\AddressTrait;
use Bci\CmsBundle\Helper\EntityTrait;
use Bci\CmsBundle\Entity\Website;
use Bci\CmsBundle\Helper\GoogleJobTrait;
use Bci\CmsBundle\Helper\SiteMapableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Entity(repositoryClass="Bci\CmsBundle\Repository\JobRepository")
 */
class Job
{
    use EntityTrait;
    use SiteMapableTrait;
    use GoogleJobTrait;
    use AddressTrait;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    protected $locale;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", unique=true)
     * @Gedmo\Slug(fields={"title","id"})
     */
    private $slug;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     */
    private $summary;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $onlyShowInForm;

    /**
     * @ORM\ManyToMany(targetEntity=Website::class, mappedBy="jobs")
     */
    private $websites;

    /**
     * Many Jobs have Many Unique Metas.
     * @ORM\ManyToMany (targetEntity="Bci\CmsBundle\Entity\Meta", cascade={"remove","persist"})
     * @ORM\JoinTable(name="jobs_metas",
     *      joinColumns={@ORM\JoinColumn(name="job_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="meta_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     */
    private $metas;

    /**
     * @ORM\ManyToMany(targetEntity="Bci\CmsBundle\Entity\JobCategory", inversedBy="jobs")
     */
    private $categories;


    public function __construct()
    {
        $this->setStatus(true);
        $this->setStartAt(new \DateTime('now'));
        $this->onlyShowInForm = false;
        $this->websites = new ArrayCollection();
        $this->metas = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Job
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }


   
    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Job
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * @return mixed $summary
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }


    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(?bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * @param mixed $startAt
     * @return Job
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * @param mixed $endAt
     * @return Job
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;
        return $this;
    }

    public function getOnlyShowInForm(): ?bool
    {
        return $this->onlyShowInForm;
    }

    public function setOnlyShowInForm(?bool $onlyShowInForm): self
    {
        $this->onlyShowInForm = $onlyShowInForm;

        return $this;
    }


    /**
     * @return Collection|Website[]
     */
    public function getWebsites(): Collection
    {
        return $this->websites;
    }

    public function addWebsite(Website $website): self
    {
        if (!$this->websites->contains($website)) {
            $this->websites[] = $website;
            $website->addJob($this);
        }

        return $this;
    }

    public function removeWebsite(Website $website): self
    {
        if ($this->websites->removeElement($website)) {
            $website->removeJob($this);
        }

        return $this;
    }

    /**
     * @return Collection|Meta[]
     */
    public function getMetas(): Collection
    {
        return $this->metas;
    }

    public function addMeta(Meta $meta): self
    {
        if (!$this->metas->contains($meta)) {
            $this->metas[] = $meta;
        }

        return $this;
    }

    public function removeMeta(Meta $meta): self
    {
        if ($this->metas->contains($meta)) {
            $this->metas->removeElement($meta);
        }

        return $this;
    }

    /**
     * @return Collection|JobCategory[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(JobCategory $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addJob($this);
        }

        return $this;
    }

    public function removeCategory(JobCategory $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
        }

        return $this;
    }
}
