<?php
namespace Bci\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Bci\CmsBundle\Repository\LogRepository")
 * @ORM\Table(name="log")
 * @ORM\HasLifecycleCallbacks
 */
class Log
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;

//    /**
//     * @ORM\Column(name="stack_trace", type="array",nullable=true)
//     */
//    private $stackTrace;

    /**
     * @ORM\Column(name="level", type="smallint",nullable=true)
     */
    private $level;

    /**
     * @ORM\Column(name="level_name", type="string", length=50,nullable=true)
     */
    private $levelName;

    /**
     * @ORM\Column(name="channel", type="string", length=50,nullable=true)
     */
    private $channel;

//    /**
//     * @ORM\Column(type="string", length=255, nullable=true)
//     */
//    private $file;

//    /**
//     * @ORM\Column(type="integer", nullable=true)
//     */
//    private $line;

//    /**
//     * @ORM\Column(type="string", length=255, nullable=true)
//     */
//    private $class;

//    /**
//     * @ORM\Column(type="string", length=255, nullable=true)
//     */
//    private $function;

//    /**
//     * @ORM\Column(type="string", length=255, nullable=true)
//     */
//    private $url;

//    /**
//     * @ORM\Column(type="string", length=255, nullable=true)
//     */
//    private $ip;

//    /**
//     * @ORM\Column(type="string", length=20, nullable=true)
//     */
//    private $http_method;

//    /**
//     * @ORM\Column(type="string", length=100, nullable=true)
//     */
//    private $server;

//    /**
//     * @ORM\Column(type="string", length=255, nullable=true)
//     */
//    private $referrer;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $formatted;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;


    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime('now', new \DateTimeZone('America/Toronto'));
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Log
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     * @return Log
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

//    /**
//     * @return mixed
//     */
//    public function getStackTrace()
//    {
//        return $this->stackTrace;
//    }
//
//    /**
//     * @param mixed $stackTrace
//     * @return Log
//     */
//    public function setStackTrace($stackTrace)
//    {
//        $this->stackTrace = $stackTrace;
//        return $this;
//    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     * @return Log
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLevelName()
    {
        return $this->levelName;
    }

    /**
     * @param mixed $levelName
     * @return Log
     */
    public function setLevelName($levelName)
    {
        $this->levelName = $levelName;
        return $this;
    }

//    public function getFile(): ?string
//    {
//        return $this->file;
//    }
//
//    public function setFile(?string $file): self
//    {
//        $this->file = $file;
//
//        return $this;
//    }
//
//    public function getLine(): ?int
//    {
//        return $this->line;
//    }
//
//    public function setLine(?int $line): self
//    {
//        $this->line = $line;
//
//        return $this;
//    }

//    public function getClass(): ?string
//    {
//        return $this->class;
//    }
//
//    public function setClass(?string $class): self
//    {
//        $this->class = $class;
//
//        return $this;
//    }
//
//    public function getFunction(): ?string
//    {
//        return $this->function;
//    }
//
//    public function setFunction(?string $function): self
//    {
//        $this->function = $function;
//
//        return $this;
//    }
//
//    public function getUrl(): ?string
//    {
//        return $this->url;
//    }
//
//    public function setUrl(?string $url): self
//    {
//        $this->url = $url;
//
//        return $this;
//    }
//
//    public function getIp(): ?string
//    {
//        return $this->ip;
//    }
//
//    public function setIp(?string $ip): self
//    {
//        $this->ip = $ip;
//
//        return $this;
//    }
//
//    public function getHttpMethod(): ?string
//    {
//        return $this->http_method;
//    }
//
//    public function setHttpMethod(?string $http_method): self
//    {
//        $this->http_method = $http_method;
//
//        return $this;
//    }
//
//    public function getServer(): ?string
//    {
//        return $this->server;
//    }
//
//    public function setServer(?string $server): self
//    {
//        $this->server = $server;
//
//        return $this;
//    }
//
//    public function getReferrer(): ?string
//    {
//        return $this->referrer;
//    }
//
//    public function setReferrer(?string $referrer): self
//    {
//        $this->referrer = $referrer;
//
//        return $this;
//    }

    public function getFormatted(): ?string
    {
        return $this->formatted;
    }

    public function setFormatted(?string $formatted): self
    {
        $this->formatted = $formatted;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @param mixed $channel
     * @return Log
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return Log
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }
    
}