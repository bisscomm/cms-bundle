<?php

namespace Bci\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Bci\CmsBundle\Repository\MenuRepository")
 */
class Menu
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="Bci\CmsBundle\Entity\Heading", mappedBy="menu")
     */
    private $headings;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $menuItems;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Bci\CmsBundle\Entity\MenuItem", mappedBy="menu")
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $items;

    public function __construct()
    {
        $this->menuItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }


    public function getMenuItems(): ?string
    {
        return $this->menuItems;
    }

    public function setMenuItems(?string $menuItems): self
    {
        $this->menuItems = $menuItems;

        return $this;
    }

    public function unserializedMenu()
    {
        return $this->menuItems ? json_decode($this->menuItems) : [];
    }

    /**
     * @return Collection|Heading[]
     */
    public function getHeadings(): Collection
    {
        return $this->headings;
    }

    public function addheading(Heading $heading): self
    {
        if (!$this->headings->contains($heading)) {
            $this->headings[] = $heading;
            $heading->setMenu($this);
        }

        return $this;
    }

    public function removeHeading(Heading $heading): self
    {
        if ($this->headings->contains($heading)) {
            $this->headings->removeElement($heading);
            // set the owning side to null (unless already changed)
            if ($heading->getMenu() === $this) {
                $heading->setMenu(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MenuItem[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function setItems($items): self
    {
        $this->items = $items;

        return $this;
    }

    public function addItem(MenuItem $menuItem): self
    {
        if (!$this->items->contains($menuItem)) {
            $this->items[] = $menuItem;
            $menuItem->setMenu($this);
        }

        return $this;
    }



    public function removeMenuItem(MenuItem $menuItem): self
    {
        if ($this->items->contains($menuItem)) {
            $this->items->removeElement($menuItem);
            // set the owning side to null (unless already changed)
            if ($menuItem->getMenu() === $this) {
                $menuItem->setMenu(null);
            }
        }

        return $this;
    }

    public function display($locale = 'fr')
    {
        return $this->items->filter(
            function($entry) use ($locale) {
                return $entry->getLocale() == $locale && $entry->getParent() == null && $entry->canDisplay();
            }
        );
    }
}
