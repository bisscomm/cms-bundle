<?php

namespace Bci\CmsBundle\Entity;

use Bci\CmsBundle\Helper\EntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Bci\CmsBundle\Repository\MenuItemRepository")
 */
class MenuItem
{

    Use EntityTrait;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $original;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=5)
     */
    private $locale;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @var Page|null
     *
     * @ORM\ManyToOne(targetEntity="Bci\CmsBundle\Entity\Page")
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id", nullable=true)
     */
    private $page;

    /**
     * @var Menu
     *
     * @ORM\ManyToOne(targetEntity="Bci\CmsBundle\Entity\Menu")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="id", nullable=false)
     */
    private $menu;

    /**
     * @var MenuItem
     *
     * @ORM\ManyToOne(targetEntity="Bci\CmsBundle\Entity\Menuitem", inversedBy="children")
     * @ORM\JoinColumn(nullable=true)
     */
    private $parent;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Bci\CmsBundle\Entity\MenuItem", mappedBy="parent", orphanRemoval=true, cascade={"persist", "remove"})
     * @ORM\OrderBy({"position"="asc"})
     */
    private $children;

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return MenuItem
     */
    public function setLabel(string $label): MenuItem
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getOriginal(): ?string
    {
        return $this->original;
    }

    /**
     * @param string $original
     * @return MenuItem
     */
    public function setOriginal(?string $original): MenuItem
    {
        $this->original = $original;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     * @return MenuItem
     */
    public function setLocale(string $locale): MenuItem
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return MenuItem
     */
    public function setPosition($position = null): MenuItem
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return MenuItem
     */
    public function setSlug(string $slug): MenuItem
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return Page|null
     */
    public function getPage(): ?Page
    {
        return $this->page;
    }

    /**
     * @param Page|null $page
     * @return MenuItem
     */
    public function setPage(?Page $page = null): MenuItem
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return Menu
     */
    public function getMenu(): Menu
    {
        return $this->menu;
    }

    /**
     * @param Menu $menu
     * @return MenuItem
     */
    public function setMenu(Menu $menu): MenuItem
    {
        $this->menu = $menu;
        return $this;
    }

    /**
     * @return MenuItem
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param MenuItem $parent
     * @return MenuItem
     */
    public function setParent(MenuItem $parent = null)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    /**
     * @return Collection
     */
    public function getDisplayChildren($locale = 'fr'): Collection
    {
        return $this->children->filter(
            function($entry) use ($locale) {
                return $entry->getLocale() == $locale && $entry->canDisplay($locale);
            }
        );
    }

    /**
     * @return Bool
     */
    public function hasDisplayChildren($locale = 'fr'): bool
    {
        $children = $this->children->filter(
            function($entry) use ($locale) {
                return $entry->getLocale() == $locale && $entry->canDisplay($locale);
            }
        );

        return $children->count() > 0;
    }

    /**
     * @param Collection $children
     * @return MenuItem
     */
    public function setChildren(Collection $children): MenuItem
    {
        $this->children = $children;
        return $this;
    }

    public function canDisplay($locale = 'fr')
    {
        try {
            return $this->page == null || $this->page->getHeadingLocale($locale)->canDisplay();
        } catch (\Exception $e) {
            //var_dump($this->page);
            return false;
        }
    }
}
