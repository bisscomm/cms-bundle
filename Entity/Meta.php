<?php

namespace Bci\CmsBundle\Entity;

use Bci\CmsBundle\Entity\UploadableFileCms;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;



/**
 * @Vich\Uploadable()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Bci\CmsBundle\Repository\MetaRepository")
 */
class Meta
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    private $locale;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $property;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $displayName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $accessor;

    /**
     * @ORM\ManyToOne(targetEntity="Bci\CmsBundle\Entity\Heading", inversedBy="metas")
     * @ORM\JoinColumn(nullable=true)
     */
    private $heading;

    /**
     * @ORM\OneToOne(targetEntity="Bci\CmsBundle\Entity\UploadableFileCms", cascade={"all"})
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="Bci\CmsBundle\Entity\Article", inversedBy="metas")
     * @ORM\JoinColumn(nullable=true)
     */
    private $article;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProperty(): ?string
    {
        return $this->property;
    }

    public function setProperty(?string $property): self
    {
        $this->property = $property;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getHeading(): ?Heading
    {
        return $this->heading;
    }

    public function setHeading(?Heading $heading): self
    {
        $this->heading = $heading;

        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Meta
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * @param mixed $displayName
     * @return Meta
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccessor()
    {
        return $this->accessor;
    }

    /**
     * @param mixed $accessor
     * @return Meta
     */
    public function setAccessor($accessor)
    {
        $this->accessor = $accessor;
        return $this;
    }

    public function getImage(): ?UploadableFileCms
    {
        return $this->image;
    }

    public function setImage(?UploadableFileCms $image): self
    {
        $this->image = $image;

        return $this;
    }

//    /**
//     * @ORM\PreFlush()
//     * @ORM\PreUpdate()
//     * @ORM\PrePersist()
//     */
//    public function preSetImageUrlToContent()
//    {
//        $info = $this->image;
//        $a = $info;
//
//    }
//
//    /**
//     * @ORM\PostPersist()
//     * @ORM\PostUpdate()
//     *
//     */
//    public function postSetImageUrlToContent()
//    {
//        $info = $this->getImage()->getFileInfo();
//        $a = $this->image;
//    }
//
//    /**
//     * @ORM\PostLoad()
//     */
//    public function loadSetImageUrlToContent()
//    {
//        $info = $this->getImage()->getFileInfo();
//        $a = $this->image;
//    }
}
