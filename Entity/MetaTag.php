<?php

namespace Bci\CmsBundle\Entity;



class MetaTag
{

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $property
     */
    private $property;

    /**
     * @var string $content
     */
    private $content;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @var string $displayName
     */
    private $displayName;

    /**
     * @var string $accessor
     */
    private $accessor;


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProperty(): ?string
    {
        return $this->property;
    }

    public function setProperty(?string $property): self
    {
        $this->property = $property;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    /**
     * @param string $displayName
     * @return MetaTag
     */
    public function setDisplayName(string $displayName): MetaTag
    {
        $this->displayName = $displayName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
        
        return $this;
    }

    /**
     * @return string
     */
    public function getAccessor(): string
    {
        return $this->accessor;
    }

    /**
     * @param string $accessor
     * @return MetaTag
     */
    public function setAccessor(string $accessor): MetaTag
    {
        $this->accessor = $accessor;
        return $this;
    }

    public function __toString()
    {
        return $this->getAccessor();
    }


}
