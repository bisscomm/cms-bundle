<?php

namespace Bci\CmsBundle\Entity;

use Bci\CmsBundle\Helper\AddressTrait;
use Bci\CmsBundle\Helper\EntityTrait;
use Bci\CmsBundle\Helper\TranslatableUploadableFileCmsTrait;
use Bci\CmsBundle\Repository\OwnerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass=OwnerRepository::class)
 * @Vich\Uploadable()
 */
class Owner
{
   use EntityTrait;
   use AddressTrait;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    private $locale;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $legalName;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phoneNumber;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fbLink;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $twitLink;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $instaLink;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lkLink;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     */
    private $timeTable;

    /**
     * Many Owners have Many Unique TranslatableUploadableFileCms.
     * @ORM\ManyToMany (targetEntity="Bci\CmsBundle\Entity\TranslatableUploadableFileCms", cascade={"remove","persist"})
     * @ORM\JoinTable(name="owner_logos",
     *      joinColumns={@ORM\JoinColumn(name="owner_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="logo_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     */
    private $logos;

    /**
     *  Unmapped use to access file in form or controller
     */
    private $logo;
    

    public function __construct()
    {
        $this->logos = new ArrayCollection();
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Owner
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLegalName()
    {
        return $this->legalName;
    }

    /**
     * @param mixed $legalName
     * @return Owner
     */
    public function setLegalName($legalName)
    {
        $this->legalName = $legalName;
        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getFbLink(): ?string
    {
        return $this->fbLink;
    }

    public function setFbLink(?string $fbLink): self
    {
        $this->fbLink = $fbLink;

        return $this;
    }

    public function getTwitLink(): ?string
    {
        return $this->twitLink;
    }

    public function setTwitLink(?string $twitLink): self
    {
        $this->twitLink = $twitLink;

        return $this;
    }

    public function getInstaLink(): ?string
    {
        return $this->instaLink;
    }

    public function setInstaLink(?string $instaLink): self
    {
        $this->instaLink = $instaLink;

        return $this;
    }

    public function getLkLink(): ?string
    {
        return $this->lkLink;
    }

    public function setLkLink(?string $lkLink): self
    {
        $this->lkLink = $lkLink;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTimeTable(): ?string
    {
        return $this->timeTable;
    }

    public function setTimeTable(?string $timeTable): self
    {
        $this->timeTable = $timeTable;

        return $this;
    }

    public function getLogo( string $locale = null): ?TranslatableUploadableFileCms
    {
        $funcLocale = $locale == null ? $this->locale : $locale;

        $translatableUploadableFileCms = $this->logos->filter(function (TranslatableUploadableFileCms $translatableUploadableFileCms) use ($funcLocale)
        {
            return $translatableUploadableFileCms->getLocale() == $funcLocale;
        });

        if ($translatableUploadableFileCms->isEmpty())
        {
            return null;
        }
        else
        {
            return $translatableUploadableFileCms->first();
        }

    }

    public function setLogo(?TranslatableUploadableFileCms $translatableFile, string $locale = null): self
    {
        $funcLocale = $locale == null ? $this->locale : $locale;
        
        if ($translatableFile->isToDelete())
        {
            $this->removeLogo();
        }
        if (!$this->logos->contains($translatableFile))
        {
            $translatableFile->setLocale($funcLocale);
            $this->logos[] = $translatableFile;
        }
        return $this;
    }


    private function removeLogo(string $locale = null): self
    {
        $funcLocale = $locale == null ? $this->locale : $locale;

        if ($this->logos->contains($this->getLogo($funcLocale))) {
            $this->logos->removeElement($this->getLogo($funcLocale));
        }

        return $this;
    }

    public function setLogoToDysplay($locale = null)
    {
        $this->logo = $this->getLogo($locale);
        return $this;
    }

    /**
     * @ORM\PreFlush()
     */
    public function preFlush()
    {
        if ($this->getLogo() instanceof TranslatableUploadableFileCms)
        {
            if ($this->getLogo()->isToDelete())
            {
                $this->removeLogo();
            }
        }
    }
}
