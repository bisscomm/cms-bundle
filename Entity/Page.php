<?php

namespace Bci\CmsBundle\Entity;

use Bci\CmsBundle\Helper\DraftTrait;
use Bci\CmsBundle\Helper\EntityTrait;
use Bci\CmsBundle\Helper\SiteMapableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Bci\CmsBundle\Repository\PageRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Page
{
    use EntityTrait;
    
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $meta;

    /**
     * @ORM\ManyToOne(targetEntity="Bci\CmsBundle\Entity\Template", inversedBy="pages")
     * @Assert\NotBlank
     */
    private $template;

    /**
     * @ORM\OneToMany(targetEntity="Bci\CmsBundle\Entity\PageTranslation", mappedBy="page", orphanRemoval=true, cascade={"persist", "remove"})
     * @ORM\OrderBy({"createdAt"="asc"})
     */
    private $translations;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Bci\CmsBundle\Entity\Heading", mappedBy="page", cascade={"remove", "persist"})
     */
    private $headings;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true, options={"default": false})
     */
    private $isHome;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true, options={"default": true})
     */
    private $siteMapable;

    public function __construct()
    {
        $this->headings = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getSlugForLocale($locale)
    {
        $heading = $this->headings->filter(
            function($entry) use ($locale) {
                return $entry->getLocale() == $locale;
            }
        );

        return !$heading->isEmpty() ? $heading->first()->getSlug() : '/';
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getMeta(): ?string
    {
        return $this->meta;
    }

    public function setMeta(?string $meta): self
    {
        $this->meta = $meta;

        return $this;
    }

    public function getTemplate(): ?Template
    {
        return $this->template;
    }

    public function setTemplate(?Template $template): self
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTranslations($templateBlockId = null)
    {
        if ($templateBlockId) {
            return [
                "templateBlockId" => $templateBlockId,
                "trans" => $this->translations->filter(
                    function($pageTranslation) use ($templateBlockId) {
                        return $pageTranslation->getTemplateBlock()->getId() == $templateBlockId;
                    }
                )
            ];
        } else {
            return [
                "templateBlockId" => $templateBlockId,
                "trans" => $this->translations
            ];
        }
    }

    /**
     * @param mixed $translations
     * @return Page
     */
    public function setTranslations($translations)
    {
        $this->translations = $translations;
        return $this;
    }


    public function getTranslation($key, $locale)
    {
        $translation = $this->translations->filter(
            function($entry) use ($key, $locale) {
                return $entry->getField() == $key && $entry->getTranslation()->getLocale() == $locale;
            }
        );

        return $translation->last()?$translation->last()->getTranslation()->getValue():'';
    }

    /**
     * @param TemplateBlock $templateBlock
     * @param $field
     * @param $locale
     * @param $value
     * @return $this
     */
    public function addTranslation(TemplateBlock $templateBlock, $field, $locale, $value)
    {
        $pageTranslation = new PageTranslation();
        $translation     = new Translation();

        $translation
            ->setLocale($locale)
            ->setValue($value);

        $pageTranslation
            ->setField($field)
            ->setPage($this)
            ->setTemplateBlock($templateBlock)
            ->setTranslation($translation);

        $disabling = $this->translations->filter(
            function (PageTranslation $entity) use($templateBlock, $field, $locale) {
                return $entity->getTemplateBlock()->getId() == $templateBlock->getId()
                    && !$entity->isDisabled()
                    && $entity->getField() == $field
                    && $entity->getTranslation()->getLocale() == $locale;
            }
        );

        foreach($disabling as $tb) {
            $tb->setDisabled(true);
        }

        $this->translations->add($pageTranslation);

        return $this;
    }

    /**
     * @return ArrayCollection|Heading[]
     */
    public function getHeadings()
    {
        return $this->headings;
    }

    public function addHeading(Heading $heading): self
    {
        if (!$this->headings->contains($heading)) {
            $this->headings[] = $heading;
            $heading->setPage($this);
        }

        return $this;
    }

    public function removeHeading(Heading $heading): self
    {
        if ($this->headings->contains($heading)) {
            $this->headings->removeElement($heading);
            // set the owning side to null (unless already changed)
            if ($heading->getPage() === $this) {
                $heading->setPage(null);
            }
        }

        return $this;
    }

    public function hasHeadingLocale($locale)
    {
        $heading = $this->headings->filter(
            function($entry) use ($locale) {
                    return $entry->getLocale() == $locale;
            }
        );

        return !$heading->isEmpty();
    }

    public function getHeadingLocale($locale)
    {
        $heading = $this->headings->filter(
            function($entry) use ($locale) {
                return $entry->getLocale() == $locale;
            }
        );

        return !$heading->isEmpty()?$heading->first(): false;
    }

    public function isHome(): ?bool
    {
        return $this->isHome;
    }

    /**
     * @param bool $isHome
     * @return Page
     */
    public function setIsHome(bool $isHome): Page
    {
        $this->isHome = $isHome;
        return $this;
    }
}
