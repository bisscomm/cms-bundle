<?php

namespace Bci\CmsBundle\Entity;

use Bci\CmsBundle\Entity\Page;
use Bci\CmsBundle\Entity\TemplateBlock;
use Bci\CmsBundle\Entity\Translation;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Bci\CmsBundle\Repository\PageTranslationRepository")
 */
class PageTranslation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Bci\CmsBundle\Entity\Page", inversedBy="translations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $page;

    /**
     * @ORM\ManyToOne(targetEntity="Bci\CmsBundle\Entity\TemplateBlock", cascade={"persist"} )
     * @ORM\JoinColumn(nullable=false)
     */
    private $templateBlock;

    /**
     * @ORM\OneToOne(targetEntity="Bci\CmsBundle\Entity\Translation", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $translation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $field;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : false})
     */
    private $disabled;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->disabled = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPage(): ?Page
    {
        return $this->page;
    }

    public function setPage(?Page $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getTemplateBlock(): ?TemplateBlock
    {
        return $this->templateBlock;
    }

    public function setTemplateBlock(?TemplateBlock $templateBlock): self
    {
        $this->templateBlock = $templateBlock;

        return $this;
    }

    public function getTranslation(): ?Translation
    {
        return $this->translation;
    }

    public function setTranslation(Translation $translation): self
    {
        $this->translation = $translation;

        return $this;
    }

    public function getField(): ?string
    {
        return $this->field;
    }

    public function setField(string $field): self
    {
        $this->field = $field;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function isDisabled()
    {
        return $this->disabled;
    }



    /**
     * @param mixed $disabled
     * @return PageTranslation
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;
        return $this;
    }
}
