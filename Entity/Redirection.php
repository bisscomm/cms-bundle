<?php

namespace Bci\CmsBundle\Entity;

use Bci\CmsBundle\Repository\RedirectionRepository;
use Bci\CmsBundle\Helper\EntityTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RedirectionRepository::class)
 */
class Redirection
{
    use EntityTrait;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $fromUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $toUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $browser = [];

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $cookie;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ipAddress;

    /**
     * @return mixed
     */
    public function getFromUrl()
    {
        return $this->fromUrl;
    }

    /**
     * @param mixed $fromUrl
     * @return Redirection
     */
    public function setFromUrl($fromUrl)
    {
        $this->fromUrl = $fromUrl;
        return $this;
    }

    public function getToUrl(): ?string
    {
        return $this->toUrl;
    }

    public function setToUrl(?string $toUrl): self
    {
        $this->toUrl = $toUrl;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getBrowser(): ?array
    {
        return $this->browser;
    }

    public function setBrowser(?array $browser): self
    {
        $this->browser = $browser;

        return $this;
    }

    public function getCookie(): ?string
    {
        return $this->cookie;
    }

    public function setCookie(?string $cookie): self
    {
        $this->cookie = $cookie;

        return $this;
    }

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    public function setIpAddress(?string $ipAddress): self
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }
}
