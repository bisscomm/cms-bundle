<?php

namespace Bci\CmsBundle\Entity;

use Bci\CmsBundle\Helper\EntityTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ScriptTagRepository::class)
 */
class ScriptTag
{
    use EntityTrait;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $position;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $script;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $presentInStaging;

    /**
     * @ORM\ManyToOne (targetEntity=Configuration::class, inversedBy="configuration", cascade={"persist"})
     */
    private $configuration;


    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     * @return ScriptTag
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    public function getScript(): ?string
    {
        return $this->script;
    }

    public function setScript(?string $script): self
    {
        $this->script = $script;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPresentInStaging()
    {
        return $this->presentInStaging;
    }

    /**
     * @param mixed $presentInStaging
     * @return ScriptTag
     */
    public function setPresentInStaging($presentInStaging)
    {
        $this->presentInStaging = $presentInStaging;
        return $this;
    }

    

    /**
     * @return mixed
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * @param mixed $configuration
     * @return ScriptTag
     */
    public function setConfiguration($configuration)
    {
        if ($configuration)
        {
            if (!$configuration->getScriptTags()->contains($this))
            {
                $configuration->addScriptTag($this);
            }
        }
        else
        {
            if ($this->configuration->getScriptTags()->contains($this))
            {
                $this->configuration->removeScriptTag($this);
            }
        }
        $this->configuration = $configuration;
        return $this;
    }


}
