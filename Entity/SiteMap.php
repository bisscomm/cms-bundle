<?php

namespace Bci\CmsBundle\Entity;

use App\Repository\SiteMapRepository;
use Bci\CmsBundle\Helper\EntityTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Bci\CmsBundle\Repository\SiteMapRepository")
 */
class SiteMap
{
    use EntityTrait;
    
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $locale;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $siteMappedEntityId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $siteMappedEntityClass;
    

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageUrl;

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     * @return SiteMap
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }
    
    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteMappedEntityId()
    {
        return $this->siteMappedEntityId;
    }

    /**
     * @param mixed $siteMappedEntityId
     * @return SiteMap
     */
    public function setSiteMappedEntityId($siteMappedEntityId)
    {
        $this->siteMappedEntityId = $siteMappedEntityId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteMappedEntityClass()
    {
        return $this->siteMappedEntityClass;
    }

    /**
     * @param mixed $siteMappedEntityClass
     * @return SiteMap
     */
    public function setSiteMappedEntityClass($siteMappedEntityClass)
    {
        $this->siteMappedEntityClass = $siteMappedEntityClass;
        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function setImageUrl(?string $imageUrl): self
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }
}
