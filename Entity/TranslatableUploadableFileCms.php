<?php

namespace Bci\CmsBundle\Entity;

use Bci\CmsBundle\Entity\UploadableFileCms;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 */
class TranslatableUploadableFileCms extends UploadableFileCms
{
    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     */
   private $locale;

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     * @return TranslatableUploadableFileCms
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }
    
}