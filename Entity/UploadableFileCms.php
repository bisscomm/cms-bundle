<?php

namespace Bci\CmsBundle\Entity;

use Bci\CmsBundle\Helper\EntityTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @Vich\Uploadable
 */
class UploadableFileCms
{
    use EntityTrait;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(
     *     mapping="uploadableFile",
     *     fileNameProperty="fileInfo.name",
     *     size="fileInfo.size",
     *     mimeType="fileInfo.mimeType",
     *     originalName="fileInfo.originalName",
     *     dimensions="fileInfo.dimensions"
     * )
     *
     * @var File|null
     */
    private $tempFile;
    
    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * Used in controller to find wether or not if the file has to be remove
     */
    private $toDelete = false;

    /**
     * NOTE : THIS CONTAIN THE FILE INFORMATIONS
     *
     * @ORM\Embedded(class="Vich\UploaderBundle\Entity\File")
     *
     * @var EmbeddedFile
     */
    private $fileInfo;


    public function __construct()
    {
        $this->fileInfo = new EmbeddedFile();
    }

    public function getFileInfo(): \Vich\UploaderBundle\Entity\File
    {
        return $this->fileInfo;
    }

    /**
     * @return File|null
     */
    public function getTempFile(): ?File
    {
        return $this->tempFile;
    }

    /**
     *
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|null $tempFile
     * @return UploadableFileCms
     * @throws \Exception
     */
    public function setTempFile(?File $tempFile): UploadableFileCms
    {
        $this->tempFile = $tempFile;

        if (null !== $tempFile)
        {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime();
        }
        return $this;
    }

    public function setFileInfo(\Vich\UploaderBundle\Entity\File $fileInfo): self
    {
        $this->fileInfo = $fileInfo;

        return $this;
    }

    /**
     * @return bool
     */
    public function isToDelete(): bool
    {
        return $this->toDelete;
    }

    /**
     * @param $toDelete
     * @return UploadableFileCms
     */
    public function setToDelete($toDelete): UploadableFileCms
    {
        if ($toDelete)
        {
            $this->toDelete = (bool) $toDelete;
        }
        return $this;
    }

}