<?php


namespace Bci\CmsBundle\Enum;

/**
 * Class FlashType
 * Flash type is the same type as it's class style in css for notify.js.
 * Ex: TYPE_INFO = info; refer to the info class for the app:buildNotification function, which use this type ('info')
 * to render the stylised flash notification. in this case it is yellow.
 *
 * Use case example in a controller.
 * $this->addFlash(FlashType::TYPE_SUCCESS, FlashType::getTypeMessage(FlashType::TYPE_SUCCESS));
 * $this->addFlash(FlashType::TYPE_WARNING, FlashType::getTypeMessage(FlashType::TYPE_WARNING));
 *
 *
 * @package Bci\CmsBundle\Enum
 */
class FlashType
{
    const TYPE_INFO    = "info";
    const TYPE_SUCCESS = "success";
    const TYPE_WARNING = "warning";
    const TYPE_DANGER  = "danger";
    const TYPE_PRIMARY = "primary";
    const TYPE_ROSE  = "rose";

    /** @var array of messages by type */
    protected static $typeMessages = [
        self::TYPE_INFO    => 'Information',
        self::TYPE_SUCCESS => 'Succès',
        self::TYPE_WARNING => 'Attention',
        self::TYPE_DANGER  => 'Danger',
        self::TYPE_PRIMARY => 'Primary',
        self::TYPE_ROSE => 'Rose',
    ];

    /**
     * @param  string $flashType
     * @return string
     */
    public static function getTypeMessage($flashType)
    {
        if (!isset(static::$typeMessages[$flashType])) {
            return "Unknown type ($flashType)";
        }
        return static::$typeMessages[$flashType];
    }

    /**
     * Just in case you need to return all available type of flash msg.
     * @return array<string>
     */
    public static function getAvailableTypes()
    {
        return [
            self::TYPE_INFO,
            self::TYPE_SUCCESS,
            self::TYPE_WARNING,
            self::TYPE_DANGER,
            self::TYPE_PRIMARY,
            self::TYPE_ROSE
        ];
    }

}