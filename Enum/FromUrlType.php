<?php


namespace Bci\CmsBundle\Enum;

class FromUrlType
{
    const EQUAL = "EQUAL";
    const REGEX = "REGEX";


    /** @var array of label by fromUrlsType */
    protected static $fromUrlTypeLabel = [
        self::EQUAL => 'bci.cms.redirection.fromUrl.types.equal',
        self::REGEX => 'bci.cms.redirection.fromUrl.types.regex'
    ];

    /**
     * @param string $fromUrlType
     * @return string
     */
    public static function getFromUrlTypeLabel($fromUrlsType)
    {
        if (!isset(static::$fromUrlTypeLabel[$fromUrlsType]))
        {
            return "Unknown type ($fromUrlsType)";
        }
        return static::$fromUrlTypeLabel[$fromUrlsType];
    }

    /**
     * Just in case you need to return all available type of redirection.
     * @return array<int>
     */
    public static function getAvailableTypes()
    {
        return [
            self::EQUAL,
            self::REGEX
        ];
    }

    /**
     * @return array|string[]
     */
    public static function getFromUrlTypeChoices()
    {
        return static::$fromUrlTypeLabel;
    }
//TODO TEST THIS + FORM URLS TYPE
}