<?php


namespace Bci\CmsBundle\Enum;

/**
 * @package Bci\CmsBundle\Enum
 */
class MetaTagType
{
    const TYPE_BASIC = "BASIC";
    const TYPE_OPEN_GRAPH = "OPENGRAPH";
    const TYPE_TWITTER = "TWITTER";
    const TYPE_CUSTOM  = "CUSTOM";
    
    
    /**
     * Just in case you need to return all available type of MetaTag.
     * @return array<string>
     */
    public static function getAvailableTypes()
    {
        return [
            self::TYPE_BASIC,
            self::TYPE_OPEN_GRAPH,
            self::TYPE_TWITTER,
            self::TYPE_CUSTOM
        ];
    }

}