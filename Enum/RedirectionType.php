<?php

namespace Bci\CmsBundle\Enum;

class RedirectionType
{
    const TYPE_301 = 301;
    const TYPE_302 = 302;
    const TYPE_307 = 307;


    /** @var array of label by redirectionType */
    protected static $redirectionTypeLabel = [
        self::TYPE_301 => 'bci.cms.redirection.types.301',
        self::TYPE_302 => 'bci.cms.redirection.types.302',
        self::TYPE_307 => 'bci.cms.redirection.types.307',
    ];

    /**
     * @param  string $redirectionType
     * @return string
     */
    public static function getRedirectionTypeLabel($redirectionType)
    {
        if (!isset(static::$redirectionTypeLabel[$redirectionType])) {
            return "Unknown type ($redirectionType)";
        }
        return static::$redirectionTypeLabel[$redirectionType];
    }

    /**
     * Just in case you need to return all available type of redirection.
     * @return array<int>
     */
    public static function getAvailableTypes()
    {
        return [
            self::TYPE_301,
            self::TYPE_302,
            self::TYPE_307,
        ];
    }

    /**
     * @return array|string[]
     */
    public static function getRedirectionTypeChoices()
    {
        return static::$redirectionTypeLabel;
    }

}