<?php


namespace Bci\CmsBundle\Enum;

class ScriptTagPosition
{
    const HEAD_TOP = "HEAD_TOP";
    const HEAD_BOTTOM = "HEAD_BOTTOM";
    const BODY_TOP = "BODY_TOP";
    const BODY_BOTTOM = "BODY_BOTTOM";


    /** @var array of label by position type */
    protected static $scriptPositions = [
        'Head (Début)' => self::HEAD_TOP,
        'Head (Fin)' => self::HEAD_BOTTOM,
        'Body (Début)' => self::BODY_TOP,
        'Body (Fin)' => self::BODY_BOTTOM,
    ];


    /**
     * Just in case you need to return all available type of positions.
     * @return array<int>
     */
    public static function getAvailableTypes()
    {
        return [
            self::HEAD_TOP,
            self::HEAD_BOTTOM,
            self::BODY_TOP,
            self::BODY_BOTTOM
        ];
    }

    /**
     * @return array|string[]
     */
    public static function getPositionTypeChoices()
    {
        return static::$scriptPositions;
    }

}