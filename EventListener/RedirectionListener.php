<?php

namespace Bci\CmsBundle\EventListener;

use Bci\CmsBundle\Entity\Log;
use Bci\CmsBundle\Entity\Redirection;
use Bci\CmsBundle\Repository\RedirectionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\RouterInterface;

class RedirectionListener
{
    private $redirectionRepository;
    
    private $entityManager;

    private $router;

    public function __construct(RedirectionRepository $redirectionRepository, RouterInterface $router)
    {
        $this->redirectionRepository = $redirectionRepository;
        
//        $this->entityManager = $entityManager;
        
    	$this->router = $router;
    }

    public function __invoke(RequestEvent $event)
    {
        if (!$event->isMasterRequest())
        {
            return;
        }


        $fromSlug = $event->getRequest()->getRequestUri();
        $fromRoute = $event->getRequest()->get('_route');
        $fromRouteParams = $event->getRequest()->get('_route_params');

        $redirectionEntitys = $this->redirectionRepository->findByFromUrl($fromSlug);

        if (empty($redirectionEntitys))
        {
            foreach ($this->redirectionRepository->findAll() as $redirection)
            {
                $regexQuotedUrl = $redirection->getFromUrl();
                if(@preg_match($regexQuotedUrl, null) === false)
                {
                    //REGEX PATTERN IS BROKEN
                    //ADDING DELIMETER
                    $regexQuotedUrl = '~'.$regexQuotedUrl.'~';
                }
                //PATTERN IS OK
                if (preg_match($regexQuotedUrl,$fromSlug))
                {
                    $redirectionEntitys[0] = $redirection;
                    break;
                }
            }
        }
        switch (count($redirectionEntitys))
        {
            case 0:
//                DO NOTING NO REDIRECTION
                break;
            case 1:
                $redirectionEntity = $redirectionEntitys[0];
                break;
            default:
//                MORE THAN 2 REDIRECTION.. PROBLEM (LOG THIS ??) EVEN IF PREVENT IN CRUD.

//                $logEntry = new Log();
//
//                $logEntry
//                    ->setMessage($record['message'])
//                    ->setLevel($record['level'])
//                    ->setLevelName($record['level_name'])
//                    ->setChannel($record['channel'])
//                    ->setFormatted($record['formatted']);
                break;
        }

        if (isset($redirectionEntity))
        {
            $response = new RedirectResponse($redirectionEntity->getToUrl(),$redirectionEntity->getType());
            $event->setResponse($response);
        }
    }
}