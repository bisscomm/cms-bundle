<?php

namespace Bci\CmsBundle\EventListener;

use Bci\CmsBundle\Entity\Heading;
use Bci\CmsBundle\Entity\SiteMap;
use Bci\CmsBundle\Entity\UploadableFileCms;
use Bci\CmsBundle\Helper\SiteMapableTrait;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Gedmo\Translatable\Entity\Translation;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use ReflectionClass;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SiteMapListener
{
    const DELETE = 'delete';
    const UPDATE = 'update';
    const CREATE = 'create';
    /**
     * URL Type for the router generated urls.
     */
    const URL_TYPE = Router::NETWORK_PATH;

    private $em;
    private $parameterBag;
    private $router;
    private $siteMapRepository;
    private $translationRepository;
    private $httpClient;

    protected $nbOfchanges;
    protected $enabled;

    public function __construct(EntityManagerInterface $entityManager, ParameterBagInterface $parameterBag, UrlGeneratorInterface $router, HttpClientInterface $httpClient)
    {
        $this->em = $entityManager;
        $this->parameterBag = $parameterBag;
        $this->router = $router;
        $this->siteMapRepository = $this->em->getRepository(SiteMap::class);
        $this->translationRepository = $this->em->getRepository(Translation::class);
        $this->enabled = true;
        $this->httpClient = $httpClient;
        $this->nbOfchanges = 0;
    }


    /**
     * After every events are dispatched, count the numbers of add/update/delete actions.
     * If there is any send http request to Google.
     */
    public function __destruct()
    {
        if ($this->nbOfchanges > 0)
        {
            $this->sendSitemapToGoogle();
        }
    }


    /**
     * @param OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        if ($this->enabled)
        {

            $em = $args->getEntityManager();
            $uow = $em->getUnitOfWork();
            $cmf = $em->getMetadataFactory();

            foreach ($uow->getScheduledEntityInsertions() as $entity)
            {
                if ($this->isSiteMapable($entity))
                {
                    $this->generateSiteMapField(self::CREATE, $entity, $em);
                    $this->nbOfchanges++;
                }
            }
        }
    }

    /**
     * Ping Google to ask for a reindex of the sitemap.
     */
    private function sendSitemapToGoogle()
    {
        try
        {
            $host = $this->router->getContext()->getHost();

            if ($host)
            {
                $response = $this->httpClient->request(
                    'GET',
                    'http://www.google.com/ping?sitemap=https://' . $host . '/sitemap.xml'
                );
                $statusCode = $response->getStatusCode();
            }
        }catch (TransportExceptionInterface $exception)
        {
            // Todo notice user and/or admin
        }
        
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->nbOfchanges++;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        if (!$args->getObject() instanceof SiteMap)
        {
            if ($this->enabled)
            {
                $this->updateSiteMapField(self::UPDATE, $args);
                $this->nbOfchanges++;
            }
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        if (!$args->getObject() instanceof SiteMap /**&& !$args->getObject() instanceof UploadableFileCms**/)
        {
            $this->removeSiteMapField(self::DELETE, $args);
            $this->nbOfchanges++;
        }
    }

    /**
     * @param string $action
     * @param LifecycleEventArgs $args
     */
    private function generateSiteMapField(string $action, $entity, $em)
    {
        //Return if entity is not sitemapable.
        if (!$this->isSiteMapable($entity))
        {
            return;
        }

        $siteMapConfig = $this->parameterBag->get('bci_cms')['sitemap'];

        $reflect = new ReflectionClass($entity);
        $entityClass = $reflect->getShortName();
        if ($entityClass === 'Heading')
        {
            //Pages with no slug return '/' if its not a homepage DISCARD them.
            /**
             * @var $entity Heading
             */
            if ($entity->getSlug() === '/' && !$entity->getPage()->isHome())
            {

            }
            else
            {
                $url =
                    $this->generateUrl('bci_cms_page_show', [
                        'slug' => $entity->getSlug(),
                    ]);

                $siteMapField = new SiteMap();
                $siteMapField
                    ->setUrl($url)
                    ->setSiteMappedEntityId($entity->getId())
                    ->setSiteMappedEntityClass($entityClass)
                    ->setLocale($entity->getLocale())
                    ->setCreatedAt(new \DateTime('now'))
                    ->setUpdatedAt(new \DateTime('now'));

                $this->updateChageSet($em, $siteMapField);
            }

        }
        else
        {
            $entityAsArray = $entity->toArray();
            if (in_array($entityClass, array_keys($siteMapConfig)))
            {
                $routes = $siteMapConfig[$entityClass]['routes'][$entityAsArray['locale']];
            }

            if (!empty($routes))
            {
                foreach ($routes as $routeName => $fields)
                {
                    $routeParams = [];
                    foreach ($fields as $field)
                    {
                        $routeParams[$field] = $entityAsArray[$field];
                    }
                    $url = $this->generateUrl($routeName, $routeParams);
                }

                $siteMapField = new SiteMap();
                $siteMapField
                    ->setUrl($url)
                    ->setLocale($entityAsArray['locale'])
                    ->setSiteMappedEntityId($entity->getId())
                    ->setSiteMappedEntityClass($entityClass)
                    ->setCreatedAt(new \DateTime('now'))
                    ->setUpdatedAt(new \DateTime('now'));

                $this->updateChageSet($em, $siteMapField);
            }
        }
    }

    /**
     * @param string $action
     * @param LifecycleEventArgs $args
     */
    private function updateSiteMapField(string $action, LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        //Return if entity is not sitemapable.
        if (!$this->isSiteMapable($entity))
        {
            if ($this->isNoLongerSiteMapable($entity))
            {
                $siteMapField = $this->siteMapRepository->findOneBy(['siteMappedEntityId' => $entity->getId(), 'locale' => $entity->toArray()['locale']]);
                if ($siteMapField)
                {
                    $this->em->remove($siteMapField);
                    $this->em->flush();
                }
            }
            return;
        }

        $siteMapConfig = $this->parameterBag->get('bci_cms')['sitemap'];

        $reflect = new ReflectionClass($entity);
        $entityClass = $reflect->getShortName();

        if ($entityClass === 'Heading')
        {
            //Pages with no slug return '/' if its not a homepage DISCARD them.
            if ($entity->getSlug() === '/' && !$entity->isHome())
            {

            }
            else
            {
                $siteMapField = $this->siteMapRepository->findOneBy(['siteMappedEntityId' => $entity->getId(), 'locale' => $entity->getLocale()]);
                if (!$siteMapField)
                {
                    $this->generateSiteMapField(self::CREATE, $entity, $args->getObjectManager());
                    $args->getObjectManager()->flush();
                    return;
                }
                $url =
                    $this->generateUrl('bci_cms_page_show', [
                        'slug' => $entity->getSlug(),
                    ]);
                $siteMapField
                    ->setUrl($url)
                    ->setUpdatedAt(new \DateTime('now'));
                $this->enabled = false;
                $this->em->persist($siteMapField);
                $this->em->flush();
                $this->enabled = true;
                return;
            }

        }
        else
        {
            $entityAsArray = $entity->toArray();
            if (in_array($entityClass, array_keys($siteMapConfig)))
            {
                $routes = $siteMapConfig[$entityClass]['routes'][$entityAsArray['locale']];
            }

            if (!empty($routes))
            {
                $siteMapField = $this->siteMapRepository->findOneBy(['siteMappedEntityId' => $entity->getId(), 'locale' => $entityAsArray['locale']]);
                if (!$siteMapField)
                {
                    $this->generateSiteMapField(self::CREATE, $entity, $args->getObjectManager());
                    $args->getObjectManager()->flush();
                    return;
                }
                foreach ($routes as $routeName => $fields)
                {
                    $routeParams = [];
                    foreach ($fields as $field)
                    {
                        $routeParams[$field] = $entityAsArray[$field];
                    }
                    $url = $this->generateUrl($routeName, $routeParams);
                }
                $siteMapField
                    ->setUrl($url)
                    ->setUpdatedAt(new \DateTime('now'));

                $this->enabled = false;

                $args->getObjectManager()->persist($siteMapField);
                $args->getObjectManager()->flush();

                $this->enabled = true;
                return;
            }
        }

    }

    /**
     * @param string $action
     * @param LifecycleEventArgs $args
     */
    private function removeSiteMapField(string $action, LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        //Return if entity is not sitemapable.
        if (!$this->isSiteMapable($entity))
        {
            return;
        }
        $siteMapFields = $this->siteMapRepository->findBy(['siteMappedEntityId' => $entity->getId()]);
        foreach ($siteMapFields as $siteMapField)
        {
            $this->em->remove($siteMapField);
        }
    }

    /**
     * @param $entity
     * @return bool
     */
    private function isSiteMapable($entity)
    {
        $simapable = in_array('isSiteMapable', get_class_methods($entity));

        return $simapable && $entity->isSiteMapable();
    }

    /**
     * @param $entity
     * @return bool
     */
    private function isNoLongerSiteMapable($entity)
    {
        $simapable = in_array('isSiteMapable', get_class_methods($entity));

        return $simapable && !$entity->isSiteMapable();
    }

    /**
     * @param $entity
     * @return mixed
     */
    private function getTranslation($entity)
    {
        $translations = $this->translationRepository->findTranslations($entity);

        return $translations;
    }

    /**
     * @param $routeName
     * @param array $routeParams
     * @return string
     */
    private function generateUrl($routeName, array $routeParams)
    {
//        $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https:' : 'http:';
        $protocol = 'https:';
        return $protocol.$this->router->generate($routeName, $routeParams, self::URL_TYPE);
    }

    /**
     * @param $em
     * @param $entity
     */
    private function updateChageSet($em, $entity)
    {
        $uow = $em->getUnitOfWork();
        $cmf = $em->getMetadataFactory();
        $meta = $cmf->getMetadataFor(get_class($entity));

        $em->persist($entity);
        $uow->computeChangeSet($meta, $entity);
    }

// **********************************THIS DOSENT WORK SINCE IT IS NOT HASH*************************************************
//  IM NOT SHURE WHY, MAYBE BECAUSE IT SET IN service.yaml .. AND NOT USIND ADDER methode of EventListner
    /**
     * @param $args
     */
    private function removeThisListener($args)
    {
//        // Preparation
//        $em = $args->getEntityManager();
//
//        // Remove listener
//        $eventManager = $em->getEventManager();
//        if ($eventManager)
//        {
//
//            foreach ($eventManager->getListeners(Events::onFlush) as $listener)
//            {
//                if (get_class($listener) == SiteMapListener::class)
//                {
//                    $eventManager->removeEventListener(
//                        [Events::onFlush],
//                        $listener
//                    );
//                    break;
//                }
//            }
//        }

        $listenerInst = null;
        $em = $args->getEntityManager(); /* entity manager */
        foreach ($em->getEventManager()->getListeners() as $event => $listeners)
        {
            foreach ($listeners as $hash => $listener)
            {
                if ($listener instanceof SiteMapListener )
                {
                    $listenerInst = $listener;
                    break 2;
                }
            }
        }
        $listenerInst ;
        // then you can remove events you like:
        $evm = $em->getEventManager();
        $evm->removeEventListener(array('onFlush'), $listenerInst);
        $evm->removeEventListener($hash, $listener);
    }

    /**
     * @param $args
     */
    private function addThisListener($args)
    {
        // Preparation
        $em = $args->getEntityManager();

        // Remove listener
        $eventManager = $em->getEventManager();

        // Add listener back again
        if ($eventManager)
        {
            $eventManager->addEventListener(
                [Events::onFlush],
                $this
            );
        }
    }

}