<?php
namespace Bci\CmsBundle\EventSubscriber\FormSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class DateTimeFormatSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SUBMIT   => 'onPreSubmit',
        ];
    }
    

    public function onPreSubmit(FormEvent $event): void
    {
        if ($event->getData()['startAt'] && isset($event->getData()['startAt']))
        {
            $data = $event->getData();
            $stringDate = $event->getData()['startAt'];
            $startAt = \DateTime::createFromFormat('Y-m-d H:i:s', $stringDate)->format('Y-m-d H:i:s');
            $data['startAt'] = $startAt;
            $event->setData($data);
        }
        
        if ($event->getData()['endAt'] && isset($event->getData()['endAt']))
        {
            $data = $event->getData();
            $stringDate = $event->getData()['endAt'];
            $endAt = \DateTime::createFromFormat('Y-m-d H:i:s', $stringDate)->format('Y-m-d H:i:s');
            $data['endAt'] = $endAt;
            $event->setData($data);
        }

    }
}

