<?php

namespace Bci\CmsBundle\EventSubscriber;

use App\Entity\User;
use Bci\CmsBundle\Enum\FlashType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;

class PasswordChangeSubscriber implements EventSubscriberInterface
{
    private $security;
    private $router;
    private $session;

    private const MSG_EN = '<b>You must change your password to be able to continue proceeding.</b>';
    private const MSG_FR = '<b>Vous devez changer votre mot de passe pour pouvoir poursuivre vos opérations.</b>';

    public function __construct(Security $security, RouterInterface $router, SessionInterface $session)
    {
        $this->security = $security;
        $this->router = $router;
        $this->session = $session;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [
                ['forcePasswordChange', 0]
            ],
        ];
    }

    public function forcePasswordChange(RequestEvent $event): void
    {
        // only deal with the main request, disregard subrequests
        if (!$event->isMasterRequest()) {
            return;
        }

        $user = $this->security->getUser();
        // if you do not have a valid user, it means it's not an authenticated request, so it's not our concern
        if (!$user instanceof User) {
            return;
        }

        // if it's not their first login, and they do not need to change their password, move on
        if (!$user->getResetPassword()) {
            return;
        }

        // if we get here, it means we need to redirect them to the password change view.
        $redirectTo = $this->router->generate('bci_cms_edit_profile',['id'=>$user->getId()]);

        if ($event->getRequest()->getRequestUri() != $redirectTo){
            $this->session->getFlashBag()->add(FlashType::TYPE_WARNING,$event->getRequest()->getLocale() == 'fr' ? self::MSG_FR : self::MSG_EN);
            $event->setResponse(new RedirectResponse($redirectTo));
        }
        return;
    }
}