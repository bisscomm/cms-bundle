<?php

namespace Bci\CmsBundle\Form;


use Bci\CmsBundle\Entity\Article;
use Bci\CmsBundle\Entity\ArticleCategory;
use Bci\CmsBundle\Entity\Blog;
use Bci\CmsBundle\Form\Type\PreviewType;
use Bci\CmsBundle\Form\Type\SiteMapableType;
use Bci\CmsBundle\Repository\ArticleCategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $listener = function (FormEvent $event) {
//           $form = $event->getForm();
//           $data = $event->getData();
//        };

        $builder
//            ->addEventListener(FormEvents::SUBMIT, $listener)
            ->add('preview', PreviewType::class)
            ->add('title', TextType::class,  [
                'label'              => 'bci.cms.blog.article.title',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col'   => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('subtitle', TextType::class,  [
                'label'              => 'bci.cms.blog.article.subtitle',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col'   => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('contentSummary',TextareaType::class,  [
                'label'              => 'bci.cms.blog.article.contentSummary',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col'   => 'col-12',
                    'labelIn' => true,
                    'data-tinyMaxLength' => 200
                ],
                'required' => false,
            ])
            ->add('content',TextareaType::class,  [
                'label'              => 'bci.cms.blog.article.content',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col'   => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('publishedAt',DateTimeType::class, [
                'label' => 'bci.cms.job.start_at',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'datetimepicker',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
                'widget' => 'single_text',
                'html5' => false
            ])
            ->add('disabled',CheckboxType::class,[
                'label' => 'bci.cms.blog.article.disabled',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col'   => 'col-12',
                    'labelIn' => true,
                ],
                'required' => false,
            ])
            ->add('image', UploadableFileCmsType::class,[
                'label' => 'bci.cms.blog.article.image',
                'translation_domain' => 'cms_bundle',
                'required' => false,
            ])
            ->add('categories', EntityType::class, [
                'class' => ArticleCategory::class,
                'choice_label' => function(ArticleCategory $articleCategory){
                    return $articleCategory->getTitle();
                },
                'choices' => $builder->getData()->getBlog()->getArticleCategories(),
                'multiple' => true,
                'expanded' => true,
                'label'              => 'bci.cms.blog.article.category.categories',
                'translation_domain' => 'cms_bundle',
                'by_reference' => false,
                'required' => true,
                'label_attr' => ['deletable'],
            ])
            ->add('metas', CollectionType::class, [
                'entry_type' => MetaType::class,
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'allow_delete' => true,
                'required' => false,
                'by_reference' => false
            ])
            ->add('siteMapable', SiteMapableType::class);
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
