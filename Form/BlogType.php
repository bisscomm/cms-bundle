<?php

namespace Bci\CmsBundle\Form;

use Bci\CmsBundle\Entity\Blog;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BlogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,  [
                'label'              => 'bci.cms.blog.title',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col'   => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
//            ->add('articles', CollectionType::class, [
//                'entry_type' => ArticleType::class,
//                'label'              => 'bci.cms.article.article',
//                'translation_domain' => 'cms_bundle',
//                'allow_add' => true,
//                'allow_delete' => true,
//                'delete_empty' => true,
//            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Blog::class,
        ]);
    }
}
