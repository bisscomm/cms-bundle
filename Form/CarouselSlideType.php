<?php

namespace Bci\CmsBundle\Form;

use Bci\CmsBundle\Entity\CarouselSlide;
use Bci\CmsBundle\EventSubscriber\FormSubscriber\DateTimeFormatSubscriber;
use Bci\CmsBundle\Form\UploadableFileCmsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class CarouselSlideType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $datePlaceHolder = [
            'year' => 'bci.cms.carousel.slide.date.year',
            'month' => 'bci.cms.carousel.slide.date.month',
            'day' => 'bci.cms.carousel.slide.date.day',
            'hour' => 'bci.cms.carousel.slide.date.hour',
            'minute' => 'bci.cms.carousel.slide.date.min',
        ];

        $builder
            ->add('startAt',DateTimeType::class, [
                'label' => 'bci.cms.carousel.slide.start_at',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'datetimepicker',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
                'widget' => 'single_text',
                'html5' => false
            ])
            ->add('endAt',DateTimeType::class, [
                'label' => 'bci.cms.carousel.slide.end_at',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'datetimepicker',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
                'widget' => 'single_text',
                'html5' => false
            ])
            ->add('status', ChoiceType::class, [
                'label' => 'bci.cms.carousel.slide.status.status',
                'placeholder' => 'bci.cms.carousel.slide.status.status',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col'   => 'col-12',
                    'labelIn' => true
                ],
                'choices' => [
                    'bci.cms.carousel.slide.status.active' => true,
                    'bci.cms.carousel.slide.status.archive' => false
                ],
                'required' => true,
                'expanded' => false,
                'multiple' => false
            ])

            ->add('title', TextareaType::class, [
                'label' => 'bci.cms.carousel.slide.title',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('url', TextType::class, [
                'label' => 'bci.cms.carousel.slide.url',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('buttonTitle', TextType::class, [
                'label' => 'bci.cms.carousel.slide.btnTitle',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('image', UploadableFileCmsType::class, [
                'label' => 'bci.cms.carousel.slide.image',
                'translation_domain' => 'cms_bundle',
                'required' => false,
            ])
            ->add('imageTitle', TextType::class, [
                'label' => 'bci.cms.carousel.slide.imageTitle',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('imageAlt', TextType::class, [
                'label' => 'bci.cms.carousel.slide.imageAlt',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('text', TextareaType::class, [
                'label' => 'bci.cms.carousel.slide.text',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->addEventSubscriber(new DateTimeFormatSubscriber())
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CarouselSlide::class,
        ]);
    }
}
