<?php

namespace Bci\CmsBundle\Form;

use Bci\CmsBundle\Entity\FormItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('presentInPostedData', ChoiceType::class, [
                'label' => 'bci.cms.formItem.presentInPostedData',
                'translation_domain' => 'cms_bundle',
                'required' => false,
                'choices' => [
                    'bci.cms.formItem.notPresentInPostedData' => null,
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                ],
                'attr' => [
                    'class' => 'postedDataOrder',
                    'onChange' => 'cms.form.onChangePostedDataOrder()'
                ],
                'placeholder' => false
            ])
            ->add('label', TextType::class, [
                'label' => 'bci.cms.formItem.label',
                'translation_domain' => 'cms_bundle',
                'required' => false
            ])
            ->add('help', TextType::class, [
                'label' => 'bci.cms.formItem.help',
                'translation_domain' => 'cms_bundle',
                'required' => false
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'bci.cms.formItem.type',
                'translation_domain' => 'cms_bundle',
                'choices' => [
                    'bci.cms.formItem.types.text' => 'text',
                    'bci.cms.formItem.types.email' => 'email',
                    'bci.cms.formItem.types.date' => 'date',
                    'bci.cms.formItem.types.textarea' => 'textarea',
                    'bci.cms.formItem.types.select' => 'select',
                    'bci.cms.formItem.types.radio' => 'radio',
                    'bci.cms.formItem.types.checkbox' => 'checkbox',
                    'bci.cms.formItem.types.file' => 'file',
                ]
            ])
            ->add('config', TextareaType::class, [
                'label' => 'bci.cms.formItem.config',
                'translation_domain' => 'cms_bundle',
                'required' => false,
                'attr' => [
                    'rows' => '8',
                ],

            ])
            ->add('required', CheckboxType::class, [
                'label' => 'bci.cms.formItem.required',
                'translation_domain' => 'cms_bundle',
                'required' => false,
            ])
            ->add('locale', HiddenType::class, [
                'empty_data' => $options['locale']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FormItem::class,
            'locale' => 'fr'
        ]);
    }
}
