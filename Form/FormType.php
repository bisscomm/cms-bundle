<?php

namespace Bci\CmsBundle\Form;

use Bci\CmsBundle\Entity\Form;
use Bci\CmsBundle\Entity\FormItem;
use Bci\CmsBundle\Entity\Page;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'bci.cms.form.name',
                'translation_domain' => 'cms_bundle',
                'required' => true
            ])
            ->add('page', EntityType::class, [
                'label' => 'bci.cms.form.page',
                'translation_domain' => 'cms_bundle',
                'class' => Page::class,
                'choice_label' => 'name'
            ])
            ->add('email', TextType::class, [
                'label' => 'bci.cms.form.email',
                'translation_domain' => 'cms_bundle',
                'required' => true
            ])
            ->add('htmlId', TextType::class, [
                'label' => 'bci.cms.form.htmlId',
                'translation_domain' => 'cms_bundle',
                'required' => false,
                'attr' => [
                    'placeholder' => 'bci.cms.form.htmlIdPlaceholder'
                ]
            ])
            ->add('formTranslations', CollectionType::class, [
                'entry_type' => FormTranslationsType::class,
                'required' => true,
                'allow_delete' => 'false'
            ])
            ->add('items', CollectionType::class, [
                'label' => 'bci.cms.form.items',
                'translation_domain' => 'cms_bundle',
                'entry_options' => [
                    'locale' => $options['locale']
                ],
                'entry_type' => FormItemType::class,
                'allow_add' => true,
                'allow_delete' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Form::class,
            'allow_extra_fields' => true,
            'locale' => 'fr'
        ]);
    }
}
