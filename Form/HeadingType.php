<?php

namespace Bci\CmsBundle\Form;

use Bci\CmsBundle\Entity\Block;
use Bci\CmsBundle\Entity\Heading;
use Bci\CmsBundle\Entity\Menu;
use Bci\CmsBundle\Form\Type\SiteMapableType;
use Bci\CmsBundle\Model\Field;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HeadingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label'              => 'bci.cms.heading.title',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col'   => 'col-12',
                    'labelIn' => true
                ]
            ])
            ->add('siteMapable', SiteMapableType::class)
            ->add('slug', TextType::class, [
                'label'              => 'bci.cms.heading.slug',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col'   => 'col-12',
                    'labelIn' => true
                ]
            ])
            ->add('menu', EntityType::class, [
                'label'              => 'bci.cms.page.menu',
                'translation_domain' => 'cms_bundle',
                'class'              => Menu::class,
                'choice_label'       => 'title',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m')
                        ->orderBy('m.title', 'ASC');
                },
                'multiple'           => false,
                'expanded'           => false,
                'placeholder'        => 'bci.cms.page.chooseMenu',
                'required'           => false
            ])
            ->add('other', TextareaType::class, [
                'label'              => 'bci.cms.heading.other',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col'   => 'col-12',
                    'labelIn' => true
                ],
                'required'          => false
            ])
            ->add('metas', CollectionType::class, [
                'entry_type' => MetaType::class,
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'allow_delete' => true,
                'required' => false,
            ])
            ->add('draft', CheckboxType::class, [
                'label'              => 'bci.cms.heading.draft',
                'translation_domain' => 'cms_bundle',
                'required'           => false
            ])
            ->add('publishedAt', DateTimeType::class, [
                'label'              => 'bci.cms.heading.publishedAt',
                'translation_domain' => 'cms_bundle',
                'date_widget' => 'single_text',
                'time_widget' => 'single_text',
                'attr' => [
                    'labelIn' => true
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Heading::class,
        ]);
    }
}
