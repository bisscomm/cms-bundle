<?php

namespace Bci\CmsBundle\Form;

use Bci\CmsBundle\Entity\Job;
use Bci\CmsBundle\Entity\JobCategory;
use Bci\CmsBundle\Entity\Website;
use Bci\CmsBundle\EventSubscriber\PreviewSubscriber;
use Bci\CmsBundle\Form\Type\AddressType;
use Bci\CmsBundle\Form\Type\GoogleJobType;
use Bci\CmsBundle\Form\Type\PreviewType;
use Bci\CmsBundle\Form\Type\SiteMapableType;
use Bci\CmsBundle\Repository\WebsiteRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobType extends AbstractType
{
    private $websiteRepository;
    
    public function __construct(WebsiteRepository $websiteRepository, ParameterBagInterface $parameterBag)
    {
        $this->websiteRepository = $websiteRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('preview', PreviewType::class)
            ->add('googleJob', GoogleJobType::class,[
                'data_class' => Job::class
            ])
            ->add('address', AddressType::class,[
                'data_class' => Job::class
            ])
            ->add('siteMapable', SiteMapableType::class)
            ->add('metas', CollectionType::class, [
                'entry_type' => MetaType::class,
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'allow_delete' => true,
                'required' => false,
                'by_reference' => true
            ])
            ->add('title', TextType::class, [
                'label' => 'bci.cms.job.title',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => true,
            ])
            ->add('description', TextareaType::class, [
                'label' => 'bci.cms.job.description',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('summary', TextareaType::class, [
                'label' => 'bci.cms.job.summary',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('onlyShowInForm', CheckboxType::class, [
                'label' => 'bci.cms.job.onlyInForm.title',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'label_attr' => [
                    'class' => '',
                ],
                'required' => false,
            ])
            ->add('status', ChoiceType::class, [
                'label' => 'bci.cms.job.status.status',
                'placeholder' => 'bci.cms.job.status.status',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'choices' => [
                    'bci.cms.job.status.active' => true,
                    'bci.cms.job.status.archive' => false
                ],
                'required' => true,
                'expanded' => false,
                'multiple' => false
            ])
            ->add('startAt',DateTimeType::class, [
                'label' => 'bci.cms.job.start_at',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'datetimepicker',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
                'widget' => 'single_text',
                'html5' => false
            ])
            ->add('endAt',DateTimeType::class, [
                'label' => 'bci.cms.job.end_at',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'datetimepicker',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
                'widget' => 'single_text',
                'html5' => false
            ])
            ->add('categories', EntityType::class, [
                'class' => JobCategory::class,
                'choice_label' => function(JobCategory $jobCategory){
                    return $jobCategory->getTitle();
                },
                //'choices' => $builder->getData()->getArticleCategories(),
                'multiple' => true,
                'expanded' => true,
                'label'              => 'bci.cms.blog.article.category.categories',
                'translation_domain' => 'cms_bundle',
                'by_reference' => false,
                'required' => true,
                // This is used in builder_form_layout.html.twig to add
                // the right javascript delete handler at the end of
                // every category checkbox.               
                'label_attr' => ['deletable_job_category'],
            ])
        ;

        if (count($this->websiteRepository->findAll()) > 0)
        {

            $builder->add('websites', EntityType::class, [
                'class' => Website::class,
                'label' => '',
                'choice_label' => 'name',
                'placeholder' => '',
                'translation_domain' => '',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'multiple' => true,
                'expanded' => false,
                'by_reference' => false,
                'required' => false
            ]);
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Job::class,
        ]);
    }
}
