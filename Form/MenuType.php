<?php

namespace Bci\CmsBundle\Form;

use Bci\CmsBundle\Entity\Menu;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MenuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label'              => 'bci.cms.menu.title',
                'translation_domain' => 'cms_bundle',
            ])
            /*->add('description', TextareaType::class, [
                'label'              => 'bci.cms.menu.description',
                'translation_domain' => 'cms_bundle',
            ])*/
            ->add('menuItems', TextareaType::class, [
                'label'              => 'bci.cms.menu.serialized',
                'translation_domain' => 'cms_bundle',
                'required'           => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Menu::class,
            'allow_extra_fields' => true,
        ]);
    }
}
