<?php

namespace Bci\CmsBundle\Form;

use Bci\CmsBundle\Entity\Meta;
use Bci\CmsBundle\Entity\UploadableFileCms;
use Bci\CmsBundle\Service\MetaService;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\ChoiceList\ArrayChoiceList;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Yaml\Yaml;

class MetaType extends AbstractType
{
    private $metaservice;

    public function __construct(MetaService $metaservice)
    {
        $this->metaservice = $metaservice;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('name', TextType::class, [
                'label' => 'bci.cms.meta.name',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ]
            ])
            ->add('property', TextType::class, [
                'label' => 'bci.cms.meta.property',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ]
            ])
            ->add('content', TextareaType::class, [
                'label' => 'bci.cms.meta.content',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ]
            ])
            ->add('image', UploadableFileCmsType::class,[
                'label' => 'image.choose',
                'translation_domain' => 'image',
                'required' => false,
            ])
            ->add('accessor', ChoiceType::class, [
                'choices' => $this->getMetasTagsChoices(),
                'choice_value' => function ($metaTag)
                {
                    if ($metaTag)
                    {
                        return is_string($metaTag) ? $metaTag : $metaTag->getAccessor();
                    }
                },
                'choice_label' => function (Meta $meta)
                {
                    return $meta ? strtoupper($meta->getDisplayName()) : '';
                },
                'choice_attr' => function (Meta $meta)
                {
                    if ($meta->getType() === 'OPENGRAPH')
                    {
                        return [
                            'data-type' => $meta->getType(),
                            'data-property' => $meta->getProperty(),
                            'data-display-name' => $meta ? strtoupper($meta->getDisplayName()) : ''
                        ];
                    }
                    return [
                        'data-type' => $meta->getType(),
                        'data-name' => $meta->getName(),
                        'data-display-name' => $meta ? strtoupper($meta->getDisplayName()) : ''
                    ];
                },
                'group_by' => function (Meta $meta)
                {
                    return $meta->getType();
                },
                'label' => 'bci.cms.meta.meta',
                'placeholder' => 'bci.cms.meta.choose',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'metaSelect form-control selectpicker',
                    'labelIn' => false,
                    'onChange' => 'app.setUpMetaSelect(this);'
                ],
                'required' => true,
                'help' => "",
                'help_attr' => [
                ],
                'by_reference' => false
            ])
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event)
            {
                /**
                 * @var Meta $data
                 */
                $data = $event->getData();
                if ($data)
                {
                    /**
                     * @var Meta $metaToMerge
                     */
                    $metaToMerge = $data->getAccessor();
                    $data
                        ->setType($metaToMerge->getType())
                        ->setDisplayName($metaToMerge->getDisplayName())
                        ->setAccessor($metaToMerge->getAccessor());

                    $event->setData($data);
                }
            });
    }

    private function getMetasTagsChoices()
    {
        return $this->metaservice->generateMetasTagsFromYaml();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Meta::class,
        ]);
    }
}
