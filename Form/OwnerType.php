<?php

namespace Bci\CmsBundle\Form;

use Bci\CmsBundle\Entity\Owner;
use Bci\CmsBundle\Form\Type\AddressType;
use Bci\CmsBundle\Form\UploadableFileCmsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OwnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('address', AddressType::class,[
                'data_class' => Owner::class
            ])
            ->add('logo', TranslatableUploadableFileCmsType::class,[
                'label' => 'bci.cms.owner.logo',
                'translation_domain' => 'cms_bundle',
                'required' => false,
                'by_reference' => true
            ])
            ->add('name', TextType::class, [
                'label' => 'bci.cms.owner.name',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('legalName', TextType::class, [
                'label' => 'bci.cms.owner.legal_name',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('phoneNumber', TextType::class, [
                'label' => 'bci.cms.owner.phone_number',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('fbLink', TextType::class, [
                'label' => 'bci.cms.owner.social_link.fb',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('twitLink', TextType::class, [
                'label' => 'bci.cms.owner.social_link.twitter',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('instaLink', TextType::class, [
                'label' => 'bci.cms.owner.social_link.insta',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('lkLink', TextType::class, [
                'label' => 'bci.cms.owner.social_link.linkedIn',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('email', TextType::class, [
                'label' => 'bci.cms.owner.email',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('timeTable', TextareaType::class, [
                'label' => 'bci.cms.owner.timetable',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Owner::class,
        ]);
    }
}
