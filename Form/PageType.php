<?php

namespace Bci\CmsBundle\Form;

use Bci\CmsBundle\Entity\Page;
use Bci\CmsBundle\Entity\Template;
use Bci\CmsBundle\Form\Type\SiteMapableType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label'              => 'bci.cms.page.name',
                'translation_domain' => 'cms_bundle',
            ])
            ->add('isHome', ChoiceType::class, [
                'label'              => 'bci.cms.page.isHome.label',
                'translation_domain' => 'cms_bundle',
                'choices' => [
                    'bci.cms.page.isHome.yes' => true,
                    'bci.cms.page.isHome.no' => false
                ]
            ])
            ->add('template', EntityType::class, [
                'label'              => 'bci.cms.page.template',
                'translation_domain' => 'cms_bundle',
                'class'              => Template::class,
                'choice_label'       => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.name', 'ASC');
                },
                'multiple'           => false,
                'expanded'           => false,
                'placeholder'        => 'bci.cms.page.chooseTemplate',
            ])
            ->add('headings', CollectionType::class, [
                'entry_type' => HeadingType::class,
                'entry_options' => array('label' => false),
                'required' => true,
                'allow_delete' => 'false'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Page::class,
            'allow_extra_fields' => true,
        ]);
    }
}
