<?php

namespace Bci\CmsBundle\Form;

use Bci\CmsBundle\Entity\Redirection;
use Bci\CmsBundle\Enum\FromUrlType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RedirectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('fromUrl', TextType::class, [
                'label' => 'bci.cms.redirection.fromUrl.title',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true,
                    'placeholder' => 'Regex et/ou Url'
                ],
                'required' => true,
            ])
            ->add('toUrl', TextType::class, [
                'label' => 'bci.cms.redirection.toUrl',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true,
                    'placeholder' => ''
                ],
                'required' => true,
            ])
            ->add('type', ChoiceType::class, [
                'label'              => 'bci.cms.redirection.type',
                'translation_domain' => 'cms_bundle',
                'choices'  =>  array_flip(\Bci\CmsBundle\Enum\RedirectionType::getRedirectionTypeChoices()),
                'attr' => [
                    'class' => 'form-control',
                    'col'   => 'col-12',
                    'labelIn' => true
                ],
                'required' => true,
//                'placeholder' => 'bci.cms.redirection.type',
                'empty_data' => '301'
            ])
            ->add('browser', ChoiceType::class, [
                'label'              => 'bci.cms.redirection.browser',
                'translation_domain' => 'cms_bundle',
                'choices'  =>  [],
                'multiple' => true,
                'attr' => [
                    'class' => 'form-control',
                    'col'   => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
                'placeholder'        => 'bci.cms.redirection.placeholder',
            ])
            ->add('cookie', TextType::class, [
                'label' => 'bci.cms.redirection.cookie',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('ipAddress', TextType::class, [
                'label' => 'bci.cms.redirection.ipAddress',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Redirection::class,
        ]);
    }
}
