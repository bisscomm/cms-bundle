<?php

namespace Bci\CmsBundle\Form;

use Bci\CmsBundle\Entity\ScriptTag;
use Bci\CmsBundle\Enum\ScriptTagPosition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ScriptTagType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'bci.cms.scriptTag.title',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true,
                    'placeholder' => 'bci.cms.scriptTag.exemple.title'
                ],
                'required' => true
            ])
            ->add('presentInStaging',CheckboxType::class,[
                'label' => false,
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                ],
                'required' => false
            ])
            ->add('position', ChoiceType::class, [
                'label'              => 'bci.cms.scriptTag.position',
                'translation_domain' => 'cms_bundle',
                'placeholder' => 'bci.cms.scriptTag.choice',
                'choices'  =>  ScriptTagPosition::getPositionTypeChoices()
                ,
                'attr' => [
                    'class' => 'form-control',
                    'col'   => 'col-12',
                    'labelIn' => true
                ],
                'required' => true

            ])
            ->add('script',TextareaType::class,  [
                'label'              => 'bci.cms.scriptTag.script',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col'   => 'col-12',
                    'labelIn' => true,
                    'placeholder' => 'bci.cms.scriptTag.exemple.textarea'
                ],
                'required' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ScriptTag::class,
        ]);
    }
}
