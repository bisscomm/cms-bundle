<?php

namespace Bci\CmsBundle\Form;

use Bci\CmsBundle\Entity\TranslatableUploadableFileCms;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class TranslatableUploadableFileCmsType extends UploadableFileCmsType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TranslatableUploadableFileCms::class,
        ]);
    }
}
