<?php

namespace Bci\CmsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GoogleJobType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $employementTypeChoices = [
            'bci.cms.job.google_job.employmentType.FULL_TIME' => "FULL_TIME",
            'bci.cms.job.google_job.employmentType.PART_TIME' => "PART_TIME",
            'bci.cms.job.google_job.employmentType.CONTRACTOR' => "CONTRACTOR",
            'bci.cms.job.google_job.employmentType.TEMPORARY' => "TEMPORARY",
            'bci.cms.job.google_job.employmentType.INTERN' => "INTERN",
            'bci.cms.job.google_job.employmentType.VOLUNTEER' => "VOLUNTEER",
            'bci.cms.job.google_job.employmentType.PER_DIEM' => "PER_DIEM",
            'bci.cms.job.google_job.employmentType.OTHER' => "OTHER"
        ];

        $boolChoices = [
            'bci.cms.general.action.boolean.yes' => true,
            'bci.cms.general.action.boolean.no' => false
        ];

        $jobSalaryCurrency = [
            'CAD' => "CAD",
            'USD' => "USD"
        ];
        
        $builder
            ->add('activated', ChoiceType::class, [
                'label' => 'bci.cms.job.google_job.activated',
                'translation_domain' => 'cms_bundle',
                'placeholder' => false,
                'choices' => $boolChoices,
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
                'expanded' => false,
                'multiple' => false
            ])
            ->add('industry', TextType::class, [
                'label' => 'bci.cms.job.google_job.industry',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('hoursSalary', TextType::class, [
                'label' => 'bci.cms.job.google_job.hours_salary',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('totalJobOpenings', NumberType::class, [
                'label' => 'bci.cms.job.google_job.totalJobOpenings',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('salaryCurrency', ChoiceType::class, [
                'label' => 'bci.cms.job.google_job.salaryCurrency',
                'translation_domain' => 'cms_bundle',
                'placeholder' => 'bci.cms.general.action.choose',
                'choices' => $jobSalaryCurrency,
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true,
                ],
                'required' => false,
                'expanded' => false,
                'multiple' => false
            ])
            ->add('jobBenefits', TextType::class, [
                'label' => 'bci.cms.job.google_job.jobBenefits',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('skills', TextType::class, [
                'label' => 'bci.cms.job.google_job.skills',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('educationRequirements', TextType::class, [
                'label' => 'bci.cms.job.google_job.educationRequirements',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('experienceRequirements', TextType::class, [
                'label' => 'bci.cms.job.google_job.experienceRequirements',
                'translation_domain' => 'cms_bundle',
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
            ])
            ->add('employmentType', ChoiceType::class, [
                'label' => 'bci.cms.job.google_job.employmentType.title',
                'translation_domain' => 'cms_bundle',
                'placeholder' => 'bci.cms.general.action.choose',
                'choices' => $employementTypeChoices,
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true,
                    'placeholder' => 'bci.cms.general.action.choose',
                ],
                'required' => false,
                'expanded' => false,
                'multiple' => true
            ])
            ->add('jobLocationType', ChoiceType::class, [
                'label' => 'bci.cms.job.google_job.jobLocationType',
                'translation_domain' => 'cms_bundle',
                'placeholder' => 'bci.cms.general.action.choose',
                'choices' => $boolChoices,
                'attr' => [
                    'class' => 'form-control',
                    'col' => 'col-12',
                    'labelIn' => true
                ],
                'required' => false,
                'expanded' => false,
                'multiple' => false
            ]);

//        $builder->addEventListener( FormEvents::PRE_SUBMIT, function( FormEvent $event ) {
//            // Remove added place holder
//            $data = $event->getData();
//            if( ($key = array_search( 'bci.cms.general.action.choose', $data['employmentType'] ) ) !== false ) {
//                unset( $data['employmentType'][$key] );
//                $event->setData( $data );
//            }
//        } );
    }

//    public function finishView( FormView $view, FormInterface $form, array $options) {
//        $placeHolder = new ChoiceView( null, 'Place holder', 'bci.cms.general.action.choose' );
//        array_unshift( $view->children['employmentType']->vars['choices'], $placeHolder );
//    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'inherit_data' => true,
        ]);
    }
}
