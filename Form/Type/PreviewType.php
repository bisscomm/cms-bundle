<?php

namespace Bci\CmsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\SubmitButtonTypeInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

//You have to implement the SubmitButtonTypeInterface so it uses SubmitButtonBuilder instead of the regular FormBuilder.
// If not error : Neither the property " .." nor one of the methods "get..()", "..()",
// Occurs
class PreviewType extends AbstractType implements SubmitButtonTypeInterface
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'label' => 'bci.cms.general.action.preview.icon',
            'translation_domain' => 'cms_bundle',
            'value' => 'allo',
            'attr' => [
                //Force the oppenig in a new widow and is not blocked by popup blocker.
                'formtarget'=> "_blank",
                'data-ice' => 'app.previewJob',
                'style' => 'padding-left: .5rem; padding-right:.5rem',
                'rel' => 'tooltip',
                'title' => 'bci.cms.general.action.preview.title'
            ],
        ]);
    }

    public function getParent()
    {
        return SubmitType::class;
    }
}