<?php
namespace Bci\CmsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiteMapableType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'label'              => 'bci.cms.page.sitemapable.label',
            'translation_domain' => 'cms_bundle',
            'choices' => [
                'bci.cms.page.sitemapable.yes' => true,
                'bci.cms.page.sitemapable.no' => false
            ],
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}