<?php

namespace Bci\CmsBundle\Form;

use Bci\CmsBundle\Entity\UploadableFileCms;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class UploadableFileCmsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tempFile', FileType::class, [
                'translation_domain' => 'image',
//                'mapped' => true,
                'required' => false,
                'multiple' => false,
                'label' => false,
                ])
            ->add('toDelete',HiddenType::class, [
//                'mapped' => false,
                'attr' => [
                    'class' => 'toDelete',
                    'data-to-delete' => false
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UploadableFileCms::class,
        ]);
    }
}
