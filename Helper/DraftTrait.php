<?php

namespace Bci\CmsBundle\Helper;


Trait DraftTrait
{
    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $draft;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $publishedat;

    /**
     * @return bool
     */
    public function isDraft(): bool
    {
        return $this->draft?:false;
    }

    /**
     * @param bool $draft
     * @return mixed
     */
    public function setDraft(bool $draft)
    {
        $this->draft = $draft;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublishedat(): \DateTime
    {
        return $this->publishedat?: new \DateTime();
    }

    /**
     * @param \DateTime $publishedat
     * @return mixed
     */
    public function setPublishedat(\DateTime $publishedat = null)
    {
        $this->publishedat = $publishedat;
        return $this;
    }

    public function canDisplay()
    {
        return !$this->isDraft() && $this->getPublishedat() <= new \DateTime();
    }
}