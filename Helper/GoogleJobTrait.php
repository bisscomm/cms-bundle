<?php

namespace Bci\CmsBundle\Helper;


Trait GoogleJobTrait
{
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $activated = false;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $industry;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $salaryCurrency;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     */
    private $jobBenefits;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     */
    private $skills;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     */
    private $educationRequirements;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", nullable=true)
     */
    private $experienceRequirements;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $employmentType;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $jobLocationType;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $totalJobOpenings;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $hoursSalary;

    /**
     * @return bool
     */
    public function isActivated(): bool
    {
        return $this->activated;
    }

    /**
     * @param bool $activated
     * @return GoogleJobTrait
     */
    public function setActivated(bool $activated)
    {
        $this->activated = $activated;
        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * @param mixed $industry
     * @return GoogleJobTrait
     */
    public function setIndustry($industry)
    {
        $this->industry = $industry;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSalaryCurrency()
    {
        return $this->salaryCurrency;
    }

    /**
     * @param mixed $salaryCurrency
     * @return GoogleJobTrait
     */
    public function setSalaryCurrency($salaryCurrency)
    {
        $this->salaryCurrency = $salaryCurrency;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getJobBenefits()
    {
        return $this->jobBenefits;
    }

    /**
     * @param mixed $jobBenefits
     * @return GoogleJobTrait
     */
    public function setJobBenefits($jobBenefits)
    {
        $this->jobBenefits = $jobBenefits;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param mixed $skills
     * @return GoogleJobTrait
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEducationRequirements()
    {
        return $this->educationRequirements;
    }

    /**
     * @param mixed $educationRequirements
     * @return GoogleJobTrait
     */
    public function setEducationRequirements($educationRequirements)
    {
        $this->educationRequirements = $educationRequirements;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExperienceRequirements()
    {
        return $this->experienceRequirements;
    }

    /**
     * @param mixed $experienceRequirements
     * @return GoogleJobTrait
     */
    public function setExperienceRequirements($experienceRequirements)
    {
        $this->experienceRequirements = $experienceRequirements;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmploymentType()
    {
        return $this->employmentType;
    }

    /**
     * @param mixed $employmentType
     * @return GoogleJobTrait
     */
    public function setEmploymentType($employmentType)
    {
        $this->employmentType = $employmentType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getJobLocationType()
    {
        return $this->jobLocationType;
    }

    /**
     * @param mixed $jobLocationType
     * @return GoogleJobTrait
     */
    public function setJobLocationType($jobLocationType)
    {
        $this->jobLocationType = $jobLocationType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalJobOpenings()
    {
        return $this->totalJobOpenings;
    }

    /**
     * @param mixed $totalJobOpenings
     * @return GoogleJobTrait
     */
    public function setTotalJobOpenings($totalJobOpenings)
    {
        $this->totalJobOpenings = $totalJobOpenings;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHoursSalary()
    {
        return $this->hoursSalary;
    }

    /**
     * @param mixed $hoursSalary
     * @return GoogleJobTrait
     */
    public function setHoursSalary($hoursSalary)
    {
        $this->hoursSalary = $hoursSalary;
        return $this;
    }


}
