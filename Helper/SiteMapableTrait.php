<?php

namespace Bci\CmsBundle\Helper;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Trait SiteMapableTrait
 * @package Bci\CmsBundle\Interfaces
 */
trait SiteMapableTrait
{
    /**
     * @Gedmo\Translatable
     * @var bool
     * @ORM\Column(type="boolean", nullable=true, options={"default": true})
     */
    private $siteMapable;


    /**
     * @return bool
     */
    public function isSiteMapable(): ?bool
    {
        return $this->siteMapable;
    }

    /**
     * @param bool $siteMapable
     */
    public function setSiteMapable(bool $siteMapable)
    {
        $this->siteMapable = $siteMapable;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function timeStamp()
    {
        if (!$this->getCreatedAt())
        {
            $this->setCreatedAt(new \DateTime('now'));
        }
        $this->setUpdatedAt(new \DateTime('now'));
    }

    /**
     * Is used to return the siteMapableEntity as an array.
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }

}