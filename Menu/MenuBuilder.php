<?php

namespace Bci\CmsBundle\Menu;

use Knp\Menu\ItemInterface;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;


class MenuBuilder
    implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $factory;
    private $request;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory, ContainerInterface $container)
    {
        $this->factory = $factory;
        $this->setContainer($container);
    }

    public function createSidebarMenu(RequestStack $requestStack)
    {
        $this->request = $requestStack;

        $menu = $this->factory->createItem('root');

        $this->setMenu($menu, $this->container->getParameter('bci_cms')['navigation']['sidebar']);

        return $menu;
    }

    private function setMenu(&$menu, $child)
    {
        foreach ( $child as $key => $value ) {
            $params = [];
            $children = null;
            foreach ($value as $k => $v) {
                if (in_array($k, ['label', 'route', 'display', 'attributes', 'extras', 'linkAttributes', 'authAttributes', 'uri', 'displayChildren'])) {
                    $params[$k] = $v;
                }
                if ($k === 'routeParameters') {
                    if ($this->request !== null) {
                        foreach (array_keys($v) as $paramName) {
                            if (!$this->request->getCurrentRequest()->get($paramName) && $v[$paramName] == null) {
                                unset($params['route']);
                                break;
                            }
                            $parameter = $this->request->getCurrentRequest()->get($paramName);
                            if ($parameter && $v[$paramName] == null) {
                                $params[$k][$paramName] = $parameter;
                            } else {
                                $params[$k][$paramName] = $v[$paramName];
                            }
                        }
                    }
                }
                if ($k === 'children') {
                    $children = $v;
                }
            }

            $menu->addChild($key, $params);

            if ($children) {
                $this->setMenu($menu[$key], $children);
            }
        }
    }

    private function injectParameter(array $options)
    {


        return $options;
    }
}