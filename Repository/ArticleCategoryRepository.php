<?php

namespace Bci\CmsBundle\Repository;

use Bci\CmsBundle\Entity\ArticleCategory;
use Bci\CmsBundle\Entity\Blog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArticleCategory::class);
    }
    
    public function findOneCategoryByBlog(Blog $blog, $title, $locale)
    {
        $qb = $this->createQueryBuilder('c')
            ->andWhere('c.title = :title')
            ->setParameter('title', $title)
            ->andWhere('c.blog = :blog')
            ->setParameter('blog', $blog);

        $query = $qb->getQuery();

        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );

        // force Gedmo Translatable to not use current locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );

        $result = $query->getResult();

        return $result;
    }

    public function findAllAvailableCategories(Blog $blog, $locale)
    {
        $query = $this->createQueryBuilder('c')
            ->select('c')
            ->leftJoin('c.articles', 'articles')
            ->andWhere('articles.disabled = false')
            ->andWhere('articles.content is not null')
            ->andWhere('c.blog = :blog')
            ->setParameter('blog', $blog)
            ->getQuery()
        ;

        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );

        // force Gedmo Translatable to not use current locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );

        $result = $query->getResult();

        return $result;
    }

    /**
     * @param $title
     * @param $locale
     * @return mixed
     */
    public function findByTitleLocale(Blog $blog, $title, $locale)
    {
        $query = $this->createQueryBuilder('c')
            ->andWhere('c.blog = :blog')
            ->setParameter('blog', $blog)
            ->andWhere('c.title = :title')
            ->setParameter('title', $title)
            ->getQuery();


        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );

        // force Gedmo Translatable to not use current locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );

        $result = $query->getResult();

        return $result;
    }
}
