<?php

namespace Bci\CmsBundle\Repository;

use Bci\CmsBundle\Entity\Article;
use Bci\CmsBundle\Entity\Blog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * @return Article[] Returns an array of Article objects
     */
    public function findXMostRecent(Blog $blog, $locale, $maxNumberToReturn)
    {
        $query = $this->createQueryBuilder('a')
            ->andWhere('a.disabled = false')
            ->andWhere('a.content is not null')
            ->andWhere('a.disabled = false')
            ->andWhere(':blog in a.blog')
            ->set('blog', $blog)
            ->orderBy('a.updatedAt', 'DESC')
            ->setMaxResults(3)
            ->getQuery();

        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );

        // force Gedmo Translatable to not use current locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );

        $result = $query->getResult();

        return $result;
    }


    public function findOneBySlug($slug, $locale): ?Article
    {
        $query = $this->createQueryBuilder('a')
            ->andWhere('a.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
        ;
        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );

        // force Gedmo Translatable to not use current locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );

        $result = $query->getOneOrNullResult();

        return $result;
    }

    /**
     * @param $category
     * @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder
     */
    public function findByCategory($category, $locale)
    {
        $query = $this->createQueryBuilder('a')
            ->select('a')
            ->leftJoin('a.categories', 'c')
            ->andWhere('a.disabled = false')
            ->addSelect('c');

        $query = $query->add('where', $query->expr()->in('lower(c.title)', ':c'))
            ->setParameter('c', $category)
            ->getQuery()
            ->getResult();

        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );

        // force Gedmo Translatable to not use current locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );

        return $query;
    }

    /**
     * @param $year
     * @return mixed
     * @throws \Exception
     */
    public function findByYear($year)
    {
        $from = new \DateTime();
        $to = new \DateTime();

        $from
            ->setDate($year, 01, 01)
            ->setTime(00, 00, 00);
        $to
            ->setDate($year, 12, 31)
            ->setTime(23, 59, 59);


        return $this->createQueryBuilder('a')
            ->andWhere('a.disabled = false')
            ->andWhere('a.publishedAt BETWEEN :from AND :to')
            ->orderBy('a.publishedAt', 'DESC')
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return mixed
     */
    public function findArticleYear(Blog $blog, $locale)
    {
        $query = $this->createQueryBuilder('a')
            ->select("to_char(a.publishedAt, 'YYYY') AS year")
            ->andWhere('a.publishedAt IS NOT NULL')
            ->andWhere('a.content IS NOT NULL')
            ->andWhere('a.blog = :blog')
            ->setParameter('blog', $blog)
            ->groupBy('year')
            ->orderBy('year', 'DESC')
            ->getQuery();

        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );

        // force Gedmo Translatable to not use current locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );

        $result = $query->getResult();

        return $result;
    }

    /**
     * Use in ArticlesServices for paginator.
     * @param $category
     * @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder
     */
    public function getFindByCategoryQuery(Blog $blog, $category, $locale)
    {
        $query = $this->createQueryBuilder('a');
        $query = $query
            ->andWhere('a.content IS NOT NULL')
            ->setParameter('empty', '""')
            ->andWhere('a.content != :empty')
            ->andWhere('a.disabled = false')
            ->andWhere('a.blog IN (:blog)')
            ->setParameter('blog', $blog)
            ->leftJoin('a.categories', 'c')
            ->andWhere($query->expr()->in('lower(c.title)', ':ctofind'))
            ->setParameter('ctofind', $category)
            ->getQuery();

//        $query = $query->add('where', $query->expr()->in('lower(c.title)', ':c'))
//            ->setParameter('c', $category)
//            ->getQuery();

        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );

        // force Gedmo Translatable to not use current locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );

        return $query;


    }

    /**
     * Use in ArticlesServices for paginator.
     * @param $year
     * @return mixed
     * @throws \Exception
     */
    public function getFindByYearQuery(Blog $blog, $year, $locale)
    {
        $from = new \DateTime();
        $to = new \DateTime();

        $from
            ->setDate($year, 01, 01)
            ->setTime(00, 00, 00);
        $to
            ->setDate($year, 12, 31)
            ->setTime(23, 59, 59);

        $query = $this->createQueryBuilder('a')
            ->andWhere('a.publishedAt BETWEEN :from AND :to')
            ->andWhere('a.disabled = false')
            ->andWhere('a.content IS NOT NULL')
            ->andWhere('a.blog = :blog')
            ->orderBy('a.publishedAt', 'DESC')
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->setParameter('blog', $blog)
            ->getQuery();

        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );

        // force Gedmo Translatable to not use current locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );

        return $query;
    }

    public function getFindAllQuery(Blog $blog, $locale)
    {
        $today = new \DateTime('now');
        $today->setTime(23, 59, 59);

        $query = $this->createQueryBuilder('a')
            ->andWhere('a.blog = :blog')
            ->andWhere('a.publishedAt <= :today')
            ->andWhere('a.disabled = false')
            ->andWhere('a.content IS NOT NULL')
            ->orderBy('a.publishedAt', 'DESC')
            ->setParameter('today', $today)
            ->setParameter('blog', $blog)
            ->getQuery();

        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );

        // force Gedmo Translatable to not use current locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );

        return $query;
    }


}
