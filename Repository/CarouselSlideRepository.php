<?php

namespace Bci\CmsBundle\Repository;

use Bci\CmsBundle\Entity\CarouselSlide;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CarouselSlide|null find($id, $lockMode = null, $lockVersion = null)
 * @method CarouselSlide|null findOneBy(array $criteria, array $orderBy = null)
 * @method CarouselSlide[]    findAll()
 * @method CarouselSlide[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CarouselSlideRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CarouselSlide::class);
    }

    /**
     * @param $id
     * @param $locale
     * @return CarouselSlide[]
     */
    public function findSlidesByCarouselId($id, $locale)
    {
        $now = new \DateTime('now');
        $query = $this->createQueryBuilder('s')
            ->join('s.carousel', 'carousel')
            ->andWhere('carousel.id = :id')
            ->orderBy('s.position','ASC')
            ->andWhere('s.status = true')
//            ->andWhere('((:now >= s.startAt AND s.startAt IS NOT NULL AND s.status = true) OR (:now <= s.endAt AND s.endAt IS NOT NULL AND s.status = true) ) OR (s.endAt IS NULL AND s.startAt IS NULL AND s.status = true)')
            ->setParameter('id', $id)
//            ->setParameter('now', $now)
            ->getQuery()
        ;

        // force Gedmo Translatable to not use current locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );


        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );

        return $query->getResult(AbstractQuery::HYDRATE_OBJECT);
    }

    // /**
    //  * @return CarouselSlide[] Returns an array of CarouselSlide objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CarouselSlide
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
