<?php

namespace Bci\CmsBundle\Repository;

use Bci\CmsBundle\Entity\Form;
use Bci\CmsBundle\Entity\FormItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FormItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormItem[]    findAll()
 * @method FormItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FormItem::class);
    }

    /**
     * DO NOT REMOVE IMPORTANT STUFF FOR CONVERSION.
     * @return mixed[]
     * @throws \Doctrine\DBAL\DBALException
     */
     public function getFrenchFormItemsAsArrayNoDoctrine()
     {
         $conn = $this->getEntityManager()->getConnection();

         $sql = "SELECT * FROM form_item fi WHERE fi.locale = 'fr' ORDER BY fi.id";
         $stmt = $conn->prepare($sql);
         $stmt->execute();
         $qResult = $stmt->fetchAll();

         return $qResult;
     }


    /*
    public function findOneBySomeField($value): ?FormItem
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
