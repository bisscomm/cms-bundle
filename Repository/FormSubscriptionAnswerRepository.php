<?php

namespace Bci\CmsBundle\Repository;

use Bci\CmsBundle\Entity\FormSubscriptionAnswer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FormSubscriptionAnswer|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormSubscriptionAnswer|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormSubscriptionAnswer[]    findAll()
 * @method FormSubscriptionAnswer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormSubscriptionAnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FormSubscriptionAnswer::class);
    }

    // /**
    //  * @return FormSubscriptionAnswer[] Returns an array of FormSubscriptionAnswer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FormSubscriptionAnswer
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
