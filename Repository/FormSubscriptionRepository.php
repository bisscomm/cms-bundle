<?php

namespace Bci\CmsBundle\Repository;

use Bci\CmsBundle\Entity\FormSubscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FormSubscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormSubscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormSubscription[]    findAll()
 * @method FormSubscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormSubscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FormSubscription::class);
    }

    // /**
    //  * @return FormSubscription[] Returns an array of FormSubscription objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FormSubscription
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
