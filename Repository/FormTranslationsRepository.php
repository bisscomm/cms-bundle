<?php

namespace Bci\CmsBundle\Repository;

use Bci\CmsBundle\Entity\FormTranslations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FormTranslations|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormTranslations|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormTranslations[]    findAll()
 * @method FormTranslations[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormTranslationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FormTranslations::class);
    }

    // /**
    //  * @return FormTranslations[] Returns an array of Form objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Form
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
