<?php

namespace Bci\CmsBundle\Repository;

use Bci\CmsBundle\Entity\Job;
use Bci\CmsBundle\Entity\JobCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JobCategory::class);
    }


    public function findAllAvailableCategories($locale)
    {
        $query = $this->createQueryBuilder('c')
            ->select('c')
            ->leftJoin('c.jobs', 'jobs')
            ->andWhere('jobs.status = true')
            ->andWhere('jobs.description is not null')
            ->getQuery()
        ;

        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );

        // force Gedmo Translatable to not use current locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );

        $result = $query->getResult();

        return $result;
    }

    /**
     * @param $title
     * @param $locale
     * @return JobCategory|null
     */
    public function findByTitleLocale($title, $locale)
    {
        $query = $this->createQueryBuilder('c')
            ->andWhere('c.title = :title')
            ->setParameter('title', $title)
            ->getQuery();


        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );

        // force Gedmo Translatable to not use current locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );

        $result = $query->getOneOrNullResult();

        return $result;
    }
    
}
