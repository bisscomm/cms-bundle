<?php

namespace Bci\CmsBundle\Repository;

use Bci\CmsBundle\Entity\Job;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\AbstractQuery;

/**
 * @method Job|null find($id, $lockMode = null, $lockVersion = null)
 * @method Job|null findOneBy(array $criteria, array $orderBy = null)
 * @method Job[]    findAll()
 * @method Job[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Job::class);
    }

    /**
     * @param $slug
     * @param $locale
     * @return int|mixed|string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findBySlugLocale($slug, $locale)
    {
        $result = $this->createQueryBuilder('j')
            ->andWhere('j.slug = :slug')
            ->andWhere('j.status = TRUE')
            ->setParameter(':slug', $slug)
            ->getQuery()
            ->setHint(
                \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
                'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
            )
            ->setHint(
                \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
                $locale
            )
//            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN,
//                true
//            )
            ->getOneOrNullResult();

        if (!$result) {
            $locale = $locale == 'fr' ? 'en' : 'fr';

            $result = $this->createQueryBuilder('j')
                ->andWhere('j.slug = :slug')
                ->andWhere('j.status = TRUE')
                ->setParameter(':slug', $slug)
                ->getQuery()
                ->setHint(
                    \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
                    'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
                )
                ->setHint(
                    \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
                    $locale
                )
//            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN,
//                true
//            )
                ->getOneOrNullResult();
        }

        return $result;
    }

    /**
     * @param $locale
     * @return Job[]
     */
    public function findAllByLocale($locale, $maxResults)
    {
        $statement = $this->createQueryBuilder('j')
            ->andWhere('j.status = TRUE')
            ->andWhere('j.onlyShowInForm = FALSE OR j.onlyShowInForm IS NULL')
            ->andWhere('j.slug IS NOT NULL')
            ->orderBy('j.createdAt','DESC')
        ;

        if ($maxResults)
        {
            $statement->setMaxResults($maxResults);
        }

        $query = $statement->getQuery();

        // force Gedmo Translatable to not use current locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );

        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );


//        $query->setHint(
//            \Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN,
//            true
//        );


        return $query->getResult(AbstractQuery::HYDRATE_OBJECT);
    }

    /**
     * @param $locale
     * @return Job[]
     */
    public function findAllByCategoryLocale($jobCategoryId, $locale, $maxResults)
    {
        $statement = $this->createQueryBuilder('j')
            ->andWhere('j.status = TRUE')
            ->andWhere('j.onlyShowInForm = FALSE OR j.onlyShowInForm IS NULL')
            ->andWhere('j.slug IS NOT NULL')
            ->innerJoin('j.categories', 'jobCategory')
            ->andWhere('jobCategory.id IN (:jobCategoryId)')
            ->setParameter('jobCategoryId', $jobCategoryId)
            ->orderBy('j.createdAt','DESC')
        ;

        if ($maxResults)
        {
            $statement->setMaxResults($maxResults);
        }

        $query = $statement->getQuery();

        // force Gedmo Translatable to not use current locale
        $query->setHint(
            \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
            $locale
        );

        $query->setHint(
            \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
            'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
        );


//        $query->setHint(
//            \Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN,
//            true
//        );


        return $query->getResult(AbstractQuery::HYDRATE_OBJECT);
    }


}
