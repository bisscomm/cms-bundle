<?php

namespace Bci\CmsBundle\Repository;

use Bci\CmsBundle\Entity\Log;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Log|null find($id, $lockMode = null, $lockVersion = null)
 * @method Log|null findOneBy(array $criteria, array $orderBy = null)
 * @method Log[]    findAll()
 * @method Log[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Log::class);
    }


    /**
     * @return mixed
     */
    public function deleteAllLogs()
    {
        $query = $this->createQueryBuilder('l')
            ->delete()
            ->getQuery()
            ->execute();
        return $query;
    }

    /**
     * Delete log by level
     * Ex : All 404.
     * @param $logLevel
     * @return mixed
     */
    public function deleteLogsByLevel($logLevel)
    {
        $query = $this->createQueryBuilder('l')
            ->delete()
            ->where('l.level = :logLevel')
            ->setParameter('logLevel', $logLevel)
            ->getQuery()
            ->execute();
        return $query;
    }

    /**
     * Delete logs older than 7 days.
     * @param $logLevel
     * @return mixed
     */
    public function deleteSevenDaysOlderLogs()
    {
        $todayMinusSevenDays = new \DateTime('now -7 days');
        $query = $this->createQueryBuilder('l')
            ->delete()
            ->where('l.createdAt < :todayMinusSevenDays')
            ->setParameter('todayMinusSevenDays', $todayMinusSevenDays)
            ->getQuery()
            ->execute();

        return $query;
    }

    // /**
    //  * @return Log[] Returns an array of Log objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Log
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
