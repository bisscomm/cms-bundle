<?php

namespace Bci\CmsBundle\Repository;

use Bci\CmsBundle\Entity\ScriptTag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ScriptTag|null find($id, $lockMode = null, $lockVersion = null)
 * @method ScriptTag|null findOneBy(array $criteria, array $orderBy = null)
 * @method ScriptTag[]    findAll()
 * @method ScriptTag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScriptTagRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ScriptTag::class);
    }

    // /**
    //  * @return ScriptTag[] Returns an array of ScriptTag objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ScriptTag
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
