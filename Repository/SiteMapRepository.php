<?php

namespace Bci\CmsBundle\Repository;

use Bci\CmsBundle\Entity\SiteMap;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SiteMap|null find($id, $lockMode = null, $lockVersion = null)
 * @method SiteMap|null findOneBy(array $criteria, array $orderBy = null)
 * @method SiteMap[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SiteMapRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SiteMap::class);
    }

    /**
     * @return SiteMap[]
     */
    public function findAll()
    {
        return $this->findBy(array(), array('siteMappedEntityClass' => 'ASC'));
    }

     /**
      * @return SiteMap[] Returns an array of SiteMap objects
      */
    public function findAllByLocale($locale)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.locale = :locale')
            ->setParameter('locale', $locale)
            ->orderBy('s.siteMappedEntityClass', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }


    /*
    public function findOneBySomeField($value): ?SiteMap
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
