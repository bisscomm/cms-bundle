<?php

namespace Bci\CmsBundle\Repository;

use Bci\CmsBundle\Entity\UploadableFileCms;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UploadableFileCms|null find($id, $lockMode = null, $lockVersion = null)
 * @method UploadableFileCms|null findOneBy(array $criteria, array $orderBy = null)
 * @method UploadableFileCms[]    findAll()
 * @method UploadableFileCms[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UploadableFileCmsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UploadableFileCms::class);
    }

    // /**
    //  * @return File[] Returns an array of File objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?File
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
