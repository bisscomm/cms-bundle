<?php

namespace Bci\CmsBundle\Repository;

use Bci\CmsBundle\Entity\Website;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Website|null find($id, $lockMode = null, $lockVersion = null)
 * @method Website|null findOneBy(array $criteria, array $orderBy = null)
 * @method Website[]    findAll()
 * @method Website[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WebsiteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Website::class);
    }

     /**
      * @return Website[] Returns an array of Website objects
      */
    /**
     * @param $siteKey
     * @return int|mixed|string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findBySiteKey($siteKey, $locale)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.key = :siteKey')
            ->setParameter('siteKey', $siteKey)
            ->leftJoin('w.jobs', 'jobs')
            ->addSelect('jobs')
            ->andWhere('jobs.status = true')
            ->getQuery()
            ->setHint(
                \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
                'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
            )
            ->setHint(
                \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
                $locale
            )
////            //All translatable field must BE TRANSLATED IN ONE IS NOT THIS DOSNT TAKE IT IN CONSIDERATION.
//            ->setHint(\Gedmo\Translatable\TranslatableListener::HINT_INNER_JOIN,
//                true
//            )
            ->getOneOrNullResult(AbstractQuery::HYDRATE_OBJECT)
        ;
    }


    /*
    public function findOneBySomeField($value): ?Website
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
