$.Class({
    namespace: 'block',
    editor: null,

    initEditor: function() {
        if ($('#block_content').length > 0) {
            this.editor = CodeMirror.fromTextArea($('#block_content').get(0), {
                mode: "htmlmixed",
                theme: "material",
                indentUnit: 4,
                lineNumbers: true,
                autoCloseBrackets: true,
                matchBrackets: true,
                lineWrapping: true,
                autoCloseTags: true,
                matchTags: true,
                styleActiveLine: true,
                scrollbarStyle: "overlay",
                height: '100%',
                viewportMargin: 0,
                extraKeys: {
                    "F6": "autocomplete",
                    "F5": function (cm) {
                        cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                        if (!$('body').hasClass('sidebar-mini')) {
                            $('#minimizeSidebar').trigger('click');
                        }
                        if (!$('body').hasClass('sidebar-mini')) {
                            $('#minimizeSidebar').trigger('click');
                            $('body').addClass('menuNotClicked');
                        }
                    },
                    "Esc": function (cm) {
                        if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                    },
                }
            });

            $('#select').on('change', function() {
                block.editor.setOption("theme", $('#select').val());
            })
            this.editor.setSize('100%','100%')
        }
        /*Copy field in block code*/
        new ClipboardJS(".btnCpy", {

            text: function(trigger) {

                //  Get Title
                let data_cc     = '"'  +  trigger.getAttribute('data-cc')   + '",'

                //  Get Tag (p, h1, h2, h3, h4)
                let data_tag    = '"'  +  trigger.getAttribute('data-tag')  + '"'

                //  Get default value
                let data_vd     = (trigger.getAttribute('data-vd')   != "")  ? ',"' + trigger.getAttribute('data-vd')   +'"'    : ',""'

                //  Get attribute (class, id)
                let data_attr   = (trigger.getAttribute('data-attr') != "")  ? ',' + trigger.getAttribute('data-attr')   : ''

                //  Get placeholder
                let data_plh    = (trigger.getAttribute('data-plh')  != "")  ? ',"' + trigger.getAttribute('data-plh')  +'"'   : ''


                return '{{ field_trans(block, ' + data_cc + data_tag + data_vd + data_attr + data_plh + ') }}';

            }

        });
    },

    toggleConfigPanel: function(params, element, event) {
        $(params.target).toggle('drop', {direction: 'right'})
    },

    loadBtnCpy: function(params, element, event){
        //Slugify clientside for field trans id/name
        //Is also slugified serverSide
        if (params == 'data-cc')
        {
            block.slugify(element);
        }

        // Get value
        let value = element.val()

        // Find his button .btnCpy
        let btnCpy = element.closest('li').find('.btnCpy')

        // Defined attr data by value
        btnCpy.attr(params, value)

        // Activate or disable btnCopy
        if (element[0].id.split('_')[3] == 'name' ) {

            if (value != "") {
                btnCpy.attr('disabled', false)
            }
            else {
                btnCpy.attr('disabled', true)
            }

        }

    },
    slugify:  function(element) {
        let str = $(element).val();

        let slugified = str
            .toString()                     // Cast to string
            .toLowerCase()                  // Convert the string to lowercase letters
            .normalize('NFD')       // The normalize() method returns the Unicode Normalization Form of a given string.
            .trim()                         // Remove whitespace from both sides of a string
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-');        // Replace multiple - with single -
        $(element).val(slugified);
    }
});









