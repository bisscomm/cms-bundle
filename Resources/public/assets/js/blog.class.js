$.Class({
    namespace: 'blog',

    initBlogArticleThumbnail:function (params, element, event)
    {
        if (window.location.href.indexOf("article/new") > -1) {
            function handleFileSelect(evt) {
                var files = evt.target.files;

                for (var i = 0, f; f = files[i]; i++) {

                    if (!f.type.match('image.*')) {
                        continue;
                    }
                    var reader = new FileReader();

                    reader.onload = (function (theFile) {
                        return function (e) {
                            $(document).find(evt.target).parent().parent().find('img').attr('src', e.target.result);
                        };
                    })(f);

                    reader.readAsDataURL(f);
                }
            }

            $(document).find('input[type="file"]').each(function (index) {
                var inputId = $(this).attr('id');
                document.getElementById(inputId).addEventListener('change', handleFileSelect, false);
            });
        }
    },
    newCategory:function (params, element, event) {

        event.preventDefault();

        String.prototype.isEmpty = function() {
            return (this.length === 0 || !this.trim());
        };

        if ($('#categoryNameFr').val().isEmpty()||$('#categoryNameEn').val().isEmpty())
        {
            $('#nameError').html("Complétez tous les champs / Fill in both fields");
            return;
        }
        else if(isNaN(params))
        {
            $('#nameError').html("Erreur recharger la page et réessayer/");
            return;
        }
        $.ajax({
            url: '/cms/new/article_category/'+params,
            type: "POST",
            dataType: "json",
            data: {
                "categoryNameFr": $('#categoryNameFr').val(),
                "categoryNameEn": $('#categoryNameEn').val()
            }
        }).done(function(data){

            var $newChoice = '<div class="checkbox" style="max-width: 75%;">' +
                '<label class=""><input type="checkbox" id="article_categories_'+data.id+'" name="article[categories][]" value="'+data.id+'">' +
                data.name +
                '</label>\n' +
                '<a data-ice="blog.deleteCategory">\n' +
                '<span data-ice="blog.deleteCategory" class="close" style="margin-left: 10px;">\n' +
                '<i class="fa fa-times" aria-hidden="true"></i>\n' +
                '</span>\n' +
                '</a>' +
                '</div>';

            $('#categoriesDiv').append($newChoice);
            $('#categoryFormModal').modal("toggle");
            $('#categoryNameFr').val('');
            $('#categoryNameEn').val('');
            app.buildNotification(
                'success',
                data.locale == 'fr' ? 'Catégorie '+data.name+' ajouté' : 'Catégory '+data.name+' added successfully',
                'top',
                'right');

        }).fail(function(e){
            app.buildNotification(
                'danger',
                ""+e.responseText,
                'top',
                'right');
        }).always(function (){

        });
    },
    deleteCategory:function (params, element, event) {

        event.preventDefault();
        var toDeleteView = $(element).parents('div.checkbox');
        var toDeleteAjax = $(element).parents('div.checkbox').find('input:checkbox').val();
        var nbAticles = 0;

        var link = window.location.origin+'/cms/NumberOfRelatedArticles/article_category';
        $.ajax({
            url: link,
            type: "POST",
            dataType: "json",
            data: {
                "articleCategoryId": toDeleteAjax,
            }
        }).done(function(data){

            nbAticles = data.numberOfRelatedArticles;
            Swal.fire({
                title: 'Êtes-vous certain ?',
                text: "Cette catégorie est associée à "+nbAticles+" article(s).\n",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: 'rgba(52,151,14,0.91)',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, supprimer la catégorie',
                cancelButtonText: 'Annuler'
            }).then((result) => {
                if (result.value) {
                    var link = window.location.origin+'/cms/delete/article_category';
                    $.ajax({
                        url: link,
                        type: "POST",
                        dataType: "json",
                        data: {
                            "articleCategoryId": toDeleteAjax,
                        }
                    }).done(function(data){

                        $(toDeleteView).remove();
                        app.buildNotification(
                            'success',
                            data.locale == 'fr' ? 'Catégorie '+data.name+' supprimé' : 'Catégory '+data.name+' deleted successfully',
                            'top',
                            'right');

                    }).fail(function(e){
                        app.buildNotification(
                            'danger',
                            ""+e.responseText,
                            'top',
                            'right');
                    }).always(function (){

                    });
                }
            })

        }).fail(function(e){
            app.buildNotification(
                'danger',
                ""+e.responseText,
                'top',
                'right');
        });

    },
});
