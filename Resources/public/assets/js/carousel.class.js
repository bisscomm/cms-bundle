$.Class({
    namespace: 'carousel',

    initDragDropSlides:function (params, element, event)
    {
        $('.drag-handle').mousedown(function()
        {
            $(this).css('cursor', 'grabbing');
        });
        $('.drag-handle').mouseup(function()
        {
            $(this).css('cursor', 'pointer');
        });
        $(".sort-img").sortable({
            placeholder: "ui-state-highlight",
            forcePlaceholderSize: true,
            handle: ".drag-handle",
            update: function (event, ui) {
                var pos = 0;
                $('input.pos').each(function () {
                    $(this).val(pos);
                    pos++;
                })
            },
            start:function (event, ui) {
                ui.helper.find('td').addClass("no-border-drag");
            },
            stop:function (event, ui) {
                ui.item.find('td').removeClass("no-border-drag");
            }
        });
    },
})