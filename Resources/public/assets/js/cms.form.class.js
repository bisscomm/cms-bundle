$.Class({
    namespace: 'cms.form',

    initPostedDataOrder: function(params, element, event) {
        $(document).ready(function () {

            var selectedOptions = $('div.row.col-12:not([style*="display: none"])').find('select.postedDataOrder').map(function(){
                return this.value
            }).get();
            // 0 is NOT PRESENT.
            var selectedOptionsNoDuplicate = selectedOptions.filter((value,index) => selectedOptions.indexOf(value) === index && value !== "0");


                if (selectedOptionsNoDuplicate)
            {
                $('form').find('select.postedDataOrder').each(function () {
                    $(this).children().each(function (){
                        if (selectedOptionsNoDuplicate.includes(this.value))
                        {
                            $(this).attr('disabled', true);
                        }
                    })
                });
            }
        });
    },
    onChangePostedDataOrder: function(params, element, event) {
        var selectedOptions = $('div.row.col-12:not([style*="display: none"])').find('select.postedDataOrder').map(function(){
            return this.value
        }).get();

        // 0 is NOT PRESENT.
        var selectedOptionsNoDuplicate = selectedOptions.filter((value,index) => selectedOptions.indexOf(value) === index && value !== "0");

        if (selectedOptionsNoDuplicate)
        {
            $('form').find('select.postedDataOrder').each(function () {
                $(this).children().each(function (){
                    if (selectedOptionsNoDuplicate.includes(this.value))
                    {
                        $(this).attr('disabled', true);
                    }
                    else {
                        $(this).attr('disabled', false);
                    }
                })
            });
        }
    },

    addRow: function (params, element, event) {
        var ctn = $(params.container),
            ctr = (ctn.data('widget-counter') | ctn.children().length) + 1,
            proto = $(ctn.data('prototype').replace(/__name__/g, ctr));

        //Disable options that already apper in other selectpicker.
        var protoSelect = proto.find('select.postedDataOrder');

        var selectedOptions = $('div.row.col-12:not([style*="display: none"])').find('select.postedDataOrder').map(function(){
            return this.value
        }).get();

        // 0 is NOT PRESENT.
        var selectedOptionsNoDuplicate = selectedOptions.filter((value,index) => selectedOptions.indexOf(value) === index && value !== "0");

        $(selectedOptionsNoDuplicate).each(function (ind,element) {
            if (element !== "0")
            {
                $(protoSelect).children('option[value="' + element + '"]').attr('disabled', true)
            }
        });

        // //Enable selectpicker plugin on protoype.
        // proto.find(".selectpicker").selectpicker();
        // proto.find("select").selectpicker();

        ctn.data('widget-counter', ctr);

        ctn.append(proto);
    },
    removeRow: function (params, element, event) {
        $(element).closest('.row').remove();
    },
    renable: function (params, element, event) {
        $(':disabled').each(function () {
            $(this).prop('disabled', false);
        });
        $(element).closest('form').submit();
    },

    copyToClipboard: (params, element, event) => {
        const el = document.createElement('textarea');
        el.value = $($('.copy-to#' + params)).text();
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    },

    showConfig: (params, element, event) => {
        var val = ["select", "radio", "checkbox"];
        var config = $(element).closest($('.row')).find('.config');
        var index = val.indexOf($(element).val());
        (index > -1) ? config.removeClass('d-none') : config.addClass('d-none');
    },
});