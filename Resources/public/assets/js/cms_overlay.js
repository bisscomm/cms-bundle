import '../scss/custom/_cms_overlay.scss'

$.Class({
    namespace: "cms_overlay",
    // Custom functions here...

    updatePreferedView: function (params, element, event){
    $.ajax({
            method: "UPDATE",
            url: '/cms/profile/updatePreviewMode'
        }).done(function (response) {
            // alert(response);
            location.reload();
        }).fail(function (jxh, textmsg, errorThrown) {

           alert(errorThrown);
        });
    },
});
