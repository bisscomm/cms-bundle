$.Class({
    namespace: 'ct_helper',

    init: function() {
        if ($('*[data-editable]').length <= 0) {
            alert('Aucune zone éditable détectée. \n Ajouter une variable à un bloc pour rendre la page éditable');
            return;
        }

        var editor;

        ContentTools.IMAGE_UPLOADER = function(dialog) {
            var image, xhr, xhrComplete, xhrProgress;

            return imagePath = "/images/pages/demo/landscape-in-eire.jpg",
                imageSize = [780, 366],
                uploadingTimeout = null,
                rotate = function() {
                    var clearBusy;
                    return dialog.busy(!0),
                        clearBusy = function(_this) {
                            return function() {
                                return dialog.busy(!1)
                            }
                        }(this),
                        setTimeout(clearBusy, 1500)
                },
                dialog.addEventListener(
                    "imageuploader.cancelupload",
                    function() {
                        if (xhr) {
                            xhr.upload.removeEventListener('progress', xhrProgress);
                            xhr.removeEventListener('readystatechange', xhrComplete);
                            xhr.abort();
                        }

                        // Set the dialog to empty
                        dialog.state('empty');
                    }
                ),
                dialog.addEventListener(
                    "imageuploader.clear",
                    function() {
                        dialog.clear();
                        image = null;
                    }
                ),
                dialog.addEventListener(
                    "imageuploader.fileready",
                    function(ev) {
                        // Upload a file to the server
                        var formData;
                        var file = ev.detail().file;
                        // Define functions to handle upload progress and completion
                        dialog.state('uploading');

                        // Build the form data to post to the server
                        formData = new FormData();
                        formData.append('image', file);

                        // Make the request
                        $.ajax({
                            url: '/cms/page/' + $('body').data('page') + '/upload-image',
                            type: 'POST',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            xhr: function () {
                                var jqXHR = null;
                                if (window.ActiveXObject) {
                                    jqXHR = new window.ActiveXObject("Microsoft.XMLHTTP");
                                } else {
                                    jqXHR = new window.XMLHttpRequest();
                                }

                                //Upload progress
                                jqXHR.upload.addEventListener("progress", function (evt) {
                                    if (evt.lengthComputable) {
                                        var percentComplete = Math.round((evt.loaded * 100) / evt.total);
                                        //Do something with upload progress
                                        dialog.progress(percentComplete);
                                    }
                                }, false);
                                //Download progress
                                jqXHR.addEventListener("progress", function (evt) {
                                    if (evt.lengthComputable) {
                                        var percentComplete = Math.round((evt.loaded * 100) / evt.total);
                                        //Do something with download progress
                                        dialog.progress(percentComplete);
                                    }
                                }, false);
                                return jqXHR;
                            },
                            complete: function(ev, status) {
                                var response;

                                // Check the request is complete
                                if (ev.readyState != 4) {
                                    return;
                                }

                                // Handle the result of the upload
                                if (parseInt(ev.status) == 200) {
                                    // Unpack the response (from JSON)
                                    response = JSON.parse(ev.responseText);

                                    // Store the image details
                                    image = {
                                        id: ev.responseJSON.id,
                                        size: ev.responseJSON.size,
                                        url: ev.responseJSON.url
                                    };

                                    // Populate the dialog
                                    dialog.populate(image.url, image.size);

                                } else {
                                    // The request failed, notify the user
                                    new ContentTools.FlashUI('no');
                                }
                            }
                        });
                    }
                ),
                dialog.addEventListener(
                    "imageuploader.rotateccw",
                    function() {
                        return null;
                    }
                ),
                dialog.addEventListener(
                    "imageuploader.rotatecw",
                    function() {
                        return null;
                    }
                ),
                dialog.addEventListener(
                    "imageuploader.save",
                    function(a,b,c,d,e) {
                        var crop, cropRegion, formData;

                        // Define a function to handle the request completion
                        xhrComplete = function (ev) {
                            // Check the request is complete
                            if (ev.target.readyState !== 4) {
                                return;
                            }

                            // Clear the request
                            xhr = null
                            xhrComplete = null

                            // Free the dialog from its busy state
                            dialog.busy(false);

                            // Handle the result of the rotation
                            if (parseInt(ev.target.status) === 200) {
                                // Unpack the response (from JSON)
                                var response = JSON.parse(ev.target.responseText);

                                // Trigger the save event against the dialog with details of the
                                // image to be inserted.
                                dialog.save(
                                    response.url,
                                    response.size,
                                    {
                                        'alt': response.alt,
                                        'data-ce-max-width': response.size[0]
                                    });

                            } else {
                                // The request failed, notify the user
                                new ContentTools.FlashUI('no');
                            }
                        }

                        // Set the dialog to busy while the rotate is performed
                        dialog.busy(true);

                        // Build the form data to post to the server
                        formData = new FormData();
                        formData.append('url', image.url);

                        // Set the width of the image when it's inserted, this is a default
                        // the user will be able to resize the image afterwards.
                        formData.append('width', 600);

                        // Check if a crop region has been defined by the user
                        if (dialog.cropRegion()) {
                            formData.append('crop', dialog.cropRegion());
                        }

                        // Make the request
                        xhr = new XMLHttpRequest();
                        xhr.addEventListener('readystatechange', xhrComplete);
                        xhr.open('POST', '/cms/image/' + image.id + '/insert-image', true);
                        xhr.send(formData);
                    }
                )
        }

        var __slice = [].slice,
            __indexOf = [].indexOf || function (item) {
                for (var i = 0, l = this.length; i < l; i++) {
                    if (i in this && this[i] === item) return i;
                }
                return -1;
            },
            __hasProp = {}.hasOwnProperty,
            __extends = function (child, parent) {
                for (var key in parent) {
                    if (__hasProp.call(parent, key)) child[key] = parent[key];
                }

                function ctor() {
                    this.constructor = child;
                }
                ctor.prototype = parent.prototype;
                child.prototype = new ctor();
                child.__super__ = parent.prototype;
                return child;
            },
            __bind = function (fn, me) {
                return function () {
                    return fn.apply(me, arguments);
                };
            };
        ContentTools.Tools.Heading3 = (function (_super) {
            __extends(Heading3, _super);

            function Heading3() {
                return Heading3.__super__.constructor.apply(this, arguments);
            }

            ContentTools.ToolShelf.stow(Heading3, 'heading3');

            Heading3.label = 'Heading3';

            Heading3.icon = 'subheading';

            Heading3.tagName = 'h3';

            return Heading3;

        })(ContentTools.Tools.Heading);

        ContentTools.Tools.Heading4 = (function (_super) {
            __extends(Heading4, _super);

            function Heading4() {
                return Heading4.__super__.constructor.apply(this, arguments);
            }

            ContentTools.ToolShelf.stow(Heading4, 'heading4');

            Heading4.label = 'Heading4';

            Heading4.icon = 'subheading';

            Heading4.tagName = 'h4';

            return Heading4;

        })(ContentTools.Tools.Heading);

        ContentTools.Tools.Heading5 = (function (_super) {
            __extends(Heading5, _super);

            function Heading5() {
                return Heading5.__super__.constructor.apply(this, arguments);
            }

            ContentTools.ToolShelf.stow(Heading5, 'heading5');

            Heading5.label = 'Heading5';

            Heading5.icon = 'subheading';

            Heading5.tagName = 'h5';

            return Heading5;

        })(ContentTools.Tools.Heading);

        ContentTools.Tools.Heading6 = (function (_super) {
            __extends(Heading6, _super);

            function Heading6() {
                return Heading6.__super__.constructor.apply(this, arguments);
            }

            ContentTools.ToolShelf.stow(Heading6, 'heading6');

            Heading6.label = 'Heading6';

            Heading6.icon = 'subheading';

            Heading6.tagName = 'h6';

            return Heading6;

        })(ContentTools.Tools.Heading);

        ContentTools.Tools.Label = (function (_super) {
            __extends(Label, _super);

            function Label() {
                return Label.__super__.constructor.apply(this, arguments);
            }

            ContentTools.ToolShelf.stow(Label, 'label');

            Label.label = 'Label';

            Label.icon = 'label';

            Label.tagName = 'label';

            Label.canApply = function (element, selection) {
                if (!element.content) {
                    return false;
                }
                return selection && !selection.isCollapsed();
            };

            Label.isApplied = function (element, selection) {
                var from, to, _ref;
                if (element.content === void 0 || !element.content.length()) {
                    return false;
                }
                _ref = selection.get(), from = _ref[0], to = _ref[1];
                if (from === to) {
                    to += 1;
                }
                return element.content.slice(from, to).hasTags(this.tagName, true);
            };

            Label.apply = function (element, selection, callback) {
                var from, to, toolDetail, _ref;
                toolDetail = {
                    'tool': this,
                    'element': element,
                    'selection': selection
                };
                if (!this.dispatchEditorEvent('tool-apply', toolDetail)) {
                    return;
                }
                element.storeState();
                _ref = selection.get(), from = _ref[0], to = _ref[1];
                if (this.isApplied(element, selection)) {
                    element.content = element.content.unformat(from, to, new HTMLString.Tag(this.tagName));
                } else {
                    element.content = element.content.format(from, to, new HTMLString.Tag(this.tagName));
                }
                element.content.optimize();
                element.updateInnerHTML();
                element.taint();
                element.restoreState();
                callback(true);
                return this.dispatchEditorEvent('tool-applied', toolDetail);
            };

            return Label;

        })(ContentTools.Tool);

        ContentTools.Tools.Span = (function (_super) {
            __extends(Span, _super);

            function Span() {
                return Span.__super__.constructor.apply(this, arguments);
            }

            ContentTools.ToolShelf.stow(Span, 'span');

            Span.label = 'Span';

            Span.icon = 'span';

            Span.tagName = 'span';

            Span.canApply = function (element, selection) {
                if (!element.content) {
                    return false;
                }
                return selection && !selection.isCollapsed();
            };

            Span.isApplied = function (element, selection) {
                var from, to, _ref;
                if (element.content === void 0 || !element.content.length()) {
                    return false;
                }
                _ref = selection.get(), from = _ref[0], to = _ref[1];
                if (from === to) {
                    to += 1;
                }
                return element.content.slice(from, to).hasTags(this.tagName, true);
            };

            Span.apply = function (element, selection, callback) {
                var from, to, toolDetail, _ref;
                toolDetail = {
                    'tool': this,
                    'element': element,
                    'selection': selection
                };
                if (!this.dispatchEditorEvent('tool-apply', toolDetail)) {
                    return;
                }
                element.storeState();
                _ref = selection.get(), from = _ref[0], to = _ref[1];
                if (this.isApplied(element, selection)) {
                    element.content = element.content.unformat(from, to, new HTMLString.Tag(this.tagName));
                } else {
                    element.content = element.content.format(from, to, new HTMLString.Tag(this.tagName));
                }
                element.content.optimize();
                element.updateInnerHTML();
                element.taint();
                element.restoreState();
                callback(true);
                return this.dispatchEditorEvent('tool-applied', toolDetail);
            };

            return Span;

        })(ContentTools.Tool);

        ContentTools.Tools.File = (function (_super) {
            __extends(File, _super);

            function File() {
                return File.__super__.constructor.apply(this, arguments);
            }

            ContentTools.ToolShelf.stow(File, 'file');

            File.label = 'File';

            File.icon = 'file';

            File.tagName = 'a';

            File.canApply = function (element, selection) {
                if (!element.content) {
                    return false;
                }
                return selection && !selection.isCollapsed();
            };

            File.isApplied = function (element, selection) {
                var from, to, _ref;
                if (element.content === void 0 || !element.content.length()) {
                    return false;
                }
                _ref = selection.get(), from = _ref[0], to = _ref[1];
                if (from === to) {
                    to += 1;
                }
                return element.content.slice(from, to).hasTags(this.tagName, true);
            };

            File.apply = function (element, selection, callback) {
                var from, to, toolDetail, _ref;
                toolDetail = {
                    'tool': this,
                    'element': element,
                    'selection': selection
                };
                if (!this.dispatchEditorEvent('tool-apply', toolDetail)) {
                    return;
                }
                element.storeState();
                _ref = selection.get(), from = _ref[0], to = _ref[1];
                if (this.isApplied(element, selection)) {
                    function uuidv4() {
                        return 'url-xxxxxxxy'.replace(/[xy]/g, function(c) {
                            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                            return v.toString(16);
                        });
                    }
                    let id = uuidv4();
                    $('#myModal iframe').attr('src' , '/bundles/bcicms/assets/js/filemanager/dialog.php?type=2&field_id='+id+'&fldr=');
                    element.content = element.content.format(from, to, new HTMLString.Tag(this.tagName, {
                        'id': id
                    }));
                } else {
                    function uuidv4() {
                        return 'url-xxxxxxxy'.replace(/[xy]/g, function(c) {
                            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                            return v.toString(16);
                        });
                    }
                    let id = uuidv4();
                    $('#myModal iframe').attr('src' , '/bundles/bcicms/assets/js/filemanager/dialog.php?type=2&field_id='+id+'&fldr=');
                    element.content = element.content.format(from, to, new HTMLString.Tag(this.tagName, {
                        'id': id
                    }));
                }
                element.content.optimize();
                element.updateInnerHTML();
                element.taint();
                element.restoreState();
                callback(true);
                return this.dispatchEditorEvent('tool-applied', toolDetail);
            };

            return File;

        })(ContentTools.Tool);

        ContentTools.Tools.Underline = (function (_super) {
            __extends(Underline, _super);

            function Underline() {
                return Underline.__super__.constructor.apply(this, arguments);
            }

            ContentTools.ToolShelf.stow(Underline, 'underline');

            Underline.label = 'Underline';

            Underline.icon = 'underline';

            Underline.tagName = 'u';

            Underline.canApply = function (element, selection) {
                if (!element.content) {
                    return false;
                }
                return selection && !selection.isCollapsed();
            };

            Underline.isApplied = function (element, selection) {
                var from, to, _ref;
                if (element.content === void 0 || !element.content.length()) {
                    return false;
                }
                _ref = selection.get(), from = _ref[0], to = _ref[1];
                if (from === to) {
                    to += 1;
                }
                return element.content.slice(from, to).hasTags(this.tagName, true);
            };

            Underline.apply = function (element, selection, callback) {
                var from, to, toolDetail, _ref;
                toolDetail = {
                    'tool': this,
                    'element': element,
                    'selection': selection
                };
                if (!this.dispatchEditorEvent('tool-apply', toolDetail)) {
                    return;
                }
                element.storeState();
                _ref = selection.get(), from = _ref[0], to = _ref[1];
                if (this.isApplied(element, selection)) {
                    element.content = element.content.unformat(from, to, new HTMLString.Tag(this.tagName));
                } else {
                    element.content = element.content.format(from, to, new HTMLString.Tag(this.tagName));
                }
                element.content.optimize();
                element.updateInnerHTML();
                element.taint();
                element.restoreState();
                callback(true);
                return this.dispatchEditorEvent('tool-applied', toolDetail);
            };

            return Underline;

        })(ContentTools.Tool);

        ContentTools.Tools.Accordion = (function (_super) {
            __extends(Accordion, _super);

            function Accordion() {
                return Accordion.__super__.constructor.apply(this, arguments);
            }

            ContentTools.ToolShelf.stow(Accordion, 'accordion');

            Accordion.label = 'Accordion';

            Accordion.icon = 'accordion';

            Accordion.tagName = 'accordion';

            Accordion.canApply = function(element, selection) {
                if (element.isFixed()) {
                    return false;
                }
                return element.content !== void 0 && ['Text', 'PreText'].indexOf(element.type()) !== -1;
            };

            Accordion.isApplied = function(element, selection) {
                if (!element.content) {
                    return false;
                }
                if (['Text', 'PreText'].indexOf(element.type()) === -1) {
                    return false;
                }
                return element.tagName() === this.tagName;
            };

            Accordion.apply = function(element, selection, callback) {
                var content, insertAt, parent, textElement, toolDetail;
                toolDetail = {
                    'tool': this,
                    'element': element,
                    'selection': selection
                };
                if (!this.dispatchEditorEvent('tool-apply', toolDetail)) {
                    return;
                }
                element.storeState();
                if (element.type() === 'PreText') {
                    content = element.content.html().replace(/&nbsp;/g, ' ');
                    textElement = new ContentEdit.Text(this.tagName, {}, content);
                    parent = element.parent();
                    insertAt = parent.children.indexOf(element);
                    parent.detach(element);
                    parent.attach(textElement, insertAt);
                    element.blur();
                    textElement.focus();
                    textElement.selection(selection);
                } else {
                    element.removeAttr('class');
                    if (element.tagName() === this.tagName) {
                        element.tagName('p');
                    } else {
                        element.tagName(this.tagName);
                    }
                    element.restoreState();
                }
                this.dispatchEditorEvent('tool-applied', toolDetail);
                return callback(true);
            };

            return Accordion;

        })(ContentTools.Tool);

        editorCls = ContentTools.EditorApp.getCls();
        editorCls.prototype.createPlaceholderElement = function(region) {
            var type = region.domElement().getAttribute('data-type');
            var placeholderText = region.domElement().getAttribute('data-ce-placeholder');
            console.log(type)
            return new ContentEdit.Text(
                type || 'p',
                {'data-ce-placeholder': placeholderText || ''},
                '');
        };

        editor = ContentTools.EditorApp.get();
        editor.init('*[data-editable]', 'data-name');
        // editor.start();

        editor.addEventListener('saved', function(event) {
            var name, payload, regions, xhr;

            // Check that something changed
            regions = event.detail().regions;
            if (Object.keys(regions).length == 0) {
                return;
            }

            // Set the editor as busy while we save our changes
            this.busy(true);

            // Collect the contents of each region into a FormData instance
            var datas = {
                locale: $('body').data('locale'),
                contents: {}
            };

            for (name in regions) {
                if (regions.hasOwnProperty(name)) {
                    $element = $('[data-name="'+name+'"]');
                    datas.contents[name] = {
                        tplBlockId: $element.data('block'),
                        field: $element.data('field'),
                        value: regions[name]
                    }
                }
            }

            $.ajax({
                type: 'POST',
                url: '/cms/page/' + $('body').data('page') + '/save_content',
                data: datas,
                success: function(data) {
                    editor.busy(false);
                    if (typeof data.msg !== 'undefined' && data.msg == 'success')
                        new ContentTools.FlashUI('ok');
                    else {
                        new ContentTools.FlashUI('no');
                        $.notify({
                            icon: "now-ui-icons",
                            message: "Problème de connexion, votre session a expirée. <br>Veuillez vous reconnecter, merci!"

                        },{
                            type: 'danger',
                            timer: 10000,
                            placement: {
                                from: 'bottom',
                                align: 'center'
                            }
                        });
                    }

                },
                error: function(xhr, msg, err) {
                    editor.busy(false);
                    new ContentTools.FlashUI('no');
                    $.notify({
                        icon: "now-ui-icons",
                        message: "Une erreur s'est produite, le contenu n'a pu être enregistrer. <br>Réessayer dans quelques instant, si le problème persiste, contacter l'administrateur du site. Merci."

                    },{
                        type: 'danger',
                        timer: 10000,
                        placement: {
                            from: 'bottom',
                            align: 'center'
                        }
                    });
                }
            });
        });
    }
});