//Initialize for frontend cms
$.Class({
    namespace: 'editor',

    hasChange: false,

    initEditor: function () {
        if ($('[data-editable]').length > 0) {
            $('body').prepend(
                '<div class="tiny-actions">' +
                '<div class="btn-tiny btn-tiny-save" data-ice="editor.save" title="Enregistrer"><i class="fa fa-save" style="color: #fff"></i></div>' +
                '<div class="btn-tiny btn-tiny-cancel" data-ice="editor.cancel" title="Annuler les changements"><i class="fa fa-trash" style="color: #fff"></i></div>' +
                '</div>' +
                '<div class="tiny-overlay" style="display: none"></div>'
            );
            $('[data-editable]').each(function () {
                // $(this).hide();
                $(this).data('origine', $(this).html());
                tinymce.init({
                    selector: '[data-name="' + $(this).data('name') + '"]',
                    inline: true,
                    // datablock: $(this).data('block'),
                    images_upload_url: '/cms/upload/image',
                    image_caption: true,
                    automatic_uploads: false,
                    valid_children: window.custom_tiny_config.valid_children || "span[*],+a[div|h1|h2|h3|h4|h5|h6|p|#text|span],+ul[div|h1|h2|h3|h4|h5|h6|p|#text|li],+div[li],+body[style]",
                    extended_valid_elements: window.custom_tiny_config.extended_valid_elements || 'span[*],i[class]',
                    // content_css: '/build/css/app.css',
                    plugins: "advlist autolink link lists image media filemanager codemirror responsivefilemanager template visualblocks",
                    toolbar: ' undo redo | styleselect | bold | alignleft alignright aligncenter alignjustify | floatleft floatright | bullist numlist | link unlink image responsivefilemanager | media | code | template | span | removeformat | visualblocks',
                    menubar: false,
                    templates: window.custom_tiny_config.templates,
                    media_live_embeds: true,
                    height: $(this).data('height') != undefined ? $(this).data('height') : '250px',
                    external_filemanager_path: "/bundles/bcicms/assets/js/filemanager/",
                    filemanager_title: "Gestionnaire de fichiers",
                    external_plugins: {
                        "responsivefilemanager": "plugins/responsivefilemanager/plugin.min.js",
                        "filemanager": "/bundles/bcicms/assets/js/filemanager/plugin.min.js",
                        "codemirror": "plugins/codemirror/codemirror-4.8/plugin.js"
                    },
                    codemirror: {
                        indentOnInit: true, // Whether or not to indent code on init.
                        fullscreen: false,   // Default setting is false
                        path: './', // Path to CodeMirror distribution
                        config: {           // CodeMirror config object
                            mode: 'application/x-httpd-php',
                            lineNumbers: true
                        },
                        width: 800,         // Default value is 800
                        height: 600,        // Default value is 550
                        jsFiles: [          // Additional JS files to load
                            'mode/clike/clike.js',
                            'mode/php/php.js'
                        ]
                    },
                    style_formats_autohide: true,
                    style_formats: window.custom_tiny_config.style_formats,
                    formats: window.custom_tiny_config.formats,
                    style_formats_merge: true,
                    //******* OLD *******
                    //PREVENT ELEMENTS BEING WRAPPED IN <p> TAGS
                    // forced_root_block : '',
                    //Relative urls
                    // In Tiny window when choosing a images from filemanage the url shown is with the https://domainname.com/uploads/files
                    // But in the code, html, the render is WITHOUT the sheme and domain -> ../../../uploads/files
                    // *** This conver the url ON SAVE ***:
                    // FROM : https://domainname.com/uploads/files/myfile.jpg
                    // TO: ../../../uploads/files/myfile.jpg
                    // relative_urls : true,
                    // remove_script_host : true,
                    // convert_urls : true,
                    // document_base_url:'uploads/',
                    // urlconverter_callback: function(url, node, on_save, name) {
                    //     // Do some custom URL conversion
                    //     //This split the full of the file and convert it to a relative filepath
                    //     // Ex: url = https://www.example.com/uploads/files/myfile.pdf
                    //     // window.location.origin = https://www.example.com
                    //     // url = /uploads/files/myfile.pdf
                    //     url = '/uploads/files/'+url.split('/uploads/files/').pop();
                    //
                    //
                    //     // Return new URL
                    //     return url;
                    // },

                    //********++++++++++++ NEW ++++++++++++++*********
                    // IN filemanager/config.php we set the base domain to be ""
                    // and we set relative_urls to false so the filemanager serve the file as /uploads/files/myfile.txt
                    relative_urls : false,

                    setup: function (editor) {
                        editor.ui.registry.addButton('span', {
                            text: 'Span',
                            onAction: function (_) {
                                editor.insertContent('&nbsp;<span class="span">' + editor.selection.getContent({format: 'text'}) + ' </span>&nbsp;');
                            }
                        });

                        // function getHistory(){
                        //     $.ajax({
                        //         type: "GET",
                        //         url: "/cms/page/" + $('body').data('locale') + "/" + $('body').data('page') + "/" + editor.settings.datablock,
                        //     }).done(function (data) {
                        //         function createHistory(date, content) {
                        //             return {
                        //                 type: 'menuitem',
                        //                 text: date,
                        //                 onAction: function () {
                        //                     editor.setContent(content);
                        //                 }
                        //             };
                        //         }
                        //
                        //         var objects = [];
                        //         for (let item of data.content) {
                        //             objects.push(createHistory(item.date, item.body));
                        //         }
                        //         editor.ui.registry.addMenuButton('history', {
                        //             text: 'History',
                        //             fetch: function (callback) {
                        //                 var items = objects;
                        //                 callback(items);
                        //             }
                        //         });
                        //         editor.bodyElement.setAttribute('style', '');
                        //     });
                        // }
                        editor.on('init', function (event) {
                            $(editor.getBody().parentNode).bind('dragover dragenter dragend drag drop', function (e) {
                                e.stopPropagation();
                                e.preventDefault();
                            });
                            $(editor.getDoc()).bind('draggesture', function (e) {
                                e.stopPropagation();
                                e.preventDefault();
                            });
                            // getHistory();
                        });
                        editor.ui.registry.addButton('floatleft', {
                            text: 'Image gauche',
                            onAction: function (_) {
                                let node = tinymce.activeEditor.selection.getNode();
                                node.setAttribute('style', "");
                                node.setAttribute('data-mce-style', "");
                                if ($(node).hasClass("right")) {
                                    $(node).removeClass("right");
                                }
                                if ($(node).hasClass("left")) {
                                    $(node).removeClass("left");
                                    return
                                }
                                $(node).addClass("left");
                            }
                        });
                        editor.ui.registry.addButton('floatright', {
                            text: 'Image droite',
                            onAction: function (_) {
                                let node = tinymce.activeEditor.selection.getNode();
                                node.setAttribute('style', "");
                                node.setAttribute('data-mce-style', "");
                                if ($(node).hasClass("left")) {
                                    $(node).removeClass("left");
                                }
                                if ($(node).hasClass("right")) {
                                    $(node).removeClass("right");
                                    return
                                }
                                $(node).addClass("right");
                            }
                        });
                        // allow overides of tinymce settings in project
                        if(window.custom_tiny_config.settings){
                            let tiny_config = window.custom_tiny_config.settings;
                            editor.settings = { ...editor.settings, ...tiny_config };
                        }
                    },
                    images_upload_handler: function (blobInfo, success, failure) {
                        var xhr, formData;
                        xhr = new XMLHttpRequest();
                        xhr.withCredentials = false;
                        xhr.open('POST', '/cms/upload/image');
                        xhr.onload = function () {
                            var json;
                            if (xhr.status != 200) {
                                failure('HTTP Error: ' + xhr.status);
                                return;
                            }
                            json = JSON.parse(xhr.responseText);
                            if (!json || typeof json.location != 'string') {
                                failure('Invalid JSON: ' + xhr.responseText);
                                return;
                            }
                            success(json.location);
                        };
                        formData = new FormData();
                        formData.append('file', blobInfo.blob(), blobInfo.filename());
                        xhr.send(formData);
                    }
                });
            })
        } else {
            alert("Il n'y a pas de zone éditable sur cette page");
        }
    },
    save: function (params, element, event) {
        var changes = false;
        datas = {
            locale: $('body').data('locale'),
            contents: {}
        };
        $('[data-editable]').each(function () {
            if ($(this).data('origine') != $(this).html()) {
                changes = true;
                datas.contents[$(this).data('name')] = {
                    tplBlockId: $(this).data('block'),
                    field: $(this).data('field'),
                    value: $(this).html()
                };
            }
        });

        if (changes) {
            this.sendData(datas);
        }
    },
    cancel: function (params, element, event) {
        $('[data-editable]').each(function () {
            console.log($(this).attr('id'));
            tinyMCE.get($(this).attr('id')).setContent($(this).data('origine'));
        });
    },
    sendData: function (datas) {
        $('.tiny-overlay').fadeIn('fast');
        $.ajax({
            type: 'POST',
            url: '/cms/page/' + $('body').data('page') + '/save_content',
            data: datas,
            success: function (data) {
                if (typeof data.msg !== 'undefined' && data.msg == 'success')
                    $.notify({
                        message: "La page a été enregistrée avec succès !"
                    }, {
                        template: '<div data-notify="container" class="col-11 col-md-4 alert alert-{0} alert-with-icon" role="alert"><button type="button" aria-hidden="true" class="close" data-notify="dismiss"><i class="fa fa-close"></i></button><span data-notify="title">{1}</span> <span data-notify="message">{2}</span><div class="progress" data-notify="progressbar"><div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div></div><a href="{3}" target="{4}" data-notify="url"></a></div>',
                        type: 'success',
                        timer: 10000,
                        placement: {
                            from: 'bottom',
                            align: 'center'
                        }
                    });
                else {
                    $.notify({
                        message: "Problème de connexion, votre session a expirée. <br>Veuillez vous reconnecter, merci!"

                    }, {
                        template: '<div data-notify="container" class="col-11 col-md-4 alert alert-{0} alert-with-icon" role="alert"><button type="button" aria-hidden="true" class="close" data-notify="dismiss"><i class="fa fa-close"></i></button><span data-notify="title">{1}</span> <span data-notify="message">{2}</span><div class="progress" data-notify="progressbar"><div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div></div><a href="{3}" target="{4}" data-notify="url"></a></div>',
                        type: 'danger',
                        timer: 10000,
                        placement: {
                            from: 'bottom',
                            align: 'center'
                        }
                    });
                }
                $('.tiny-overlay').fadeOut('fast');
            },
            error: function (xhr, msg, err) {
                $.notify({
                    message: "Une erreur s'est produite, le contenu n'a pu être enregistrer. <br>Réessayer dans quelques instant, si le problème persiste, contacter l'administrateur du site. Merci."

                }, {
                    template: '<div data-notify="container" class="col-11 col-md-4 alert alert-{0} alert-with-icon" role="alert"><button type="button" aria-hidden="true" class="close" data-notify="dismiss"><i class="fa fa-close"></i></button><span data-notify="title">{1}</span> <span data-notify="message">{2}</span><div class="progress" data-notify="progressbar"><div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div></div><a href="{3}" target="{4}" data-notify="url"></a></div>',
                    type: 'danger',
                    timer: 10000,
                    placement: {
                        from: 'bottom',
                        align: 'center'
                    }
                });
                $('.tiny-overlay').fadeOut('fast');
            }
        });
    }
});









