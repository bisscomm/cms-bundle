$.Class({
    namespace: 'job',

    newCategory:function (params, element, event) {

        event.preventDefault();

        String.prototype.isEmpty = function() {
            return (this.length === 0 || !this.trim());
        };

        if ($('#categoryNameFr').val().isEmpty()||$('#categoryNameEn').val().isEmpty())
        {
            $('#nameError').html("Complétez tous les champs / Fill in both fields");
            return;
        }
        $.ajax({
            url: '/cms/new/job_category',
            type: "POST",
            dataType: "json",
            data: {
                "categoryNameFr": $('#categoryNameFr').val(),
                "categoryNameEn": $('#categoryNameEn').val()
            }
        }).done(function(data){

            var $newChoice = '<div class="checkbox col-6 j_categories" style="max-width: 75%;">' +
                '<label class=""><input type="checkbox" id="job_categories_'+data.id+'" name="job[categories][]" value="'+data.id+'">' +
                data.name +
                '</label>\n' +
                '<a data-ice="job.deleteCategory">\n' +
                '<span data-ice="job.deleteCategory" class="close" style="margin-left: 10px;">\n' +
                '<i class="fa fa-times" aria-hidden="true"></i>\n' +
                '</span>\n' +
                '</a>' +
                '</div>';

            $('#categoriesDiv').append($newChoice);
            $('#categoryFormModal').modal("toggle");
            $('#categoryNameFr').val('');
            $('#categoryNameEn').val('');
            app.buildNotification(
                'success',
                data.locale == 'fr' ? 'Catégorie '+data.name+' ajouté' : 'Catégory '+data.name+' added successfully',
                'top',
                'right');

        }).fail(function(e){
            app.buildNotification(
                'danger',
                ""+e.responseText,
                'top',
                'right');
        }).always(function (){

        });
    },
    deleteCategory:function (params, element, event) {

        event.preventDefault();
        var toDeleteView = $(element).parents('div.checkbox');
        var toDeleteAjax = $(element).parents('div.checkbox').find('input:checkbox').val();
        var nbJobs = 0;

        var link = window.location.origin+'/cms/NumberOfRelatedJobs/job_category';
        $.ajax({
            url: link,
            type: "POST",
            dataType: "json",
            data: {
                "jobCategoryId": toDeleteAjax,
            }
        }).done(function(data){

            nbJobs = data.numberOfRelatedArticles;
            Swal.fire({
                title: 'Êtes-vous certain ?',
                text: "Cette catégorie est associée à "+( nbJobs == undefined ? '0' : nbJobs )+" emploi(s).\n",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: 'rgba(52,151,14,0.91)',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, supprimer la catégorie',
                cancelButtonText: 'Annuler'
            }).then((result) => {
                if (result.value) {
                    var link = window.location.origin+'/cms/delete/job_category';
                    $.ajax({
                        url: link,
                        type: "POST",
                        dataType: "json",
                        data: {
                            "jobCategoryId": toDeleteAjax,
                        }
                    }).done(function(data){

                        $(toDeleteView).remove();
                        app.buildNotification(
                            'success',
                            data.locale == 'fr' ? 'Catégorie '+data.name+' supprimé' : 'Catégory '+data.name+' deleted successfully',
                            'top',
                            'right');

                    }).fail(function(e){
                        app.buildNotification(
                            'danger',
                            ""+e.responseText,
                            'top',
                            'right');
                    }).always(function (){

                    });
                }
            })

        }).fail(function(e){
            app.buildNotification(
                'danger',
                ""+e.responseText,
                'top',
                'right');
        });

    },
});
