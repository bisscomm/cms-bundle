$(window).resize(function () {
    if ($(window).width() >= 1199.99 || $(window).width() < 992) {
        $('.block-adder').css('padding-left', '15px');
    } else {
        if ($('body').hasClass('sidebar-mini')) {
            $('.block-adder').css('padding-left', '90px');
        } else {
            $('.block-adder').css('padding-left', '260px');
        }
    }
});

$(window).ready(function () {
    $('.sidebar .active-nav').addClass('active');
    $('.visible-on-sidebar-regular').append('<p class="txt-toggle">Réduire</p>');
});

$(window).load(function () {
    $('.spinner-wrapper').fadeOut('400');
    $('.spinner-').fadeOut('400');
});

$('.sidebar').on('click', '.load-nav', function () {
    $('.spinner-wrapper').fadeIn('400');
    $('.sidebar .active-nav').removeClass('active');
    $(this).addClass('active');
});

$('#dark-colors').click(function () {
    if ($.cookie("admin-color") !== 'dark') {
        $.cookie("admin-color", 'dark');
        location.reload();
    } else {
        $.cookie("admin-color", null);
        location.reload();
    }
});

$('#minimizeSidebar').click(function () {
    if ($.cookie("minimize") !== 'minimize') {
        $('body').toggleClass('sidebar-mini');
        $.cookie("minimize", 'minimize', {path: '/'});
    } else {
        $('body').toggleClass('sidebar-mini');
        $.cookie("minimize", null, {path: '/'});
    }
});

$(document).ready(function () {
    let $body = $('body');
    let degrees = 0;

    $body.on('click', '.div-drop-down', function () {
        if (degrees == 0) {
            degrees = 180;
            rotate($(this), degrees)
        } else {
            degrees = 0;
            rotate($(this), degrees)
        }
    });

    $('#alert-flashes .close-alert').click(function () {
        $('#alert-flashes').hide()
    });
    keyShortcut();
    $("#form-page").validate({ignore: ""});
});

function rotate(cible, degrees) {
    cible.children(':last-child').css({'transform': 'rotate(' + degrees + 'deg)'})
}

function keyShortcut() {
    jQuery(window).keydown(function (e) {
        if ((e.ctrlKey || e.metaKey) && e.keyCode == 83) {
            e.preventDefault();
            $('.content button').get(0).click();
        }
    })
}


