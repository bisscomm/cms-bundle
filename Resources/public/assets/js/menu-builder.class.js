$.Class({
    namespace: 'cms.menu',
    config: {
        menuItemCheckboxes:     $('#item-list .form-check input'),
        menuItemFilter:         $('#page-search'),
        currentMenu:            $('.dd-list.main'),
        itemOptionTogglers:     $('.dd-item .show-options'),
        itemRemoveButtons:      $('.dd-item .remove-item'),
        tplMenuItem:            '<li class="dd-item" data-id="__id__" data-name="__name__" data-original="__originalName__"> ' +
                                '<div class="dd-handle">__name__</div>' +
                                '<div class="actions">' +
                                '<a class="btn btn-link btn-sm px-2 show-options" data-ice="cms.menu.toggle">' +
                                '<i class="fa fa-gear"></i></a>' +
                                '<a class="btn btn-link btn-sm px-2 remove-item" data-ice="cms.menu.removeItem">' +
                                '<i class="fa fa-trash"></i></a></div><div class="options"> ' +
                                '<div class="form-group mt-3 mb-2 p-2"> <span class="d-block mb-1">Original: __originalName__</span> ' +
                                '<input id="__input_id__" type="text" placeholder="Libellé personnalisé ..." ' +
                                'data-ice-keyUp="cms.menu.updateLabel" data-ice-keydown="cms.menu.eventPrevent" class="form-control lbl-chg w-100 mx-0 mb-1">' +
                                ' <a class="btn btn-link btn-sm px-2 show-options" data-ice="cms.menu.toggle"><i class="fa fa-check"></i></a>' +
                                '</div></div></li>',
        ctrId:                  0
    },

    initMenuBuilder: function () {
        if ($('#nestable').length > 0) {
            $('#nestable').nestable({group: 1}).on('change', cms.menu.serialize);
            $('body').on('blur', 'input.lbl-chg', function() {
                // cms.menu.toggle({},$(this),null);
            });
            cms.menu.setInputIds();
            cms.menu.serialize();
        }
    },

    setInputIds: function () {
        $('.lbl-chg').each(function () {
            cms.menu.config.ctrId++;
            $(this).attr('id', 'input-lbl-'+cms.menu.config.ctrId);
        });
    },

    nested: function (params, element, event) {
        $('.dd').nestable($(element).data('action'));
    },

    serialize: function () {
        if (window.JSON) {
            $('#menu_menuItems').val(window.JSON.stringify($('#nestable').nestable('serialize'))); //, null, 2));
        } else {
            $('#nmenu_menuItems').val('JSON browser support required for this demo.');
        }
    },

    update: function (params, element, event) {
        if ( $('.dd-list .dd-item[data-id="' + $(element).val() + '"] > ol').length ) {
            event.preventDefault();
            $(element).prop( "checked", !$(element).is(':checked') );
            cms.menu.showWarning();
        } else {
            cms.menu.updateMenu($(element).closest('.form-check'));
        }
    },

    menuFilter: function (params, element, event) {
        if ($(element).val().length > 0) {
            $('#item-list .form-check').each(function(){
                if (!($(this).data('name').toLowerCase().indexOf($(element).val().toLowerCase()) == -1)) {
                    $(this).fadeIn('fast');
                } else {
                    $(this).fadeOut('fast');
                }
            });
        } else {
            $('#item-list .form-check').fadeIn('fast');
        }
    },

    updateMenu: function (container) {
        if($('.dd-list .dd-item[data-id="' + container.data('id') + '"]').length > 0){
            cms.menu.removeMenuItem(container);
        } else {
            cms.menu.addMenuItem(container);
        }

        cms.menu.serialize();
    },

    updateLabel: function (params, element, event)
    {
        var newLabel = $(element).val() != "" ? $(element).val() : $(element).closest('.dd-item').data('original');

        $(element).closest('.dd-item').data('name', newLabel);
        $(element).closest('.dd-item').find('>.dd-handle').text(newLabel);

        cms.menu.serialize();
    },

    addMenuItem: function (container) {
        cms.menu.config.ctrId++;
        cms.menu.config.currentMenu.append(
            $(cms.menu.config.tplMenuItem
                .replace(/__id__/g, container.data('id'))
                .replace(/__name__/g, container.data('name'))
                .replace(/__originalName__/g, container.data('name'))
                .replace(/__input_id__/g, 'input-lbl-'+cms.menu.config.ctrId)))
            .data('name', container.data('name'))
            .data('original', container.data('name')
            );
    },

    addCustomMenu: function (params, element, event) {
        var modal = $(element).closest('.modal');
        cms.menu.config.ctrId++;
        cms.menu.config.currentMenu.append(
            $(cms.menu.config.tplMenuItem
                .replace(/__id__/g, null)
                .replace(/__name__/g, modal.find('input#new-item-label').val())
                .replace(/__originalName__/g, modal.find('input#new-item-label').val())
                .replace(/__input_id__/g, 'input-lbl-'+ cms.menu.config.ctrId))
                .data('name', modal.find('input#new-item-label').val())
                .data('url', modal.find('input#new-item-url').val())
                .data('original', modal.find('input#new-item-label').val())
        );
        cms.menu.serialize();
        modal.modal('hide');

        modal.find('input#new-item-url').val('');
        modal.find('input#new-item-label').val('');
    },

    removeMenuItem: function (item) {
        var id = item.data('id');
        if(id === "") {
            item.closest('.dd-item').remove();
            cms.menu.serialize();
            return
        }
        if ($('.dd-item[data-id=' + id + ']').parent().children('.dd-item').length === 1) {
            $('.dd-item[data-id=' + id + ']').parent().parent().children('button').remove();

            if (!($('.dd-item[data-id=' + id + ']').parent().hasClass('included'))) {
                $('.dd-item[data-id=' + id + ']').parent().remove();
            }
        }

        $('.dd-item[data-id=' + id + ']').remove();

        if($('#item-list .form-check[data-id="' + id + '"] input').length > 0) {
            $('#item-list .form-check[data-id="' + id + '"] input').prop('checked', false);
        }

        cms.menu.serialize();
    },

    showWarning: function () {
        $('#hasChildrenWarning').modal('show');
    },

    toggle: function (params, element, event) {
        let toggler = $(element).closest('.dd-item').find('>.options');
        toggler.toggle();
    },

    removeItem: function (params, element, event) {
        if ($(element.closest('.dd-item').find('> ol')).length > 0){
            cms.menu.showWarning();
        } else {
            cms.menu.updateMenu($(element).closest('.dd-item'), null);
        }
    },

    eventPrevent: function (params, element, event) {
        if(event.keyCode == 13) {
            event.preventDefault();
        }
    },
});
