$(document).ready(function() {
  if (1 ==2 ) {
  // Basic config from the plugin ==============================================
  var updateOutput = function(e) {
    var list = e.length ? e : $(e.target),
      output = list.data('output');
    if (window.JSON) {
      output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
    } else {
      output.val('JSON browser support required for this demo.');
    }
  };

  // activate Nestable for list 2
  $('#nestable').nestable({group: 1}).on('change', updateOutput);

  // output initial serialised data
  updateOutput($('#nestable').data('output', $('#nestable2-output')));

  $('#nestable-menu').on('click', function(e) {
    var target = $(e.target),
      action = target.data('action');
    if (action === 'expand-all') {
      $('.dd').nestable('expandAll');
    }
    if (action === 'collapse-all') {
      $('.dd').nestable('collapseAll');
    }
  });

  // Custom add-ons ============================================================
  const menuItemCheckboxes = $('#item-list .form-check input');
  const menuItemFilter = $('#page-search');
  const currentMenu = $('.dd-list');
  let itemOptionTogglers = $('.dd-item .show-options');
  let itemRemoveButtons = $('.dd-item .remove-item');

  menuItemCheckboxes.click(function(e){
    const thisID = $(this).closest('.form-check').attr('data-id');
    const thisName = $(this).closest('.form-check-label').text();
    const hasChildren = $('.dd-list .dd-item[data-id="' + thisID + '"] > ol').length;
    if(hasChildren){
      e.preventDefault();
      showWarning();
    } else {
      updateMenu(thisID, thisName);
    }
  });

  menuItemFilter.on('keyup', function(){
    const value = $(this).val();
    const allItems = $('#item-list .form-check ~ .form-check');
    const allItemNames = $('#item-list .form-check .form-check-label');
    if(value.length > 0){
      allItems.hide();
      allItemNames.each(function(){
        const name = $(this).text();
        if(!(name.indexOf(value) == -1)){
          $(this).closest('.form-check').show();
        }
      });
    } else {
      allItems.show();
    }
  });

  function updateMenu(id, name){
    const isIncluded = $('.dd-list .dd-item[data-id="' + id + '"]').length;

    if(isIncluded){
      removeMenuItem(id);
    } else {
      addMenuItem(id, name);
    }
  }

  function addMenuItem(id, name){
    currentMenu.append('<li class="dd-item" data-id="' + id + '"> <div class="dd-handle">' + name + '</div><div class="actions"> <span class="btn btn-link btn-sm px-2 show-options">Options</span> <span class="btn btn-link btn-sm px-2 remove-item">Remove</span> </div><div class="options"> <div class="form-group p-2"> <label for="">ewfijwaefjw</label> <input id="new-item-label" type="text" placeholder="Libellé..." class="form-control w-100 mb-1"> </div></div></li>');
    initOptionTogglers();
    initItemRemoveButtons();
    updateOutput($('#nestable').data('output', $('#nestable2-output')));
  }

  function addCustomMenuItem(id, name){
    currentMenu.append('<li class="dd-item" data-id="' + id + '"> <div class="dd-handle">' + name + '</div><div class="actions"> <span class="btn btn-link btn-sm px-2 show-options">Options</span> <span class="btn btn-link btn-sm px-2 remove-item">Remove</span> </div><div class="options"> <div class="form-group p-2"> <label for="">ewfijwaefjw</label> <input id="new-item-label" type="text" placeholder="Libellé..." class="form-control w-100 mb-1"> </div></div></li>');
    initOptionTogglers();
    initItemRemoveButtons();
    updateOutput($('#nestable').data('output', $('#nestable2-output')));
  }

  function removeMenuItem(id){
    const item = $('.dd-item[data-id=' + id + ']');
    const itemCheckbox = $('#item-list .form-check[data-id="' + id + '"] input');
    const itemParent = item.parent();
    const aboveExpandButtons = itemParent.parent().children('button');
    const isOnlyChild = itemParent.children('.dd-item').length === 1;
    const isAtSecondLevel = itemParent.hasClass('included');
    if(isOnlyChild){
      aboveExpandButtons.remove();
      if(!(isAtSecondLevel)){
        itemParent.remove();
      }
    }
    item.remove();
    if(itemCheckbox.length > 0)
      itemCheckbox.prop('checked', false);
    initOptionTogglers();
    initItemRemoveButtons();
    updateOutput($('#nestable').data('output', $('#nestable2-output')));
  }

  function showWarning(){
    $('#hasChildrenWarning').modal('show');
  }

  function initOptionTogglers(){
    if(itemOptionTogglers)
      itemOptionTogglers.off('click');
    itemOptionTogglers = $('.dd-item .show-options');
    itemOptionTogglers.on('click', function(e){
      $(this).closest('.dd-item').find('.options').slideToggle(200);
    });
  }

  function initItemRemoveButtons(){
    if(itemRemoveButtons)
      itemRemoveButtons.off('click');
    itemRemoveButtons = $('.dd-item .remove-item');
    itemRemoveButtons.on('click', function(){
      const thisID = $(this).closest('.dd-item').attr('data-id');
      const hasChildren = $('.dd-list .dd-item[data-id="' + thisID + '"] > ol').length;
      if(hasChildren){
        showWarning();
      } else {
        updateMenu(thisID, null);
      }
    });
  }
  }
});
