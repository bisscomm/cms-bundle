$.Class({
    namespace: 'page',

    getFields: function(params, element, event) {
       $.getJSON('/cms/template/' +$(element).val()+ '/fields', function (data) {
           console.log(data)
           var fieldsHtml = "";
           $.each(data.blocks, function () {
               block = this;
               fieldsHtml = fieldsHtml + '<hr>';

               $.each(block.fields, function() {
                   field = this;
                   fieldsHtml = fieldsHtml +
                   '<div class="form-group bmd-form-group is-filled">\n' +
                   '    <label class="form-group bmd-label-floating col-12 label-on-left required" for="page_field_'+block.tb_id+'_'+field.identifier+'">'+field.name+'</label>\n' +
                   '    <textarea id="page_field_'+block.tb_id+'_'+field.identifier+'" name="page[field]['+block.tb_id+']['+field.identifier+']" required="required" class="form-control form-control"></textarea>\n' +
                   '</div>'
               });
           });
           $(element).parent().after($(fieldsHtml));
        });
    },
    draftMode: function(params, element, event) {
        if ($(element).is(':checked')) {
            $('.publishedAt').attr("disabled", true);
        } else {
            $('.publishedAt').attr("disabled", false);
        }
    }

});
