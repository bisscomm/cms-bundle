$.Class({
    namespace: 'postcss',
    editor: null,
    hasChange: false,
    activeEditor: '_custom-utilities.scss',

    initEditor: function () {
        if ($('#scss_content').length > 0) {
            $('#scss_content').data('origine', $('#scss_content').html());
            $('#scss_content').data('origine', $('#scss_content').data('origine').replace(/amp;/g, ''));

            this.editor = CodeMirror.fromTextArea($('#scss_content').get(0), {
                mode: "text/x-scss",
                theme: "material",
                indentUnit: 4,
                lineNumbers: true,
                autoCloseBrackets: true,
                matchBrackets: true,
                lineWrapping: true,
                autoCloseTags: true,
                matchTags: true,
                styleActiveLine: true,
                scrollbarStyle: "overlay",
                height: '100%',
                viewportMargin: 0,
                extraKeys: {
                    "F6": "autocomplete",
                    "F5": function (cm) {
                        cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                        if (!$('body').hasClass('sidebar-mini')) {
                            $('#minimizeSidebar').trigger('click');
                        }
                        if (!$('body').hasClass('sidebar-mini')) {
                            $('#minimizeSidebar').trigger('click');
                            $('body').addClass('menuNotClicked');
                        }
                    },
                    "Esc": function (cm) {
                        if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                    },
                },
            });


            $('#select').on('change', function () {
                postcss.editor.setOption("theme", $('#select').val());
            })

            this.editor.setSize('100%', '100%')

            $('[data-name="' + postcss.activeEditor + '"]').addClass('active');
        }
    },

    save: function (params, element) {
        postcss.editor.save();
        let content = $('#scss_content').val();

        if (postcss.activeEditor === null) {
            postcss.activeEditor = '_custom-utilities.scss';
        }

        if (content != $('#scss_content').data('origine')) {
            postcss.hasChange = true
            $('#scss_content').data('origine', content)
        }

        if (postcss.hasChange) {
            let value = {
                content: content,
                fileName: postcss.activeEditor
            };
            let ajax = $.post('/cms/postcss/save', value);

            $(element).prop('disabled', true);
            $(element).addClass('loading');

            ajax.done(function (result) {
                $('.main-panel > .navbar').after()
                $('.load').prop('disabled', false);
                $('.load').removeClass('loading');
                postcss.hasChange = false;

                if (result.isValid) {
                    $.notify({
                        message: result.message
                    }, {
                        template: '<div data-notify="container" class="col-11 col-md-4 alert alert-{0} alert-with-icon" role="alert"><button type="button" aria-hidden="true" class="close" data-notify="dismiss"><i class="fa fa-close"></i></button><span data-notify="title">{1}</span> <span data-notify="message">{2}</span><div class="progress" data-notify="progressbar"><div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div></div><a href="{3}" target="{4}" data-notify="url"></a></div>',
                        type: 'success',
                        timer: 10000,
                        placement: {
                            from: 'bottom',
                            align: 'center'
                        }
                    });
                } else {
                    $.notify({
                        message: result.message
                    }, {
                        template: '<div data-notify="container" class="col-11 col-md-4 alert alert-{0} alert-with-icon" role="alert"><button type="button" aria-hidden="true" class="close" data-notify="dismiss"><i class="fa fa-close"></i></button><span data-notify="title">{1}</span> <span data-notify="message">{2}</span><div class="progress" data-notify="progressbar"><div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div></div><a href="{3}" target="{4}" data-notify="url"></a></div>',
                        type: 'danger',
                        timer: 3000,
                        placement: {
                            from: 'bottom',
                            align: 'center'
                        }
                    });
                }
            })
        }
    },

    getContent: function (params, element, event) {
        postcss.editor.save();
        let content = $('#scss_content').val();

        if (content !== $('#scss_content').data('origine')) {
            postcss.hasChange = true
        } else {
            postcss.hasChange = false
        }

        if (postcss.activeEditor != $(element).data('path')) {
            if (postcss.hasChange === false) {
                $('#blockCode').addClass('awaiting');
                postcss.activeEditor = $(element).data('path');
                $('[data-path]').removeClass('active');
                $('[data-path="' + postcss.activeEditor + '"]').addClass('active');
                $('h4.card-title').text(postcss.activeEditor)
                let value = {
                    'filePath': postcss.activeEditor
                }

                $.ajax({
                    type: 'POST',
                    url: '/cms/postcss/show',
                    data: value,
                    success: function (data) {
                        $('.bmd-form-group').remove();
                        $('.CodeMirror').remove();
                        $('.codeMirror-border-radius-wrap').prepend('<textarea id="scss_content">' + data + '</textarea>')
                        postcss.initEditor();
                        $('#blockCode').removeClass('awaiting');
                    }
                });
            } else {
                $.notify({
                    message: 'Vous avez des modifications non sauvegardées'
                }, {
                    template: '<div data-notify="container" id="unsaved" class="col-11 col-md-4 alert alert-{0} pl-4 d-flex justify-content-center align-items-center" role="alert">' +
                        '<span data-notify="title">{1}</span> <span class="d-flex mr-5" data-notify="message">{2}</span>' +
                        '<a href="{3}" target="{4}" data-notify="url"></a></div>',
                    type: 'danger',
                    timer: 10000,
                    placement: {
                        from: 'bottom',
                        align: 'center'
                    }
                });
            }
        }
    }
});









