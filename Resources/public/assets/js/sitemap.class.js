$.Class({
    namespace: 'sitemap',

    regenererSitemap:function (params, element, event) {

        event.preventDefault();
        let getBaseDomain = params[0];
        let regenerateSiteMap = params[1];

        $.ajax({
            url: getBaseDomain,
            type: "POST",
            dataType: "json",
            data: {
                'getBaseDomain': 'getBaseDomain',
            }
        }).done(function (data) {

            Swal.fire({
                title: 'Vous êtes sur le point de reconstruire la sitemap.',
                html: "<p>La sitemap sera regénérée avec le domaine:</p>" + "<b>"+data.baseUrl+"</b>",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: 'rgba(52,151,14,0.91)',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oui, regénérer la sitemap',
                cancelButtonText: 'Annuler',
                showLoaderOnConfirm: true,
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        setTimeout(function() {
                            resolve();
                        }, 1000);
                    });
                },
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: regenerateSiteMap,
                        type: "POST",
                        dataType: "json",
                        data: {
                            'performRegeneration': 'performRegeneration',
                        }
                    }).done(function (data) {

                        Swal.fire({
                            title: 'Consulter les nouvelle sitemaps',
                            html: "<h3>Sitemaps</h3>" +
                                  "<div><ul>"+"<li><a style='margin-left: -250px;' target='_blank' href='"+data.sitemapUrl.index+"'>"+"Sitemap Index"+"</a></li>" +
                                  "<li><a style='margin-left: -250px;' target='_blank' href='"+data.sitemapUrl.fr+"'>"+"Sitemap FR"+"</a></li>" +
                                  "<li><a style='margin-left: -250px;' target='_blank' href='"+data.sitemapUrl.en+"'>"+"Sitemap EN"+"</a></li>" +
                                  "</ul></div>",
                            icon: 'succes',
                            confirmButtonColor: 'rgb(29,39,76)',
                            confirmButtonText: 'Quitter',
                        })

                    }).fail(function (e) {
                        app.buildNotification(
                            'danger',
                            "" + e.responseText,
                            'top',
                            'right');
                    }).always(function () {

                    });
                }
            })

        }).fail(function (e) {
            app.buildNotification(
                'danger',
                "" + e.responseText,
                'top',
                'right');
        }).always(function () {

        });

    }
        

});
