$.Class({
    namespace: 'template',
    editor: null,
    gridValue: 0,
    drag: {
        sections: null,
        blocks: null
    },
    drop: {
        sections: null,
        blocks: null
    },
    count: {
        sections: 3,
        blocks: 0
    },
    structure: [],

    /**
     * Initialize template ui at page load
     */
    initTplDraft: function() {
        if ($('#tpl-draft').length > 0) {
            this.drag.blocks = $('.availBlocks');
            this.enableDroppable($('#tpl-draft .card-body[data-container-type="col"]'));
            this.enableDroppable($('#tpl-draft .card-body[data-container-type="row"]'));
            this.enableDroppable($('#tpl-draft .card-body[data-container-type="section"]'));
            this.enableDraggable($('.card[data-card-type="block"]', this.drag.blocks));
            $('.no-context-menu').contextmenu(function() {
                return false;
            })
        }
    },

    /**
     * Enable droppable zone
     * @param $element
     */
    enableDroppable: function($element) {
        $element.sortable({
            revert: true,
            placeholder: 'ui-state-highlight',
            handle: '.drag-handle',
            connectWith: $element.selector,
            // containment: "parent",
            tolerance: pointer,
            start: function(event, ui){
                ui.placeholder.height(ui.helper.height());
                ui.placeholder.width(ui.helper.width());
            }
        });
    },

    /**
     * Enable draggable container
     * @param $element
     */
    enableDraggable: function($element) {
        $element.draggable({
            connectToSortable: '#tpl-draft .card-body[data-container-type="col"], #tpl-draft .card-body[data-container-type="row"], #tpl-draft .card-body[data-container-type="section"]',
            revert: 'invalid', // when not dropped, the item will revert back to its initial position
            helper: 'clone',
            cursor: 'move',
            stop: function(event, ui) {
                if ($(ui.helper).find('.card-header .dropdown-menu').length === 0) {
                    $(ui.helper).removeClass('m-0');
                    $(ui.helper).find('.card-header').append($('#block_actions').html());
                }
            }
        });
    },

    /**
     * Enable resizable col system
     * @param $element
     */
    enableResizable: function ($element) {
        $element.resizable({
            grid: [$element.width()/12, 0]
        })
    },

    /**
     * Append Row to section
     *   or insert row before the current row
     *   or insert row after the current row
     * @param params
     * @param element
     * @param event
     */
    addRow: function (params, element, event) {
        var tplRow = $('#row_actions').html();

        switch (params) {
            case 'before':
                $(element).closest('.card-body[data-container-type="section"]').prepend(tplRow);
                break;
            case 'after':
                $(element).closest('.card-body[data-container-type="section"]').append(tplRow);
                break;
            default:
                $(element).closest('.card').find('.card-body[data-container-type="section"]').append(tplRow);
        }
    },

    addCol: function (params, element, event) {
        var colLen = 12;
        newCol = template.getColTemplate(colLen);

        $(element).closest('.card').find('.card-body[data-container-type="row"]').append(newCol);

        template.enableDroppable(newCol.find('.card-body[data-container-type="col"]'))
    },

    splitCol: function (params, element, event) {
        var colLen = Math.floor($(element).closest('.card').data('col')/2),
            newCol = template.getColTemplate(colLen);

        $(element).closest('.card').parent().attr('class', 'col-' + colLen);
        $(element).closest('.card').data('col', colLen);
        $(element).closest('.card').parent().after(newCol);

        template.enableDroppable(newCol.find('.card-body[data-container-type="col"]'))
    },

    configCol: function(params, element, event) {
        var col = $(element).closest('.card').data('col') || 12,
            cssClass = $(element).closest('.card').data('class') || '',
            attr = $(element).closest('.card').data('attr') || '',
            tpl = $('#col_modal').html()
                .replace(/__col_len__/g, col)
                .replace(/__col_class__/g, cssClass)
                .replace(/__col_attr__/g, attr);
        swal({
            title: $('#col_modal').data('title'),
            html: tpl,
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            $(element).closest('.card').data({
                'col': $('#swal2-content #col_len').val(),
                'class': $('#swal2-content #col_class').val(),
                'attr' : $('#swal2-content #col_attr').val()
            })

            if ($(element).closest('.card').parent().attr('class') != 'col-' + $('#swal2-content #col_len').val()) {
                $(element).closest('.card').parent().switchClass(
                    $(element).closest('.card').parent().attr('class'),
                    'col-' + $('#swal2-content #col_len').val(),
                    400
                );
            }
        })
    },

    configRow: function(params, element, event) {
        var cssClass = $(element).closest('.card').data('class') || '',
            attr = $(element).closest('.card').data('attr') || '',
            wrapper = $(element).closest('.card').data('wrapper') || 0,
            wrapperClass = $(element).closest('.card').data('wrapper-class') || 0,
            tpl = $('#row_modal').html()
                .replace(/__row_class__/g, cssClass)
                .replace(/__row_attr__/g, attr)
                .replace(/__row_wrapper__/g, wrapper == 0 ? '' : 'CHECKED="CHECKED"')
                .replace(/__row_wrapper_class__/g, wrapperClass);
        swal({
            title: $('#row_modal').data('title'),
            html:   tpl,
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            $(element).closest('.card').data({
                'class': $('#swal2-content #row_class').val(),
                'attr': $('#swal2-content #row_attr').val(),
                'wrapper' : $('#swal2-content #row_wrapper').is(':checked') ? 1 : 0,
                'wrapper-class': $('#swal2-content #row_wrapper_class').val()
            })
        });
    },

    configSection: function(params, element, event) {
        var cssClass = $(element).closest('.card').data('class') || '',
            attr = $(element).closest('.card').data('attr') || '',
            wrapper = $(element).closest('.card').data('wrapper') || 0,
            wrapperClass = $(element).closest('.card').data('wrapper-class') || 0,
            tpl = $('#section_modal').html()
                .replace(/__section_class__/g, cssClass)
                .replace(/__section_attr__/g, attr)
                .replace(/__section_wrapper__/g, wrapper == 0 ? '' : 'CHECKED="CHECKED"')
                .replace(/__section_wrapper_class__/g, wrapperClass);
        swal({
            title: $('#section_modal').data('title'),
            html:   tpl,
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            $(element).closest('.card').data({
                'class': $('#swal2-content #section_class').val(),
                'attr': $('#swal2-content #section_attr').val(),
                'wrapper' : $('#swal2-content #section_wrapper').is(':checked') ? 1 : 0,
                'wrapper-class': $('#swal2-content #rsection_wrapper_class').val()
            })
        });
    },

    removeRow: function(params, element, event) {
        $(element).closest('.card').fadeOut('slow', function () {
            $(this).remove();
        });
    },

    removeCol: function(params, element, event) {
        $(element).closest('.card').parent().fadeOut('slow', function () {
            $(this).remove();
        });
    },

    removeBlock: function(params, element, event) {
        $(element).closest('.card').fadeOut('slow', function () {
            $(this).remove();
        });
    },

    getColTemplate: function (colLen) {
        return $($('#col_actions').html().replace(/__colLength__/g, colLen));
    },

    inspectStructure: function(params, element, event) {
        var structure = {sections: []};
        $('#tpl-draft .card[data-card-type="section"]').each(function() {
            structure.sections.push({
                params: {
                    'name': $(this).data('name'),
                    'class': $(this).data('class') || '',
                    'attributes': $(this).data('attr') || '',
                    'wrapper': $(this).data('wrapper') || 0,
                    'wrapperClass': $(this).data('wrapper-class') || ''
                },
                'children': template.inspectChildren($(this), '>.card-body>.card[data-card-type]')
            });
        });

        return structure;
    },

    inspectChildren: function($element, cssSelector) {
        var children = [];

        $element.find(cssSelector).each(function() {
            switch ($(this).data('cardType')) {
                case 'row':
                    children.push(
                        template.inspectRow($(this))
                    );
                    break;
                case 'col':
                    children.push(
                        template.inspectCol($(this))
                    );
                    break;
                case 'block':
                    children.push(
                        template.inspectBlock($(this))
                    );
                    break;
            }
        });

        return children;
    },

    inspectRow: function($element) {
        return {
            params: {
                'type': 'row',
                'class': $element.data('class') || '',
                'attributes': $element.data('attr') || '',
                'wrapper': $element.data('wrapper') || 0,
                'wrapperClass': $element.data('wrapper-class') || ''
            },
            'children': template.inspectChildren($element, '>.card-body>.card[data-card-type], >.card-body>div>.card[data-card-type]')
        };
    },

    inspectCol: function($element) {
        return {
            'params' : {
                'type': 'col',
                'len': $element.data('col') || 12,
                'class': $element.data('class') || '',
                'attributes':$element.data('attr') || ''
            },
            'children': template.inspectChildren($element, '>.card-body>.card[data-card-type]')
        };
    },

    inspectBlock: function($element) {
        return {
            'params' : {
                'type': 'block',
                'id': $element.data('id'),
                'name': $element.data('name'),
                'link': $element.data('link') || ''
            }
        };
    },

    save: function(params, element, event) {
        event.preventDefault();

        $('#template_structure').val(JSON.stringify(template.inspectStructure()));

        if ($(element).valid()) {
            $(element).closest('form').submit();
        } else {

            swal({
                type: 'error',
                title: 'Oops...',
                text: 'Did you enter a name for the template!',
            })
        }

    }
});
