<?php

namespace Bci\CmsBundle\Service;

use Bci\CmsBundle\Entity\Block;
use Symfony\Component\Filesystem\Filesystem;

class BlockManager
{
    private $fs;
    private $container;
    private $rootTpl;
    public function __construct($container)
    {
        $this->fs = new Filesystem();
        $this->container = $container;

        $this->rootTpl = $this->container->get('kernel')->getRootDir(). '/../templates';
    }

    public function getBlockFolder()
    {
        if (!$this->fs->exists($this->rootTpl.'/blocks')) {
            $this->fs->mkdir($this->rootTpl.'/blocks');
        }

        return $this->rootTpl.'/blocks/';
    }

    public function writeBlock(Block $block)
    {
        $this->fs->dumpFile($this->getBlockFolder().'block_'.$block->getId().'.html.twig', $this->getBlockContent($block));
    }

    public function getBlockContent($block)
    {
        $content = "{# //".chr(13).
                   " # Block ID  : ".$block->getId().chr(13).
                   " # Block Name: ".$block->getName().chr(13).
                   " # Last modified from CMS: ".$block->getUpdatedAt()->format('Y-m-d H:i:s').chr(13).
                   " // #}".chr(13).
                   $block->getContent();


        return $content;
    }

    public function getFileBlockContent($block)
    {
        $content = '';
        if (file_exists($this->getBlockFolder().'block_'.$block->getId().'.html.twig')) {
            $content = explode(chr(13), file_get_contents($this->getBlockFolder().'block_'.$block->getId().'.html.twig'));
            if ($content[0] == '{# //') {
                $startRemove = false;
                for($line = count($content)-1; $line>=0; $line--) {
                    if ($startRemove || $content[$line] == ' // #}') {
                        unset($content[$line]);
                        $startRemove = true;
                    }
                }
            }

            $content = implode(chr(13), $content);
        }

        return $content;
    }
}