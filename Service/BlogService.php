<?php


namespace Bci\CmsBundle\Service;

use Bci\CmsBundle\Entity\Article;
use Bci\CmsBundle\Entity\ArticleCategory;
use Bci\CmsBundle\Entity\Blog;
use Bci\CmsBundle\Repository\ArticleCategoryRepository;
use Bci\CmsBundle\Repository\ArticleRepository;
use Bci\CmsBundle\Repository\BlogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Bundle\PaginatorBundle\KnpPaginatorBundle;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * =================
 * CLASS BLOGSERVICE
 * =================
 * Service use to access related data from the blog BLOGS, ARTICLES & CATEGORIES.
 * ORDERING DISPLAY AND PAGINATE.
 * Service can be used in twig. 
 * 
 * 
 * @package Bci\CmsBundle\Service
 */
class BlogService 
{
    protected $em;
    protected $paginator;
    protected $blogRepository;
    protected $articleRepository;
    protected $articleCategoryRepository;

    public function __construct(EntityManagerInterface $entityManager, PaginatorInterface $paginator,BlogRepository $blogRepository, ArticleRepository $articleRepository, ArticleCategoryRepository $articleCategoryRepository)
    {
        $this->em = $entityManager;
        $this->paginator = $paginator;
        $this->blogRepository = $blogRepository;
        $this->articleRepository = $articleRepository;
        $this->articleCategoryRepository = $articleCategoryRepository;
    }
    
    //=====================BLOGS==========================

    /**
     * @param $id
     * @return Blog|null
     */
    public function getBlogById($id, $locale)
    {
        return $this->blogRepository->findBlogById($id, $locale);
    }

    /**
     * @param $title
     * @return Blog[]
     */
    public function getBlogByTitle($title, $locale)
    {
        return $this->blogRepository->findBlogByTitle($title, $locale);
    }

    /**
     * @param Article $article
     * @return Blog[]
     */
    public function getBlogByArticle(Article $article, $locale)
    {
        return $this->blogRepository->findBlogByArticle($article, $locale);
    }
    
    
    //=====================ARTICLES==========================
    /**
     * @param Request $request
     * @param Blog $blog
     * @param bool $paginated
     * @param null $limit
     * @return \Knp\Component\Pager\Pagination\PaginationInterface|Article[]
     */
    public function getAllBlogArticles(Request $request, Blog $blog, bool $paginated, $limit = null)
    {
        $query = $this->articleRepository->getFindAllQuery($blog, $request->getLocale());

        if ($paginated)
        {
            return $this->paginateQuery($request, $query, $limit);
        }
        
        return $query->getResult();
    }

    /**
     * @param Request $request
     * @param $category
     * @param Blog $blog
     * @param bool $paginated
     * @param null $limit
     * @return \Knp\Component\Pager\Pagination\PaginationInterface|mixed
     */
    public function getBlogArticlesByCategories(Request $request, $category, Blog $blog, bool $paginated, $limit = null)
    {
        $query = $this->articleRepository->getFindByCategoryQuery($blog, $category , $request->getLocale());

        if ($paginated)
        {
            return $this->paginateQuery($request, $query, $limit);
        }

        return $query->getResult();
    }

    /**
     * @param Request $request
     * @param $year
     * @param Blog $blog
     * @param bool $paginated
     * @param null $limit
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     * @throws \Exception
     */
    public function getBlogArticlesByYear(Request $request, $year, Blog $blog, bool $paginated, $limit = null)
    {
        $query = $this->articleRepository->getFindByYearQuery($blog, $year, $request->getLocale());

        if ($paginated)
        {
            return $this->paginateQuery($request, $query, $limit);
        }

        return $query->getResult();
    }

    
    //=====================ARTICLES CATEGORIES==========================

    /**
     * @return mixed
     */
    public function getAvailableYear(Request $request, Blog $blog)
    {
        $year =  $this->articleRepository->findArticleYear($blog, $request->getLocale());
        return $year;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getAllAvailableCategories(Request $request, Blog $blog)
    {
        return $this->articleCategoryRepository->findAllAvailableCategories($blog, $request->getLocale());
    }
    

    /**
     * Take in a request, a query and a limit by page and paginate them, 
     * if limit isn't set the default is 10 by pages. 
     * @param $query
     * @param int $limit
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    private function paginateQuery(Request $request, $query, $limit = 10)
    {
        $all = $this->paginator->paginate(
        // Doctrine Query, not results
            $query,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            $limit
        );

        return $all;
    }
}