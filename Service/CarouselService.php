<?php


namespace Bci\CmsBundle\Service;

use Bci\CmsBundle\Entity\Carousel;
use Bci\CmsBundle\Entity\CarouselSlide;
use Bci\CmsBundle\Repository\CarouselRepository;
use Bci\CmsBundle\Repository\CarouselSlideRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * =================
 * CLASS CAROUSELSERVICE
 * =================
 * Service use to access related data from the CAROUSELS & CAROUSEL SLIDES.
 * ORDERING AND DISPLAY
 * Service can be used in twig.
 *
 *
 * @package Bci\CmsBundle\Service
 */
class CarouselService
{
    protected $em;
    protected $carouselRepository;
    protected $carouselSlideRepository;
    protected $translationService;

    public function __construct(EntityManagerInterface $entityManager, CarouselRepository $carouselRepository, CarouselSlideRepository $carouselSlideRepository, TranslationService $translationService)
    {
        $this->em = $entityManager;
        $this->carouselRepository = $carouselRepository;
        $this->carouselSlideRepository = $carouselSlideRepository;
        $this->translationService = $translationService;
    }


    /**
     * @param Request $request
     * @param $id
     * @return \ArrayIterator|\Traversable
     * @throws \Exception
     */
    public function getCarouselById(Request $request, $id)
    {
        $carouselSlides = new ArrayCollection($this->carouselSlideRepository->findSlidesByCarouselId($id, $request->getLocale()));
        return $this->filterDisplayableSlides($carouselSlides, $request->getLocale());
    }

    /**
     * @param ArrayCollection $carouselSlides
     * @return ArrayCollection
     * @throws \Exception
     * Filter slides by translation in current locale, published, not before date and not after date.
     * Sort slides by positions.
     */
    private function filterDisplayableSlides(ArrayCollection $carouselSlides, $locale)
    {
        $now = new \DateTime('now');
        $filteredCarouselSlides = $carouselSlides->filter(function (CarouselSlide $slide) use ($now, $locale)
        {
            return (
                (($now >= $slide->getStartAt()) || ($slide->getStartAt() === null)) &&
                (($now <= $slide->getEndAt()) || ($slide->getEndAt() === null)) &&
                ($slide->getStatus() === true) &&
                (array_key_exists($locale, $this->translationService->getEntityTranslations($slide)))
            );

        });
        $filteredCarouselSlides->getIterator()->uasort(function (CarouselSlide $first, CarouselSlide $second)
        {
            return (int)$first->getPosition() > (int)$second->getPosition() ? 1 : -1;
        });

        return $filteredCarouselSlides;
    }
}