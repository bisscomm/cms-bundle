<?php

namespace Bci\CmsBundle\Service;

use Bci\CmsBundle\Entity\Form;
use Bci\CmsBundle\Entity\FormItem;
use Bci\CmsBundle\Entity\FormSubscription;
use Bci\CmsBundle\Entity\FormSubscriptionAnswer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class FormManager
{
    private $em;
    private $router;
    private $currentForm;
    private $container;

    public function __construct(EntityManagerInterface $em, UrlGeneratorInterface $router, ContainerInterface $container)
    {
        $this->em = $em;
        $this->router = $router;
        $this->container = $container;
    }

    public function hasForm()
    {
        return $this->em->getRepository(Form::class)->findAll();
    }

    public function itemHasData($id)
    {
        $data = $this->em->getRepository(FormSubscriptionAnswer::class)->findBy(['item' => $id]);

        return count($data) > 0 ? true : false;
    }

    public function getForm($formToken, $locale)
    {
        return $this->em->getRepository(Form::class)->findOneBy(['token' => $formToken]);
    }

    public function getItems($formToken, $locale)
    {
        $form = $this->em->getRepository(Form::class)->findOneBy(['token' => $formToken]);

        return $form->getItemsByLocale($locale);
    }

    public function getSubscriptions()
    {
        $subscriptions = $this->em->getRepository(FormSubscription::class)->findBy(['statut' => null]);

        return count($subscriptions);
    }

    public function getItem($itemToken)
    {
        $item = $this->currentForm->getItems()->filter(
            function ($item) use ($itemToken) {
                return $item->getToken() == $itemToken;
            }
        );

        return $item->first();
    }

    public function getItemLabel($itemToken, $attr = [])
    {
        return $this->getLabel($this->getItem($itemToken), $attr);
    }

    public function getItemField($itemToken, $attr = [])
    {
        return $this->getField($this->getItem($itemToken), $attr);
    }

    public function formStart($formToken, $locale = 'fr', $attrs = [])
    {
        $this->currentForm = $this->getForm($formToken, $locale);

        if (!array_key_exists('action', $attrs))
        {
            $attrs['action'] = $this->router->generate('form_post_data', ['token' => $formToken, 'locale' => $locale]);
        }

        $idPart = !empty($this->currentForm->getHtmlId()) ? 'id="' . $this->currentForm->getHtmlId() . '"' : '';

        return '<form enctype="multipart/form-data" ' . $idPart . ' name="' . $formToken . '" method="POST" ' . $this->buildAttributes($attrs) . '>';
    }

    public function formEnd()
    {
        return '</form>';
    }

    public function getField(FormItem $formItem, $attrs = [])
    {
        switch ($formItem->getType()) {
            case 'text':
                return $this->getInputText($formItem, $attrs);
                break;
            case 'email':
                return $this->getInputEmail($formItem, $attrs);
                break;
            case 'date':
                return $this->getInputDate($formItem, $attrs);
                break;
            case 'textarea':
                return $this->getTextarea($formItem, $attrs);
                break;
            case 'select':
                return $this->getSelect($formItem, $attrs);
                break;
            case 'radio':
                return $this->getRadio($formItem, $attrs);
                break;
            case 'checkbox':
                return $this->getCheckbox($formItem, $attrs);
                break;
            case 'file':
                return $this->getInputFile($formItem, $attrs);
                break;
            default:
                return $this->getInputText($formItem, $attrs);
                break;
        }
    }

    public function getLabel($formItem, $attrs = [])
    {
        $attrs['class'] = (isset($attrs['class']) ? $attrs['class'] : '') . ' ' .  ($formItem->getRequired() ? 'required' : '');

        return '<label '. $this->buildAttributes($attrs) . ' for="' . $formItem->getForm()->getToken() . '_' . $formItem->getToken() . '">' . $formItem->getlabel() . '</label>';
    }

    public function getInputText($formItem, $attrs = [])
    {
        $required = $formItem->getRequired() ? 'required' : '';
        return '<input type="text"' . $required . $this->buildAttributes($this->buildIdName($formItem, $attrs)) . '/>';
    }

    public function getInputEmail($formItem, $attrs = [])
    {
        $required = $formItem->getRequired() ? 'required' : '';
        return '<input type="email"' . $required . $this->buildAttributes($this->buildIdName($formItem, $attrs)) . '/>';
    }

    public function getInputDate($formItem, $attrs = [])
    {
        $required = $formItem->getRequired() ? 'required' : '';
        return '<input type="date"' . $required . $this->buildAttributes($this->buildIdName($formItem, $attrs)) . '/>';
    }

    public function getTextarea($formItem, $attrs = [])
    {
        $attrs['class'] = (isset($attrs['class']) ? $attrs['class'] : '') . ' ' .  ($formItem->getRequired() ? 'required' : '');
        return '<textarea' . $this->buildAttributes($this->buildIdName($formItem, $attrs)) . '></textarea>';
    }

    public function getSelect($formItem, $attrs = [])
    {
        $optGroupDelemiter = '**';
        $optGroupOpen = false;

        $placeholderDelemiter = '##';

        $options = explode("\n", $formItem->getConfig());
        $optsHtml = '';
        $required = $formItem->getRequired() ? ' required' : '';
        foreach ($options as $option) {
            $option = trim($option);
            if ($this->startsWith($option, $placeholderDelemiter) && $this->endsWith($option, $placeholderDelemiter))
            {
                $optsHtml .= '<option value="" disabled selected hidden>'.str_ireplace($placeholderDelemiter, '', $option).'</option>';
            }
            elseif ($this->startsWith($option, $optGroupDelemiter))
            {
                if ($optGroupOpen)
                {
                    $optsHtml .= '</optgroup>';
                    $optGroupOpen = false;
                }
                else
                {
                    $optsHtml .= '<optgroup label="' . str_ireplace($optGroupDelemiter, '', $option) . '">';
                    $optGroupOpen = true;
                }
            }
            else
            {
                $optsHtml .= '<option value="' . $option . '">' . $option . '</option>';
            }
        }
        return '<select' . $required . $this->buildAttributes($this->buildIdName($formItem, $attrs)) . '>' . $optsHtml . '</select>';
    }

    public function getInputFile($formItem, $attrs = [])
    {
        $required = $formItem->getRequired() ? 'required' : '';
        return '<input type="file"' . $required . $this->buildAttributes($this->buildIdName($formItem, $attrs)) . '/>';
    }

    public function getRadio($formItem, $attrs = [])
    {
        $options = explode("\n", $formItem->getConfig());
        $optsHtml = '';
        $ctr = 0;
        foreach ($options as $option) {
            $option = trim($option);
            $optsHtml .= '<div class="choice"><input id="'
                . $formItem->getForm()->getToken() . '_' . $formItem->getToken() . '_' . $ctr . '" 
                type="radio" name="' . $formItem->getForm()->getToken() . '[' . $formItem->getToken() . ']' . '" value="' . $option . '"/><label for="' . $formItem->getForm()->getToken() . '_' . $formItem->getToken() . '_' . $ctr . '">' . $option . '</label></div>';
            $ctr++;
        }

        return $optsHtml;
    }

    public function getCheckbox($formItem, $attrs = [])
    {
        $options = explode("\n", $formItem->getConfig());

        $optsHtml = '';
        $ctr = 0;
        foreach ($options as $option) {
            $option = trim($option);
            $optsHtml .= '<div class="choice"><input id="' . $formItem->getForm()->getToken() . '_' . $formItem->getToken() . '_' . $ctr . '" type="checkbox"'.$this->buildAttributes( $attrs).' name="' . $formItem->getForm()->getToken() . '[' . $formItem->getToken() . '][]' . '" value="' . $option . '"/><label for="' . $formItem->getForm()->getToken() . '_' . $formItem->getToken() . '_' . $ctr . '">' . $option . '</label></div>';
            $ctr++;
        }

        return $optsHtml;
    }

    private function buildIdName($formItem, $attrs = [])
    {
        $attrs['id'] = $formItem->getForm()->getToken() . '_' . $formItem->getToken();
        $attrs['name'] = $formItem->getForm()->getToken() . '[' . $formItem->getToken() . ']';
        $attrs['placeholder'] = $formItem->getHelp();

        return $attrs;
    }

    public function buildAttributes($attrs = [])
    {
        $attributes = '';

        foreach ($attrs as $key => $val) {
            $attributes .= ' ' . $key . '="' . $val . '"';
        }

        return $attributes;
    }


    public function getRecaptcha($formToken)
    {
        $siteKey = isset($this->container->getParameter('bci_cms')['recaptcha']['siteKey'])
            ? $this->container->getParameter('bci_cms')['recaptcha']['siteKey']
            : false;

        if ($siteKey) {
            $twig = $this->container->get('twig');
            return $twig->render('@BciCms/Layout/_partial/_recaptcha.html.twig', ['siteKey' => $siteKey, 'formToken' => $formToken]);
        }

        return false;
    }


    public function getRecaptchaValidation()
    {
        if(
            isset($this->container->getParameter('bci_cms')['recaptcha']['secretKey']) &&
            isset($this->container->getParameter('bci_cms')['recaptcha']['siteKey'])
        )
        {
            $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
            $recaptcha_secret = $this->container->getParameter('bci_cms')['recaptcha']['secretKey'];
            $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $_POST['recaptcha_response']);
            $recaptcha = json_decode($recaptcha);

            if (isset($recaptcha->score) && $recaptcha->score >= 0.5)
            {
                return true;
            }
        } else {
            return true;
        }

        return false;
    }

    private function startsWith( $haystack, $needle ) {
        $length = strlen( $needle );
        return substr( $haystack, 0, $length ) === $needle;
    }

    private function endsWith( $haystack, $needle ) {
        $length = strlen( $needle );
        if( !$length ) {
            return true;
        }
        return substr( $haystack, -$length ) === $needle;
    }
}
