<?php


namespace Bci\CmsBundle\Service;


use Bci\CmsBundle\Entity\Form;
use Bci\CmsBundle\Entity\FormSubscription;
use Bci\CmsBundle\Entity\Job;
use Bci\CmsBundle\Entity\JobCategory;
use Bci\CmsBundle\Entity\Website;
use Bci\CmsBundle\Repository\FormSubscriptionRepository;
use Bci\CmsBundle\Repository\JobRepository;
use Bci\CmsBundle\Repository\FormRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Bundle\PaginatorBundle\KnpPaginatorBundle;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Twig\Environment;

class JobService
{

    private $em;
    private $paginator;
    private $jobRepository;
    private $templating;
    protected $router;
    protected $translationService;
    private $careerFormID;

    public function __construct(EntityManagerInterface $entityManager, PaginatorInterface $paginator, JobRepository $jobRepository, Environment $templating, UrlGeneratorInterface $router, TranslationService $translationService, ParameterBagInterface $parameterBag)
    {
        $this->em = $entityManager;
        $this->paginator = $paginator;
        $this->jobRepository = $jobRepository;
        $this->templating = $templating;
        $this->router = $router;
        $this->translationService = $translationService;
        $this->careerFormID = $parameterBag->get('bci_cms')['default_job']['career_form_id'];
    }

    /**
     * @param Request $request
     * @return string|string[]
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function generateJobForm(Request $request, $slug)
    {
        $locale = $request->getLocale();

        /**
         * @var $job Job
         */
        $job = $this->jobRepository->findBySlugLocale($slug, $locale);
        if ($job)
        {

            $job->setTranslatableLocale($locale);
            $this->em->refresh($job);

            //Render form in a variable.
            $viewHtmlContent = $this->templating->render('job/_partial/_form.html.twig', [
                'job' => $job,
                'locale' => $locale,
                'selectHidden' => true
            ]);


            //Find the form job's selectPicker and tick it to the right job.
            $viewHtmlContent = str_replace('<option value="' . $job->getTitle() . '">', '<option selected value="' . $job->getTitle() . '">', $viewHtmlContent);
        }
        else
        {
            $viewHtmlContent = "";
        }
        return $viewHtmlContent;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function generateJobsForm(Request $request)
    {
        $locale = $request->getLocale();

        //Render form in a variable.
        $viewHtmlContent = $this->templating->render('job/_partial/_form.html.twig', [
            'locale' => $locale
        ]);

        return $viewHtmlContent;
    }

    /**
     * @param Request $request
     * @return Job[]
     * Filter Jobs by translation in current locale, published, not before date and not after date.
     */
    public function getAvailableJobs(Request $request, int $maxResults = null)
    {
        $locale = $request->getLocale();
        $availableJobs = $this->jobRepository->findAllByLocale($locale, $maxResults);
        foreach ($availableJobs as $job)
        {
            $job->setTranslatableLocale($locale);
            $this->em->refresh($job);
        }
        $availableJobs = new ArrayCollection($availableJobs);
        $now = new \DateTime('now');
        $availableJobs = $availableJobs->filter(function (Job $job) use ($now, $locale)
        {
            return (
                (($now >= $job->getStartAt()) || ($job->getStartAt() === null)) &&
                (($now <= $job->getEndAt()) || ($job->getEndAt() === null)) &&
                ($job->getStatus() === true) &&
                ($job->getSlug()) &&
                (!$job->getOnlyShowInForm()) &&
                (array_key_exists($locale, $this->translationService->getEntityTranslations($job)))
            );
        });
        $availableJobs->getIterator()->uasort(function (Job $first, Job $second)
        {
            return $first->getCreatedAt() > $second->getCreatedAt() ? 1 : -1;
        });

        return $availableJobs;
    }


    /**
     * @param $jobCategoryIdOrTitle
     * @param Request $request
     * @param int|null $maxResults
     * @return ArrayCollection
     * @throws \Exception
     * @return Job[]
     * Filter Jobs that have the same category, by translation in current locale, published, not before date and not after date.
     */
    public function getAvailableJobsByCategory($jobCategoryIdOrTitle, Request $request, int $maxResults = null)
    {
        // If the value is not numeric perhaps not an ID
        // Try to find the category by title
        // and set its id to call findAllByCategoryLocale()
        if (!is_numeric($jobCategoryIdOrTitle))
        {
            $jobCategory = $this->em->getRepository(JobCategory::class)->findByTitleLocale($jobCategoryIdOrTitle, $request->getLocale());
            if ($jobCategory)
            {
                $jobCategoryIdOrTitle = $jobCategory->getId();
            }
            else
            {
                $availableJobs = null;
            }
        }
        if ($jobCategoryIdOrTitle)
        {
            $locale = $request->getLocale();
            $availableJobs = $this->jobRepository->findAllByCategoryLocale($jobCategoryIdOrTitle, $locale, $maxResults);
            foreach ($availableJobs as $job)
            {
                $job->setTranslatableLocale($locale);
                $this->em->refresh($job);
            }
            $availableJobs = new ArrayCollection($availableJobs);
            $now = new \DateTime('now');
            $availableJobs = $availableJobs->filter(function (Job $job) use ($now, $locale)
            {
                return (
                    (($now >= $job->getStartAt()) || ($job->getStartAt() === null)) &&
                    (($now <= $job->getEndAt()) || ($job->getEndAt() === null)) &&
                    ($job->getStatus() === true) &&
                    ($job->getSlug()) &&
                    (!$job->getOnlyShowInForm()) &&
                    (array_key_exists($locale, $this->translationService->getEntityTranslations($job)))
                );
            });
            $availableJobs->getIterator()->uasort(function (Job $first, Job $second)
            {
                return $first->getCreatedAt() > $second->getCreatedAt() ? 1 : -1;
            });
        }
        return $availableJobs;
    }

    /**
     * @param Request $request
     * @param ArrayCollection $entityToPAginate
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function paginate(Request $request, ArrayCollection $entityToPaginate)
    {

        $all = $this->paginator->paginate(
        // Doctrine Query, ArrayCollection
            $entityToPaginate,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            7
        );

        return $all;
    }

    /**
     * @return bool
     * If the website repository is empty,
     * that means that the job module isn't used as a centralize API.
     *
     */
    public function isUsedAsApi()
    {
        return count($this->em->getRepository(Website::class)->findAll()) > 0;
    }


    /**
     * @param Job $job
     * @param Request $request
     * @return string
     */
    public function generateJobUrl(Job $job, Request $request)
    {
        $jobUrl = $this->router->generate('bci_cms_job_show_' . $request->getLocale(), [
            'slug' => $job->getSlug()
        ],
            UrlGeneratorInterface::ABSOLUTE_URL);

        return $jobUrl;
    }


    /**
     * @param Job $job
     * @param Request $request
     * @return int Numbers of unseen subscriptions
     */
    public function jobFormSubscriptionsNotifications(Job $job)
    {
        //Get the form associated with jobs
        $careerForm = $this->em->getRepository(Form::class)->find($this->careerFormID);

        //Get the subscription based on aswered values (EN/FR)
        $subscriptions = new ArrayCollection($this->findJobSubscriptionsBillingual($job));
        $subscriptionsInfo = [
            'newSubs' => 0,
            'subsCount' => 0
        ];
        $subscriptionsInfo['subsCount'] = $subscriptions->count();
        $subscriptionsInfo['newSubs'] = $subscriptions->filter(function(FormSubscription $formSubscription){
                return $formSubscription->getStatut() == null;
            }
        )->count();
        
        return $subscriptionsInfo;
    }
    
    /**
     * @param Job $job
     * @return array
     */
    private function findJobSubscriptionsBillingual(Job $job)
    {
        $formSubscriptionRepository = $this->em->getRepository(FormSubscription::class);
        $locales = ['fr','en'];
        $subscriptions = [];
        foreach ($locales as $locale)
        {
            //Get the subscription based on aswered values (EN/FR)
            $job->setTranslatableLocale($locale);
            $this->em->refresh($job);
            $subscriptions[$locale] = $formSubscriptionRepository->createQueryBuilder('f')
                ->innerJoin('f.answers', 'fanswers')
                ->andWhere('fanswers.value = :jobTitle')
                ->andWhere('f.form = :formId')
                ->setParameter('formId', $this->careerFormID)
                ->setParameter('jobTitle', $job->getTitle())
                ->getQuery()
                ->getResult();
        }
        $subscriptions = array_merge($subscriptions['fr'],$subscriptions['en']);

        return $subscriptions;
    }

}