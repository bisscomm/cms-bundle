<?php

namespace Bci\CmsBundle\Service;

use Bci\CmsBundle\Entity\Article;
use Bci\CmsBundle\Entity\Blog;
use Bci\CmsBundle\Entity\Job;
use Bci\CmsBundle\Entity\Meta;
use Bci\CmsBundle\Entity\Page;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class MetaService
{
    private $em;

    private $parameterBag;

    private $uploaderHelper;


    public function __construct(EntityManagerInterface $em, ParameterBagInterface $parameterBag, UploaderHelper $uploaderHelper)
    {
        $this->em = $em;
        $this->parameterBag = $parameterBag;
        $this->uploaderHelper = $uploaderHelper;
    }

    /**
     * @return array
     */
    public function generateMetasTagsFromYaml()
    {
        $metaTags = Yaml::parseFile(__DIR__ . '/../Resources/config/metatag.yaml');
        $metaTypeChoices = array();
        foreach ($metaTags['metatag']['type'] as $type => $tags)
        {
            foreach ($tags as $displayName => $tag)
            {
                $newMeta = new Meta();
                $newMeta
                    ->setType(mb_strtoupper($type))
                    ->setDisplayName($displayName)
                    ->setAccessor($type . "|" . $displayName);
                $type === 'opengraph' ? $newMeta->setProperty($tag['property']) : $newMeta->setName($tag['name']);

                $metaTypeChoices[$newMeta->getAccessor()] = $newMeta;
            }
        }

        return $metaTypeChoices;
    }

    public function searchToReplace( $text, Request $request, $entity)
    {
        if ($text)
        {
            $replacor = $this->tagsToReplace($entity, $request);
            if ($replacor)
            {
                $text = str_replace(array_keys($replacor), $replacor, $text);
            }
        }
        return $text;
    }

    private function tagsToReplace($entity, Request $request )
    {
        if (is_object($entity))
        {
            switch (get_class($entity))
            {
                case Page::class:
                    $toReplace = [
                        '[page.title]' => $entity->getName(),
                        '[page.tabtitle]' => $entity->getHeadingLocale($request->getLocale())->getTitle(),
                        '[page.url]' => $entity->getSlug(),
                    ];
                    break;
                case Article::class:
                    $toReplace = [
                        '[article.title]' => $entity->getTitle(),
                        '[article.summary]' => $entity->getContentSummary(),
                        '[article.image]' => $this->imagesPathGenerator($entity, $request)
                    ];
                    break;
                case Job::class:
                    $toReplace = [
                        '[job.title]' => $entity->getTitle(),
                        '[job.summary]' => $entity->getContentSummary()
                    ];
                    break;
                default:
                    $toReplace = null;
                    break;
            }
        }
        else
        {
            $toReplace = null;
        }
        return $toReplace;
    }

    public function imagesPathGenerator($entity, Request $request)
    {
        if (is_array($entity))
        {
            $path = $request->getSchemeAndHttpHost();
            $path .= $this->uploaderHelper->asset($entity['image'], 'tempFile');
        }
        elseif ($entity->getImage())
        {
            $path = $request->getSchemeAndHttpHost();
            $path .= $this->uploaderHelper->asset($entity->getImage(), 'tempFile');
        }

        return isset($path) ? $path : null;
    }

//    private function tagsToReplace($entity, string $locale )
//    {
//        $toReplace = [];
//        switch (get_class($entity))
//        {
//            case Page::class:
//                $toReplace = [
//                    '[page.title]' => $entity->getName(),
//                    '[page.tabtitle]' => $entity->getHeadingLocale($locale)->getTitle(),
//                    '[page.url]' => $page->getSlug(),
//                ];
//                break;
//            case Blog::class:
//                $toReplace = [
//                    '[blog.title]' => $page->getName(),
//                    '[page.tabtitle]' => $page->getHeadingLocale($locale)->getTitle(),
//                    '[page.url]' => $page->getSlug(),
//                ];
//        }
//
//
//        return $toReplace;
//    }

//    public function getGenericMetaImage(Request $request)
//    {
//        $finder = new Finder();
//        $finder
//            ->name(['*.jpg', '*.svg', '*.jpeg', '*.png', '*.gif','*.bmp' ]);
//        $finder
//            ->sortByType();
//        $finder->in($this->parameterBag->get('full_generic_meta_directory'));
//        return $finder->hasResults() ? $request->getSchemeAndHttpHost().$this->parameterBag->get('generic_meta_directory').$finder->getIterator()->current()->getFilename() : 'NO_IMAGE';
//
//    }

// ********************************** VERSION 2.0 EN/FR **********************************
    public function getGenericMetaImage(Request $request)
    {
        $finder = new Finder();
        $localFolder = $request->getLocale();

        if ($finder->in($this->parameterBag->get('full_generic_meta_directory').'/')->directories()->count() > 0)
        {
            $finder = new Finder();
            $finder
                ->name(['*.jpg', '*.svg', '*.jpeg', '*.png', '*.gif','*.bmp' ]);
            $finder
                ->sortByType();
            $finder->in($this->parameterBag->get('full_generic_meta_directory').'/'.$localFolder);
            return $finder->hasResults() ? $request->getSchemeAndHttpHost().$this->parameterBag->get('generic_meta_directory').$localFolder.'/'.$finder->getIterator()->current()->getFilename() : 'NO_IMAGE';
        }
        else{
            return 'NO_IMAGE';
        }
    }
}


