<?php

namespace Bci\CmsBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor;

class MockerService
{
    private $propertyInfoExtractor;
    private $phpDocExtractor;
    private $reflectionExtractor;
    private $doctrineExtractor;
    private $em;

    /**
     * MockerService constructor.
     * @param EntityManager         $em
     */
    public function __construct( EntityManagerInterface $em )
    {
        $this->reflectionExtractor   = new ReflectionExtractor();
        $this->doctrineExtractor     = new DoctrineExtractor($em);
        $this->propertyInfoExtractor = new PropertyInfoExtractor([
                $this->reflectionExtractor,
                $this->doctrineExtractor,
            ], [
                $this->doctrineExtractor,
                $this->reflectionExtractor,
            ]
        );

        $this->em = $em;

    }

    public function create($object, $ns = 'App\\Entity\\')
    {
        $entity = $this->initClass($ns.$object);

        return $this->buildMock($entity);;
    }

    private function initClass($class)
    {
        return new $class();
    }

    private function buildMock($entity, $parent = null)
    {
        foreach ($this->propertyInfoExtractor->getProperties($entity) as $prop) {
            $fct = null;
            $ph = $this->generatePlaceHolderFor(
                $entity,
                $prop
            );
            switch(true) {
                case method_exists($entity, 'set'.ucfirst($prop)):
                    $fct = 'set';
                  //  $entity->{$fct.ucfirst($prop)}($ph);
                    break;
                case method_exists($entity, 'add'.ucfirst($prop)):
                    $fct = 'add';
                    if ($ph) {
                        $entity->{$fct . ucfirst($prop)}($ph);
                    }
                    break;
            }
        }

        return $entity;
    }

    private function generatePlaceHolderFor($entity, $prop)
    {
        $definition = $this->propertyInfoExtractor->getTypes(get_class($entity), $prop);
        $ph = '';

        if(!isset($definition[0])) {
            if ($definition[0] == null) {
                var_dump($definition, $prop);
            } else {
                switch (true) {
                    case $definition[0]->getBuiltinType() == 'int':
                        $ph = 0;
                        break;
                    case $definition[0]->getBuiltinType() == 'string' && $prop != "locale":
                        $ph = "[ Placeholder $prop ... ]";
                        break;
                    case $definition[0]->getBuiltinType() == 'string' && $prop == "locale":
                        $ph = "fr";
                        break;
                    case $definition[0]->getBuiltinType() == 'array' && $definition[0]->isCollection() :
                        $subEntity = $this->initClass($definition[0]->getCollectionValueType()->getClassName());
                        if ($subEntity instanceof $entity) {
                            $ph = $entity;
                        } else {
                            $ph = $this->buildMock($subEntity, $entity);
                        }
                        break;
                    case $definition[0]->getBuiltinType() == 'array' && !$definition[0]->isCollection() :
                        $ph = [];
                        break;
                    case $definition[0]->getBuiltinType() == 'bool':
                        $ph = false;
                        break;
                    case $definition[0]->getBuiltinType() == 'object' && $definition[0]->isCollection():
                        $ph = null;
                        break;
                }
            }
        }

        return $ph;
    }
}

#