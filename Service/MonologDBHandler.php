<?php
namespace Bci\CmsBundle\Service;

use Bci\CmsBundle\Entity\Log;
use Doctrine\ORM\EntityManagerInterface;
use Bci\CmsBundle\Service\ExceptionHtmlFormatter;
use Monolog\Formatter\HtmlFormatter;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;

class MonologDBHandler extends AbstractProcessingHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(EntityManagerInterface $em, $level = Logger::CRITICAL, $bubble = true)
    {
        $this->em = $em;
        parent::__construct($level, $bubble);
        $this->setFormatter(new ExceptionHtmlFormatter());
    }

    /**
     * Called when writing to our database
     * @param array $record
     */
    protected function write(array $record):void
    {
        try {
            $logEntry = new Log();
            $logEntry
                ->setMessage($record['message'])
                ->setLevel($record['level'])
                ->setLevelName($record['level_name'])
                ->setChannel($record['channel'])
                ->setFormatted($record['formatted']);

//            if(is_array($record['extra']))
//            {
//                $logEntry
//                    ->setFile($record['extra']['file'])
//                    ->setLine($record['extra']['line'])
//                    ->setClass($record['extra']['class'])
//                    ->setFunction($record['extra']['function'])
//                    ->setUrl($record['extra']['url'])
//                    ->setIp($record['extra']['ip'])
//                    ->setHttpMethod($record['extra']['http_method'])
//                    ->setServer($record['extra']['server'])
//                    ->setReferrer($record['extra']['referrer']);
//            }
//
//            if (is_array($record['context']))
//            {
//                $logEntry->setStackTrace($record['context']['exception']->getTraceAsString());
//            }
            
            $this->em->persist($logEntry);
            $this->em->flush();
        } catch (\Exception $e)
        {
        }
    }

}