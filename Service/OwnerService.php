<?php


namespace Bci\CmsBundle\Service;



use Bci\CmsBundle\Repository\OwnerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class OwnerService
{

    private $owner;
    private $entityManager;

    public function __construct(OwnerRepository $ownerRepository, EntityManagerInterface $entityManager)
    {
        $this->owner = $ownerRepository->findAll()[0];
        $this->entityManager = $entityManager;
    }


    /**
     * @param $locale
     * @return \Bci\CmsBundle\Entity\Owner
     */
    public function getOwnerLocale($locale)
    {
        $this->owner->setTranslatableLocale($locale);
        $this->entityManager->refresh($this->owner);

        return $this->owner->setLogoToDysplay($locale);
    }

}