<?php

namespace Bci\CmsBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use ScssPhp\ScssPhp\Compiler;
use Symfony\Component\Finder\Finder;

class PostCssService
{
    private $fs;
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->fs = new Filesystem();
        $this->compiler = new Compiler();
        $this->container = $container;
        $this->rootDir = $this->container->get('kernel')->getRootDir() . '/../';

        $this->scss = $this->rootDir . 'assets/scss/_partials/';
        $this->theme = $this->rootDir . 'assets/scss/style.scss';
        $this->publicFolder = $this->rootDir . 'public/build/app.css';
    }


    public function getScss($file = '_custom-utilities.scss')
    {
        return file_get_contents($this->scss . $file);

    }

    public function compileScss($file, $scss)
    {
        $this->fs->dumpFile($this->scss.$file, $scss);
        $this->compiler->setImportPaths($this->rootDir . 'assets/scss/');

        $css = $this->compiler->compile(file_get_contents($this->rootDir . 'assets/scss/style.scss'));
        $this->fs->dumpFile($this->rootDir . 'public/build/app.css', $css);
    }


    function listFiles()
    {
        $finder = new Finder();
        $finder->files()->in($this->scss);
        $files = array();
        foreach ($finder as $file) {
            $files[] = [
                'relativePathname' => $file->getRelativePathname(),
                'fileName' => $file->getFilename()
            ];
        }
        return $files;
    }
}
