<?php

namespace Bci\CmsBundle\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Postmark\PostmarkClient;
use Postmark\Models\PostmarkException;

class PostMarkService
{

    
    private $postMarkClient;
    
    private $from;
    
    private $to;
    
    private $replyTo;
    
    public function __construct(ParameterBagInterface $parameterBag)
    {
        if (array_key_exists("postmark", $parameterBag->get('bci_cms')))
        {
            $this->postMarkClient = new PostmarkClient($parameterBag->get('bci_cms')['postmark']['serverAPItokens']);

            $this->from = $parameterBag->get('bci_cms')['postmark']['from'];

            $this->to = $parameterBag->get('bci_cms')['postmark']['to'];

            $this->replyTo = $parameterBag->get('bci_cms')['postmark']['replyTo'];
        }
        else
        {
            $this->postMarkClient = new PostmarkClient('NO_KEY');
            $this->from = NULL;
            $this->to = NULL;
            $this->replyTo = NULL;
        }

    }

    /**
     * @param null $from
     * @param null $to
     * @param null $subject
     * @param null $htmlBody
     * @param null $tag
     * @param null $trackOpens
     * @param null $replyTo
     * @param null $trackLinks
     * @param null $messageStream
     */
    public function send($from = null, $to = null, $subject = null, $htmlBody = null, $tag = null, $trackOpens = null, $replyTo = null, $trackLinks = null, $messageStream = null)
    {
        if (!$from)
        {
            $from = $this->from;
        }
        if (!$to)
        {
            $to = $this->to;
        }
        if (!$replyTo)
        {
            $replyTo = $this->replyTo;
        }
        
        // Send an email:
        $sendResult = $this->postMarkClient->sendEmail(
            $from,
            implode(',', $to),
            $subject,
            $htmlBody,
            NULL, // Text Body
            $tag,
            $trackOpens,
            NULL, // Reply To
            NULL, // CC
            NULL, // BCC
            NULL, // Header array
            NULL, // Attachment array
            $trackLinks,
            NULL, // Metadata array
            $messageStream
        );
        
        return $sendResult;
    }
}