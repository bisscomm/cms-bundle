<?php


namespace Bci\CmsBundle\Service;

use Bci\CmsBundle\Entity\Configuration;
use Bci\CmsBundle\Entity\ScriptTag;
use Bci\CmsBundle\Repository\ConfigurationRepository;
use Doctrine\ORM\EntityManagerInterface;


class ScripTagsService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var ConfigurationRepository
     */
    protected $configurationRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;

        $this->configurationRepository = $this->em->getRepository(Configuration::class);
    }

    
    public function getConfigurationScriptTagsByPosition($position)
    {
        return $this->configurationRepository->findTheConfiguration()->getScriptTagsByPosition($position);
    }
}