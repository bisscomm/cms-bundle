<?php

namespace Bci\CmsBundle\Service;


use Symfony\Component\DependencyInjection\ContainerInterface;

class SendGridService
{
    private $mailer;
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        if (isset($this->container->getParameter('bci_cms')['sendgrid']))
        {
            $this->mailer = new \SendGrid($this->container->getParameter('bci_cms')['sendgrid']['apikey']);
        }
    }

    public function send($from, $to, $subject, $contents)
    {
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom($from['email'], $from['name']);
        $email->setSubject($subject);
        $email->addTo($to['email'], $to['name']);

        foreach ($contents as $content) {
            $email->addContent($content['type'], $content['body']);
        }

        try {
            $response = $this->mailer->send($email);

            return [
                'success' => true,
                'message' => $response->statusCode()." : ". $response->body()
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => 'Caught exception: '. $e->getMessage()
            ];
        }
    }
}