<?php


namespace Bci\CmsBundle\Service;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SitemapService
{

    private $router;
    private $httpClient;

    public function __construct(UrlGeneratorInterface $router, HttpClientInterface $httpClient)
    {
        $this->router = $router;
        $this->httpClient = $httpClient;
    }


    /**
     * Ping Google to ask for a reindex of the sitemap.
     */
    public function sendSitemapToGoogle()
    {
        try
        {
            $host = $this->router->getContext()->getHost();
            if ($host)
            {
                $response = $this->httpClient->request(
                    'GET',
                    'https://www.google.com/ping?sitemap=https://' . $host . '/sitemap.xml'
                );
                $statusCode = $response->getStatusCode();
            }
        }catch (TransportExceptionInterface $exception)
        {
            $statusCode = 'ERROR: '.$exception->getCode().' MESSAGE: '.$exception->getMessage();
            // Todo notice user and/or admin
        }
        
        return $statusCode;
    }
}