<?php

namespace Bci\CmsBundle\Service;

use Bci\CmsBundle\Entity\Template;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\Filesystem\Filesystem;

class TemplateManager
{
    private $fs;
    private $container;
    private $rootTpl;
    private $ctr;
    public function __construct($container)
    {
        $this->ctr = 0;
        #echo '<pre>';print_r($container->getParameterBag()); die();
        $this->fs = new Filesystem();
        $this->container = $container;

        $this->rootTpl = $this->container->get('kernel')->getRootDir(). '/../templates';
    }

    public function getTemplateFolder()
    {
        if (!$this->fs->exists($this->rootTpl.'/templates')) {
            $this->fs->mkdir($this->rootTpl.'/templates');
        }

        return $this->rootTpl.'/templates/';
    }

    public function writeTemplate(Template $template)
    {
        $this->fs->dumpFile($this->getTemplateFolder().'template_'.$template->getId().'.html.twig', $this->getContent($template));
    }

    public function getContent(Template $template)
    {
        $structures = $template->explodeStructure();
        $content    = "{#".$this->nl().
            " # Template ID   : ".$template->getId().$this->nl().
            " # Template name : ".$template->getName().$this->nl().
            " #}".$this->nl().
            "{% extends 'Layout/layout.html.twig' %}".
            $this->nl().$this->nl()."{% block body %}".$this->inc();

        foreach ($structures['sections'] as $section) {
            if (isset($section['children']))
                $content = $content . $this->createSection($section);
        }

        return $content.$this->dec().'{% endblock %}';
    }

    private function createSection($section)
    {

        $sectionHtml = '';
        $children = [];
        $blockName = $section['params']['name'];
        $sectionType = $section['params']['name'];
        $startBlock = '{% block '.$blockName.' %}'.$this->inc();

        $sectionHtml = '<'.$sectionType.' class="'.
            (isset($section['params']['class']) ? $section['params']['class'] : '').'" '.
            (isset($section['params']['attributes']) ? $section['params']['attributes'] : '').
            ">".$this->inc();

        foreach ($section['children'] as $child) {
            $children[] = $this->{'create'.ucfirst($child['params']['type'])}($child);
        }

        $sectionHtml = $sectionHtml.implode($this->tab(), $children);

        $sectionHtml = $sectionHtml.$this->dec().'</'.$sectionType.">";

        if (isset($section['params']['wrapper']) && $section['params']['wrapper'] > 0) {
            if (strpos($section['params']['wrapperClass'], '|') !== false) {
                $wrappers = explode('|', $section['params']['wrapperClass']);
                foreach(array_reverse($wrappers) as $wrapperClass) {
                    $sectionHtml = '<div class="'.$wrapperClass.'">'.$this->nl().$sectionHtml.$this->nl()."</div>";
                }
            } else {
                $sectionHtml = '<div class="'.$row['params']['wrapperClass'].'">'.$this->nl().$sectionHtml.$this->nl().'</div>';
            }
        }

        return $startBlock.$sectionHtml.$this->dec().'{% endblock %}';
    }

    private function createRow($row)
    {
        $rowHtml = '';
        $children = [];

        if ($row['params']['wrapper'] > 0) {
            if (strpos($row['params']['wrapperClass'], '|') !== false) {
                $wrappers = explode('|', $row['params']['wrapperClass']);
                foreach(array_reverse($wrappers) as $wrapperClass) {
                    $rowHtml = '<div class="'.$wrapperClass.'">'.$this->inc().$rowHtml;
                }
            } else {
                $rowHtml = '<div class="'.$row['params']['wrapperClass'].'">'.$this->inc().$rowHtml;
            }
        }

        $rowHtml = $rowHtml. '<div class="row '.
            (isset($row['params']['class']) ? $row['params']['class'] : '').'" '.
            (isset($row['params']['attributes']) ? $row['params']['attributes'] : '').
            '>'.$this->inc();

        foreach ($row['children'] as $child) {
            $children[] = $this->{'create'.ucfirst($child['params']['type'])}($child);
        }

        if (count($children) > 1 )
            $rowHtml = $rowHtml.implode($this->tab(), $children).$this->dec().'</div>';
        elseif (count($children) == 1 )
            $rowHtml= $rowHtml.$children[0].$this->dec().'</div>';
        else
            $rowHtml = $rowHtml.$this->dec().'</div>';

        if ($row['params']['wrapper'] > 0) {
            if (strpos($row['params']['wrapperClass'], '|') !== false) {
                $wrappers = explode('|', $row['params']['wrapperClass']);
                foreach(array_reverse($wrappers) as $wrapperClass) {
                    $rowHtml = $rowHtml.$this->dec().'</div>'.$this->tab();
                }
            } else {
                $rowHtml = $rowHtml.$this->dec().'</div>';
            }
        }

        return $rowHtml;
    }

    private function createCol($col)
    {
        $children = [];
        $colHtml='<div class="col-'.$col['params']['len'].' '.($col['params']['class']?:'').'" '.($col['params']['attributes']).'>'.$this->inc();

        foreach ($col['children'] as $child) {
            $children[] = $this->{'create'.ucfirst($child['params']['type'])}($child);
        }

        return $colHtml.implode($this->tab(), $children).$this->dec().'</div>';
    }

    private function createBlock($block)
    {
        return "{% include 'blocks/block_".$block['params']['id'].".html.twig' with {block: page.getTranslations(".$block['params']['link'].")} %}";
    }

    private function inc()
    {
        $this->ctr++;

        return $this->tab();
    }

    private function dec()
    {
        $this->ctr--;

        return $this->tab();
    }

    private function nl()
    {
        return "\n";
    }

    private function tab()
    {
        $tab = "    ";
        $tabIndent = "";

        for($i = 0; $i < $this->ctr; $i++) {
            $tabIndent .= $tab;
        }

        return $this->nl().$tabIndent;
    }
}