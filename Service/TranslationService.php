<?php

namespace Bci\CmsBundle\Service;

use Bci\CmsBundle\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Translatable\Entity\Repository\TranslationRepository;

class TranslationService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TranslationRepository
     */
    protected $translationRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;

        $this->translationRepository = $this->em->getRepository('Gedmo\Translatable\Entity\Translation');

    }

    /**
     * Use in twig to find entity have been translated in wich language.
     * @param $entity
     * @return array
     */
    public function getEntityTranslations($entity)
    {
        $translations = $this->translationRepository->findTranslations($entity);

        return $translations;
    }

    
    public function getEntityTranslationsById($id)
    {
        $translations = $this->translationRepository->findTranslationsByObjectId($id);

        return $translations;
    }

    /**
     * @param $field
     * @param $entity
     * @param $className
     * @return object|null
     */
    public function getEntityByTranslatableField($field, $entity, $className)
    {
        return $this->translationRepository->findObjectByTranslatedField($field, $entity, $className);
    }
}