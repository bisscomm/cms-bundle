<?php

namespace Bci\CmsBundle\Twig;

use Bci\CmsBundle\Entity\Heading;
use Bci\CmsBundle\Entity\MenuItem;
use Bci\CmsBundle\Entity\Page;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Symfony\Component\HttpFoundation\RequestStack;

class CmsExtension
    extends AbstractExtension
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var Router
     */
    private $container;

    public $uniqueIds = [];

    /**
     * CmsExtension constructor.
     * @param RequestStack $requestStack
     * @param EntityManager $em
     * @param Router $router
     */
    public function __construct(RequestStack $requestStack, EntityManager $em, $router, ContainerInterface $container)
    {
        $this->requestStack = $requestStack;
        $this->em = $em;
        $this->router = $router;
        $this->container = $container;
    }

    public function getFilters()
    {
        return array(
            new TwigFilter('to_keys', array($this, 'getKeys')),
            new TwigFilter('to_values', array($this, 'getValues')),
            new TwigFilter('get', array($this, 'getFilter')),
        );
    }

    public function getFunctions()
    {
        return array(
            new TwigFunction('field_trans', array($this, 'getFieldTrans'), ['needs_context' => true, 'is_safe' => ['all']]),
            new TwigFunction('page_slug', array($this, 'getPageSlug'), ['needs_context' => true, 'is_safe' => ['all']]),
            new TwigFunction('path_page', array($this, 'getPathPage'), ['needs_context' => true, 'is_safe' => ['all']]),
            new TwigFunction('avail_pages', array($this, 'getAvailPages'), ['needs_context' => true, 'is_safe' => ['all']]),
            new TwigFunction('url_menu_item', array($this, 'getSlugFromMenuItem'), ['needs_context' => true, 'is_safe' => ['all']]),
            new TwigFunction('lbl_menu_item', array($this, 'getLabelFromMenuItem'), ['needs_context' => true, 'is_safe' => ['all']]),
            new TwigFunction('is_active_menu_item', array($this, 'isActiveMenuItem'), ['needs_context' => true, 'is_safe' => ['all']]),
            new TwigFunction('can_display', array($this, 'getCanDisplay'), ['needs_context' => true, 'is_safe' => ['all']]),
            new TwigFunction('long_date', array($this, 'getLongDate'), ['needs_context' => true, 'is_safe' => ['all']]),
            new TwigFunction('file_exist', array($this, 'getValidationFileExist'), ['needs_context' => true, 'is_safe' => ['all']]),
            new TwigFunction('get_form_start', array($this, 'getFormStart'), ['is_safe' => ['all']]),
            new TwigFunction('get_form_label', array($this, 'getFormLabel'), ['is_safe' => ['all']]),
            new TwigFunction('get_form_field', array($this, 'getFormField'), ['is_safe' => ['all']]),
            new TwigFunction('get_form_end', array($this, 'getFormEnd'), ['is_safe' => ['all']]),
            new TwigFunction('get_form_recaptcha', array($this, 'getRecaptcha'), ['is_safe' => ['all']]),
            new TwigFunction('get_form', array($this, 'getForm'), ['is_safe' => ['all']]),
            new TwigFunction('uniqueId', [$this, 'getUniqueId'],['is_safe' => ['all']]),
            new TwigFunction('isEnvLive', [$this, 'isEnvLive'],['is_safe' => ['all']]),

        );
    }

    public function getForm()
    {
        return $this->container->get('bci_cms.formulaire');
    }

    public function getRecaptcha($formToken)
    {
        return $this->container->get('bci_cms.formulaire')->getRecaptcha($formToken);
    }

    public function getFormStart($formToken, $locale = 'fr', $attr = [])
    {
        return $this->container->get('bci_cms.formulaire')->formStart($formToken, $locale, $attr);
    }

    public function getFormLabel($itemToken, $attr = [])
    {
        return $this->container->get('bci_cms.formulaire')->getItemLabel($itemToken, $attr);
    }

    public function getFormField($itemToken, $attr = [])
    {
        return $this->container->get('bci_cms.formulaire')->getItemField($itemToken, $attr);
    }

    public function getFormEnd($val, $attr = [])
    {
        $attrs = $this->container->get('bci_cms.formulaire')->buildAttributes($attr);
        return '<input type="submit"'.$attrs.' value="'.$val.'"></form>';
    }

    public function getKeys($stdClassObject)
    {
        // Just typecast it to an array
        $keys = array_keys((array)$stdClassObject);
        $response = [];

        foreach ($keys as $key => $value) {
            $ex = explode($value[0], $value);
            $response[] = end($ex);
        }

        return $response;
    }

    public function getValues($stdClassObject)
    {
        // Just typecast it to an array
        $array = (array)$stdClassObject;
        $response = [];

        foreach ($array as $key => $value) {
            $response[] = $value;
        }

        return $response;
    }

    public function getFilter()
    {
        $params = func_get_args();
        $object = array_shift($params);
        $field = implode('', $params);

        return $object->$field();
    }

    /**
     * @param $context
     * @param $block
     * @param $fieldId
     * @param string $defaultTagType
     * @param $placeholder
     * @param $attr
     * @param $defaultTexte
     * @return string
     */
    public function getFieldTrans(
        $context,
        $block,
        $fieldId,
        $defaultTagType = 'p',
        $defaultTexte = 'Saisir le texte ici',
        $attr = null, /* Format JSON ex: {"class":"mt-5"} */
        $placeholder = 'Saisir le texte ici'
    )
    {
        $locale = $this->requestStack->getCurrentRequest()->getLocale();

       $defaultTexte = empty($defaultTexte) ? 'Saisir le texte ici' : $defaultTexte;
       
        $editable = isset($context['editable'])
            ? $context['editable']
            : false;


        if ($block['trans']) {
            $translation = $block['trans']->filter(
                function ($pageTranslation) use ($fieldId, $locale) {
                    return $pageTranslation->getField() == $fieldId
                        && $pageTranslation->getTranslation()->getLocale() == $locale
                        && !$pageTranslation->isDisabled();
                }
            );
        } else {
            $translation = new ArrayCollection();
        }

        $attrs = [];
        if (isset($attr) && is_array($attr)) {
            foreach ($attr as $key => $value) {
                $attrs[] = $key . '="' . $value . '"';
            }
        }
        // If not the fist time edited set content to be $defaultTexte
        // Else display the value of the edited content.
        $content = $translation->last()
            ? $translation->last()->getTranslation()->getValue()
            : "<$defaultTagType " . implode(' ', $attrs) . ">$defaultTexte</$defaultTagType>";

        $content = preg_replace('/<!--(.*)-->/Uis', '', $content);

        if ($editable) {
            $content = '<div data-editable data-tiny-placeholder="' . $placeholder . '" data-name="tiny_' . $context['block']['templateBlockId'] . '_' . $fieldId . '" data-type="' . $defaultTagType . '" data-block="' . $context['block']['templateBlockId'] . '" data-field="' . $fieldId . '">' . $content . "</div>";
        }

        return $content;
    }

    public function getPageSlug($context, $slug, $locale = null)
    {
        if ($locale === null) {
            $locale = $this->requestStack->getCurrentRequest()->getLocale();
        }

        $heading = $this->em->getRepository(Heading::class)->findBy(['slug' => $slug]);

        if ($heading) {
            return $heading[0]->getPage()->getSlugForLocale($locale);
        }

        return '/';
    }

    public function getPathPage($context, $slugOrRouteName, $params = [])
    {
        if (!isset($params['locale']) && isset($params['_locale'])) {
            $params['locale'] = $params['_locale'];
        }
        if (!isset($params['locale'])) {
            $params['locale'] = $this->requestStack->getCurrentRequest()->getLocale();
        }

        $heading = $this->em->getRepository(Heading::class)->findBy(['slug' => $slugOrRouteName]);

        if ($heading) {
            $params['slug'] = $heading[0]->getPage()->getSlugForLocale($params['locale']);
            unset($params['locale']);

            return $this->router->generate('bci_cms_page_show', $params);
        }

        try {
            unset($params['locale']);
            return $this->router->generate($slugOrRouteName, $params);
        } catch (\Exception $e) {
            return '/';
        }
    }

    public function getAvailPages()
    {
        return $this->em->getRepository(Page::class)->listAll();
    }

    public function getSlugFromMenuItem($context, $item, $locale = 'fr')
    {
        $slug = false;
        if ($item instanceof MenuItem) {
            if ($item->getPage()) {
                $slug = $item->getPage()->getHeadingLocale($locale)->getSlug();
            } else {
                $slug = $item->getSlug();
            }
        } elseif ($item->id) {
            $slug = $this->em->getRepository(Page::class)->find($item->id)->getHeadingLocale($locale)->getSlug();
        } elseif (isset($item->url)) {
            $slug = $item->url;
        }

        return $slug !== false ? str_replace('"', '', '/' . $slug) : '#';
    }

    public function getLabelFromMenuItem($context, $item, $locale = 'fr')
    {
        $lbl = false;
        if (isset($item->name)) {
            $lbl = $item->name;
        } elseif ($item->id) {
            $lbl = $this->em->getRepository(Page::class)->find($item->id)->getHeadingLocale($locale)->getTitle();
        }

        return $lbl !== false ? $lbl : '-';
    }

    public function isActiveMenuItem($context, $item, $request, $locale = 'fr')
    {
        $isActive = false;

        $isActive = $this->isActive($context, $item, $request, $locale);

        return $isActive;
    }

    private function isActive($context, $item, $request, $locale)
    {
        $isActive = false;

        if ($this->router->getRouteCollection()->get($request->attributes->get('_route'))
            && $this->getSlugFromMenuItem($context, $item, $locale) == $this->router->generate($request->attributes->get('_route'), $request->attributes->get('_route_params'))
        ) {
            $isActive = true;
        }

        if (!$isActive && $item->hasDisplayChildren($locale)) {
            foreach ($item->getDisplayChildren($locale) as $sItem) {
                if ($this->isActive($context, $sItem, $request, $locale)) {
                    $isActive = true;
                    break;
                }
            }
        }

        return $isActive;
    }


    public function getCanDisplay($context, $item, $locale = 'fr')
    {
        $canDisplay = false;
        if ($item instanceof MenuItem) {
            if ($item->getPage()) {
                $canDisplay = $item->getPage()->getHeadingLocale($locale)->isDraft();
            }
        } elseif ($item->id) {
            $canDisplay = $this->em->getRepository(Page::class)->find($item->id)->getHeadingLocale($locale)->isDraft();
        }

        return !$canDisplay;
    }

    public function getLongDate($context, \DateTime $date = null, $locale = 'fr', $toLower = true, $withHours = true)
    {
        $toLower = ($locale == 'fr' ? true : false);
        if (!$date) {
            $date = new \DateTime();
        }
        $months = [
            'fr' => ['', 'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            'en' => ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        ];

        $formattted = $date->format('j') . ' ' . $months[$locale][(int)$date->format('n')] . ' ' . $date->format('Y') . ' ' . ($withHours ? $date->format('H:i') : '');

        return $toLower == true ? strtolower($formattted) : $formattted;
    }

    /**
     * Check if the form validation bundle is install.
     * Used in _javascript.html.twig
     * @return bool
     */
    public function getValidationFileExist()
    {
        $path = '../vendor/fp/jsformvalidator-bundle/src/Resources/public/js/FpJsFormValidatorWithJqueryInit.js';
        return file_exists($path);
    }

    /**
     * getUniqueId
     *
     * @return string
     */
    public function getUniqueId(): string
    {
        // generate a random string
        $id = md5(random_bytes(12));

        // check if it's already set
        while (\in_array($id, $this->uniqueIds, true)){
            // if so, use another one
            $id =  md5(random_bytes(12));
        }
        // set it as "used"
        $this->uniqueIds[] = $id;
        return $id;
    }

    public function isEnvLive()
    {
        return ($_ENV['SHOW_SCRIPT_TAG'] === 'true') && isset($_ENV['SHOW_SCRIPT_TAG']);
    }

}