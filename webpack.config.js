var Encore = require('@symfony/webpack-encore');
const CopyWebpackPlugin = require('copy-webpack-plugin');


var encoreConfig = Encore
    // directory where compiled assets will be stored
        .setOutputPath('public/build/')
        // public path used by the web server to access the output path
        .setPublicPath('/build')
        // only needed for CDN's or sub-directory deploy
        //.setManifestKeyPrefix('build/')

        /*
         * ENTRY CONFIG
         */
        .addPlugin(new CopyWebpackPlugin([
            // copies to {output}/static
            { from: './assets/images', to: 'images' }
        ]))
        /* Add 1 entry for each "page" of your app
        * (including one that's included on every page - e.g. "app")
        *
        * Each entry will result in one JavaScript file (e.g. app.js)
        * and one CSS file (e.g. app.css) if you JavaScript imports CSS.
        */

        .addEntry('app', './assets/js/app.js')
        .addEntry('main.class', './assets/js/main.class.js')
        .addEntry('content-tools.extend', './assets/js/content-tools.extend.js')

        .enableSingleRuntimeChunk()

        .addStyleEntry('/cms/app', './public/bundles/bcicms/assets/scss/cms.scss')
        .addStyleEntry('/cms/live-editor', './public/bundles/bcicms/assets/scss/live-editor.scss')



        //.addEntry('page1', './assets/js/page1.js')
        //.addEntry('page2', './assets/js/page2.js')

        /*
         * FEATURE CONFIG
         *
         * Enable & configure other features below. For a full
         * list of features, see:
         * https://symfony.com/doc/current/frontend.html#adding-more-features
         */
        .cleanupOutputBeforeBuild()
        .enableBuildNotifications()
        .enableSourceMaps(!Encore.isProduction())
        // enables hashed filenames (e.g. app.abc123.css)
        .enableVersioning(Encore.isProduction())

        // enables Sass/SCSS support
        .enableSassLoader(function(options) {}, { resolveUrlLoader: false })
        .enablePostCssLoader()

        // uncomment if you use TypeScript
        //.enableTypeScriptLoader()

        // uncomment if you're having problems with a jQuery plugin
        .autoProvidejQuery()
;
const fs = require('fs');
const path = './vendor/fp/jsformvalidator-bundle/src/Resources/public/js/FpJsFormValidatorWithJqueryInit.js';
if (fs.existsSync(path))
{
    encoreConfig.addEntry('FpJsFormElement', './vendor/fp/jsformvalidator-bundle/src/Resources/public/js/FpJsFormValidatorWithJqueryInit.js')
}
else
{   let FgWhite = "\x1b[37m";
    let BgRed = "\x1b[41m";
    let Blink = "\x1b[5m";
    let Reset = "\x1b[0m";
    console.error(FgWhite+BgRed+Blink,"JS VALIDATION NOT ENABLE",Reset);
    console.error(FgWhite+BgRed,'Please run : composer require "fp/jsformvalidator-bundle":"dev-master"');
    console.error(FgWhite+BgRed,'After run : yarn encore dev');
    console.error(FgWhite+BgRed,'This will enable form validation',Reset);
}

module.exports = encoreConfig.getWebpackConfig();
